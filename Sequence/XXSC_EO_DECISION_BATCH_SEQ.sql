CREATE SEQUENCE xxsc.xxsc_eo_decision_batch_seq
MINVALUE 1
MAXVALUE 999999999999999999999999999
START WITH 1
INCREMENT BY 1;
/