CREATE OR REPLACE PACKAGE apps.xxsc_eo_query_pkg AS

--====================================================================================
-- $Header: xxsc_eo_query_pkg.sql, v1.1, 22-NOV-2021
--------------------------------------------------------------------------------
--
-- File Name:   XXSC_EO_QUERY_PKG.sql
--address
-- Description: This package is to return SQL query for LOV and Report regions for E&O apex application
--
--
-- History:

-- Version  Date           Modified By                Comments
-- -------  -----------    -------------            -----------------------------------------------------------------------------
--     1.0  22-NOV-2021    Ganesh Raghavan           Original version

TYPE eo_parameter_rec IS RECORD (
	 organization_code xxsc.xxsc_eo_dashboard_tbl.organization_code%TYPE,
	 item xxsc.xxsc_eo_dashboard_tbl.item%TYPE,
	 l3_bus_group xxsc.xxsc_eo_dashboard_tbl.l3_bus_group%TYPE,
	 l4_bus_unit xxsc.xxsc_eo_dashboard_tbl.l4_bus_unit%TYPE,
	 l6_module xxsc.xxsc_eo_dashboard_tbl.l6_module%TYPE,
	 gl_line_descr xxsc.xxsc_eo_dashboard_tbl.gl_line_descr%TYPE,
	 planner_name xxsc.xxsc_eo_dashboard_tbl.planner_name%TYPE,
	 buyer_name xxsc.xxsc_eo_dashboard_tbl.buyer_name%TYPE,
	 plm_name xxsc.xxsc_eo_dashboard_tbl.plm_name%TYPE,
	 lite_cm_item VARCHAR2(10),
	 item_with_reserve VARCHAR2(100),
	 period VARCHAR2(10),
	 fiscal_year xxsc.xxsc_eo_dashboard_tbl.fiscal_year%TYPE,
	 fiscal_qtr xxsc.xxsc_eo_dashboard_tbl.fiscal_qtr%TYPE,
	 week VARCHAR2(10),
	 fiscal_period xxsc.xxsc_eo_dashboard_tbl.fiscal_period%TYPE,
	 eo_review_items VARCHAR2(10),
	 net_inv_flg VARCHAR2(10)
	 );
	 

TYPE eo_parameter_tbl IS TABLE OF eo_parameter_rec
INDEX BY BINARY_INTEGER;
    
    eo_parameter eo_parameter_tbl;

TYPE eo_error_rec IS RECORD (
	fiscal_year xxsc.xxsc_eo_dashboard_tbl.fiscal_year%TYPE,
	fiscal_qtr xxsc.xxsc_eo_dashboard_tbl.fiscal_qtr%TYPE,
	fiscal_period xxsc.xxsc_eo_dashboard_tbl.fiscal_period%TYPE,
	organization_code xxsc.xxsc_eo_dashboard_tbl.organization_code%TYPE,
	item xxsc.xxsc_eo_dashboard_tbl.item%TYPE,
	error_msg VARCHAR2(2000)
	);

TYPE eo_error_tbl IS TABLE OF eo_error_rec
INDEX BY BINARY_INTEGER;
    
    eo_error eo_error_tbl;
	
FUNCTION lov_query(p_column IN VARCHAR2)
RETURN VARCHAR2;

FUNCTION eo_review_search_query(p_eo_parameter IN eo_parameter_tbl := eo_parameter)
RETURN VARCHAR2;

FUNCTION review_column_access(p_dashboard_rec_id IN NUMBER,
					   p_column IN VARCHAR2,
					   p_user_id IN NUMBER)
RETURN BOOLEAN;

FUNCTION subledger_column_access(p_dashboard_rec_id IN NUMBER,
					   p_column IN VARCHAR2,
					   p_user_id IN NUMBER)
RETURN BOOLEAN;


FUNCTION eo_subled_search_query(p_eo_parameter IN eo_parameter_tbl := eo_parameter)
RETURN VARCHAR2;

END xxsc_eo_query_pkg;


/
