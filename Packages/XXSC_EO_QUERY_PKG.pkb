SET DEFINE OFF;

CREATE OR REPLACE PACKAGE BODY  apps.xxsc_eo_query_pkg AS

--====================================================================================
-- $Header: xxsc_eo_query_pkg.sql, v1.1, 22-NOV-2021
--------------------------------------------------------------------------------
--
-- File Name:   xxsc_eo_query_pkg.sql
--address
-- Description: This package is to return SQL query for LOV and Report regions for E&O apex application
--
--
-- History:

-- Version  Date           Modified By                Comments
-- -------  -----------    -------------            -----------------------------------------------------------------------------
--     1.0  22-NOV-2021    Ganesh Raghavan           Original version

   

FUNCTION lov_query(p_column IN VARCHAR2)
RETURN VARCHAR2 IS

/***********************************************************************************************************
--this fucntion returns SQL query for Search page items. p_column is the column name in xxsc_eo_dashboard_tbl
**************************************************************************************************************/


l_sql VARCHAR2(4000);

BEGIN
	l_sql := 'SELECT /*+ PARALLEL(AUTO) +*/
			DISTINCT '||p_column||' d,'||p_column||' r FROM xxsc.xxsc_eo_dashboard_tbl WHERE '||p_column||' IS NOT NULL';
			
	RETURN l_sql;
END lov_query;

FUNCTION eo_review_search_query(p_eo_parameter IN eo_parameter_tbl := eo_parameter)
RETURN VARCHAR2
IS
/***********************************************************************************************************
--this fucntion returns SQL query for E & O Review form based on search criteria
**************************************************************************************************************/
l_sql1 VARCHAR2(4000);
l_sql2 VARCHAR2(4000);
l_condition VARCHAR2(4000);
l_fiscal_year NUMBER;
l_quarter_num NUMBER;
l_period_num NUMBER;
l_period_num_max NUMBER;
l_period_num_min NUMBER;

BEGIN
	IF p_eo_parameter(1).period ='C' THEN -- Current Quarter of E&O 
		---to get current peroiod and quarter for SYSDATE
		BEGIN
			SELECT /*+ PARALLEL(AUTO) +*/
				   gp.period_year,
				   gp.quarter_num
			INTO l_fiscal_year,
				l_quarter_num
			FROM gl_periods_v gp     
			WHERE TRUNC(SYSDATE) BETWEEN trunc(gp.start_date) AND TRUNC(gp.end_date)
			AND gp.period_set_name = 'XXSC ACC CAL';
		EXCEPTION WHEN OTHERS THEN
			l_fiscal_year	:= 0;
			l_quarter_num   := 0;
		END;
	
	ELSIF p_eo_parameter(1).period ='S' THEN ---specific period of the quarter
			l_fiscal_year := p_eo_parameter(1).fiscal_year;
			l_quarter_num := p_eo_parameter(1).fiscal_qtr;
			
			
	END IF; --IF p_eo_parameter(1).period ='C'
	
	---SQL query for the E&O review
			
	l_sql1 :='SELECT  /*+ PARALLEL(AUTO) +*/
					xed.dashboard_rec_id,
					xed.fiscal_year	,
					xed.fiscal_qtr	,
					xed.period_name	,
					xed.eo_review_flag 	,
					xed.organization_id	,
					xed.organization_code	,
					xed.item	,
					xed.item_descr,--ITEM_DECRIPTION	,
					xed.item_type	,
					xed.item_procure	,
					xed.prod_heirarchy	,
					xed.l3_bus_group	,
					xed.l4_bus_unit	,
					xed.l6_module	,
					xed.gl_line_descr	,
					xed.gl_line_code	,
					xed.item_status	,
					xed.planner_name	,
					xed.buyer_name	,
					xed.plm_name	,
					xed.sys_recomm	,
					xed.sys_reasons	,
					xed.sys_comments	,
					xed.disposition_qty1	,
					xed.disposition_code1	,
					xed.recomm_action1	,
					xed.comments_1	,
					xed.disposition_qty2	,
					xed.disposition_code2	,
					xed.recomm_action2	,
					xed.comments_2	,
					NVL(xed.disposition_qty2,0) * NVL(xed.item_cost,0) exception_amt,
					NVL(xed.disposition_qty1,0) * NVL(xed.item_cost,0) provision_amt,
					GREATEST((NVL(xed.net_excess_usage,0)* NVL(xed.item_cost,0)) -(NVL(xed.disposition_qty2,0) * NVL(xed.item_cost,0)) - (NVL(xed.disposition_qty1,0) * NVL(xed.item_cost,0)),0)not_reviewed,
					xed.completion_dt	,
					xed.action_comment	,
					xed.action_owner,--EO_OWNER	,
					xed.review_comment	,
					DECODE(xed.planner_approved	,''Y'',''Yes'',NULL) planner_approved,
					xed.planner_approved_by	,
					DECODE(xed.plm_approved	,''Y'',''Yes'',NULL) plm_approved,
					xed.plm_approved_by	,
					NVL(xed.total_supply,0)	total_supply,
					NVL(xed.net_oh,0)	net_oh,
					NVL(xed.open_po,0)	open_po,
					NVL(xed.cur_eo_reserve_qty,0)	cur_eo_reserve_qty,
					NVL(xed.cur_po_reserve_qty,0)	cur_po_reserve_qty,
					NVL(xed.cur_mrb_reserve_qty,0)	cur_mrb_reserve_qty,
					NVL(xed.cur_rma_reserve_qty,0)	cur_rma_reserve_qty,
					NVL(xed.cur_sp_reserve_qty,0)	cur_sp_reserve_qty,
					NVL(xed.cur_eo_reserve_qty,0) + NVL(xed.cur_po_reserve_qty,0)+NVL(xed.cur_rma_reserve_qty,0)
					+NVL(xed.cur_sp_reserve_qty,0)+NVL(xed.cur_mrb_reserve_qty,0) total_reserve,
					NVL(xed.ww_total_reserve,0) ww_total_reserve,
					NVL(xed.srv_net_oh,0) srv_net_oh	,
					NVL(xed.loan_net_oh,0)	loan_net_oh,
					DECODE(NVL(xed.net_inv,0),0,GREATEST(NVL(net_oh,0) + NVL(open_po,0) - NVL(srv_net_oh,0) - NVL(loan_net_oh,0)  - NVL(cur_mrb_reserve_qty,0) - NVL(cur_rma_reserve_qty,0) - NVL(cur_sp_reserve_qty,0) - NVL(cur_po_reserve_qty,0)-NVL(cur_eo_reserve_qty,0),0),NVL(xed.net_inv,0)) net_inv,
					NVL(xed.ww_net_inv,0)	ww_net_inv,
					NVL(xed.usage_6mo,0)	usage_6mo,
					NVL(xed.gross_demand_12mo,0)	gross_demand_12mo,
					NVL(xed.full_demand,0)	full_demand,';
					
	l_sql2 :='DECODE(NVL(xed.total_excess_demand,0),0,GREATEST(NVL(net_oh,0) + NVL(open_po,0) - NVL(srv_net_oh,0) - NVL(loan_net_oh,0)  - NVL(cur_mrb_reserve_qty,0) - NVL(cur_rma_reserve_qty,0) - NVL(cur_sp_reserve_qty,0) - NVL(cur_po_reserve_qty,0)-NVL(cur_eo_reserve_qty,0) - NVL(gross_demand_12mo,0),0),NVL(xed.net_excess_usage,0)) total_excess_demand,
					(DECODE(NVL(xed.total_excess_demand,0),0,GREATEST(NVL(net_oh,0) + NVL(open_po,0) - NVL(srv_net_oh,0) - NVL(loan_net_oh,0)  - NVL(cur_mrb_reserve_qty,0) - NVL(cur_rma_reserve_qty,0) - NVL(cur_sp_reserve_qty,0) - NVL(cur_po_reserve_qty,0)-NVL(cur_eo_reserve_qty,0) - NVL(gross_demand_12mo,0),0),NVL(xed.net_excess_usage,0)) * NVL(xed.item_cost,0)) total_excess_amt,
					DECODE(NVL(xed.net_excess_usage,0),0,GREATEST(DECODE(organization_id,''CM'',NVL(net_oh,0) + NVL(open_po,0) - NVL(srv_net_oh,0) - NVL(loan_net_oh,0)  - NVL(cur_mrb_reserve_qty,0) - NVL(cur_rma_reserve_qty,0) - NVL(cur_sp_reserve_qty,0) - NVL(cur_po_reserve_qty,0)-NVL(cur_eo_reserve_qty,0) - NVL(gross_demand_12mo,0),NVL(net_oh,0) + NVL(open_po,0) - NVL(srv_net_oh,0) - NVL(loan_net_oh,0)  - NVL(cur_mrb_reserve_qty,0) - NVL(cur_rma_reserve_qty,0) - NVL(cur_sp_reserve_qty,0) - NVL(cur_po_reserve_qty,0)-NVL(cur_eo_reserve_qty,0) - NVL(usage_6mo,0)),0),NVL(xed.net_excess_usage,0)) net_excess_usage, 
					(DECODE(NVL(xed.net_excess_usage,0),0,GREATEST(DECODE(organization_id,''CM'',NVL(net_oh,0) + NVL(open_po,0) - NVL(srv_net_oh,0) - NVL(loan_net_oh,0)  - NVL(cur_mrb_reserve_qty,0) - NVL(cur_rma_reserve_qty,0) - NVL(cur_sp_reserve_qty,0) - NVL(cur_po_reserve_qty,0)-NVL(cur_eo_reserve_qty,0) - NVL(gross_demand_12mo,0),NVL(net_oh,0) + NVL(open_po,0) - NVL(srv_net_oh,0) - NVL(loan_net_oh,0)  - NVL(cur_mrb_reserve_qty,0) - NVL(cur_rma_reserve_qty,0) - NVL(cur_sp_reserve_qty,0) - NVL(cur_po_reserve_qty,0)-NVL(cur_eo_reserve_qty,0) - NVL(usage_6mo,0)),0),NVL(xed.net_excess_usage,0)) * NVL(xed.item_cost,0)) cal_excess_amt,
					xed.last_eo_exception	,
					xed.last_eo_exc_reason	,
					xed.last_eo_provision	,
					xed.last_eo_prov_reason	,
					xed.last_excess_usage	,
					xed.last_excess_demand	,
					xed.last_action_exception	,
					xed.last_comment	,
					xed.last_action	,
					xed.last_sce_approved_by	,
					NVL(xed.provision_onhand,0)	provision_onhand,
					NVL(xed.provision_onhand,0) * NVL(xed.item_cost,0) provision_onhand_amt,
					NVL(xed.provision_po,0) provision_po	,
					NVL(xed.provision_po,0) * NVL(xed.item_cost,0) provision_po_amt,
					NVL(xed.keep_po_reserve,0) keep_po_reserve	,
					NVL(xed.keep_po_reserve,0) * NVL(xed.item_cost,0) keep_po_reserve_amt,
					xed.cm_item,
					fu.user_name  last_updated_by_name,
					xed.last_update_date,
					xed.item_cost,
					xed.dashboard_rec_id change_hist,
					NULL overwrite_data
			FROM xxsc.xxsc_eo_dashboard_tbl xed,
			 apps.fnd_user fu 
		WHERE xed.last_updated_by = fu.user_id(+)
		AND xed.fiscal_year = '||l_fiscal_year||'
		AND xed.fiscal_qtr = '||l_quarter_num;

	IF p_eo_parameter(1).organization_code IS NOT NULL THEN
		l_condition := l_condition||' AND xed.organization_code = '||''''||p_eo_parameter(1).organization_code||'''';
	END IF;
	
	IF p_eo_parameter(1).item IS NOT NULL THEN
		l_condition := l_condition||' AND xed.item = '||''''||p_eo_parameter(1).item||'''';
	END IF;
	
	IF p_eo_parameter(1).l3_bus_group IS NOT NULL THEN
		l_condition := l_condition||' AND xed.l3_bus_group = '||''''||p_eo_parameter(1).l3_bus_group||'''';
	END IF;
	
	IF p_eo_parameter(1).l4_bus_unit IS NOT NULL THEN
		l_condition := l_condition||' AND xed.l4_bus_unit = '||''''||p_eo_parameter(1).l4_bus_unit||'''';
	END IF;
	
	IF p_eo_parameter(1).l6_module IS NOT NULL THEN
		l_condition := l_condition||' AND xed.l6_module = '||''''||p_eo_parameter(1).l6_module||'''';
	END IF;
	
	IF p_eo_parameter(1).gl_line_descr IS NOT NULL THEN
		l_condition := l_condition||' AND xed.gl_line_descr = '||''''||p_eo_parameter(1).gl_line_descr||'''';
	END IF;
	
	IF p_eo_parameter(1).planner_name IS NOT NULL THEN
		l_condition := l_condition||' AND xed.planner_name = '||''''||p_eo_parameter(1).planner_name||'''';
	END IF;
	
	IF p_eo_parameter(1).buyer_name IS NOT NULL THEN
		l_condition := l_condition||' AND xed.buyer_name = '||''''||p_eo_parameter(1).buyer_name||'''';
	END IF;
	
	IF p_eo_parameter(1).plm_name IS NOT NULL THEN
		l_condition := l_condition||' AND xed.plm_name = '||''''||p_eo_parameter(1).plm_name||'''';
	END IF;
	
	--if CM item then organization_id is CM
	IF p_eo_parameter(1).lite_cm_item IS NOT NULL THEN
		IF p_eo_parameter(1).lite_cm_item = 'LITE' THEN
			l_condition := l_condition||' AND xed.organization_id <> ''CM''';
		ELSIF p_eo_parameter(1).lite_cm_item = 'CM' THEN
			l_condition := l_condition||' AND xed.organization_id = ''CM''';
		END IF;
	END IF;
	
	IF NVL(p_eo_parameter(1).eo_review_items,'N') ='Y' THEN
		l_condition := l_condition||' AND NVL(xed.eo_review_flag,''N'') IN (''A'',''C'')';
	END IF;
	
	IF NVL(p_eo_parameter(1).net_inv_flg,'N') ='Y' THEN
		l_condition := l_condition||' AND NVL(xed.net_inv,0) > 0';
	END IF;
	
	IF p_eo_parameter(1).week = 'WW05' THEN --week 5 is 1st period of qtr
		BEGIN
			SELECT /*+ PARALLEL(AUTO) +*/
				MIN(gp.period_num)
			INTO l_period_num
			FROM gl_periods_v gp     
			WHERE gp.period_year = l_fiscal_year
            AND gp.quarter_num = l_quarter_num
			AND gp.period_set_name = 'XXSC ACC CAL';
		EXCEPTION WHEN OTHERS THEN
			l_period_num    := 0;
		END;
	ELSIF p_eo_parameter(1).week = 'WW13' THEN --week 13 is last period of qtr
		BEGIN
			SELECT /*+ PARALLEL(AUTO) +*/
				MAX(gp.period_num)
			INTO l_period_num
			FROM gl_periods_v gp     
			WHERE gp.period_year = l_fiscal_year
            AND gp.quarter_num = l_quarter_num
			AND gp.period_set_name = 'XXSC ACC CAL';
		EXCEPTION WHEN OTHERS THEN
			l_period_num    := 0;
		END;
	END IF;
	
	l_condition := l_condition||' AND xed.fiscal_period = '||l_period_num;

	RETURN l_sql1||' '||l_sql2||' '||l_condition;
	
END eo_review_search_query;


FUNCTION review_column_access(p_dashboard_rec_id IN NUMBER,
					   p_column IN VARCHAR2,
					   p_user_id IN NUMBER)
RETURN BOOLEAN IS
/***********************************************************************************************************
--this Function returns boolean value to make column updatable or not for E&O Review
***********************************************************************************************************/
l_resp_count NUMBER;
l_review_flag VARCHAR2(10);
l_max_peroid NUMBER;
l_period NUMBER;
l_net_inv NUMBER;
l_net_excess_usage NUMBER;
BEGIN
	---to get the review flag of dashboard record
	BEGIN
		SELECT NVL(xed.eo_review_flag,'N'),
			 NVL(xed.net_inv,0),
			 NVL(xed.net_excess_usage,0)
		INTO l_review_flag,
			 l_net_inv,
			 l_net_excess_usage
		FROM xxsc.xxsc_eo_dashboard_tbl xed
		WHERE xed.dashboard_rec_id = p_dashboard_rec_id;
	EXCEPTION WHEN OTHERS THEN
		l_review_flag := 'N';
	END;
	
	IF NVL(l_net_inv,0) <= 0 AND p_column= 'DISPOSITION_QTY1' THEN
		RETURN TRUE;
	END IF;
	
	IF NVL(l_net_excess_usage,0) <= 0 AND p_column= 'DISPOSITION_QTY2' THEN
		RETURN TRUE;
	END IF;
	
	IF l_review_flag = 'A' THEN --- if the review is activated
		
		--to get then period number of dashboard record and last period of the quarter
		BEGIN
			SELECT /*+ PARALLEL(AUTO) +*/
				   MAX(gp.period_num) period_num_max,
				   xed.fiscal_period
			INTO l_max_peroid,
				 l_period
			FROM gl_periods_v gp,
				 xxsc.xxsc_eo_dashboard_tbl xed
			WHERE gp.period_year = xed.fiscal_year
			AND gp.quarter_num = xed.fiscal_qtr
			AND gp.period_set_name = 'XXSC ACC CAL'
			AND xed.dashboard_rec_id = p_dashboard_rec_id
			GROUP BY xed.fiscal_period;
		EXCEPTION WHEN OTHERS THEN
			l_max_peroid := 0; 
			l_period := 0;
		END;
		
		IF NVL(l_period,0) <> NVL(l_max_peroid,0) THEN -- if not WW13 
			---to chek if user is assigned to responsibility and has the column access
			BEGIN
				SELECT COUNT(*)
				INTO l_resp_count
				FROM apps.fnd_user_resp_groups_direct        furg,
					   apps.fnd_user                   fu,
					   apps.fnd_responsibility_tl      frt,
					   apps.fnd_responsibility         fr,
					   apps.fnd_application_tl         fat,
					   apps.fnd_application            fa,
					   (
						SELECT DISTINCT flt.meaning resp_access
						FROM fnd_lookup_values flv,
							 fnd_lookup_types_vl flt
						WHERE flv.lookup_type IN('XXSC_EO_MANAGER_ACCESS','XXSC_EO_PLM_ACCESS','XXSC_EO_OPERATIONS_ACCESS')
						AND flv.lookup_type = flt.lookup_type
						AND flv.language='US'
						AND NVL(flv.enabled_flag,'N') ='Y'
						AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
						AND flv.lookup_code = p_column)resp
				 WHERE furg.user_id             =  fu.user_id
				   AND furg.responsibility_id   =  frt.responsibility_id
				   AND fr.responsibility_id     =  frt.responsibility_id
				   AND fa.application_id        =  fat.application_id
				   AND fr.application_id        =  fat.application_id
				--   AND frt.language             =  USERENV('LANG')
				   AND fu.user_id = p_user_id  
				   AND (furg.end_date IS NULL OR furg.end_date >= TRUNC(SYSDATE))
				   AND frt.responsibility_name = resp.resp_access
				 ORDER BY frt.responsibility_name;
			EXCEPTION WHEN OTHERS THEN
				l_resp_count := 0;
			END;
		 
			IF NVL(l_resp_count,0) > 0 THEN
				RETURN FALSE;
			ELSE 
				RETURN TRUE;
			END IF;
		ELSIF NVL(l_period,0) = NVL(l_max_peroid,0) THEN -- if WW13 
		---to chek if user is assigned to responsibility and has the column access
			BEGIN
				SELECT COUNT(*)
				INTO l_resp_count
				FROM apps.fnd_user_resp_groups_direct        furg,
					   apps.fnd_user                   fu,
					   apps.fnd_responsibility_tl      frt,
					   apps.fnd_responsibility         fr,
					   apps.fnd_application_tl         fat,
					   apps.fnd_application            fa,
					   (
						SELECT DISTINCT flt.meaning resp_access
						FROM fnd_lookup_values flv,
							 fnd_lookup_types_vl flt
						WHERE flv.lookup_type IN('XXSC_EO_MANAGER_ACCESS','XXSC_EO_PLM_ACCESS','XXSC_EO_OPERATIONS_ACCESS')
						AND flv.lookup_type = flt.lookup_type
						AND flv.language='US'
						AND NVL(flv.enabled_flag,'N') ='Y'
						AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
						AND flv.lookup_code = p_column)resp
				 WHERE furg.user_id             =  fu.user_id
				   AND furg.responsibility_id   =  frt.responsibility_id
				   AND fr.responsibility_id     =  frt.responsibility_id
				   AND fa.application_id        =  fat.application_id
				   AND fr.application_id        =  fat.application_id
				--   AND frt.language             =  USERENV('LANG')
				   AND fu.user_id = p_user_id  -- <change it>
				   AND (furg.end_date IS NULL OR furg.end_date >= TRUNC(SYSDATE))
				   AND frt.responsibility_name = resp.resp_access
				   AND frt.responsibility_name = 'E&O - Manager'
				 ORDER BY frt.responsibility_name;
			EXCEPTION WHEN OTHERS THEN
				l_resp_count := 0;
			END;
		 
			IF NVL(l_resp_count,0) > 0 THEN
				RETURN FALSE;
			ELSE 
				RETURN TRUE;
			END IF;
		END IF;
	ELSE --review is not activated
		RETURN TRUE;
	END IF;
			
EXCEPTION WHEN OTHERS THEN 
	RETURN TRUE;
END review_column_access;

FUNCTION eo_subled_search_query(p_eo_parameter IN eo_parameter_tbl := eo_parameter)
RETURN VARCHAR2
IS
/***********************************************************************************************************
--this fucntion returns SQL query for E & O Subledger form based on search criteria
**************************************************************************************************************/
l_sql VARCHAR2(4000);
l_condition VARCHAR2(4000);
l_fiscal_year NUMBER;
l_quarter_num NUMBER;
l_fiscal_period NUMBER;

BEGIN
	---SQL query for the E&O Subledger
	l_sql := 'SELECT  /*+ PARALLEL(AUTO) +*/
					xed.dashboard_rec_id,
					xed.fiscal_year	,
					xed.fiscal_qtr	,
					xed.period_name	,
					xed.organization_code	,
					xed.item	,
					xed.item_descr	,
					xed.item_type	,
					xed.item_procure	,
					xed.l3_bus_group	,
					xed.l4_bus_unit	,
					xed.l6_module	,
					xed.gl_line_descr	,
					xed.gl_line_code	,
					xed.item_status	,
					xed.planner_name	,
					xed.buyer_name	,
					xed.plm_name	,
					NVL(xed.net_oh,0) net_oh	,
					NVL(xed.cm_oh,0) cm_oh	,
					NVL(xed.non_net_oh,0) non_net_oh	,
					NVL(xed.srv_net_oh,0) srv_net_oh,
					NVL(xed.total_open_wo,0) total_open_wo,
					NVL(xed.loan_net_oh,0) loan_net_oh,
					NVL(xed.open_po,0) open_po	,
					NVL(xed.po_rcv,0) po_rcv	,
					NVL(xed.po_rcv_intransit,0) po_rcv_intransit	,
					NVL(xed.int_order_intransit,0) int_order_intransit	,
					NVL(xed.int_order_rcv,0) 	int_order_rcv,
					NVL(xed.buyback_open_po,0) 	buyback_open_po,
					NVL(xed.buyback_qty,0) 	buyback_qty,
					NVL(xed.cur_mrb_reserve_qty,0) cur_mrb_reserve_qty 	,
					NVL(xed.cur_rma_reserve_qty,0) 	cur_rma_reserve_qty,
					NVL(xed.cur_sp_reserve_qty,0) cur_sp_reserve_qty	,
					NVL(xed.cur_po_reserve_qty,0) 	cur_po_reserve_qty,
					NVL(xed.begin_eo_reserve_qty,0) begin_eo_reserve_qty	,
					NVL(xed.begin_eo_qty_gaap,0) begin_eo_qty_gaap	,
					xed.gaapcode_begin	,
					NVL(xed.buyback_eo_qty,0) buyback_eo_qty	,
					NVL(xed.trans_eo_reserve_qty,0) trans_eo_reserve_qty	,
					NVL(xed.cur_eo_reserve_qty,0) cur_eo_reserve_qty	,
					NVL(xed.eo_reserve_inc_provision,0) eo_reserve_inc_provision	,
					NVL(xed.eo_reevaluation,0) eo_reevaluation	,
					NVL(xed.item_cost,0) item_cost	,
					NVL(xed.item_cost_pre_period,0) item_cost_pre_period	,
					NVL(xed.std_ord_ship,0) std_ord_ship	,
					NVL(xed.con_ord_ship,0) con_ord_ship	,
					NVL(xed.wip_issue,0) 	wip_issue,
					NVL(xed.wip_completion,0) 	wip_completion,
					NVL(xed.iso_ship,0) iso_ship	,
					NVL(xed.misc_issue,0) misc_issue	,
					NVL(xed.eando_scrap,0) 	eando_scrap,
					NVL(xed.total_eo_usage,0) total_eo_usage	,
					NVL(xed.total_eo_usage,0) * NVL(xed.item_cost,0) total_eo_usage_amt	,
					NVL(xed.unrel_eo_usage,0)	unrel_eo_usage,
					NVL(xed.unrel_eo_usage,0) * NVL(xed.item_cost,0) unrel_eo_usage_amt	,
					NVL(xed.eo_usage_so,0)eo_usage_so	,
					NVL(xed.eo_usage_so,0) * NVL(xed.item_cost,0) eo_usage_so_amt	,
					NVL(xed.eo_usage_conord,0)	eo_usage_conord,
					NVL(xed.eo_usage_conord,0) * NVL(xed.item_cost,0) eo_usage_conord_amt	,
					NVL(xed.eo_usage_misc_issue	,0) eo_usage_misc_issue,
					NVL(xed.eo_usage_misc_issue,0) * NVL(xed.item_cost,0) eo_usage_misc_amt	,
					NVL(xed.eo_usage_int_ord,0)	eo_usage_int_ord,
					NVL(xed.eo_usage_int_ord,0) * NVL(xed.item_cost,0) eo_usage_int_ord_amt	,
					NVL(xed.eo_usage_wip,0)	eo_usage_wip,
					NVL(xed.eo_usage_wip,0) * NVL(xed.item_cost,0) eo_usage_wip_amt	,
					NVL(xed.eo_usage_others,0) eo_usage_others,
					NVL(xed.eo_usage_others,0) * NVL(xed.item_cost,0) eo_usage_others_amt	,
					xed.gaap_code	,
					DECODE(NVL(xed.total_provision,0),0,NVL(xed.disposition_qty1,0),xed.total_provision) total_provision,
					DECODE(NVL(xed.total_provision,0),0,NVL(xed.disposition_qty1,0),xed.total_provision) * NVL(xed.item_cost,0) total_provision_amt,
					NVL(xed.provision_onhand,0) provision_onhand	,
					NVL(xed.provision_onhand,0) * NVL(xed.item_cost,0) provision_onhand_amt,
					NVL(xed.provision_po,0)provision_po	,
					NVL(xed.provision_po,0) * NVL(xed.item_cost,0) provision_po_amt,
					NVL(xed.keep_po_reserve,0)keep_po_reserve	,
					NVL(xed.keep_po_reserve,0) * NVL(xed.item_cost,0) keep_po_reserve_amt,
					xed.reserve_comments,
					xed.cm_item	,
					xed.eo_review_flag
			FROM xxsc.xxsc_eo_dashboard_tbl xed,
				 apps.fnd_user fu 
			WHERE xed.last_updated_by = fu.user_id(+)';
			
	IF p_eo_parameter(1).organization_code IS NOT NULL THEN
		l_condition := l_condition||' AND xed.organization_code = '||''''||p_eo_parameter(1).organization_code||'''';
	END IF;
	
	IF p_eo_parameter(1).item IS NOT NULL THEN
		l_condition := l_condition||' AND xed.item = '||''''||p_eo_parameter(1).item||'''';
	END IF;
	
	IF p_eo_parameter(1).l3_bus_group IS NOT NULL THEN
		l_condition := l_condition||' AND xed.l3_bus_group = '||''''||p_eo_parameter(1).l3_bus_group||'''';
	END IF;
	
	IF p_eo_parameter(1).l4_bus_unit IS NOT NULL THEN
		l_condition := l_condition||' AND xed.l4_bus_unit = '||''''||p_eo_parameter(1).l4_bus_unit||'''';
	END IF;
	
	IF p_eo_parameter(1).l6_module IS NOT NULL THEN
		l_condition := l_condition||' AND xed.l6_module = '||''''||p_eo_parameter(1).l6_module||'''';
	END IF;
	
	IF p_eo_parameter(1).gl_line_descr IS NOT NULL THEN
		l_condition := l_condition||' AND xed.gl_line_descr = '||''''||p_eo_parameter(1).gl_line_descr||'''';
	END IF;
	
	IF p_eo_parameter(1).planner_name IS NOT NULL THEN
		l_condition := l_condition||' AND xed.planner_name = '||''''||p_eo_parameter(1).planner_name||'''';
	END IF;
	
	IF p_eo_parameter(1).buyer_name IS NOT NULL THEN
		l_condition := l_condition||' AND xed.buyer_name = '||''''||p_eo_parameter(1).buyer_name||'''';
	END IF;
	
	IF p_eo_parameter(1).plm_name IS NOT NULL THEN
		l_condition := l_condition||' AND xed.plm_name = '||''''||p_eo_parameter(1).plm_name||'''';
	END IF;
	

	IF NVL(p_eo_parameter(1).item_with_reserve,'N') = 'MRB' THEN 
		l_condition := l_condition||' AND NVL(xed.cur_mrb_reserve_qty,0) >0';
	ELSIF NVL(p_eo_parameter(1).item_with_reserve,'N') = 'OP' THEN 
		l_condition := l_condition||' AND NVL(xed.cur_po_reserve_qty,0) >0';
	ELSIF NVL(p_eo_parameter(1).item_with_reserve,'N') = 'RMA' THEN 
		l_condition := l_condition||' AND NVL(xed.cur_rma_reserve_qty,0) >0';
	ELSIF NVL(p_eo_parameter(1).item_with_reserve,'N') = 'WR' THEN 
		l_condition := l_condition||' AND NVL(xed.cur_sp_reserve_qty,0) >0';
	ELSIF NVL(p_eo_parameter(1).item_with_reserve,'N') = 'BB' THEN 
		l_condition := l_condition||' AND NVL(xed.buyback_eo_qty,0) >0';
	ELSIF NVL(p_eo_parameter(1).item_with_reserve,'N') = 'IR' THEN 
		l_condition := l_condition||' AND NVL(xed.trans_eo_reserve_qty,0) >0'	;
	ELSIF NVL(p_eo_parameter(1).item_with_reserve,'N') = 'EB' THEN 
		l_condition := l_condition||' AND NVL(xed.cur_eo_reserve_qty,0) >0';
	ELSIF NVL(p_eo_parameter(1).item_with_reserve,'N') = 'IE' THEN 
		l_condition := l_condition||' AND (NVL(xed.begin_eo_reserve_qty,0) +NVL(xed.buyback_eo_qty,0) + NVL(xed.trans_eo_reserve_qty,0) ) >0';	
	ELSIF NVL(p_eo_parameter(1).item_with_reserve,'N') = 'TR' THEN 
		l_condition := l_condition||' AND (NVL(xed.begin_eo_reserve_qty,0) + NVL(xed.cur_mrb_reserve_qty,0) +NVL(xed.cur_po_reserve_qty,0) + NVL(xed.cur_rma_reserve_qty,0) + NVL(xed.cur_sp_reserve_qty,0)+
				 NVL(xed.buyback_eo_qty,0) + NVL(xed.trans_eo_reserve_qty,0) + NVL(xed.cur_eo_reserve_qty,0)) >0';
	END IF;
	
	IF p_eo_parameter(1).lite_cm_item IS NOT NULL THEN
		IF p_eo_parameter(1).lite_cm_item = 'LITE' THEN
			l_condition := l_condition||' AND xed.organization_id <> ''CM''';
		ELSIF p_eo_parameter(1).lite_cm_item = 'CM' THEN
			l_condition := l_condition||' AND xed.organization_id = ''CM''';
		END IF;
	END IF;
	
	--below query is to get the Fiscal year, Quarter and Period of the current date
	
	
	IF p_eo_parameter(1).period ='C' THEN -- Current Quarter of E&O 
		--if Current closed fiscal period then get previous period of current date
		BEGIN
			SELECT gpv.period_year,
				   gpv.period_num
			INTO l_fiscal_year,
				 l_fiscal_period
			FROM    gl_periods_v gpv
			WHERE gpv.end_date = (
						SELECT MIN(start_date) - 1
						FROM gl_periods_v gp
						WHERE TRUNC(SYSDATE) BETWEEN trunc(gp.start_date) AND TRUNC(gp.end_date)
							AND gp.period_set_name = 'XXSC ACC CAL')
			AND gpv.period_set_name = 'XXSC ACC CAL';
		EXCEPTION WHEN OTHERS THEN
			l_fiscal_year := 0;
			l_fiscal_period := 0;
		END;
		
		l_condition := l_condition||' AND xed.fiscal_year = TO_CHAR('||l_fiscal_year||') AND xed.fiscal_period = TO_CHAR('||l_fiscal_period||')';
	
	ELSIF p_eo_parameter(1).period ='S' THEN
		
		l_condition := l_condition||' AND xed.fiscal_year = TO_CHAR('||p_eo_parameter(1).fiscal_year||') AND xed.fiscal_period = TO_CHAR('||p_eo_parameter(1).fiscal_period||')';
	
	END IF; --IF p_eo_parameter(1).period ='C'
	
	
	RETURN l_sql || ' '||l_condition;
	
END eo_subled_search_query;

FUNCTION subledger_column_access(p_dashboard_rec_id IN NUMBER,
					   p_column IN VARCHAR2,
					   p_user_id IN NUMBER)
RETURN BOOLEAN
IS
/***********************************************************************************************************
--this Function returns boolean value to make column updatable or not for E&O Review
***********************************************************************************************************/
l_resp_count NUMBER;
l_review_flag VARCHAR2(10);
l_max_peroid NUMBER;
l_period NUMBER;
l_frozen_flag VARCHAR2(10);
BEGIN
	---to get the review flag of dashboard record
	BEGIN
		SELECT NVL(xed.eo_review_flag,'N'),
			  NVL(xed.frozen_flag,'N')
		INTO l_review_flag,
			 l_frozen_flag
		FROM xxsc.xxsc_eo_dashboard_tbl xed
		WHERE xed.dashboard_rec_id = p_dashboard_rec_id;
	EXCEPTION WHEN OTHERS THEN
		l_review_flag := 'N';
	END;
	
	IF l_review_flag <>'C' AND l_frozen_flag <> 'Y' THEN --- if the record is cancelled and frozen_flag is not yes
	---to chek if user is assigned to responsibility and has the column access
		BEGIN
			SELECT COUNT(*)
			INTO l_resp_count
			FROM apps.fnd_user_resp_groups_direct  furg,
				   apps.fnd_user                   fu,
				   apps.fnd_responsibility_tl      frt,
				   apps.fnd_responsibility         fr,
				   apps.fnd_application_tl         fat,
				   apps.fnd_application            fa,
				   (
					SELECT DISTINCT flt.meaning resp_access
					FROM fnd_lookup_values flv,
						 fnd_lookup_types_vl flt
					WHERE flv.lookup_type IN ('XXSC_EO_MANAGER_ACCESS','XXSC_EO_PLM_ACCESS','XXSC_EO_OPERATIONS_ACCESS','XXSC_EO_FINANCE_ACCESS')
					AND flv.lookup_type = flt.lookup_type
					AND flv.language='US'
					AND NVL(flv.enabled_flag,'N') ='Y'
					AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
					AND flv.lookup_code = p_column)resp
			 WHERE furg.user_id             =  fu.user_id
			   AND furg.responsibility_id   =  frt.responsibility_id
			   AND fr.responsibility_id     =  frt.responsibility_id
			   AND fa.application_id        =  fat.application_id
			   AND fr.application_id        =  fat.application_id
			--   AND frt.language             =  USERENV('LANG')
			   AND fu.user_id = p_user_id  -- <change it>
			   AND (furg.end_date IS NULL OR furg.end_date >= TRUNC(SYSDATE))
			   AND frt.responsibility_name = resp.resp_access
			 ORDER BY frt.responsibility_name;
		EXCEPTION WHEN OTHERS THEN
			l_resp_count := 0;
		END;
		 
		 
		IF NVL(l_resp_count,0) > 0 THEN
			RETURN FALSE;
		ELSE 
			RETURN TRUE;
		END IF;
		
	ELSE --review is not activated
		RETURN TRUE;
	END IF;
			
EXCEPTION WHEN OTHERS THEN 
	RETURN TRUE;
END subledger_column_access;

END xxsc_eo_query_pkg;

/
