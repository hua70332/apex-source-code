SET DEFINE OFF;
CREATE OR REPLACE PACKAGE BODY apps.xxsc_eo_process_pkg AS

--====================================================================================
-- $Header: xxsc_eo_process_pkg.sql, v1.1, 22-NOV-2021
--------------------------------------------------------------------------------
--
-- File Name:   xxsc_eo_process_pkg.sql
--address
-- Description: This package is to insert or update data in xxsc_eo_dashboard_tbl table
--				and to Activate, Cancel activation, Confirm E&O records
--
-- History:

-- Version  Date           Modified By                Comments
-- -------  -----------    -------------            -----------------------------------------------------------------------------
--     1.0  14-Dec-2021    Ganesh Raghavan           Original version


PROCEDURE update_eo_review(p_eo_review IN eo_review_tbl := eo_review,
							 p_user_id		IN NUMBER)
IS

/***********************************************************************************************************
--this updates the E&O Review data in dasboard table
**************************************************************************************************************/
BEGIN
	FOR i IN 1..p_eo_review.COUNT LOOP
		INSERT INTO xxsc.xxsc_eo_dashboard_hist(
								eo_rec_id			,
								change_date			,
								change_by			,
								disposition_qty1	,
                                disposition_code1 	,
                                recomm_action1 		,
                                comments_1 			,
                                disposition_qty2 	,
                                disposition_code2 	,
                                recomm_action2 		,
                                comments_2 			,
                                action_comment 		,
                                action_owner 		,
                                review_comment 		,
                                planner_approved 	,
                                plm_approved 		)
		(SELECT xed.dashboard_rec_id,
			   SYSDATE,
               p_user_id,
			   xed.disposition_qty1,
               xed.disposition_code1,
               xed.recomm_action1,
               xed.comments_1,
               xed.disposition_qty2,
               xed.disposition_code2,
               xed.recomm_action2,
               xed.comments_2,
               xed.action_comment,
               xed.action_owner,
               xed.review_comment,
               xed.planner_approved,
               xed.plm_approved
		FROM xxsc_eo_dashboard_tbl xed
		WHERE xed.dashboard_rec_id = p_eo_review(i).dashboard_rec_id);
		
		UPDATE xxsc.xxsc_eo_dashboard_tbl xed
		SET	xed.disposition_qty1	 = 	p_eo_review(i).disposition_qty1,
			xed.disposition_code1 	 = 	p_eo_review(i).disposition_code1,
			xed.recomm_action1 		 =	p_eo_review(i).recomm_action1,
			xed.comments_1 			 =  p_eo_review(i).comments_1,
			xed.disposition_qty2 	 =	p_eo_review(i).disposition_qty2,
			xed.disposition_code2 	 = 	p_eo_review(i).disposition_code2,
			xed.recomm_action2 		 =	p_eo_review(i).recomm_action2,
			xed.comments_2 			 = 	p_eo_review(i).comments_2,
			xed.completion_dt 		 =  p_eo_review(i).completion_dt,
			xed.action_comment 		 =  p_eo_review(i).action_comment,
			xed.action_owner 		 =	p_eo_review(i).action_owner,
			xed.review_comment 		 = 	p_eo_review(i).review_comment,
			xed.planner_approved 	 = 	p_eo_review(i).planner_approved,
			xed.planner_approved_by	 =	p_eo_review(i).planner_approved_by,
			xed.planner_approved_dt	 =  p_eo_review(i).planner_approved_dt,
			xed.plm_approved 		 = 	p_eo_review(i).plm_approved,
			xed.plm_approved_by		 =	p_eo_review(i).plm_approved_by,
			xed.plm_approved_dt		 =  p_eo_review(i).plm_approved_dt,
			xed.last_updated_by 	 = 	p_user_id,
			xed.last_update_date 	 = 	SYSDATE
		WHERE xed.dashboard_rec_id 	 = 	p_eo_review(i).dashboard_rec_id;
    
	
		
    END LOOP;
END update_eo_review;

PROCEDURE eo_review_activate(p_eo_parameter IN eo_parameter_tbl := eo_parameter,
							 p_user_id		IN NUMBER,
							 p_rec_count	OUT NUMBER,
							 p_eo_error		OUT  eo_error_tbl )
IS
/***********************************************************************************************************
--this fucntion returns SQL query for E & O Reciew form based on search criteria
***********************************************************************************************************/


l_eo_dashboard eo_dashboard_tbl := eo_dashboard;
l_eo_dashboard_id eo_dashboard_tbl := eo_dashboard;
l_err_cnt NUMBER := 0;
l_updt_cnt NUMBER := 0;
l_eo_error   eo_error_tbl := eo_error;
l_prev_year NUMBER;
l_prev_qtr  NUMBER;
l_prev_period NUMBER;
l_skip_flag VARCHAR2(10);
	
BEGIN
	IF p_eo_parameter(1).week = 'WW05' THEN -- Week05 or 1st period of Quarter
	
	/*********************************************************************************
	--	If selected period is WW05, then 
		--To identify E&O result of last quarter 
		--Identify last quarter by start date of WW05 period (START_DATE – 1 BETWEEN start date and end date of quarter) 
		--Identify valid E&O result of last quarter by checking E&O_REVIEW_FLAG = ’A’
		--Roll-out decision data of last quarter to WW05 
	********************************************************************************/
		BEGIN
			---below query is to get previous quarter 
			SELECT gpv.period_year,
				   gpv.quarter_num,
				   gpv.period_num
			INTO l_prev_year,
				 l_prev_qtr,
				 l_prev_period
			FROM  apps.gl_periods_v gpv
			WHERE gpv.end_date = (
						SELECT MIN(start_date) - 1
						FROM gl_periods_v gp
						WHERE gp.period_year = p_eo_parameter(1).fiscal_year
							AND gp.quarter_num = p_eo_parameter(1).fiscal_qtr
							AND gp.period_set_name = 'XXSC ACC CAL')
			AND gpv.period_set_name = 'XXSC ACC CAL';
		EXCEPTION WHEN OTHERS THEN
			l_prev_year := 0;
			l_prev_qtr  := 0;
			l_prev_period := 0;
		END;
		
		BEGIN
			SELECT  /*+ PARALLEL(AUTO) +*/
				DISTINCT xed.dashboard_rec_id,
				xed.fiscal_year,
				xed.fiscal_qtr,
				xed.fiscal_period,
				xed.organization_code,
				xed.organization_id,
				xed.item,
				xed.frozen_flag,
				xed.eo_review_flag,
				xed.net_oh,
				xed.cm_oh,
				xed.buyback_eo_qty,
				xed.cur_eo_reserve_qty,
				xed.cur_po_reserve_qty,
				xed.total_res,
                xed.open_po,
                xed.srv_net_oh,
                xed.loan_net_oh,
				xed.usage_6mo,
				xed.gross_demand_12mo,
				xed.cmoh_ref_id,
				xed.provision_ref_id,
				xed.item_cost,
				NULL rec_id_ww05,
				b.last_eo_exception	,
				b.last_eo_exc_reason	,
				b.last_eo_provision	,
				b.last_eo_prov_reason	,
				b.last_excess_usage	,
				b.last_excess_demand	,
				b.last_comment	,
				b.last_action_exception	,
				b.last_action_provision	,
				b.last_action	,
				b.last_sce_approved_by	,
				b.ep_type1	,
				b.disposition_code1	,
				b.disposition_qty1	,
				b.recomm_action1	,
				b.comments_1	,
				b.ep_type2	,
				b.disposition_code2	,
				b.disposition_qty2	,
				b.recomm_action2	,
				b.comments_2	,
				b.ep_type3	,
				b.disposition_code3	,
				b.disposition_qty3	,
				b.recomm_action3	,
				b.comments_3	,
				b.not_review	,
				b.total_exception	,
				b.total_provision	,
				b.action_comment	,
				b.completion_dt	,
				b.sys_recomm	,
				b.sys_reasons	,
				b.sys_comments	,
				b.review_comment	,
				b.action_owner	,
				b.planner_approved	,
				b.planner_approved_by	,
				b.planner_approved_dt	,
				b.plm_approved	,
				b.plm_approved_by,
				b.net_excess_usage    ,
				b.net_excess_demand ,
				c.ww_net_inv,
                c.ww_total_reserve,
				NULL,
				NULL,
				NULL
			BULK COLLECT INTO l_eo_dashboard
			FROM 
					(SELECT xed.dashboard_rec_id,
							xed.fiscal_year,
							 xed.fiscal_qtr ,
							xed.fiscal_period,
							xed.item,
							xed.organization_code,
							xed.organization_id,
							xed.frozen_flag,
							xed.eo_review_flag,
							xed.net_oh,
							xed.cm_oh,
							xed.buyback_eo_qty,
							xed.cur_eo_reserve_qty,
							xed.cur_po_reserve_qty,
							(NVL(xed.cur_mrb_reserve_qty,0)+NVL(xed.cur_po_reserve_qty,0)+
                            NVL(xed.cur_sp_reserve_qty,0)+NVL(xed.cur_eo_reserve_qty,0)+NVL(xed.cur_rma_reserve_qty,0)) total_res,
                            xed.open_po,
                            xed.srv_net_oh,
                            xed.loan_net_oh,
							xed.usage_6mo,
							xed.gross_demand_12mo,
							xed.cmoh_ref_id,
							xed.provision_ref_id,
							xed.item_cost
					FROM xxsc_eo_dashboard_tbl xed
					WHERE  NVL(xed.organization_code,'N') = NVL(p_eo_parameter(1).organization_code,NVL(xed.organization_code,'N'))
					AND NVL(xed.item,'N') = NVL(p_eo_parameter(1).item,NVL(xed.item,'N'))
					AND NVL(xed.l3_bus_group,'N') = NVL(p_eo_parameter(1).l3_bus_group, NVL(xed.l3_bus_group,'N'))
					AND NVL(xed.l4_bus_unit,'N') = NVL(p_eo_parameter(1).l4_bus_unit,NVL(xed.l4_bus_unit,'N'))
					AND NVL(xed.l6_module,'N') = NVL(p_eo_parameter(1).l6_module,NVL(xed.l6_module,'N'))
					AND NVL(xed.gl_line_descr,'N') = NVL(p_eo_parameter(1).gl_line_descr,NVL(xed.gl_line_descr,'N'))
					AND NVL(xed.planner_name,'N') = NVL(p_eo_parameter(1).planner_name,NVL(xed.planner_name,'N'))
					AND NVL(xed.buyer_name,'N') = NVL(p_eo_parameter(1).buyer_name,NVL(xed.buyer_name,'N')) 
					AND NVL(xed.plm_name,'N') = NVL(p_eo_parameter(1).plm_name,NVL(xed.plm_name,'N'))
					AND ((NVL(p_eo_parameter(1).lite_cm_item,'N') = 'LITE' AND xed.organization_id <> 'CM')
						OR (NVL(p_eo_parameter(1).lite_cm_item,'N')= 'CM' AND xed.organization_id = 'CM')
						OR(NVL(p_eo_parameter(1).lite_cm_item,'N')= 'N')) 
					AND xed.fiscal_year = TO_CHAR(p_eo_parameter(1).fiscal_year)
					AND xed.fiscal_qtr = TO_CHAR(p_eo_parameter(1).fiscal_qtr)
					AND xed.fiscal_period =(SELECT  MIN(gp.period_num) period_num_min
												FROM gl_periods_v gp     
												WHERE gp.period_year = p_eo_parameter(1).fiscal_year
												AND gp.quarter_num = p_eo_parameter(1).fiscal_qtr
												AND gp.period_set_name = 'XXSC ACC CAL'))xed,

			(SELECT xed.fiscal_year,
					xed.fiscal_qtr ,
					xed.fiscal_period,
					xed.item,
					xed.organization_code,
					xed.last_eo_exception	,
					xed.last_eo_exc_reason	,
					xed.last_eo_provision	,
					xed.last_eo_prov_reason	,
					xed.last_excess_usage	,
					xed.last_excess_demand	,
					xed.last_comment	,
					xed.last_action_exception	,
					xed.last_action_provision	,
					xed.last_action	,
					xed.last_sce_approved_by	,
					xed.ep_type1	,
					xed.disposition_code1	,
					xed.disposition_qty1	,
					xed.recomm_action1	,
					xed.comments_1	,
					xed.ep_type2	,
					xed.disposition_code2	,
					xed.disposition_qty2	,
					xed.recomm_action2	,
					xed.comments_2	,
					xed.ep_type3	,
					xed.disposition_code3	,
					xed.disposition_qty3	,
					xed.recomm_action3	,
					xed.comments_3	,
					xed.not_review	,
					xed.total_exception	,
					xed.total_provision	,
					xed.action_comment	,
					xed.completion_dt	,
					xed.sys_recomm	,
					xed.sys_reasons	,
					xed.sys_comments	,
					xed.review_comment	,
					xed.action_owner	,
					xed.planner_approved	,
					xed.planner_approved_by	,
					xed.planner_approved_dt	,
					xed.plm_approved	,
					xed.plm_approved_by,
					xed.net_excess_usage    ,
					xed.net_excess_demand   
			FROM xxsc_eo_dashboard_tbl xed
			WHERE  1=1
			AND xed.fiscal_year = l_prev_year
			AND  xed.fiscal_qtr  =  l_prev_qtr
			AND xed.fiscal_period = l_prev_period) b,
			(SELECT   xed.fiscal_year,
				xed.fiscal_period,
				xed.item,
				SUM(NVL(xed.net_inv,0)) ww_net_inv ,
				SUM(NVL(xed.cur_eo_reserve_qty,0) + NVL(xed.cur_po_reserve_qty,0)+NVL(xed.cur_rma_reserve_qty,0)
				+NVL(xed.cur_sp_reserve_qty,0)+NVL(xed.cur_mrb_reserve_qty,0)) ww_total_reserve 
			FROM xxsc_eo_dashboard_tbl xed
			WHERE  xed.fiscal_year = TO_CHAR(p_eo_parameter(1).fiscal_year)
				AND xed.fiscal_qtr = TO_CHAR(p_eo_parameter(1).fiscal_qtr)
				AND xed.fiscal_period =(SELECT  MIN(gp.period_num) period_num_min
											FROM gl_periods_v gp     
											WHERE gp.period_year = TO_CHAR(p_eo_parameter(1).fiscal_year)
											AND gp.quarter_num = TO_CHAR(p_eo_parameter(1).fiscal_qtr)
											AND gp.period_set_name = 'XXSC ACC CAL')
			GROUP BY xed.fiscal_year,
				xed.fiscal_period,
				xed.item) c
			WHERE xed.item = b.item(+)
			AND xed.organization_code = b.organization_code(+)
			AND xed.fiscal_year = c.fiscal_year
            AND xed.fiscal_period = c.fiscal_period
            AND xed.item = c.item;
		END;
		
		

	--Exclude CM ITEM is not created by CM Data Load (CMOH_REF_ID =NULLor 0) AND is not review item (PROVISION_REF_ID = NULL or 0)
	FOR i IN 1..l_eo_dashboard.COUNT LOOP
		/*IF l_eo_dashboard(i).organization_id = 'CM' AND l_eo_dashboard(i).net_oh IS NULL AND NVL(l_eo_dashboard(i).cm_oh,0) <>0 THEN --to activate CM record that are loaded and confirmed by E&O Manager 
			l_skip_flag := 'Y'; 
		ELSIF l_eo_dashboard(i).organization_id = 'CM' AND l_eo_dashboard(i).cmoh_ref_id IS NULL THEN 
			l_skip_flag := 'Y'; 
		ELSIF l_eo_dashboard(i).organization_id <> 'CM' AND l_eo_dashboard(i).provision_ref_id IS NULL THEN 
			l_skip_flag := 'Y';
		ELSIF l_eo_dashboard(i).organization_id <> 'CM' AND NVL(l_eo_dashboard(i).item_cost,0) = 0 AND NVL(l_eo_dashboard(i).open_po,0) = 0 THEN 
				l_skip_flag := 'Y';*/
		IF l_eo_dashboard(i).organization_id = 'CM' AND NVL(l_eo_dashboard(i).cmoh_ref_id,0) = 0 AND NVL(l_eo_dashboard(i).provision_ref_id,0) = 0  THEN 
			l_skip_flag := 'Y'; 
		ELSIF l_eo_dashboard(i).organization_id <> 'CM' AND NVL(l_eo_dashboard(i).provision_ref_id,0) = 0  THEN 
			l_skip_flag := 'Y';
		ELSE
			/************************************************************************************************************************
			--Terminate record rules
			--It is frozen item (FROZEN_FLAG = ‘Y’)
			--Unconfirmed LITE item (E&O_REVIEW_FLAG <> ‘R’)
			--Unconfirmed CM item which reserve data (buyback reserve (BUYBACK_EO_QTY), Ending Reserve(CUR_EO_RESERVE_QTY), PO reserve) are not zero
			*************************************************************************************************************************/
		
			IF NVL(l_eo_dashboard(i).frozen_flag,'N') = 'Y' THEN
			----to check if Item forzen 
				l_err_cnt := l_err_cnt + 1;
				l_eo_error(l_err_cnt).fiscal_year := l_eo_dashboard(i).fiscal_year;
				l_eo_error(l_err_cnt).fiscal_qtr  := l_eo_dashboard(i).fiscal_qtr;
				l_eo_error(l_err_cnt).fiscal_period := l_eo_dashboard(i).fiscal_period;
				l_eo_error(l_err_cnt).organization_code := l_eo_dashboard(i).organization_code;
				l_eo_error(l_err_cnt).item  := l_eo_dashboard(i).item;
				l_eo_error(l_err_cnt).error_msg := 'E & O Review of specified quarter isn’t confirmed or item is frozen, please contact finance team';
			ELSIF l_eo_dashboard(i).organization_id <>'CM' AND (NVL(l_eo_dashboard(i).eo_review_flag ,'N') = 'N') THEN
				----to check if Lite Item Review record is not confirmed
				l_err_cnt := l_err_cnt + 1;
				l_eo_error(l_err_cnt).fiscal_year := l_eo_dashboard(i).fiscal_year;
				l_eo_error(l_err_cnt).fiscal_qtr  := l_eo_dashboard(i).fiscal_qtr;
				l_eo_error(l_err_cnt).fiscal_period := l_eo_dashboard(i).fiscal_period;
				l_eo_error(l_err_cnt).organization_code := l_eo_dashboard(i).organization_code;
				l_eo_error(l_err_cnt).item  := l_eo_dashboard(i).item;
				l_eo_error(l_err_cnt).error_msg := 'E & O Review of specified quarter isn’t confirmed or item is frozen, please contact finance team';
			ELSIF (NVL(l_eo_dashboard(i).eo_review_flag ,'N')) IN('A','C') THEN
				----to check if record is already activated or closed
				l_err_cnt := l_err_cnt + 1;
				l_eo_error(l_err_cnt).fiscal_year := l_eo_dashboard(i).fiscal_year;
				l_eo_error(l_err_cnt).fiscal_qtr  := l_eo_dashboard(i).fiscal_qtr;
				l_eo_error(l_err_cnt).fiscal_period := l_eo_dashboard(i).fiscal_period;
				l_eo_error(l_err_cnt).organization_code := l_eo_dashboard(i).organization_code;
				l_eo_error(l_err_cnt).item  := l_eo_dashboard(i).item;
				l_eo_error(l_err_cnt).error_msg := 'E & O Review is aleady activated or closed';
			ELSIF l_eo_dashboard(i).organization_id = 'CM' AND (NVL(l_eo_dashboard(i).eo_review_flag ,'N') <> 'R') AND (NVL(l_eo_dashboard(i).buyback_eo_qty,0) <>0 OR NVL(l_eo_dashboard(i).cur_eo_reserve_qty,0) <>0 OR NVL(l_eo_dashboard(i).cur_po_reserve_qty,0) <>0) THEN
				----to check if CM Item Review record is not confirmed and Reserve qty is not 0
				l_err_cnt := l_err_cnt + 1;
				l_eo_error(l_err_cnt).fiscal_year := l_eo_dashboard(i).fiscal_year;
				l_eo_error(l_err_cnt).fiscal_qtr  := l_eo_dashboard(i).fiscal_qtr;
				l_eo_error(l_err_cnt).fiscal_period := l_eo_dashboard(i).fiscal_period;
				l_eo_error(l_err_cnt).organization_code := l_eo_dashboard(i).organization_code;
				l_eo_error(l_err_cnt).item  := l_eo_dashboard(i).item;
				l_eo_error(l_err_cnt).error_msg := 'E & O Review of specified quarter isn’t confirmed or item is frozen, please contact finance team';
			ELSE
				---dasboard records to update review flag
				l_updt_cnt := l_updt_cnt+1;
				l_eo_dashboard_id(l_updt_cnt).dashboard_rec_id 		:= l_eo_dashboard(i).dashboard_rec_id	;
				l_eo_dashboard_id(l_updt_cnt).disposition_qty2		:= l_eo_dashboard(i).disposition_qty2	;
				l_eo_dashboard_id(l_updt_cnt).disposition_code2     := l_eo_dashboard(i).disposition_code2  ;
				l_eo_dashboard_id(l_updt_cnt).disposition_qty1      := l_eo_dashboard(i).disposition_qty1   ;
				l_eo_dashboard_id(l_updt_cnt).disposition_code1     := l_eo_dashboard(i).disposition_code1  ;
				l_eo_dashboard_id(l_updt_cnt).net_excess_usage      := l_eo_dashboard(i).net_excess_usage   ;
				l_eo_dashboard_id(l_updt_cnt).net_excess_demand     := l_eo_dashboard(i).net_excess_demand  ;
				l_eo_dashboard_id(l_updt_cnt).comments_2            := l_eo_dashboard(i).comments_2         ;
				l_eo_dashboard_id(l_updt_cnt).recomm_action2        := l_eo_dashboard(i).recomm_action2     ;
				l_eo_dashboard_id(l_updt_cnt).recomm_action1        := l_eo_dashboard(i).recomm_action1     ;
				l_eo_dashboard_id(l_updt_cnt).action_comment        := l_eo_dashboard(i).action_comment     ;
				l_eo_dashboard_id(l_updt_cnt).planner_approved_by   := l_eo_dashboard(i).planner_approved_by;
				l_eo_dashboard_id(l_updt_cnt).ww_net_inv		    := l_eo_dashboard(i).ww_net_inv;
				l_eo_dashboard_id(l_updt_cnt).ww_total_reserve   	:= l_eo_dashboard(i).ww_total_reserve;
				
				IF l_eo_dashboard(i).organization_id <>'CM' THEN --for LITE items
					l_eo_dashboard_id(l_updt_cnt).net_inv := GREATEST((NVL(l_eo_dashboard(i).net_oh,0) + NVL(l_eo_dashboard(i).open_po,0)) - NVL(l_eo_dashboard(i).total_res,0)
															- NVL(l_eo_dashboard(i).srv_net_oh,0) - NVL(l_eo_dashboard(i).loan_net_oh,0),0);
					--l_eo_dashboard_id(l_updt_cnt).cal_excess := GREATEST(NVL(l_eo_dashboard(i).net_oh,0) - NVL(l_eo_dashboard(i).usage_6mo,0),0);
					l_eo_dashboard_id(l_updt_cnt).cal_excess := GREATEST(((NVL(l_eo_dashboard(i).net_oh,0) + NVL(l_eo_dashboard(i).open_po,0)) - NVL(l_eo_dashboard(i).total_res,0)
															 -NVL(l_eo_dashboard(i).srv_net_oh,0) - NVL(l_eo_dashboard(i).loan_net_oh,0)) - NVL(l_eo_dashboard(i).usage_6mo,0),0);
					l_eo_dashboard_id(l_updt_cnt).total_excess_demand := GREATEST(((NVL(l_eo_dashboard(i).net_oh,0) + NVL(l_eo_dashboard(i).open_po,0)) - NVL(l_eo_dashboard(i).total_res,0)
															 -NVL(l_eo_dashboard(i).srv_net_oh,0) - NVL(l_eo_dashboard(i).loan_net_oh,0)) - NVL(l_eo_dashboard(i).gross_demand_12mo,0),0);
					
				ELSIF l_eo_dashboard(i).organization_id = 'CM' THEN--for CM items
					l_eo_dashboard_id(l_updt_cnt).net_inv := GREATEST((NVL(l_eo_dashboard(i).net_oh,0) + NVL(l_eo_dashboard(i).open_po,0)) - NVL(l_eo_dashboard(i).total_res,0),0);
					--l_eo_dashboard_id(l_updt_cnt).cal_excess := GREATEST(NVL(l_eo_dashboard(i).net_oh,0) - NVL(l_eo_dashboard(i).gross_demand_12mo,0),0);
					l_eo_dashboard_id(l_updt_cnt).cal_excess := GREATEST(((NVL(l_eo_dashboard(i).net_oh,0) + NVL(l_eo_dashboard(i).open_po,0)) - NVL(l_eo_dashboard(i).total_res,0)) - NVL(l_eo_dashboard(i).gross_demand_12mo,0),0);
					l_eo_dashboard_id(l_updt_cnt).total_excess_demand := GREATEST(((NVL(l_eo_dashboard(i).net_oh,0) + NVL(l_eo_dashboard(i).open_po,0)) - NVL(l_eo_dashboard(i).total_res,0)) - NVL(l_eo_dashboard(i).gross_demand_12mo,0),0);
				END IF;
				
			END IF;
		END IF;
	END LOOP;
		
		--to update records with eo_review_flag as 'A' to identify it as review confirmed and rollout previous quarter data to WW05
		FORALL i IN 1..l_eo_dashboard_id.COUNT 
			UPDATE xxsc_eo_dashboard_tbl xedu
			SET xedu.eo_review_flag = 'A',
				xedu.last_eo_exception			=	l_eo_dashboard_id(i).disposition_qty2	,
				xedu.last_eo_exc_reason      	=	l_eo_dashboard_id(i).disposition_code2   ,
				xedu.last_eo_provision       	=	l_eo_dashboard_id(i).disposition_qty1    ,
				xedu.last_eo_prov_reason     	=	l_eo_dashboard_id(i).disposition_code1   ,
				xedu.last_excess_usage       	=	l_eo_dashboard_id(i).net_excess_usage    ,
				xedu.last_excess_demand      	=	l_eo_dashboard_id(i).net_excess_demand   ,
				xedu.last_comment             	=	l_eo_dashboard_id(i).comments_2          ,
				xedu.last_action_exception      =	l_eo_dashboard_id(i).recomm_action2      ,
				xedu.last_action_provision       =	l_eo_dashboard_id(i).recomm_action1      ,
				xedu.last_action         		=	l_eo_dashboard_id(i).action_comment      ,
				xedu.last_sce_approved_by    	=	l_eo_dashboard_id(i).planner_approved_by ,
				xedu.ww_net_inv					=	l_eo_dashboard_id(i).ww_net_inv			 ,
				xedu.ww_total_reserve			=	l_eo_dashboard_id(i).ww_total_reserve	 ,
			--	xedu.net_inv					= 	l_eo_dashboard_id(i).net_inv,
			--	xedu.net_excess_usage			= 	l_eo_dashboard_id(i).cal_excess,
			--	xedu.total_excess_demand		= 	l_eo_dashboard_id(i).total_excess_demand,
				xedu.last_updated_by 			= 	p_user_id,
				xedu.last_update_date 			= 	SYSDATE
			WHERE xedu.dashboard_rec_id = l_eo_dashboard_id(i).dashboard_rec_id;
			
	ELSIF p_eo_parameter(1).week = 'WW13' THEN
		
	/*********************************************************************************
	--	If selected period is WW13, then 
		--Roll-out decision data of WW05 records to WW13 if there is corresponding record in the WW05 and the WW13 record isn’t ever activated (E&O_REVIEW_FLAG <> ‘A’)
		--Check whether WW05 record of same item is activated (E&O_REVIEW_FLAG = ‘A’), if yes, update E&O_REVIEW_FLAG of WW05 records to ‘C’ to avoid modification.
	********************************************************************************/
	
		BEGIN
			SELECT  /*+ PARALLEL(AUTO) +*/
				DISTINCT xed.dashboard_rec_id,
				xed.fiscal_year,
				xed.fiscal_qtr,
				xed.fiscal_period,
				xed.organization_code,
				xed.organization_id,
				xed.item,
				xed.frozen_flag,
				xed.eo_review_flag,
				xed.net_oh,
				xed.cm_oh,
				xed.buyback_eo_qty,
				xed.cur_eo_reserve_qty,
				xed.cur_po_reserve_qty,
				xed.total_res,
                xed.open_po,
                xed.srv_net_oh,
                xed.loan_net_oh,
				xed.usage_6mo,
				xed.gross_demand_12mo,
				xed.cmoh_ref_id,
				xed.provision_ref_id,
				xed.item_cost,
				b.dashboard_rec_id rec_id_ww05,
				b.last_eo_exception	,
				b.last_eo_exc_reason	,
				b.last_eo_provision	,
				b.last_eo_prov_reason	,
				b.last_excess_usage	,
				b.last_excess_demand	,
				b.last_comment	,
				b.last_action_exception	,
				b.last_action_provision	,
				b.last_action	,
				b.last_sce_approved_by	,
				b.ep_type1	,
				b.disposition_code1	,
				b.disposition_qty1	,
				b.recomm_action1	,
				b.comments_1	,
				b.ep_type2	,
				b.disposition_code2	,
				b.disposition_qty2	,
				b.recomm_action2	,
				b.comments_2	,
				b.ep_type3	,
				b.disposition_code3	,
				b.disposition_qty3	,
				b.recomm_action3	,
				b.comments_3	,
				b.not_review	,
				b.total_exception	,
				b.total_provision	,
				b.action_comment	,
				b.completion_dt	,
				b.sys_recomm	,
				b.sys_reasons	,
				b.sys_comments	,
				b.review_comment	,
				b.action_owner	,
				b.planner_approved	,
				b.planner_approved_by	,
				b.planner_approved_dt	,
				b.plm_approved	,
				b.plm_approved_by,
				b.net_excess_usage    ,
				b.net_excess_demand ,
				c.ww_net_inv,
				c.ww_total_reserve,
				NULL,
				NULL,
				NULL
			BULK COLLECT INTO l_eo_dashboard
			FROM 
					(SELECT xed.dashboard_rec_id,
							xed.fiscal_year,
							 xed.fiscal_qtr ,
							xed.fiscal_period,
							xed.item,
							xed.organization_code,
							xed.organization_id,
							xed.frozen_flag,
							xed.eo_review_flag,
							xed.net_oh,
							xed.cm_oh,
							xed.buyback_eo_qty,
							xed.cur_eo_reserve_qty,
							xed.cur_po_reserve_qty,
							(NVL(xed.cur_mrb_reserve_qty,0)+NVL(xed.cur_po_reserve_qty,0)+
                            NVL(xed.cur_sp_reserve_qty,0)+NVL(xed.cur_eo_reserve_qty,0)+NVL(xed.cur_rma_reserve_qty,0)) total_res,
                            xed.open_po,
                            xed.srv_net_oh,
                            xed.loan_net_oh,
							xed.usage_6mo,
							xed.gross_demand_12mo,
							xed.cmoh_ref_id,
							xed.provision_ref_id,
							xed.item_cost
					FROM xxsc_eo_dashboard_tbl xed
					WHERE  NVL(xed.organization_code,'N') = NVL(p_eo_parameter(1).organization_code,NVL(xed.organization_code,'N'))
					AND NVL(xed.item,'N') = NVL(p_eo_parameter(1).item,NVL(xed.item,'N'))
					AND NVL(xed.l3_bus_group,'N') = NVL(p_eo_parameter(1).l3_bus_group, NVL(xed.l3_bus_group,'N'))
					AND NVL(xed.l4_bus_unit,'N') = NVL(p_eo_parameter(1).l4_bus_unit,NVL(xed.l4_bus_unit,'N'))
					AND NVL(xed.l6_module,'N') = NVL(p_eo_parameter(1).l6_module,NVL(xed.l6_module,'N'))
					AND NVL(xed.gl_line_descr,'N') = NVL(p_eo_parameter(1).gl_line_descr,NVL(xed.gl_line_descr,'N'))
					AND NVL(xed.planner_name,'N') = NVL(p_eo_parameter(1).planner_name,NVL(xed.planner_name,'N'))
					AND NVL(xed.buyer_name,'N') = NVL(p_eo_parameter(1).buyer_name,NVL(xed.buyer_name,'N')) 
					AND NVL(xed.plm_name,'N') = NVL(p_eo_parameter(1).plm_name,NVL(xed.plm_name,'N'))
					AND ((NVL(p_eo_parameter(1).lite_cm_item,'N') = 'LITE' AND xed.organization_id <> 'CM')
						OR (NVL(p_eo_parameter(1).lite_cm_item,'N')= 'CM' AND xed.organization_id = 'CM')
						OR(NVL(p_eo_parameter(1).lite_cm_item,'N')= 'N')) 
					AND xed.fiscal_year = TO_CHAR(p_eo_parameter(1).fiscal_year)
					AND xed.fiscal_qtr = TO_CHAR(p_eo_parameter(1).fiscal_qtr)
					AND xed.fiscal_period =(SELECT  MAX(gp.period_num) period_num
												FROM gl_periods_v gp     
												WHERE gp.period_year = p_eo_parameter(1).fiscal_year
												AND gp.quarter_num = p_eo_parameter(1).fiscal_qtr
												AND gp.period_set_name = 'XXSC ACC CAL'))xed,

			(SELECT xed.fiscal_year,
					xed.fiscal_qtr ,
					xed.fiscal_period,
					xed.item,
					xed.organization_code,
					xed.dashboard_rec_id ,
					xed.last_eo_exception	,
					xed.last_eo_exc_reason	,
					xed.last_eo_provision	,
					xed.last_eo_prov_reason	,
					xed.last_excess_usage	,
					xed.last_excess_demand	,
					xed.last_comment	,
					xed.last_action_exception	,
					xed.last_action_provision	,
					xed.last_action	,
					xed.last_sce_approved_by	,
					xed.ep_type1	,
					xed.disposition_code1	,
					xed.disposition_qty1	,
					xed.recomm_action1	,
					xed.comments_1	,
					xed.ep_type2	,
					xed.disposition_code2	,
					xed.disposition_qty2	,
					xed.recomm_action2	,
					xed.comments_2	,
					xed.ep_type3	,
					xed.disposition_code3	,
					xed.disposition_qty3	,
					xed.recomm_action3	,
					xed.comments_3	,
					xed.not_review	,
					xed.total_exception	,
					xed.total_provision	,
					xed.action_comment	,
					xed.completion_dt	,
					xed.sys_recomm	,
					xed.sys_reasons	,
					xed.sys_comments	,
					xed.review_comment	,
					xed.action_owner	,
					xed.planner_approved	,
					xed.planner_approved_by	,
					xed.planner_approved_dt	,
					xed.plm_approved	,
					xed.plm_approved_by,
					xed.net_excess_usage    ,
					xed.net_excess_demand   
				FROM xxsc_eo_dashboard_tbl xed
				WHERE  1=1
				AND xed.fiscal_year = TO_CHAR(p_eo_parameter(1).fiscal_year)
				AND  xed.fiscal_qtr  =  TO_CHAR(p_eo_parameter(1).fiscal_qtr)
				AND xed.fiscal_period = (SELECT  MIN(gp.period_num) period_num
												FROM gl_periods_v gp     
												WHERE gp.period_year = p_eo_parameter(1).fiscal_year
												AND gp.quarter_num = p_eo_parameter(1).fiscal_qtr
												AND gp.period_set_name = 'XXSC ACC CAL')) b,
			(SELECT   xed.fiscal_year,
				xed.fiscal_period,
				xed.item,
				SUM(NVL(xed.net_inv,0)) ww_net_inv ,
				SUM(NVL(xed.cur_eo_reserve_qty,0) + NVL(xed.cur_po_reserve_qty,0)+NVL(xed.cur_rma_reserve_qty,0)
				+NVL(xed.cur_sp_reserve_qty,0)+NVL(xed.cur_mrb_reserve_qty,0)) ww_total_reserve 
			FROM xxsc_eo_dashboard_tbl xed
			WHERE  xed.fiscal_year = TO_CHAR(p_eo_parameter(1).fiscal_year)
				AND xed.fiscal_qtr = TO_CHAR(p_eo_parameter(1).fiscal_qtr)
				AND xed.fiscal_period =(SELECT  MAX(gp.period_num) period_num_min
											FROM gl_periods_v gp     
											WHERE gp.period_year = TO_CHAR(p_eo_parameter(1).fiscal_year)
											AND gp.quarter_num = TO_CHAR(p_eo_parameter(1).fiscal_qtr)
											AND gp.period_set_name = 'XXSC ACC CAL')
			GROUP BY xed.fiscal_year,
				xed.fiscal_period,
				xed.item) c
			WHERE xed.item = b.item(+)
			AND xed.organization_code = b.organization_code(+)
			AND xed.fiscal_year = c.fiscal_year
            AND xed.fiscal_period = c.fiscal_period
            AND xed.item = c.item;
		END;
		
		--Exclude CM ITEM is not created by CM Data Load (CMOH_REF_ID =NULLor 0) AND is not review item (PROVISION_REF_ID = NULL or 0)
		FOR i IN 1..l_eo_dashboard.COUNT LOOP
			/*IF l_eo_dashboard(i).organization_id = 'CM' AND l_eo_dashboard(i).net_oh IS NULL AND NVL(l_eo_dashboard(i).cm_oh,0) <>0 THEN --to activate CM record that are loaded and confirmed by E&O Manager 
				l_skip_flag := 'Y';
			ELSIF l_eo_dashboard(i).organization_id = 'CM' AND l_eo_dashboard(i).cmoh_ref_id IS NULL THEN 
				l_skip_flag := 'Y'; 
			ELSIF l_eo_dashboard(i).organization_id <> 'CM' AND l_eo_dashboard(i).provision_ref_id IS NULL THEN 
				l_skip_flag := 'Y';
			ELSIF l_eo_dashboard(i).organization_id <> 'CM' AND NVL(l_eo_dashboard(i).item_cost,0) = 0 AND NVL(l_eo_dashboard(i).open_po,0) = 0 THEN 
				l_skip_flag := 'Y';*/
			IF l_eo_dashboard(i).organization_id = 'CM' AND NVL(l_eo_dashboard(i).cmoh_ref_id,0) = 0 AND NVL(l_eo_dashboard(i).provision_ref_id,0) = 0  THEN 
				l_skip_flag := 'Y'; 
			ELSIF l_eo_dashboard(i).organization_id <> 'CM' AND NVL(l_eo_dashboard(i).provision_ref_id,0) = 0  THEN 
				l_skip_flag := 'Y';
			ELSE
				/************************************************************************************************************************
				--Terminate record rules
				--It is frozen item (FROZEN_FLAG = ‘Y’)
				--Unconfirmed LITE item (E&O_REVIEW_FLAG <> ‘R’)
				--Unconfirmed CM item which reserve data (buyback reserve (BUYBACK_EO_QTY), Ending Reserve(CUR_EO_RESERVE_QTY), PO reserve) are not zero
				*************************************************************************************************************************/
				IF NVL(l_eo_dashboard(i).frozen_flag,'N') = 'Y' THEN
				----to check if Item forzen 
					l_err_cnt := l_err_cnt + 1;
					l_eo_error(l_err_cnt).fiscal_year := l_eo_dashboard(i).fiscal_year;
					l_eo_error(l_err_cnt).fiscal_qtr  := l_eo_dashboard(i).fiscal_qtr;
					l_eo_error(l_err_cnt).fiscal_period := l_eo_dashboard(i).fiscal_period;
					l_eo_error(l_err_cnt).organization_code := l_eo_dashboard(i).organization_code;
					l_eo_error(l_err_cnt).item  := l_eo_dashboard(i).item;
					l_eo_error(l_err_cnt).error_msg := 'E & O Review of specified quarter isn’t confirmed or item is frozen, please contact finance team';
				ELSIF l_eo_dashboard(i).organization_id <> 'CM' AND (NVL(l_eo_dashboard(i).eo_review_flag ,'N') = 'N') THEN
					----to check if Litem Item Review record is not confirmed
					l_err_cnt := l_err_cnt + 1;
					l_eo_error(l_err_cnt).fiscal_year := l_eo_dashboard(i).fiscal_year;
					l_eo_error(l_err_cnt).fiscal_qtr  := l_eo_dashboard(i).fiscal_qtr;
					l_eo_error(l_err_cnt).fiscal_period := l_eo_dashboard(i).fiscal_period;
					l_eo_error(l_err_cnt).organization_code := l_eo_dashboard(i).organization_code;
					l_eo_error(l_err_cnt).item  := l_eo_dashboard(i).item;
					l_eo_error(l_err_cnt).error_msg := 'E & O Review of specified quarter isn’t confirmed or item is frozen, please contact finance team';
				ELSIF (NVL(l_eo_dashboard(i).eo_review_flag ,'N')) IN('A','C') THEN
					----to check if record is already activated or closes
					l_err_cnt := l_err_cnt + 1;
					l_eo_error(l_err_cnt).fiscal_year := l_eo_dashboard(i).fiscal_year;
					l_eo_error(l_err_cnt).fiscal_qtr  := l_eo_dashboard(i).fiscal_qtr;
					l_eo_error(l_err_cnt).fiscal_period := l_eo_dashboard(i).fiscal_period;
					l_eo_error(l_err_cnt).organization_code := l_eo_dashboard(i).organization_code;
					l_eo_error(l_err_cnt).item  := l_eo_dashboard(i).item;
					l_eo_error(l_err_cnt).error_msg := 'E & O Review is aleady activated or closed';
				ELSIF l_eo_dashboard(i).organization_id = 'CM' AND (NVL(l_eo_dashboard(i).eo_review_flag ,'N') <> 'R') AND (NVL(l_eo_dashboard(i).buyback_eo_qty,0) <>0 OR NVL(l_eo_dashboard(i).cur_eo_reserve_qty,0) <>0 OR NVL(l_eo_dashboard(i).cur_po_reserve_qty,0) <>0) THEN
					----to check if CM Item Review record is not confirmed and Reserve qty is not 0
					l_err_cnt := l_err_cnt + 1;
					l_eo_error(l_err_cnt).fiscal_year := l_eo_dashboard(i).fiscal_year;
					l_eo_error(l_err_cnt).fiscal_qtr  := l_eo_dashboard(i).fiscal_qtr;
					l_eo_error(l_err_cnt).fiscal_period := l_eo_dashboard(i).fiscal_period;
					l_eo_error(l_err_cnt).organization_code := l_eo_dashboard(i).organization_code;
					l_eo_error(l_err_cnt).item  := l_eo_dashboard(i).item;
					l_eo_error(l_err_cnt).error_msg := 'E & O Review of specified quarter isn’t confirmed or item is frozen, please contact finance team';
				ELSE
					---dasboard records to update review flag
					l_updt_cnt := l_updt_cnt+1;
					l_eo_dashboard_id(l_updt_cnt).dashboard_rec_id 		:= l_eo_dashboard(i).dashboard_rec_id		;
					l_eo_dashboard_id(l_updt_cnt).rec_id_ww05 			:= l_eo_dashboard(i).rec_id_ww05		;
					l_eo_dashboard_id(l_updt_cnt).last_eo_exception		:= l_eo_dashboard(i).last_eo_exception		;
					l_eo_dashboard_id(l_updt_cnt).last_eo_exc_reason    := l_eo_dashboard(i).last_eo_exc_reason     ;
					l_eo_dashboard_id(l_updt_cnt).last_eo_provision     := l_eo_dashboard(i).last_eo_provision      ;
					l_eo_dashboard_id(l_updt_cnt).last_eo_prov_reason   := l_eo_dashboard(i).last_eo_prov_reason    ;
					l_eo_dashboard_id(l_updt_cnt).last_excess_usage     := l_eo_dashboard(i).last_excess_usage      ;
					l_eo_dashboard_id(l_updt_cnt).last_excess_demand    := l_eo_dashboard(i).last_excess_demand     ;
					l_eo_dashboard_id(l_updt_cnt).last_comment          := l_eo_dashboard(i).last_comment           ;
					l_eo_dashboard_id(l_updt_cnt).last_action_exception := l_eo_dashboard(i).last_action_exception  ;
					l_eo_dashboard_id(l_updt_cnt).last_action_provision := l_eo_dashboard(i).last_action_provision  ;
					l_eo_dashboard_id(l_updt_cnt).last_action           := l_eo_dashboard(i).last_action            ;
					l_eo_dashboard_id(l_updt_cnt).last_sce_approved_by  := l_eo_dashboard(i).last_sce_approved_by   ;
					l_eo_dashboard_id(l_updt_cnt).ep_type1              := l_eo_dashboard(i).ep_type1               ;
					l_eo_dashboard_id(l_updt_cnt).disposition_code1     := l_eo_dashboard(i).disposition_code1      ;
					l_eo_dashboard_id(l_updt_cnt).disposition_qty1      := l_eo_dashboard(i).disposition_qty1       ;
					l_eo_dashboard_id(l_updt_cnt).recomm_action1        := l_eo_dashboard(i).recomm_action1         ;
					l_eo_dashboard_id(l_updt_cnt).comments_1            := l_eo_dashboard(i).comments_1             ;
					l_eo_dashboard_id(l_updt_cnt).ep_type2              := l_eo_dashboard(i).ep_type2               ;
					l_eo_dashboard_id(l_updt_cnt).disposition_code2     := l_eo_dashboard(i).disposition_code2      ;
					l_eo_dashboard_id(l_updt_cnt).disposition_qty2      := l_eo_dashboard(i).disposition_qty2       ;
					l_eo_dashboard_id(l_updt_cnt).recomm_action2        := l_eo_dashboard(i).recomm_action2         ;
					l_eo_dashboard_id(l_updt_cnt).comments_2            := l_eo_dashboard(i).comments_2             ;
					l_eo_dashboard_id(l_updt_cnt).ep_type3              := l_eo_dashboard(i).ep_type3               ;
					l_eo_dashboard_id(l_updt_cnt).disposition_code3     := l_eo_dashboard(i).disposition_code3      ;
					l_eo_dashboard_id(l_updt_cnt).disposition_qty3      := l_eo_dashboard(i).disposition_qty3       ;
					l_eo_dashboard_id(l_updt_cnt).recomm_action3        := l_eo_dashboard(i).recomm_action3         ;
					l_eo_dashboard_id(l_updt_cnt).comments_3            := l_eo_dashboard(i).comments_3             ;
					l_eo_dashboard_id(l_updt_cnt).not_review            := l_eo_dashboard(i).not_review             ;
					l_eo_dashboard_id(l_updt_cnt).total_exception       := l_eo_dashboard(i).total_exception        ;
					l_eo_dashboard_id(l_updt_cnt).total_provision       := l_eo_dashboard(i).total_provision        ;
					l_eo_dashboard_id(l_updt_cnt).action_comment        := l_eo_dashboard(i).action_comment         ;
					l_eo_dashboard_id(l_updt_cnt).completion_dt         := l_eo_dashboard(i).completion_dt          ;
					l_eo_dashboard_id(l_updt_cnt).sys_recomm            := l_eo_dashboard(i).sys_recomm             ;
					l_eo_dashboard_id(l_updt_cnt).sys_reasons           := l_eo_dashboard(i).sys_reasons            ;
					l_eo_dashboard_id(l_updt_cnt).sys_comments          := l_eo_dashboard(i).sys_comments           ;
					l_eo_dashboard_id(l_updt_cnt).review_comment        := l_eo_dashboard(i).review_comment         ;
					l_eo_dashboard_id(l_updt_cnt).action_owner          := l_eo_dashboard(i).action_owner           ;
					l_eo_dashboard_id(l_updt_cnt).planner_approved      := l_eo_dashboard(i).planner_approved       ;
					l_eo_dashboard_id(l_updt_cnt).planner_approved_by   := l_eo_dashboard(i).planner_approved_by    ;
					l_eo_dashboard_id(l_updt_cnt).planner_approved_dt   := l_eo_dashboard(i).planner_approved_dt    ;
					l_eo_dashboard_id(l_updt_cnt).plm_approved          := l_eo_dashboard(i).plm_approved           ;
					l_eo_dashboard_id(l_updt_cnt).plm_approved_by       := l_eo_dashboard(i).plm_approved_by        ;
					l_eo_dashboard_id(l_updt_cnt).ww_net_inv		    := l_eo_dashboard(i).ww_net_inv;
					l_eo_dashboard_id(l_updt_cnt).ww_total_reserve   	:= l_eo_dashboard(i).ww_total_reserve;
					
				IF l_eo_dashboard(i).organization_id <>'CM' THEN --for LITE items
					l_eo_dashboard_id(l_updt_cnt).net_inv := GREATEST((NVL(l_eo_dashboard(i).net_oh,0) + NVL(l_eo_dashboard(i).open_po,0)) - NVL(l_eo_dashboard(i).total_res,0)
															- NVL(l_eo_dashboard(i).srv_net_oh,0) - NVL(l_eo_dashboard(i).loan_net_oh,0),0);
					--l_eo_dashboard_id(l_updt_cnt).cal_excess := GREATEST(NVL(l_eo_dashboard(i).net_oh,0) - NVL(l_eo_dashboard(i).usage_6mo,0),0);
					l_eo_dashboard_id(l_updt_cnt).cal_excess := GREATEST(((NVL(l_eo_dashboard(i).net_oh,0) + NVL(l_eo_dashboard(i).open_po,0)) - NVL(l_eo_dashboard(i).total_res,0)
															 -NVL(l_eo_dashboard(i).srv_net_oh,0) - NVL(l_eo_dashboard(i).loan_net_oh,0)) - NVL(l_eo_dashboard(i).usage_6mo,0),0);
					l_eo_dashboard_id(l_updt_cnt).total_excess_demand := GREATEST(((NVL(l_eo_dashboard(i).net_oh,0) + NVL(l_eo_dashboard(i).open_po,0)) - NVL(l_eo_dashboard(i).total_res,0)
															 -NVL(l_eo_dashboard(i).srv_net_oh,0) - NVL(l_eo_dashboard(i).loan_net_oh,0)) - NVL(l_eo_dashboard(i).gross_demand_12mo,0),0);
					
				ELSIF l_eo_dashboard(i).organization_id = 'CM' THEN--for CM items
					l_eo_dashboard_id(l_updt_cnt).net_inv := GREATEST((NVL(l_eo_dashboard(i).net_oh,0) + NVL(l_eo_dashboard(i).open_po,0)) - NVL(l_eo_dashboard(i).total_res,0),0);
					--l_eo_dashboard_id(l_updt_cnt).cal_excess := GREATEST(NVL(l_eo_dashboard(i).net_oh,0) - NVL(l_eo_dashboard(i).gross_demand_12mo,0),0);
					l_eo_dashboard_id(l_updt_cnt).cal_excess := GREATEST(((NVL(l_eo_dashboard(i).net_oh,0) + NVL(l_eo_dashboard(i).open_po,0)) - NVL(l_eo_dashboard(i).total_res,0)) - NVL(l_eo_dashboard(i).gross_demand_12mo,0),0);
					l_eo_dashboard_id(l_updt_cnt).total_excess_demand := GREATEST(((NVL(l_eo_dashboard(i).net_oh,0) + NVL(l_eo_dashboard(i).open_po,0)) - NVL(l_eo_dashboard(i).total_res,0)) - NVL(l_eo_dashboard(i).gross_demand_12mo,0),0);
				END IF;
									
				END IF;---IF NVL(l_eo_dashboard(i).frozen_flag,'N') = 'Y' THEN
			END IF;
		END LOOP;
	
		--to update records with eo_review_flag as 'A' to identify it as review confirmed and rollout data from WW05 ro WW13
		FORALL i IN 1..l_eo_dashboard_id.COUNT 
			UPDATE xxsc_eo_dashboard_tbl xedu
			SET xedu.eo_review_flag = 'A',
				xedu.last_eo_exception		= l_eo_dashboard_id(i).last_eo_exception	,
				xedu.last_eo_exc_reason     = l_eo_dashboard_id(i).last_eo_exc_reason	,
				xedu.last_eo_provision      = l_eo_dashboard_id(i).last_eo_provision	,
				xedu.last_eo_prov_reason    = l_eo_dashboard_id(i).last_eo_prov_reason	,
				xedu.last_excess_usage      = l_eo_dashboard_id(i).last_excess_usage	,
				xedu.last_excess_demand     = l_eo_dashboard_id(i).last_excess_demand	,
				xedu.last_comment           = l_eo_dashboard_id(i).last_comment	,
				xedu.last_action_exception  = l_eo_dashboard_id(i).last_action_exception	,
				xedu.last_action_provision  = l_eo_dashboard_id(i).last_action_provision	,
				xedu.last_action            = l_eo_dashboard_id(i).last_action	,
				xedu.last_sce_approved_by   = l_eo_dashboard_id(i).last_sce_approved_by	,
				xedu.ep_type1               = l_eo_dashboard_id(i).ep_type1	,
				xedu.disposition_code1      = l_eo_dashboard_id(i).disposition_code1	,
				xedu.disposition_qty1       = l_eo_dashboard_id(i).disposition_qty1	,
				xedu.recomm_action1         = l_eo_dashboard_id(i).recomm_action1	,
				xedu.comments_1             = l_eo_dashboard_id(i).comments_1	,
				xedu.ep_type2               = l_eo_dashboard_id(i).ep_type2	,
				xedu.disposition_code2      = l_eo_dashboard_id(i).disposition_code2	,
				xedu.disposition_qty2       = l_eo_dashboard_id(i).disposition_qty2	,
				xedu.recomm_action2         = l_eo_dashboard_id(i).recomm_action2	,
				xedu.comments_2             = l_eo_dashboard_id(i).comments_2	,
				xedu.ep_type3               = l_eo_dashboard_id(i).ep_type3	,
				xedu.disposition_code3      = l_eo_dashboard_id(i).disposition_code3	,
				xedu.disposition_qty3       = l_eo_dashboard_id(i).disposition_qty3	,
				xedu.recomm_action3         = l_eo_dashboard_id(i).recomm_action3	,
				xedu.comments_3             = l_eo_dashboard_id(i).comments_3	,
				xedu.not_review             = l_eo_dashboard_id(i).not_review	,
				xedu.total_exception        = l_eo_dashboard_id(i).total_exception	,
				xedu.total_provision        = l_eo_dashboard_id(i).total_provision	,
				xedu.action_comment         = l_eo_dashboard_id(i).action_comment	,
				xedu.completion_dt          = l_eo_dashboard_id(i).completion_dt	,
				xedu.sys_recomm             = l_eo_dashboard_id(i).sys_recomm	,
				xedu.sys_reasons            = l_eo_dashboard_id(i).sys_reasons	,
				xedu.sys_comments           = l_eo_dashboard_id(i).sys_comments	,
				xedu.review_comment         = l_eo_dashboard_id(i).review_comment	,
				xedu.action_owner           = l_eo_dashboard_id(i).action_owner	,
				xedu.planner_approved       = l_eo_dashboard_id(i).planner_approved	,
				xedu.planner_approved_by    = l_eo_dashboard_id(i).planner_approved_by	,
				xedu.planner_approved_dt    = l_eo_dashboard_id(i).planner_approved_dt	,
				xedu.plm_approved           = l_eo_dashboard_id(i).plm_approved	,
				xedu.plm_approved_by        = l_eo_dashboard_id(i).plm_approved_by,
				xedu.ww_net_inv				= l_eo_dashboard_id(i).ww_net_inv,
				xedu.ww_total_reserve		= l_eo_dashboard_id(i).ww_total_reserve,
			--	xedu.net_inv				= l_eo_dashboard_id(i).net_inv,
				--xedu.net_excess_usage   	= l_eo_dashboard_id(i).cal_excess,
			--	xedu.total_excess_demand	= l_eo_dashboard_id(i).total_excess_demand,
				xedu.last_updated_by = p_user_id,
				xedu.last_update_date = SYSDATE
			WHERE xedu.dashboard_rec_id = l_eo_dashboard_id(i).dashboard_rec_id;
		
		--to update records with eo_review_flag as 'C' to identify it as review closed
		FORALL i IN 1..l_eo_dashboard_id.COUNT 
		 UPDATE xxsc_eo_dashboard_tbl xed
			SET xed.eo_review_flag = 'C',
				xed.last_updated_by = p_user_id,
				xed.last_update_date = SYSDATE
		WHERE xed.dashboard_rec_id = l_eo_dashboard_id(i).rec_id_ww05;
				
	END IF; --IF p_eo_parameter(1).week = 'WW05'
	
	p_rec_count := l_eo_dashboard_id.COUNT;
	p_eo_error := l_eo_error;
END eo_review_activate;

PROCEDURE eo_cancel_review_activate(p_eo_parameter IN eo_parameter_tbl := eo_parameter,
							 p_user_id		IN NUMBER,
							 p_rec_count	OUT NUMBER,
							 p_eo_error		OUT  eo_error_tbl )
IS
/***********************************************************************************************************
--this PROCEDURE is to cancel activate of E&O Review
***********************************************************************************************************/

l_eo_dashboard eo_dashboard_id_tbl := eo_dashboard_id;
l_eo_dashboard_id eo_dashboard_id_tbl := eo_dashboard_id;
l_err_cnt NUMBER := 0;
l_updt_cnt NUMBER := 0;
l_eo_error   eo_error_tbl := eo_error;
l_prev_year NUMBER;
l_prev_qtr  NUMBER;
	
BEGIN
	---below query to get the records based on search citeria parameters
	IF p_eo_parameter(1).week = 'WW05' THEN -- Week05 or 1st period of Quarter
		/***************************************************************
		--	Cancelling WW05: 
			--	If E&O_REVIEW_FLAG of specified records is ‘A’, update flag to ‘R’;
			--	If E&O_REVIEW_FLAG of specified records isn’t ‘A’, popup error: “The E&O review of record isn’t activated yet, please select right period to roll-out”.
	*****************************************************************/
	
		BEGIN
			SELECT /*+ PARALLEL(AUTO) +*/
			    xed.dashboard_rec_id,
				xed.fiscal_year,
				xed.fiscal_qtr,
				xed.fiscal_period,
				xed.organization_code,
				xed.item,
				xed.frozen_flag,
				xed.eo_review_flag,
				NULL rec_id_ww05
			BULK COLLECT INTO l_eo_dashboard
			FROM xxsc_eo_dashboard_tbl xed
			WHERE  NVL(xed.organization_code,'N') = NVL(p_eo_parameter(1).organization_code,NVL(xed.organization_code,'N'))
			AND NVL(xed.item,'N') = NVL(p_eo_parameter(1).item,NVL(xed.item,'N'))
			AND NVL(xed.l3_bus_group,'N') = NVL(p_eo_parameter(1).l3_bus_group, NVL(xed.l3_bus_group,'N'))
			AND NVL(xed.l4_bus_unit,'N') = NVL(p_eo_parameter(1).l4_bus_unit,NVL(xed.l4_bus_unit,'N'))
			AND NVL(xed.l6_module,'N') = NVL(p_eo_parameter(1).l6_module,NVL(xed.l6_module,'N'))
			AND NVL(xed.gl_line_descr,'N') = NVL(p_eo_parameter(1).gl_line_descr,NVL(xed.gl_line_descr,'N'))
			AND NVL(xed.planner_name,'N') = NVL(p_eo_parameter(1).planner_name,NVL(xed.planner_name,'N'))
			AND NVL(xed.buyer_name,'N') = NVL(p_eo_parameter(1).buyer_name,NVL(xed.buyer_name,'N')) 
			AND NVL(xed.plm_name,'N') = NVL(p_eo_parameter(1).plm_name,NVL(xed.plm_name,'N'))
			AND ((NVL(p_eo_parameter(1).lite_cm_item,'N') = 'LITE' AND xed.organization_id <> 'CM')
				OR (NVL(p_eo_parameter(1).lite_cm_item,'N')= 'CM' AND xed.organization_id = 'CM')
				OR(NVL(p_eo_parameter(1).lite_cm_item,'N')= 'N')) 
			AND xed.fiscal_year = TO_CHAR(p_eo_parameter(1).fiscal_year)
			AND xed.fiscal_qtr = TO_CHAR(p_eo_parameter(1).fiscal_qtr)
			AND xed.fiscal_period = (SELECT  MIN(gp.period_num) period_num_min
									FROM gl_periods_v gp     
									WHERE gp.period_year = p_eo_parameter(1).fiscal_year
									AND gp.quarter_num = p_eo_parameter(1).fiscal_qtr
									AND gp.period_set_name = 'XXSC ACC CAL');
		END;
		
		FOR i IN 1..l_eo_dashboard.COUNT LOOP
			IF (NVL(l_eo_dashboard(i).eo_review_flag ,'N') <> 'A') THEN
				----to check if Item forzed or Review record is not confirmed
				l_err_cnt := l_err_cnt + 1;
				l_eo_error(l_err_cnt).fiscal_year := l_eo_dashboard(i).fiscal_year;
				l_eo_error(l_err_cnt).fiscal_qtr  := l_eo_dashboard(i).fiscal_qtr;
				l_eo_error(l_err_cnt).fiscal_period := l_eo_dashboard(i).fiscal_period;
				l_eo_error(l_err_cnt).organization_code := l_eo_dashboard(i).organization_code;
				l_eo_error(l_err_cnt).item  := l_eo_dashboard(i).item;
				l_eo_error(l_err_cnt).error_msg := 'E&O review of record isn’t activated yet, please select right period to roll-out';
			ELSE
				---dasboard records to update review flag
				l_updt_cnt := l_updt_cnt+1;
				l_eo_dashboard_id(l_updt_cnt).dashboard_rec_id := l_eo_dashboard(i).dashboard_rec_id;
			END IF;
		END LOOP;
	
	
		--to update records with eo_review_flag as 'R' to identify it as review confirmed
		FORALL i IN 1..l_eo_dashboard_id.COUNT 
			UPDATE xxsc.xxsc_eo_dashboard_tbl
			SET eo_review_flag = 'R',
				last_updated_by = p_user_id,
				last_update_date = SYSDATE
			WHERE dashboard_rec_id = l_eo_dashboard_id(i).dashboard_rec_id
			AND NVL(eo_review_flag,'N') = 'A';
			
	ELSIF p_eo_parameter(1).week = 'WW13' THEN
		/***********************************************************************************************************
		-	Cancelling WW13:
			--	If E&O_REVIEW_FLAG of specified records is ‘A’, 
			--	Update E&O_REVIEW_FLAG of WW13 to ‘R’.
			--	Check if E&O_REVIEW_FLAG of corresponding WW05 record is ‘C’, if yes, update it back to ‘A’; otherwise, bypass.
			--	If E&O_REVIEW_FLAG of specified records isn’t ‘A’, popup error: “The E&O review of record isn’t activated yet, please select right period to deactivate”.

		***********************************************************************************************************/

		BEGIN
			SELECT /*+ PARALLEL(AUTO) +*/
			    xed.dashboard_rec_id,
				xed.fiscal_year,
				xed.fiscal_qtr,
				xed.fiscal_period,
				xed.organization_code,
				xed.item,
				xed.frozen_flag,
				xed.eo_review_flag,
				b.dashboard_rec_id rec_id_ww05
			BULK COLLECT INTO l_eo_dashboard
			FROM(
				SELECT * 
				FROM xxsc_eo_dashboard_tbl xed
				WHERE  NVL(xed.organization_code,'N') = NVL(p_eo_parameter(1).organization_code,NVL(xed.organization_code,'N'))
				AND NVL(xed.item,'N') = NVL(p_eo_parameter(1).item,NVL(xed.item,'N'))
				AND NVL(xed.l3_bus_group,'N') = NVL(p_eo_parameter(1).l3_bus_group, NVL(xed.l3_bus_group,'N'))
				AND NVL(xed.l4_bus_unit,'N') = NVL(p_eo_parameter(1).l4_bus_unit,NVL(xed.l4_bus_unit,'N'))
				AND NVL(xed.l6_module,'N') = NVL(p_eo_parameter(1).l6_module,NVL(xed.l6_module,'N'))
				AND NVL(xed.gl_line_descr,'N') = NVL(p_eo_parameter(1).gl_line_descr,NVL(xed.gl_line_descr,'N'))
				AND NVL(xed.planner_name,'N') = NVL(p_eo_parameter(1).planner_name,NVL(xed.planner_name,'N'))
				AND NVL(xed.buyer_name,'N') = NVL(p_eo_parameter(1).buyer_name,NVL(xed.buyer_name,'N')) 
				AND NVL(xed.plm_name,'N') = NVL(p_eo_parameter(1).plm_name,NVL(xed.plm_name,'N'))
				AND ((NVL(p_eo_parameter(1).lite_cm_item,'N') = 'LITE' AND xed.organization_id <> 'CM')
					OR (NVL(p_eo_parameter(1).lite_cm_item,'N')= 'CM' AND xed.organization_id = 'CM')
					OR(NVL(p_eo_parameter(1).lite_cm_item,'N')= 'N')) 
				AND xed.fiscal_year = TO_CHAR(p_eo_parameter(1).fiscal_year)
				AND xed.fiscal_qtr = TO_CHAR(p_eo_parameter(1).fiscal_qtr)
				AND xed.fiscal_period = (SELECT  MAX(gp.period_num) period_num_min
										FROM gl_periods_v gp     
										WHERE gp.period_year = p_eo_parameter(1).fiscal_year
										AND gp.quarter_num = p_eo_parameter(1).fiscal_qtr
										AND gp.period_set_name = 'XXSC ACC CAL'))xed,
				(SELECT xed.dashboard_rec_id,
                    xed.fiscal_year,
					xed.fiscal_qtr ,
					xed.fiscal_period,
					xed.item,
					xed.organization_code,
                    xed.eo_review_flag
				FROM xxsc_eo_dashboard_tbl xed
				WHERE  1=1
				AND xed.fiscal_year = p_eo_parameter(1).fiscal_year
				AND  xed.fiscal_qtr  =  p_eo_parameter(1).fiscal_qtr
                AND xed.eo_review_flag ='C'
				AND xed.fiscal_period =(SELECT  MIN(gp.period_num) period_num_min
												FROM gl_periods_v gp     
												WHERE gp.period_year = p_eo_parameter(1).fiscal_year
												AND gp.quarter_num = p_eo_parameter(1).fiscal_qtr
												AND gp.period_set_name = 'XXSC ACC CAL')) b
			WHERE xed.item = b.item(+)
			AND xed.organization_code = b.organization_code(+);
		END;
		
		FOR i IN 1..l_eo_dashboard.COUNT LOOP
			IF (NVL(l_eo_dashboard(i).eo_review_flag ,'N') <> 'A') THEN
				----to check if Item forzed or Review record is not confirmed
				l_err_cnt := l_err_cnt + 1;
				l_eo_error(l_err_cnt).fiscal_year := l_eo_dashboard(i).fiscal_year;
				l_eo_error(l_err_cnt).fiscal_qtr  := l_eo_dashboard(i).fiscal_qtr;
				l_eo_error(l_err_cnt).fiscal_period := l_eo_dashboard(i).fiscal_period;
				l_eo_error(l_err_cnt).organization_code := l_eo_dashboard(i).organization_code;
				l_eo_error(l_err_cnt).item  := l_eo_dashboard(i).item;
				l_eo_error(l_err_cnt).error_msg := 'E&O review of record isn’t activated yet, please select right period to roll-out';
			ELSE
				---dasboard records to update review flag
				l_updt_cnt := l_updt_cnt+1;
				l_eo_dashboard_id(l_updt_cnt).dashboard_rec_id := l_eo_dashboard(i).dashboard_rec_id;
				l_eo_dashboard_id(l_updt_cnt).rec_id_ww05	   := l_eo_dashboard(i).rec_id_ww05;
			END IF;
		END LOOP;
	
	
						
		--to update records with eo_review_flag as 'A' to identify it as review confirmed
		FORALL i IN 1..l_eo_dashboard_id.COUNT 
			UPDATE xxsc.xxsc_eo_dashboard_tbl
			SET eo_review_flag = 'R',
				last_updated_by = p_user_id,
				last_update_date = SYSDATE
			WHERE dashboard_rec_id = l_eo_dashboard_id(i).dashboard_rec_id
			AND NVL(eo_review_flag,'N') = 'A';
		
		--to update records with eo_review_flag as 'A' to identify it as review confirmed
		FORALL i IN 1..l_eo_dashboard_id.COUNT 
		 UPDATE xxsc.xxsc_eo_dashboard_tbl xed
			SET xed.eo_review_flag = 'A',
				xed.last_updated_by = p_user_id,
				xed.last_update_date = SYSDATE
			WHERE dashboard_rec_id = l_eo_dashboard_id(i).rec_id_ww05
			AND xed.eo_review_flag = 'C';
				
	END IF; --IF p_eo_parameter(1).week = 'WW05'
	
	p_rec_count := l_eo_dashboard_id.COUNT;
	p_eo_error := l_eo_error;
END eo_cancel_review_activate;

PROCEDURE eo_confrim_reserve(p_eo_parameter IN eo_parameter_tbl := eo_parameter,
							 p_user_id		IN NUMBER,
							 p_rec_count	OUT NUMBER,
							 p_eo_error		OUT  eo_error_tbl )
IS
/***********************************************************************************************************
--this procedure is to Cofirm E&O review
***********************************************************************************************************/

l_eo_dashboard eo_dashboard_id_tbl := eo_dashboard_id;
l_eo_dashboard_id eo_dashboard_id_tbl := eo_dashboard_id;
l_err_cnt NUMBER := 0;
l_updt_cnt NUMBER := 0;
l_eo_error   eo_error_tbl := eo_error;
l_eo_ww_total eo_ww_total_tbl := eo_ww_total;
l_fiscal_year NUMBER;
l_fiscal_period  NUMBER;
	
BEGIN
	/***********************************************************************************************************
	--Rules for confirm E&O
	--	Check whether specified records is frozen (FROZEN_FLAG = ‘Y’), if yes,  popup error: ”Specified items is frozen”, terminate confirm request;
	--	Check whether specified records is activated for E&O review (E&O_REVIEW_FLAG = ‘A’), if not activated, update ‘R’ to E&O_REVIEW_FLAG of specified records; if already activated, bypass.
	--	Calculate total reserve of item (MRB Reserve + RMA Reserve + PO Reserve + SP Reserve + Ending Reserve) (only sum by item and period, don’t consider organization) and update calculation result to WW_TOTAL_RESERVE
	***********************************************************************************************************/
	
	
	IF p_eo_parameter(1).period = 'C' THEN
	--if Current closed fiscal period then get previous period of current date
		BEGIN
			SELECT gpv.period_year,
				   gpv.period_num
			INTO l_fiscal_year,
				 l_fiscal_period
			FROM    gl_periods_v gpv
			WHERE gpv.end_date = (
						SELECT MIN(start_date) - 1
						FROM gl_periods_v gp
						WHERE TRUNC(SYSDATE) BETWEEN trunc(gp.start_date) AND TRUNC(gp.end_date)
							AND gp.period_set_name = 'XXSC ACC CAL')
			AND gpv.period_set_name = 'XXSC ACC CAL';
		EXCEPTION WHEN OTHERS THEN
			l_fiscal_year := 0;
			l_fiscal_period := 0;
		END;
	ELSE
		l_fiscal_year := p_eo_parameter(1).fiscal_year;
		l_fiscal_period := p_eo_parameter(1).fiscal_period;
	END IF;
	
	---below query to get the records based on search citeria parameters
	BEGIN
		SELECT /*+ PARALLEL(AUTO) +*/
			xed.dashboard_rec_id,
			xed.fiscal_year,
			xed.fiscal_qtr,
			xed.fiscal_period,
			xed.organization_code,
			xed.item,
			xed.frozen_flag,
			xed.eo_review_flag,
			NULL rec_id_ww05
		BULK COLLECT INTO l_eo_dashboard
		FROM 
		(SELECT * FROM xxsc_eo_dashboard_tbl xed
		WHERE NVL(xed.organization_code,'N') = NVL(p_eo_parameter(1).organization_code,NVL(xed.organization_code,'N'))
		AND NVL(xed.item,'N') = NVL(p_eo_parameter(1).item,NVL(xed.item,'N'))
		AND NVL(xed.l3_bus_group,'N') = NVL(p_eo_parameter(1).l3_bus_group, NVL(xed.l3_bus_group,'N'))
		AND NVL(xed.l4_bus_unit,'N') = NVL(p_eo_parameter(1).l4_bus_unit,NVL(xed.l4_bus_unit,'N'))
		AND NVL(xed.l6_module,'N') = NVL(p_eo_parameter(1).l6_module,NVL(xed.l6_module,'N'))
		AND NVL(xed.gl_line_descr,'N') = NVL(p_eo_parameter(1).gl_line_descr,NVL(xed.gl_line_descr,'N'))
		AND NVL(xed.planner_name,'N') = NVL(p_eo_parameter(1).planner_name,NVL(xed.planner_name,'N'))
		AND NVL(xed.buyer_name,'N') = NVL(p_eo_parameter(1).buyer_name,NVL(xed.buyer_name,'N')) 
		AND NVL(xed.plm_name,'N') = NVL(p_eo_parameter(1).plm_name,NVL(xed.plm_name,'N'))
		AND ((NVL(p_eo_parameter(1).lite_cm_item,'N') = 'LITE' AND xed.organization_id <> 'CM')
			OR (NVL(p_eo_parameter(1).lite_cm_item,'N')= 'CM' AND xed.organization_id = 'CM')
			OR(NVL(p_eo_parameter(1).lite_cm_item,'N')= 'N')) 
		AND xed.fiscal_year = TO_CHAR(l_fiscal_year)
		AND xed.fiscal_period = TO_CHAR(l_fiscal_period)
		AND (CASE
			WHEN p_eo_parameter(1).item_with_reserve IS NULL THEN 1
			WHEN p_eo_parameter(1).item_with_reserve = 'MRB' THEN NVL(xed.cur_mrb_reserve_qty,0)
			WHEN p_eo_parameter(1).item_with_reserve = 'OP' THEN NVL(xed.cur_po_reserve_qty,0)
			WHEN p_eo_parameter(1).item_with_reserve = 'RMA' THEN NVL(xed.cur_rma_reserve_qty,0)
			WHEN p_eo_parameter(1).item_with_reserve = 'WR' THEN NVL(xed.cur_sp_reserve_qty,0)
			WHEN p_eo_parameter(1).item_with_reserve = 'BB' THEN NVL(xed.buyback_eo_qty,0)
			WHEN p_eo_parameter(1).item_with_reserve = 'IR' THEN NVL(xed.trans_eo_reserve_qty,0)
			WHEN p_eo_parameter(1).item_with_reserve = 'EB' THEN NVL(xed.cur_eo_reserve_qty,0)
			WHEN p_eo_parameter(1).item_with_reserve = 'IE' 
			THEN (NVL(xed.begin_eo_reserve_qty,0) +NVL(xed.buyback_eo_qty,0) + NVL(xed.trans_eo_reserve_qty,0) )
			WHEN p_eo_parameter(1).item_with_reserve = 'TR' 
			THEN (NVL(xed.begin_eo_reserve_qty,0) + NVL(xed.cur_mrb_reserve_qty,0) +NVL(xed.cur_po_reserve_qty,0) + NVL(xed.cur_rma_reserve_qty,0) + NVL(xed.cur_sp_reserve_qty,0)+
				 NVL(xed.buyback_eo_qty,0) + NVL(xed.trans_eo_reserve_qty,0) + NVL(xed.cur_eo_reserve_qty,0))
		   END) >0)xed ;
	END;
	
	dbms_output.put_line('l_eo_dashboard.COUNT  '|| l_eo_dashboard.COUNT );
	
	FOR i IN 1..l_eo_dashboard.COUNT LOOP
		IF (NVL(l_eo_dashboard(i).frozen_flag,'N') = 'Y')  THEN
			----to check if Item forzed or Review record is not frozen
			l_err_cnt := l_err_cnt + 1;
			l_eo_error(l_err_cnt).fiscal_year := l_eo_dashboard(i).fiscal_year;
			l_eo_error(l_err_cnt).fiscal_qtr  := l_eo_dashboard(i).fiscal_qtr;
			l_eo_error(l_err_cnt).fiscal_period := l_eo_dashboard(i).fiscal_period;
			l_eo_error(l_err_cnt).organization_code := l_eo_dashboard(i).organization_code;
			l_eo_error(l_err_cnt).item  := l_eo_dashboard(i).item;
			l_eo_error(l_err_cnt).error_msg := 'E & O Review for item is frozen';
		ELSIF (NVL(l_eo_dashboard(i).eo_review_flag ,'N')) IN('A','C','R') THEN
		----to check if Item forzed or Review record is already activated confirmed or closed
			l_err_cnt := l_err_cnt + 1;
			l_eo_error(l_err_cnt).fiscal_year := l_eo_dashboard(i).fiscal_year;
			l_eo_error(l_err_cnt).fiscal_qtr  := l_eo_dashboard(i).fiscal_qtr;
			l_eo_error(l_err_cnt).fiscal_period := l_eo_dashboard(i).fiscal_period;
			l_eo_error(l_err_cnt).organization_code := l_eo_dashboard(i).organization_code;
			l_eo_error(l_err_cnt).item  := l_eo_dashboard(i).item;
			l_eo_error(l_err_cnt).error_msg := 'E & O Review of item is Confirmed or Activated or Closed';
		ELSIF (NVL(l_eo_dashboard(i).eo_review_flag ,'N')) NOT IN('A','C','R') THEN
			---dasboard records to update review flag
			l_updt_cnt := l_updt_cnt+1;
			l_eo_dashboard_id(l_updt_cnt).dashboard_rec_id := l_eo_dashboard(i).dashboard_rec_id;
			--l_eo_dashboard_id(l_updt_cnt).ww_total_reserve := l_eo_dashboard(i).ww_total_reserve; commented as per requirement change
		END IF;
	END LOOP;
	
	dbms_output.put_line('l_eo_dashboard_id.COUNT  '|| l_eo_dashboard_id.COUNT );
	
	FORALL i IN 1..l_eo_dashboard_id.COUNT 
		
		UPDATE xxsc_eo_dashboard_tbl xedu
		SET xedu.eo_review_flag ='R',
			--xedu.ww_total_reserve = l_eo_dashboard_id(i).ww_total_reserve,commented as per requirement change
		    xedu.last_updated_by = p_user_id,
			xedu.last_update_date = SYSDATE
		WHERE xedu.dashboard_rec_id =l_eo_dashboard_id(i).dashboard_rec_id;
		
	
	p_rec_count := l_eo_dashboard_id.COUNT;
	p_eo_error := l_eo_error;
END eo_confrim_reserve;

PROCEDURE eo_usage_calculation(p_eo_parameter IN eo_parameter_tbl := eo_parameter,
							 p_user_id		IN NUMBER,
							 p_request_id 	OUT NUMBER)
IS
/***********************************************************************************************************
--this procedure is to submit concurrent program for E&O usage calculation
***********************************************************************************************************/
l_resp_id NUMBER;
l_resp_appl_id NUMBER;
l_request_id NUMBER;
l_fiscal_year NUMBER;
l_fiscal_period NUMBER;

BEGIN

	IF p_eo_parameter(1).period = 'C' THEN
	--if Current closed fiscal period then get previous period of current date
		BEGIN
			SELECT gpv.period_year,
				   gpv.period_num
			INTO l_fiscal_year,
				 l_fiscal_period
			FROM    gl_periods_v gpv
			WHERE gpv.end_date = (
						SELECT MIN(start_date) - 1
						FROM gl_periods_v gp
						WHERE TRUNC(SYSDATE) BETWEEN trunc(gp.start_date) AND TRUNC(gp.end_date)
							AND gp.period_set_name = 'XXSC ACC CAL')
			AND gpv.period_set_name = 'XXSC ACC CAL';
		EXCEPTION WHEN OTHERS THEN
			l_fiscal_year := 0;
			l_fiscal_period := 0;
		END;
	ELSE
		l_fiscal_year := p_eo_parameter(1).fiscal_year;
		l_fiscal_period := p_eo_parameter(1).fiscal_period;
	END IF;
	
	BEGIN
		--to get reponsibility ID
		SELECT fresp.responsibility_id, 
			   fresp.application_id 
		INTO l_resp_id,
			 l_resp_appl_id
		FROM   fnd_responsibility_tl fresp 
		WHERE   fresp.responsibility_name = 'E&O - Finance Costing';
	EXCEPTION WHEN OTHERS THEN
		l_resp_id := 53386;
	    l_resp_appl_id := 20003;
	END;
	
	fnd_global.APPS_INITIALIZE(user_id=>p_user_id, 
							   resp_id=>l_resp_id, 
							   resp_appl_id=>l_resp_appl_id);
							   
	---submiting concurrent program LITE E & O Usage Calculation Program
	 l_request_id:=  FND_REQUEST.SUBMIT_REQUEST (
               application  =>  'XXSC'
              ,program      =>  'XXSCEOUSAGECALC'
              ,description  =>  'LITE E & O Usage Calculation Program'
              ,start_time   =>  SYSDATE
              ,sub_request  =>  NULL
              ,argument1    =>  l_fiscal_year
              ,argument2    =>  l_fiscal_period
              ,argument3    =>  p_eo_parameter(1).item
              ,argument4    =>  p_eo_parameter(1).organization_code
              ,argument5    =>  p_eo_parameter(1).lite_cm_item
              ,argument6    =>  p_eo_parameter(1).eo_usage
              ,argument7    =>  p_eo_parameter(1).reevaluation
              ,argument8    =>  p_eo_parameter(1).eo_reserve
              ,argument9    =>  p_eo_parameter(1).provision
              ,argument10   =>  'Y'
             );
	
	p_request_id := l_request_id;
	
EXCEPTION WHEN OTHERS THEN
	p_request_id := 0;
	
END eo_usage_calculation;

PROCEDURE update_eo_subledger(p_eo_subledger IN eo_subledger_tbl := eo_subledger,
							 p_user_id		IN NUMBER,
							 p_error_flag	OUT VARCHAR2,
							 p_error_msg	OUT VARCHAR2)
IS

/***********************************************************************************************************
--this updates the E&O Subledger data in dasboard table
**************************************************************************************************************/
BEGIN
	FOR i IN 1..p_eo_subledger.COUNT LOOP
	 
		UPDATE xxsc.xxsc_eo_dashboard_tbl xed
		SET	xed.cm_oh					=	p_eo_subledger(i).cm_oh,
			xed.buyback_open_po	        =	p_eo_subledger(i).buyback_open_po,
			xed.buyback_qty	            =	p_eo_subledger(i).buyback_qty,
			xed.cur_mrb_reserve_qty	    =	p_eo_subledger(i).cur_mrb_reserve_qty,
			xed.cur_rma_reserve_qty	    =	p_eo_subledger(i).cur_rma_reserve_qty,
			xed.cur_sp_reserve_qty	    =	p_eo_subledger(i).cur_sp_reserve_qty,
			xed.cur_po_reserve_qty	    =	p_eo_subledger(i).cur_po_reserve_qty,
			xed.begin_eo_reserve_qty	=	p_eo_subledger(i).begin_eo_reserve_qty,
			xed.begin_eo_qty_gaap	    =	p_eo_subledger(i).begin_eo_qty_gaap,
			xed.gaapcode_begin	        =	p_eo_subledger(i).gaapcode_begin,
			xed.buyback_eo_qty	        =	p_eo_subledger(i).buyback_eo_qty,
			xed.trans_eo_reserve_qty	=	p_eo_subledger(i).trans_eo_reserve_qty,
			xed.cur_eo_reserve_qty	    =	p_eo_subledger(i).cur_eo_reserve_qty,
			xed.eando_scrap				=	p_eo_subledger(i).eando_scrap,
			xed.total_eo_usage	        =	p_eo_subledger(i).total_eo_usage,
			xed.unrel_eo_usage	        =	p_eo_subledger(i).unrel_eo_usage,
			xed.gaap_code               =	p_eo_subledger(i).gaap_code,
			xed.provision_onhand        =	p_eo_subledger(i).provision_onhand,
			xed.provision_po            =	p_eo_subledger(i).provision_po,
			xed.keep_po_reserve         =	p_eo_subledger(i).keep_po_reserve,
			xed.last_updated_by 		= 	p_user_id,
			xed.last_update_date 	 	= 	SYSDATE
		WHERE xed.dashboard_rec_id 		= 	p_eo_subledger(i).dashboard_rec_id;
   
    END LOOP;
EXCEPTION WHEN OTHERS THEN
	p_error_flag := 'E';    
	p_error_msg := SQLERRM;
END update_eo_subledger;

PROCEDURE clean_cm_data(p_user_id IN NUMBER,
						p_error_flag OUT VARCHAR2,
						p_error_msg	OUT VARCHAR2)
IS

/***********************************************************************************************************
--this deletes the dashboard data to clean CM data
**************************************************************************************************************/
l_cm_data_count NUMBER;
BEGIN
	/***********************************************************************************************************
	--rules to clean CM Data
	--	Check whether user is authorized user (E&O Manager)
	--	Check E&O records with below conditions, if match all of conditions, program delete the records.
	--E&O review status is not active (A) or close (C)
	--It is a CM item which reserve data (buyback reserve (BUYBACK_EO_QTY), Ending Reserve, PO reserve) all are zero or null
	**************************************************************************************************************/
	
	BEGIN
		SELECT COUNT(*)
		INTO l_cm_data_count
		FROM xxsc.xxsc_eo_dashboard_tbl xed 
		WHERE xed.organization_id = 'CM'
		AND NVL(xed.eo_review_flag ,'N') NOT IN ('A','C') 
		AND NVL(xed.buyback_eo_qty,0) =0 
		AND NVL(xed.cur_eo_reserve_qty,0) = 0 
		AND NVL(xed.cur_po_reserve_qty,0) =0;
	EXCEPTION WHEN OTHERS THEN
		l_cm_data_count := 0;
	END;
	
	IF NVL(l_cm_data_count,0) > 0 THEN
		BEGIN
			DELETE FROM xxsc.xxsc_eo_dashboard_tbl xed 
			WHERE xed.organization_id = 'CM'
			AND NVL(xed.eo_review_flag ,'N') NOT IN ('A','C') 
			AND NVL(xed.buyback_eo_qty,0) =0 
			AND NVL(xed.cur_eo_reserve_qty,0) = 0 
			AND NVL(xed.cur_po_reserve_qty,0) =0;
		EXCEPTION WHEN OTHERS THEN
			p_error_flag := 'E';
			p_error_msg := 'Error while deleting records '||SQLERRM;
		END;
	ELSE
		p_error_flag := 'E';
		p_error_msg := 'No CM records found to delete';
	END IF;
	
END clean_cm_data;

END xxsc_eo_process_pkg;
/