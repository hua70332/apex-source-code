SET DEFINE OFF;
CREATE OR REPLACE PACKAGE BODY apps.xxsc_eo_data_load_pkg AS

--====================================================================================
-- $Header: xxsc_eo_data_load_pkg.sql, v1.1, 03-JAN-2021
--------------------------------------------------------------------------------
--
-- File Name:   xxsc_eo_data_load_pkg.sql
--address
-- Description: This package is to get data loaded in staging from APEX for Decision, Reserve, 
--				 CM and CM Usage validate the data and process it to dashborad table. 
--
--
-- History:

-- Version  Date           Modified By                Comments
-- -------  -----------    -------------            -----------------------------------------------------------------------------
--     1.0  03-JAN-2021    Ganesh Raghavan           Original version

PROCEDURE get_inv_item_dtls (p_inv_item IN VARCHAR2, 
							p_inv_org_code IN VARCHAR2,
							p_inv_item_id OUT NUMBER, 
							p_inv_org_id OUT NUMBER)
IS
/***********************************************************************************************
--this procedure is to get inventory item details 
************************************************************************************************/
l_inv_item_id NUMBER(10);
l_inv_org_id  NUMBER(10);

BEGIN

	BEGIN
		SELECT
			inventory_item_id,
			b.organization_id
		INTO
			l_inv_item_id,
			l_inv_org_id
		FROM mtl_system_items_b           a,
			org_organization_definitions b
		WHERE a.segment1 = p_inv_item
		AND a.organization_id = b.organization_id
		AND b.organization_code = p_inv_org_code;
	EXCEPTION WHEN OTHERS THEN
		l_inv_item_id:=0;
		l_inv_org_id :=0;
	END;

	p_inv_item_id := l_inv_item_id;
	p_inv_org_id  := l_inv_org_id;

END get_inv_item_dtls;
   
PROCEDURE error_duplicate_data(p_decision_batch_id IN NUMBER)
IS
/***********************************************************************************************
--this procedure is to identify the duplicate records in Decision load data and mark as error 
************************************************************************************************/
CURSOR cur_rec(p_decision_batch_id NUMBER) IS ---cursor to get duplicate records for the provided batch
SELECT decision_rec_id
FROM xxsc.xxsc_eo_decision_tbl xed,
    (SELECT COUNT(*),
           fiscal_year,
           fiscal_qtr,
           organization_code,
           item
    FROM xxsc.xxsc_eo_decision_tbl
    WHERE decision_batch_id = p_decision_batch_id
    GROUP BY fiscal_year,
           fiscal_qtr,
           organization_code,
           item
    HAVING COUNT(*) >1) dup
WHERE xed.fiscal_year = dup.fiscal_year
AND xed.fiscal_qtr = dup.fiscal_qtr
AND xed.organization_code = dup.organization_code
AND xed.item = dup.item
AND xed.decision_batch_id = p_decision_batch_id;

BEGIN
	FOR c_rec IN cur_rec(p_decision_batch_id) LOOP
		BEGIN
			UPDATE xxsc.xxsc_eo_decision_tbl 
			SET status = 'ERROR',
				error_msg = 'Item rejected, PN lines are duplicated'
			WHERE decision_rec_id = c_rec.decision_rec_id;
		EXCEPTION WHEN OTHERS THEN
			dbms_output.put_line('ERROR updating duplicate');
		END;
	END LOOP;
	COMMIT;
END;

PROCEDURE decision_data(p_decision_batch_id IN NUMBER,
						p_user_id IN NUMBER)
IS
/***********************************************************************************************
--this procedure is to process and update Decision records from custom table to dashboard table 
************************************************************************************************/

--cursor to get all the records to be processed with batch id with latest period record of the fiscal year and qtr in dashboard table
CURSOR cur_desn(p_decision_batch_id NUMBER) IS
SELECT  xd.decision_rec_id	,
        xd.fiscal_year	,
        xd.fiscal_qtr	,
        xd.fiscal_period	,
        xd.organization_code	,
        xd.item	,
        xd.disposition_qty1	,
        xd.disposition_code1	,
        xd.recomm_action1	,
        xd.comments_1	,
        xd.disposition_qty2	,
        xd.disposition_code2	,
        xd.recomm_action2	,
        xd.comments_2	,
        xd.action_comment	,
        xd.action_owner	,
        xd.completion_dt	,
        xd.review_comment	,
        xd.planner_approved	,
        xd.planner_approved_by	,
        xd.plm_approved	,
        xd.plm_approved_by	,
        xd.decision_batch_id	,
        xd.status	,
        xd.error_msg	,
        xd.creation_date	,
        xd.created_by	,
        xd.last_update_date	,
        xd.last_updated_by	,
        xd.filename	,
        xd.sys_recomm	,
        xd.sys_reasons	,
        xd.sys_comments	,
        xd.overwrite_data	,
		xed.dashboard_rec_id,
        xed.fiscal_year_d,
        xed.fiscal_qtr_d,
        xed.fiscal_period_d,
        xed.organization_code_d,
        xed.item_d,
        xed.disposition_qty1_d,
        xed.disposition_code1_d,
        xed.recomm_action1_d,
        xed.comments_1_d,
        xed.disposition_qty2_d,
        xed.disposition_code2_d,
        xed.recomm_action2_d,
        xed.comments_2_d,
        xed.action_comment_d,
        xed.action_owner_d,
        xed.completion_dt_d,
        xed.review_comment_d,
        xed.planner_approved_d,
        xed.planner_approved_by_d,
		xed.planner_approved_dt_d,
        xed.plm_approved_d,
        xed.plm_approved_by_d,
		xed.plm_approved_dt_d,
        xed.net_inv_d,
        xed.net_excess_usage_d,
        xed.eo_review_flag_d,
        xed.frozen_flag_d,
        xed.sys_recomm_d,
        xed.sys_reasons_d,
        xed.sys_comments_d,
		 (SELECT COUNT(1)
				FROM xxsc.xxsc_eo_dashboard_tbl xed
				WHERE xed.item = xd.item
				AND xed.organization_code = xd.organization_code
				AND xed.fiscal_year = xd.fiscal_year
				AND xed.fiscal_qtr  = xd.fiscal_qtr) rec_count
FROM  xxsc.xxsc_eo_decision_tbl xd
LEFT OUTER JOIN
    (SELECT /*+ PARALLEL(AUTO) +*/
           xed.dashboard_rec_id,
		   xed.fiscal_year			fiscal_year_d,
		   xed.fiscal_qtr          fiscal_qtr_d,
		   xed.fiscal_period       fiscal_period_d,
		   xed.organization_code   organization_code_d,
		   xed.item                item_d,
		   xed.disposition_qty1    disposition_qty1_d,
		   xed.disposition_code1   disposition_code1_d,
		   xed.recomm_action1      recomm_action1_d,
		   xed.comments_1           comments_1_d,
		   xed.disposition_qty2    disposition_qty2_d,
		   xed.disposition_code2   disposition_code2_d,
		   xed.recomm_action2      recomm_action2_d,
		   xed.comments_2           comments_2_d,
		   xed.action_comment      action_comment_d,
		   xed.action_owner        action_owner_d,
		   xed.completion_dt       completion_dt_d,
		   xed.review_comment      review_comment_d,
		   xed.planner_approved    planner_approved_d,
		   xed.planner_approved_by planner_approved_by_d,
		   xed.planner_approved_dt planner_approved_dt_d,
		   xed.plm_approved        plm_approved_d,
		   xed.plm_approved_by	   plm_approved_by_d,
		   xed.plm_approved_dt	   plm_approved_dt_d,
		   xed.net_inv			   net_inv_d,
		   xed.net_excess_usage	   net_excess_usage_d,
		   xed.eo_review_flag 		eo_review_flag_d,	
		   xed.frozen_flag			frozen_flag_d,
		   xed.sys_recomm 			sys_recomm_d,
		   xed.sys_reasons 			sys_reasons_d,
		   xed.sys_comments 		sys_comments_d
      FROM xxsc.xxsc_eo_dashboard_tbl xed,
           (SELECT xed.fiscal_year,
                   xed.fiscal_qtr,
                   xed.organization_code,
                   xed.item,
                   MAX(xed.fiscal_period) fiscal_period
            FROM xxsc.xxsc_eo_dashboard_tbl xed
            WHERE NVL(xed.eo_review_flag,'N') = 'A'
            AND NVL(xed.frozen_flag,'N') = 'N'
            GROUP BY xed.fiscal_year,
                   xed.fiscal_qtr,
                   xed.organization_code,
                   xed.item) prd
    WHERE xed.item = prd.item
    AND xed.organization_code = prd.organization_code
    AND xed.fiscal_year = prd.fiscal_year
    AND xed.fiscal_qtr = prd.fiscal_qtr
    AND xed.fiscal_period = prd.fiscal_period) xed
ON (xd.item = xed.item_d
    AND xd.organization_code = xed.organization_code_d
    AND xd.fiscal_year = xed.fiscal_year_d
    AND xd.fiscal_qtr =xed.fiscal_qtr_d
    )
WHERE 1=1
AND xd.decision_batch_id = p_decision_batch_id
AND UPPER(xd.status) = 'NEW'; 

--TYPE eo_decision_tbl IS TABLE OF xxsc.xxsc_eo_decision_tbl%ROWTYPE;

TYPE eo_decision_tbl IS TABLE OF cur_desn%ROWTYPE;
c_desn eo_decision_tbl;

l_eo_status eo_status_tbl := eo_status;

l_dashboard_record_id	NUMBER;
l_fiscal_year			VARCHAR2(20);
l_fiscal_qtr         	VARCHAR2(20);
l_fiscal_period      	VARCHAR2(20);
l_organization_code  	VARCHAR2(20);
l_item               	VARCHAR2(80);
l_disposition_qty1		NUMBER;
l_disposition_code1     VARCHAR2(80);
l_recomm_action1        VARCHAR2(260);
l_comments_1             VARCHAR2(260);
l_disposition_qty2      NUMBER(15,5);
l_disposition_code2     VARCHAR2(80);
l_recomm_action2        VARCHAR2(260);
l_comments_2             VARCHAR2(260);
l_action_comment        VARCHAR2(260);
l_action_owner          VARCHAR2(260);
l_completion_dt         DATE;
l_review_comment        VARCHAR2(260);
l_planner_approved      VARCHAR2(10);
l_planner_approved_by   VARCHAR2(40);
l_planner_approved_dt	DATE;
l_plm_approved          VARCHAR2(10);
l_plm_approved_by       VARCHAR2(40);
l_plm_approved_dt       DATE;
l_valid_period_cnt		NUMBER;
l_err_flag				VARCHAR2(10);
l_period_num_max 		NUMBER;
l_period_num_min 		NUMBER;
l_eo_mgr_resp			VARCHAR2(10);
l_eo_plm_resp			VARCHAR2(10);
l_eo_ops_resp			VARCHAR2(10);
l_provision_reason		VARCHAR2(250);
l_exception_reason		VARCHAR2(250);
l_err_msg				VARCHAR2(2000);
l_cm_or_lite			VARCHAR2(20);
l_net_inv 				NUMBER;
l_net_excess_usage		NUMBER;
l_eo_review_flag		VARCHAR2(10);
l_frozen_flag			VARCHAR2(10);
l_rec_count				NUMBER;
l_sys_recomm 			VARCHAR2(30);
l_sys_reasons 			VARCHAR2(80);
l_sys_comments 			VARCHAR2(260);
l_overwrite_data 		VARCHAR2(1);
l_status				VARCHAR2(80);

l_item_id				NUMBER;
l_org_id				NUMBER;
l_user_name				VARCHAR2(250);

BEGIN
	
	---to get the Employee name
	BEGIN
		SELECT NVL(ppf.full_name,fu.user_name) 
		INTO l_user_name
		FROM apps.fnd_user fu,
			(SELECT * 
			FROM  per_all_people_f ppf
			WHERE TRUNC(SYSDATE) BETWEEN TRUNC(ppf.effective_start_date) AND TRUNC(NVL(ppf.effective_end_date,SYSDATE))
			)ppf
		WHERE fu.employee_id = ppf.person_id(+)
		AND fu.user_id = p_user_id;
	EXCEPTION WHEN OTHERS THEN
		l_user_name := NULL;
	END;	
	
	----to check if user has E&O Manger responsibility
	BEGIN
		SELECT 'Y' resp
		INTO l_eo_mgr_resp
		FROM apps.fnd_user_resp_groups_direct  furg,
			   apps.fnd_user                   fu,
			   apps.fnd_responsibility_tl      frt,
			   apps.fnd_responsibility         fr,
			   apps.fnd_application_tl         fat,
			   apps.fnd_application            fa
		 WHERE furg.user_id             =  fu.user_id
		   AND furg.responsibility_id   =  frt.responsibility_id
		   AND fr.responsibility_id     =  frt.responsibility_id
		   AND fa.application_id        =  fat.application_id
		   AND fr.application_id        =  fat.application_id
		   AND fu.user_id = p_user_id
		   AND (furg.end_date IS NULL OR furg.end_date >= TRUNC(SYSDATE))
		   AND frt.responsibility_name = 'E&O - Manager';
	EXCEPTION WHEN OTHERS THEN
		l_eo_mgr_resp := 'N';
	END;
	
	----to check if user has E&O Operations responsibility
	BEGIN
		SELECT 'Y' resp
		INTO l_eo_ops_resp
		FROM apps.fnd_user_resp_groups_direct  furg,
			   apps.fnd_user                   fu,
			   apps.fnd_responsibility_tl      frt,
			   apps.fnd_responsibility         fr,
			   apps.fnd_application_tl         fat,
			   apps.fnd_application            fa
		 WHERE furg.user_id             =  fu.user_id
		   AND furg.responsibility_id   =  frt.responsibility_id
		   AND fr.responsibility_id     =  frt.responsibility_id
		   AND fa.application_id        =  fat.application_id
		   AND fr.application_id        =  fat.application_id
		   AND fu.user_id = p_user_id
		   AND (furg.end_date IS NULL OR furg.end_date >= TRUNC(SYSDATE))
		   AND frt.responsibility_name = 'E&O - Operations';
	EXCEPTION WHEN OTHERS THEN
		l_eo_ops_resp := 'N';
	END;
	
	----to check if user has E&O PLM responsibility
	BEGIN
		SELECT 'Y' resp
		INTO l_eo_plm_resp
		FROM apps.fnd_user_resp_groups_direct  furg,
			   apps.fnd_user                   fu,
			   apps.fnd_responsibility_tl      frt,
			   apps.fnd_responsibility         fr,
			   apps.fnd_application_tl         fat,
			   apps.fnd_application            fa
		 WHERE furg.user_id             =  fu.user_id
		   AND furg.responsibility_id   =  frt.responsibility_id
		   AND fr.responsibility_id     =  frt.responsibility_id
		   AND fa.application_id        =  fat.application_id
		   AND fr.application_id        =  fat.application_id
		   AND fu.user_id = p_user_id
		   AND (furg.end_date IS NULL OR furg.end_date >= TRUNC(SYSDATE))
		   AND frt.responsibility_name = 'E&O - PLM';
	EXCEPTION WHEN OTHERS THEN
		l_eo_plm_resp := 'N';
	END;
	
	----to check the  duplicate records and exculde them
	error_duplicate_data(p_decision_batch_id );
	
	OPEN cur_desn(p_decision_batch_id);
	LOOP
		
		FETCH cur_desn BULK COLLECT INTO c_desn;
		
		FOR i IN 1..c_desn.COUNT LOOP
			l_dashboard_record_id	:= NULL;
			l_fiscal_year           := NULL;
			l_fiscal_qtr            := NULL;
			l_fiscal_period         := NULL;
			l_organization_code     := NULL;
			l_item                  := NULL;
			l_disposition_qty1		:= NULL;	
			l_disposition_code1     := NULL;
			l_recomm_action1        := NULL;
			l_comments_1             := NULL;
			l_disposition_qty2      := NULL;
			l_disposition_code2     := NULL;
			l_recomm_action2        := NULL;
			l_comments_2             := NULL;
			l_action_comment        := NULL;
			l_action_owner          := NULL;
			l_completion_dt         := NULL;
			l_review_comment        := NULL;
			l_planner_approved      := NULL;
			l_planner_approved_by   := NULL;
			l_planner_approved_dt   := NULL;
			l_plm_approved          := NULL;
			l_plm_approved_by       := NULL;
			l_plm_approved_dt       := NULL;
			l_item_id				:= NULL;
			l_org_id				:= NULL;
			l_err_flag				:= NULL;
			l_valid_period_cnt		:= NULL;
			l_period_num_max 		:= NULL;
			l_period_num_min        := NULL;
			l_provision_reason		:= NULL;
			l_exception_reason		:= NULL;
			l_cm_or_lite			:= NULL;
			l_net_inv				:= NULL;
			l_net_excess_usage		:= NULL;
			l_eo_review_flag		:= NULL;
			l_frozen_flag			:= NULL;
			l_rec_count				:= NULL;
			l_sys_recomm 			:= NULL;
			l_sys_reasons 	        := NULL;
			l_sys_comments 	        := NULL;
			l_overwrite_data        := NULL;
			l_status				:= NULL;
			l_err_msg				:= NULL;

			
			---to check if fiscal_year and fiscal_qtr is valid in EBS and not null
			IF c_desn(i).fiscal_year IS NOT NULL AND c_desn(i).fiscal_qtr IS NOT NULL THEN
				BEGIN
					SELECT /*+ PARALLEL(AUTO) +*/
						   MAX(gp.period_num) period_num_max,
						   MIN(gp.period_num) period_num_min
					INTO l_period_num_max,
						 l_period_num_min
					FROM gl_periods_v gp     
					WHERE gp.period_year = c_desn(i).fiscal_year
					AND gp.quarter_num = c_desn(i).fiscal_qtr
					AND gp.period_set_name = 'XXSC ACC CAL';
				EXCEPTION WHEN OTHERS THEN	
					l_period_num_max := 0;
					l_period_num_min := 0;
				END;
				
				IF NVL(l_period_num_max,0) = 0 THEN
					l_err_flag := 'E';
					--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
					l_status := 'ERROR';
					l_err_msg := 'Fiscal year/quarter is not valid period';
					
				END IF; --IF NVL(l_valid_period_cnt,0) = 0
			ELSE
				l_err_flag := 'E';
				--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
				l_status := 'ERROR';
				l_err_msg := 'Fiscal year/quarter cannot be blank';
				
			END IF; --IF c_desn(i).fiscal_year IS NOT NULL AND c_desn(i).fiscal_qtr IS NOT NULL
			
			---to check if the oraganization is LITE or CM
			IF ASCII(SUBSTR(NVL(c_desn(i).organization_code,'X'),1,1)) BETWEEN 48 AND 57 THEN
			  l_cm_or_lite := 'LITE';
			ELSE
			  l_cm_or_lite := 'CM';
			END IF;
			
			----to check if inventory item and organization code is valid and not blank
			IF c_desn(i).item IS NOT NULL AND  c_desn(i).organization_code IS NOT NULL  AND l_cm_or_lite = 'LITE' THEN
				
				get_inv_item_dtls(c_desn(i).item,c_desn(i).organization_code,l_item_id,l_org_id);
				
				IF l_item_id = 0 OR l_org_id = 0 THEN
					l_err_flag := 'E';
					--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
					l_status := 'ERROR';
					l_err_msg := 'Inventory Item/Organization code is invalid';
				
				END IF;
			/*ELSIF c_desn(i).item IS NOT NULL AND c_desn(i).organization_code IS NOT NULL  AND l_cm_or_lite = 'CM' THEN
				BEGIN
					SELECT COUNT(*)
					INTO l_org_id
					FROM xxsc.xxsc_eo_dashboard_tbl xed
					WHERE xed.fiscal_year = nvl(c_desn(i).fiscal_year, 0)
					AND xed.fiscal_qtr = nvl(c_desn(i).fiscal_qtr, 0)
					AND xed.item = c_desn(i).item
					AND xed.organization_code = c_desn(i).organization_code;
				EXCEPTION WHEN OTHERS THEN
					l_org_id := 0;
				END;
				
				IF l_org_id = 0 THEN
					l_err_flag := 'E';
					--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
					l_status := 'ERROR';
					l_err_msg := 'Inventory Item/Organization code is invalid';
					
				END IF;*/
			ELSIF c_desn(i).item IS  NULL AND  c_desn(i).organization_code IS  NULL THEN
				l_err_flag := 'E';
				--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
				l_status := 'ERROR';
				l_err_msg := 'Inventory Item/Organization code cannot be blank';
				
			END IF; --IF c_desn(i).item IS NOT NULL and c_desn(i).organization_code IS NOT NULL
			
		
			
			IF c_desn(i).disposition_qty1 IS NOT NULL AND c_desn(i).disposition_code1 IS NULL THEN
				l_err_flag := 'E';
				--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
				l_status := 'ERROR';
				l_err_msg := 'Provision Reason is requried as Provision Accepted Qty is more than 0';
			END IF;
			
			IF c_desn(i).disposition_qty2 IS NOT NULL AND c_desn(i).disposition_code2 IS NULL THEN
				l_err_flag := 'E';
				--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
				l_status := 'ERROR';
				l_err_msg := 'Exception Reason is requried as Exception Accepted Qty is more than 0';
			END IF;
			
			IF 	c_desn(i).disposition_code1 IS NOT NULL THEN
			--to check Provision Reason is valid
				BEGIN
					SELECT flv.meaning
					INTO l_provision_reason
					FROM fnd_lookup_values flv
					WHERE flv.lookup_type IN ('XXSC_EO_PROVISION_REASON')
					AND flv.language='US'
					AND UPPER(flv.meaning) = UPPER(c_desn(i).disposition_code1)
					AND NVL(flv.tag,'N') IN('REASON','N') 
					AND NVL(flv.enabled_flag,'N') ='Y'
					AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE));
				EXCEPTION WHEN OTHERS THEN
					l_provision_reason := NULL;
				END;
				
				IF l_provision_reason IS NULL THEN
					l_err_flag := 'E';
					--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
					l_status := 'ERROR';
					l_err_msg := 'Provision Reason is not pre-defined reason code';
					
				END IF;
			END IF;
			
			IF 	c_desn(i).recomm_action1 IS NOT NULL THEN
			--to check Provision Recomemended Action is valid
				BEGIN
					SELECT flv.meaning
					INTO l_recomm_action1
					FROM fnd_lookup_values flv
					WHERE flv.lookup_type IN ('XXSC_EO_PROVISION_REASON')
					AND flv.language='US'
					AND UPPER(flv.meaning) = UPPER(c_desn(i).recomm_action1)
					AND NVL(flv.tag,'N') IN('ACTION','N') 
					AND NVL(flv.enabled_flag,'N') ='Y'
					AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE));
				EXCEPTION WHEN OTHERS THEN
					l_recomm_action1 := NULL;
				END;
				
				IF l_recomm_action1 IS NULL THEN
					l_err_flag := 'E';
					--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
					l_status := 'ERROR';
					l_err_msg := 'Provision Recommended Action is not pre-defined reason code';
					
				END IF;
			END IF;
			
			IF 	c_desn(i).disposition_code2 IS NOT NULL THEN
			--to check Exception Reason is valid
				BEGIN
					SELECT flv.meaning
					INTO l_exception_reason
					FROM fnd_lookup_values flv
					WHERE flv.lookup_type IN ('XXSC_EO_EXCEPTION_REASON')
					AND flv.language='US'
					AND NVL(flv.tag,'N') IN('REASON','N')
					AND NVL(flv.enabled_flag,'N') ='Y'
					AND UPPER(flv.meaning) = UPPER(c_desn(i).disposition_code2)
					AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE));
				EXCEPTION WHEN OTHERS THEN
					l_provision_reason := 0;
				END;
				
				IF l_exception_reason IS NULL  THEN
					l_err_flag := 'E';
					--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
					l_status := 'ERROR';
					l_err_msg := 'Exception Reason is not pre-defined reason code';
					
				END IF;
			END IF;
			
			IF 	c_desn(i).recomm_action2 IS NOT NULL THEN
			--to check Exception Recomemended Action is valid
				BEGIN
					SELECT flv.meaning
					INTO l_recomm_action2
					FROM fnd_lookup_values flv
					WHERE flv.lookup_type IN ('XXSC_EO_EXCEPTION_REASON')
					AND flv.language='US'
					AND UPPER(flv.meaning) = UPPER(c_desn(i).recomm_action2)
					AND NVL(flv.tag,'N') IN('ACTION','N') 
					AND NVL(flv.enabled_flag,'N') ='Y'
					AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE));
				EXCEPTION WHEN OTHERS THEN
					l_recomm_action1 := NULL;
				END;
				
				IF l_recomm_action2 IS NULL THEN
					l_err_flag := 'E';
					--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
					l_status := 'ERROR';
					l_err_msg := 'Exception Recommended Action is not pre-defined reason code';
					
				END IF;
			END IF;
				
			
			--to check if user is authorized for Sign off SCE
			IF UPPER(NVL(c_desn(i).planner_approved,'N')) IN('YES','Y') AND NVL(l_eo_ops_resp,'N') = 'N' AND NVL(l_eo_mgr_resp,'N') = 'N' THEN
				l_err_flag := 'E';
				--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
				l_status := 'ERROR';
				l_err_msg := 'Not authorized to Sign-OFF SCE';
					
			END IF;
			
			--to check if user is authorized for Sign off PLM
			IF UPPER(NVL(c_desn(i).plm_approved,'N')) IN('YES','Y') AND NVL(l_eo_plm_resp,'N') = 'N' AND  NVL(l_eo_mgr_resp,'N') = 'N' THEN
				l_err_flag := 'E';
				--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
				l_status := 'ERROR';
				l_err_msg := 'Not authorized to Sign-OFF PLM';
				
			END IF;
			
			--to check if user is authorized for Sign off sce
			IF (c_desn(i).sys_recomm IS NOT NULL OR c_desn(i).sys_reasons IS NOT NULL OR c_desn(i).sys_comments IS NOT NULL) AND  NVL(l_eo_mgr_resp,'N') = 'N' THEN
				l_err_flag := 'E';
				--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
				l_status := 'ERROR';
				l_err_msg := 'Not authorized to update Recommendation/Reason/Comment';
				
			END IF;
			
			
			IF NVL(c_desn(i).rec_count,0) = 0 THEN
				l_err_flag := 'E';
				--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
				l_status := 'ERROR';
				l_err_msg := 'E&O review record does not exsists ';
			END IF; 
				
			
			IF NVL(c_desn(i).dashboard_rec_id,0) = 0 THEN
				l_err_flag := 'E';
				--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
				l_status := 'ERROR';
				l_err_msg := 'The E&O review of quarter/item is not activated yet or frozen';
				
			END IF; 
			
			----to check the fiscal_period is WW13 for fiscal year and fiscal quarter		
			IF l_period_num_max = c_desn(i).fiscal_period_d AND NVL(l_eo_mgr_resp,'N') = 'N' AND NVL(l_err_flag,'N') <>'E' THEN
				l_err_flag := 'E';
				--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
				l_status := 'ERROR';
				l_err_msg := 'Not authorized to change WW13 data';
				
			END IF;
			
			---to check if provision and exception qty is less thatn net onhand and cal excesss
			IF c_desn(i).disposition_qty1 IS NOT NULL OR c_desn(i).disposition_qty2 IS NOT NULL THEN
				IF (NVL(c_desn(i).disposition_qty1,0) + NVL(c_desn(i).disposition_qty2,0)) > NVL(c_desn(i).net_inv_d,0) THEN
					l_err_flag := 'E';
					--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
					l_status := 'ERROR';
					l_err_msg := 'Accepted quantity of Provision/Exception exceed maximum quantity';
				END IF;
				
				---to check if provision and exception qty is less thatn 0
				IF NVL(c_desn(i).disposition_qty1,0) < 0 OR NVL(c_desn(i).disposition_qty2,0) < 0 THEN
					l_err_flag := 'E';
					--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
					l_status := 'ERROR';
					l_err_msg := 'Accepted quantity of Provision/Exception must be greater than 0';
				END IF;
				
				---to check if  exception qty is greater than cal excesss - provision qty
				IF NVL(c_desn(i).disposition_qty2,0) > (NVL(c_desn(i).net_excess_usage_d,0) - NVL(c_desn(i).disposition_qty1,0)) THEN
					l_err_flag := 'E';
					--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
					l_status := 'ERROR';
					l_err_msg := 'Exception Accepted Qty must be less than Cal Excess - Provision Accepted Qty ';
				END IF;
				
				---to check if provision + exception qty is greater than cal excesss and Planner or PLM is Approved
				IF (UPPER(NVL(c_desn(i).planner_approved,'N')) IN('YES','Y') OR UPPER(NVL(c_desn(i).plm_approved,'N')) IN('YES','Y')) AND 
					NVL(c_desn(i).net_excess_usage_d,0) > NVL(c_desn(i).disposition_qty1,0) + NVL(c_desn(i).disposition_qty2,0) THEN
					l_err_flag := 'E';
					--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
					l_status := 'ERROR';
					l_err_msg := NVL(c_desn(i).net_excess_usage_d,0) -( NVL(c_desn(i).disposition_qty1,0) + NVL(c_desn(i).disposition_qty2,0))||' units not reviewed';
				END IF;
			END IF;
			
			
			
			IF NVL(l_err_flag,'N') <> 'E' THEN
				---check if overwrite_data is X then update the column values provided in data template even if it is null
				IF NVL(c_desn(i).overwrite_data,'N') ='X' THEN 
					
						l_disposition_qty1 := c_desn(i).disposition_qty1;
					
						l_disposition_code1 := c_desn(i).disposition_code1;
					
						l_recomm_action1 := c_desn(i).recomm_action1;
					
						l_comments_1 := c_desn(i).comments_1;
					
						l_disposition_qty2 := c_desn(i).disposition_qty2;
					
						l_disposition_code2 := c_desn(i).disposition_code2;
					
						l_recomm_action2 := c_desn(i).recomm_action2;
					
						l_comments_2 := c_desn(i).comments_2;
					
						l_action_comment := c_desn(i).action_comment;
					
						l_action_owner := c_desn(i).action_owner;
					
						l_completion_dt := c_desn(i).completion_dt;
					
						l_review_comment := c_desn(i).review_comment;
					
						--l_planner_approved := c_desn(i).planner_approved;
						IF UPPER(c_desn(i).planner_approved) = 'YES' THEN
							l_planner_approved := 'Y';
						ELSE
							l_planner_approved := c_desn(i).planner_approved;
						END IF;
					
						l_planner_approved_by := c_desn(i).planner_approved_by;
					
						--l_plm_approved := c_desn(i).plm_approved;
						IF UPPER(c_desn(i).plm_approved) = 'YES' THEN
							l_plm_approved := 'Y';
						ELSE
							l_plm_approved := c_desn(i).plm_approved;
						END IF;
					
						l_plm_approved_by := c_desn(i).plm_approved_by;
					
						l_sys_recomm := c_desn(i).sys_recomm;
				
						l_sys_reasons := c_desn(i).sys_reasons;
					
						l_sys_comments := c_desn(i).sys_comments;
					
				
				ELSE  --overwrite_data is not X
					----set values as data sheet if column value is not null else set the exisiting column value in table
					IF c_desn(i).disposition_qty1 IS NOT NULL THEN
						l_disposition_qty1 := c_desn(i).disposition_qty1;
					ELSE
						l_disposition_qty1 := c_desn(i).disposition_qty1_d;
					END IF;
					
					IF c_desn(i).disposition_code1 IS NOT NULL THEN
						l_disposition_code1 := l_provision_reason;
					ELSE
						l_disposition_code1 := c_desn(i).disposition_code1_d;
					END IF;
					
					IF c_desn(i).recomm_action1 IS NOT NULL THEN
						l_recomm_action1 := c_desn(i).recomm_action1;
					ELSE
						l_recomm_action1 := c_desn(i).recomm_action1_d;
					END IF;
					
					IF c_desn(i).comments_1 IS NOT NULL THEN
						l_comments_1 := c_desn(i).comments_1;
					ELSE
						l_comments_1 := c_desn(i).comments_1_d;
					END IF;
					
					----
					
					IF c_desn(i).disposition_qty2 IS NOT NULL THEN
						l_disposition_qty2 := c_desn(i).disposition_qty2;
					ELSE
						l_disposition_qty2 := c_desn(i).disposition_qty2_d;
					END IF;
					
					IF c_desn(i).disposition_code2 IS NOT NULL THEN
						l_disposition_code2 := l_exception_reason;
					ELSE
						l_disposition_code2 := c_desn(i).disposition_code2_d;
					END IF;
					
					IF c_desn(i).recomm_action2 IS NOT NULL THEN
						l_recomm_action2 := c_desn(i).recomm_action2;
					ELSE
						l_recomm_action2 := c_desn(i).recomm_action2_d;
					END IF;
					
					IF c_desn(i).comments_2 IS NOT NULL THEN
						l_comments_2 := c_desn(i).comments_2;
					ELSE
						l_comments_2 := c_desn(i).comments_2_d;
					END IF;
					
					IF c_desn(i).action_comment IS NOT NULL THEN
						l_action_comment := c_desn(i).action_comment;
					ELSE
						l_action_comment := c_desn(i).action_comment_d;
					END IF;
					
					IF c_desn(i).action_owner IS NOT NULL THEN
						l_action_owner := c_desn(i).action_owner;
					ELSE
						l_action_owner := c_desn(i).action_owner_d;
					END IF;
					
					IF c_desn(i).completion_dt IS NOT NULL THEN
						l_completion_dt := c_desn(i).completion_dt;
					ELSE
						l_completion_dt := c_desn(i).completion_dt_d;
					END IF;
					
					IF c_desn(i).review_comment IS NOT NULL THEN
						l_review_comment := c_desn(i).review_comment;
					ELSE
						l_review_comment := c_desn(i).review_comment_d;
					END IF;
					
					IF c_desn(i).planner_approved IS NOT NULL THEN
						IF UPPER(c_desn(i).planner_approved) = 'YES' THEN
							l_planner_approved := 'Y';
						ELSE
							l_planner_approved := c_desn(i).planner_approved;
						END IF;
					ELSE
						l_planner_approved := c_desn(i).planner_approved_d;
					END IF;
					
					IF c_desn(i).planner_approved_by IS NOT NULL AND c_desn(i).planner_approved IS NOT NULL THEN
						l_planner_approved_by := c_desn(i).planner_approved_by;
						l_planner_approved_dt := SYSDATE;
					ELSIF c_desn(i).planner_approved_by IS NULL AND c_desn(i).planner_approved IS NOT NULL THEN
						l_planner_approved_by :=  l_user_name;
						l_planner_approved_dt := SYSDATE;
					ELSIF c_desn(i).planner_approved_by IS NULL THEN
						l_planner_approved_by := c_desn(i).planner_approved_by_d;
						l_planner_approved_dt := c_desn(i).planner_approved_dt_d;
					END IF;
					
					IF c_desn(i).plm_approved IS NOT NULL THEN
						IF UPPER(c_desn(i).plm_approved) = 'YES' THEN
							l_plm_approved := 'Y';
						ELSE
							l_plm_approved := c_desn(i).plm_approved;
						END IF;
					ELSE
						l_plm_approved := l_plm_approved;
					END IF;
					
					IF c_desn(i).plm_approved_by IS NOT NULL AND c_desn(i).plm_approved IS NOT NULL THEN
						l_plm_approved_by := c_desn(i).plm_approved_by;
						l_plm_approved_dt := SYSDATE;
					ELSIF c_desn(i).plm_approved_by IS NULL AND c_desn(i).plm_approved IS NOT NULL THEN
						l_plm_approved_by := l_user_name;
						l_plm_approved_dt := SYSDATE;
					ELSIF  c_desn(i).plm_approved_by IS  NULL THEN
						l_plm_approved_by := c_desn(i).plm_approved_by_d;
						l_plm_approved_dt := c_desn(i).plm_approved_dt_d;
					END IF;
					
					IF c_desn(i).sys_recomm IS NOT NULL THEN
						l_sys_recomm := c_desn(i).sys_recomm;
					ELSE
						l_sys_recomm := c_desn(i).sys_recomm_d;
					END IF;
					
					IF c_desn(i).sys_reasons IS NOT NULL THEN
						l_sys_reasons := c_desn(i).sys_reasons;
					ELSE
						l_sys_reasons := c_desn(i).sys_reasons_d;
					END IF;
					
					IF c_desn(i).sys_comments IS NOT NULL THEN
						l_sys_comments := c_desn(i).sys_comments;
					ELSE
						l_sys_comments := c_desn(i).sys_comments_d;
					END IF;
				END IF;---IF NVL(c_desn(i).overwrite_data,'N') ='X'
			
				IF NVL(l_err_flag,'N') <> 'E' THEN
				---insert the data into dashboard history table befoe update
					BEGIN
						INSERT INTO xxsc.xxsc_eo_dashboard_hist(
										eo_rec_id			,
										change_date			,
										change_by			,
										disposition_qty1	,
										disposition_code1 	,
										recomm_action1 		,
										comments_1 			,
										disposition_qty2 	,
										disposition_code2 	,
										recomm_action2 		,
										comments_2 			,
										action_comment 		,
										action_owner 		,
										review_comment 		,
										planner_approved 	,
										plm_approved 		)
										(SELECT xed.dashboard_rec_id,
											   SYSDATE,
											   p_user_id,
											   xed.disposition_qty1,
											   xed.disposition_code1,
											   xed.recomm_action1,
											   xed.comments_1,
											   xed.disposition_qty2,
											   xed.disposition_code2,
											   xed.recomm_action2,
											   xed.comments_2,
											   xed.action_comment,
											   xed.action_owner,
											   xed.review_comment,
											   xed.planner_approved,
											   xed.plm_approved
										FROM xxsc_eo_dashboard_tbl xed
										WHERE xed.dashboard_rec_id = c_desn(i).dashboard_rec_id);
				
				
						UPDATE xxsc.xxsc_eo_dashboard_tbl xed
						SET xed.disposition_qty1    = l_disposition_qty1	,
							xed.disposition_code1   = l_disposition_code1  	,
							xed.recomm_action1      = l_recomm_action1     	,
							xed.comments_1           = l_comments_1          	,
							xed.disposition_qty2    = l_disposition_qty2   	,
							xed.disposition_code2   = l_disposition_code2  	,
							xed.recomm_action2      = l_recomm_action2     	,
							xed.comments_2           = l_comments_2          	,
							xed.action_comment      = l_action_comment     	,
							xed.action_owner        = l_action_owner       	,
							xed.completion_dt       = l_completion_dt      	,
							xed.review_comment      = l_review_comment     	,
							xed.planner_approved    = l_planner_approved 	,
							xed.planner_approved_by = l_planner_approved_by	,
							xed.planner_approved_dt = l_planner_approved_dt ,
							xed.plm_approved        = l_plm_approved       	,
							xed.plm_approved_by     = l_plm_approved_by   	,
							xed.plm_approved_dt		= l_plm_approved_dt ,
							xed.sys_recomm			= l_sys_recomm,
							xed.sys_reasons			= l_sys_reasons,
							xed.sys_comments		= l_sys_comments,
							xed.last_updated_by 	= p_user_id,
							xed.last_update_date 	= SYSDATE
						WHERE dashboard_rec_id = c_desn(i).dashboard_rec_id;
						
						
						--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
						l_status := 'UPDATED';
						l_err_msg := NULL;
						
						
					EXCEPTION WHEN OTHERS THEN
						l_err_msg := SQLERRM;
						--l_eo_status(i).record_id := c_desn(i).decision_rec_id;
						l_status := 'ERROR';
						l_err_msg := 'Error while updating dashboard record '||l_err_msg;
					END;
				END IF;--IF NVL(l_err_flag,'N') <> 'E'
			END IF; --IF NVL(l_err_flag,'N') <> 'E'
			
			----update status and message in staging table
			BEGIN
				UPDATE xxsc.xxsc_eo_decision_tbl 
				SET status = l_status,
					error_msg = l_err_msg
				WHERE decision_rec_id = c_desn(i).decision_rec_id;
			END;
		END LOOP;
	
		EXIT WHEN cur_desn%NOTFOUND;
				
	END LOOP;	
	CLOSE cur_desn;
	
END decision_data;

PROCEDURE reserve_data(p_reserve_batch_id IN NUMBER,
						p_user_id IN NUMBER)
IS
/***********************************************************************************************
--this procedure is to process and update Reserve records from custom table to dashboard table 
************************************************************************************************/

--cursor to get all the records to be processed with batch id
CURSOR cur_res(p_reserve_batch_id NUMBER) IS
SELECT *
FROM  xxsc.xxsc_eo_reserve_tbl a
WHERE a.reserve_batch_id  = p_reserve_batch_id 
AND UPPER(status) = 'NEW';    -- STATUS

TYPE eo_reserve_tbl IS TABLE OF xxsc.xxsc_eo_reserve_tbl%ROWTYPE;
c_res eo_reserve_tbl;

l_eo_status eo_status_tbl := eo_status;

l_dashboard_rec_id			NUMBER;
l_fiscal_year				VARCHAR2(20);
l_fiscal_qtr                VARCHAR2(20);
l_fiscal_period             VARCHAR2(20);
l_organization_code         VARCHAR2(20);
l_item                      VARCHAR2(80);
l_lite_org_code             VARCHAR2(20);
l_cur_mrb_reserve_qty       NUMBER(15,5);
l_cur_rma_reserve_qty       NUMBER(15,5);
l_cur_sp_reserve_qty        NUMBER(15,5);
l_cur_po_reserve_qty        NUMBER(15,5);
l_cur_eo_reserve_qty        NUMBER(15,5);
l_begin_eo_reserve_qty      NUMBER(15,5);
l_begin_eo_qty_gaap         NUMBER(15,5);
l_gaapcode_begin            VARCHAR2(20);
l_trans_eo_reserve_qty   NUMBER(15,5);
l_buyback_eo_qty            NUMBER(15,5);
l_total_eo_usage            NUMBER(15,5);
l_unrel_eo_usage            NUMBER(15,5);
l_gaap_code                 VARCHAR2(20);
l_provision_onhand          NUMBER(15,5);
l_provision_po              NUMBER(15,5);
l_keep_po_reserve           NUMBER(15,5);
l_frozen_flag               VARCHAR2(1) ;
l_cm_item                   VARCHAR2(40);
l_cm_oh                     NUMBER(15,5);
l_eando_scrap               NUMBER(15,5);
l_valid_period				VARCHAR2(10);
l_err_flag					VARCHAR2(10);
l_eo_mgr_resp				VARCHAR2(10);
l_eo_plm_resp				VARCHAR2(10);
l_eo_ops_resp				VARCHAR2(10);
l_provision_reason			VARCHAR2(250);
l_exception_reason			VARCHAR2(250);
l_err_msg					VARCHAR2(2000);
l_eo_review_flag			VARCHAR2(10);
l_period_name				VARCHAR2(100);
l_cm_or_lite				VARCHAR2(100);
l_errm                      VARCHAR2(2000);
l_total_res					NUMBER;
l_prev_unrel_usg            NUMBER;
l_con_ord_ship              NUMBER;
l_concat_column				VARCHAR2(2000);
l_prev_year 				NUMBER;
l_prev_qtr  				NUMBER;
l_prev_period 				NUMBER;
l_cm_lite_org				NUMBER;

l_item_descr			VARCHAR2(1000);
l_item_type             VARCHAR2(40);
l_prod_heirarchy        VARCHAR2(100);
l_l3_bus_group          VARCHAR2(40);
l_l4_bus_unit           VARCHAR2(40);
l_l6_module             VARCHAR2(40);
l_gl_line_code          VARCHAR2(40);
l_gl_line_descr         VARCHAR2(40);

	
l_item_id					NUMBER;
l_org_id					NUMBER;
l_user_name					VARCHAR2(250);

BEGIN
	dbms_output.put_line('batchid '||p_reserve_batch_id);
	
	OPEN cur_res(p_reserve_batch_id);
	LOOP
	
		FETCH cur_res BULK COLLECT INTO c_res;
 
		FOR i IN 1..c_res.COUNT LOOP
			l_dashboard_rec_id			:= NULL;
			l_fiscal_year			    := NULL;
			l_fiscal_qtr                := NULL;
			l_fiscal_period             := NULL;
			l_organization_code         := NULL;
			l_item                      := NULL;
			l_lite_org_code             := NULL;
			l_cur_mrb_reserve_qty       := NULL;
			l_cur_rma_reserve_qty       := NULL;
			l_cur_sp_reserve_qty        := NULL;
			l_cur_po_reserve_qty        := NULL;
			l_cur_eo_reserve_qty        := NULL;
			l_begin_eo_reserve_qty      := NULL;
			l_begin_eo_qty_gaap         := NULL;
			l_gaapcode_begin            := NULL;
			l_trans_eo_reserve_qty   	:= NULL;
			l_buyback_eo_qty            := NULL;
			l_total_eo_usage            := NULL;
			l_unrel_eo_usage            := NULL;
			l_gaap_code                 := NULL;
			l_provision_onhand          := NULL;
			l_provision_po              := NULL;
			l_keep_po_reserve           := NULL;
			l_frozen_flag               := NULL;
			l_cm_item                   := NULL;
			l_cm_oh                     := NULL;
			l_eando_scrap               := NULL;
			l_valid_period				:= NULL;
			l_eo_review_flag			:= NULL;
			l_cm_or_lite				:= NULL;
			l_err_flag					:= NULL;
			l_item_id					:= NULL;
			l_org_id	                := NULL;
			l_total_res					:= NULL;
			l_prev_unrel_usg            := NULL;
			l_con_ord_ship              := NULL;
			l_concat_column	            := NULL;
			l_prev_year 				:= NULL;
			l_prev_qtr  				:= NULL;
			l_prev_period 				:= NULL;
			l_cm_lite_org				:= NULL;
			l_item_descr				:= NULL;
			l_item_type                 := NULL;
			l_prod_heirarchy            := NULL;
			l_l3_bus_group              := NULL;
			l_l4_bus_unit               := NULL;
			l_l6_module                 := NULL;
			l_gl_line_code              := NULL;
			l_gl_line_descr             := NULL;
			l_item_id					:= NULL;
			
				
		dbms_output.put_line('reserveid '||c_res(i).reserve_rec_id);
		
			---to check if fiscal_year, fiscal_qtr and fiscal_period is valid in EBS and not null
			IF c_res(i).fiscal_year IS NOT NULL AND c_res(i).fiscal_qtr IS NOT NULL AND c_res(i).fiscal_period IS NOT NULL THEN
				BEGIN
					SELECT 'Y'
					INTO l_valid_period
					FROM gl_periods_v gp     
					WHERE gp.period_year = c_res(i).fiscal_year
					AND gp.quarter_num = c_res(i).fiscal_qtr
					AND gp.period_num = c_res(i).fiscal_period
					AND gp.period_set_name = 'XXSC ACC CAL';
				EXCEPTION WHEN OTHERS THEN	
					l_valid_period := 'N';
				END;
				
				IF NVL(l_valid_period,'N') = 'N' THEN
					l_err_flag := 'E';
					l_eo_status(i).record_id := c_res(i).reserve_rec_id;
					l_eo_status(i).status := 'ERROR';
					l_eo_status(i).error_msg :='Fiscal year/quarter/period is invalid';
				END IF; --IF NVL(l_valid_period_cnt,0) = 0
			ELSE
				l_err_flag := 'E';
				l_eo_status(i).record_id := c_res(i).reserve_rec_id;
				l_eo_status(i).status := 'ERROR';
				l_eo_status(i).error_msg := 'Fiscal year/quarter/period cannot be blank';
			END IF; --IF c_res(i).fiscal_year IS NOT NULL AND c_res(i).fiscal_qtr IS NOT NULL
			
			---to check if the oraganization is LITE or CM
			IF ASCII(SUBSTR(NVL(c_res(i).organization_code,'X'),1,1)) BETWEEN 48 AND 57 THEN
			  l_cm_or_lite := 'LITE';
			ELSE
			  l_cm_or_lite := 'CM';
			END IF;
			
			----to check if inventory item and organization code is valid and not blank
			IF c_res(i).item IS NOT NULL AND  c_res(i).organization_code IS NOT NULL AND l_cm_or_lite = 'LITE'   THEN
				
				get_inv_item_dtls(c_res(i).item,c_res(i).organization_code,l_item_id,l_org_id);
				
				IF l_item_id = 0 OR l_org_id = 0 THEN
					l_err_flag := 'E';
					l_eo_status(i).record_id := c_res(i).reserve_rec_id;
					l_eo_status(i).status := 'ERROR';
					l_eo_status(i).error_msg := 'Inventory Item/Organization code is invalid';
				END IF;
			/*ELSIF c_res(i).item IS NOT NULL  AND l_cm_or_lite = 'CM' THEN
				get_inv_item_dtls(c_res(i).item,NVL(c_res(i).lite_org_code,89),l_item_id,l_org_id);
				
				IF l_item_id = 0 OR l_org_id = 0 THEN
					l_err_flag := 'E';
					UPDATE xxsc.xxsc_eo_reserve_tbl
					SET status = 'ERROR',
						error_msg = 'Inventory Item/Organization code is invalid',
						last_update_date = trunc(sysdate),
						last_updated_by = p_user_id
					WHERE  reserve_rec_id = c_res(i).reserve_rec_id;
					COMMIT;
				END IF;*/
			ELSIF c_res(i).item IS NULL  AND c_res(i).organization_code IS NULL THEN
				l_err_flag := 'E';
				l_eo_status(i).record_id := c_res(i).reserve_rec_id;
				l_eo_status(i).status := 'ERROR';
				l_eo_status(i).error_msg := 'Inventory Item/Organization code cannot be blank';
				
			END IF; --IF c_res(i).item IS NOT NULL and c_res(i).organization_code IS NOT NULL
			
			
			---to check if the CM Name and LITE Org is matching
			IF l_cm_or_lite = 'CM' THEN
				BEGIN
					SELECT COUNT(*)
					INTO l_cm_lite_org
					FROM apps.fnd_lookup_values flv
					WHERE flv.lookup_type ='XXSC_EO_CM_LITE_ORG'
					AND flv.lookup_code = c_res(i).lite_org_code
					AND NVL(flv.enabled_flag,'N') ='Y'
					AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
					AND flv.meaning = c_res(i).organization_code;
				EXCEPTION WHEN OTHERS THEN 
					l_cm_lite_org:= 0;
				END;
			
			
				IF NVL(l_cm_lite_org,0) = 0 THEN
					l_err_flag := 'E';
					l_eo_status(i).record_id := c_res(i).reserve_rec_id;
					l_eo_status(i).status := 'ERROR';
					l_eo_status(i).error_msg := 'CM name and LITE Org don’t match';
				END IF;
			END IF;
			
			----to get the existing data from dashboard table
			BEGIN
				SELECT xed.dashboard_rec_id,	
					xed.fiscal_year			    ,
					xed.fiscal_qtr              ,  
					xed.fiscal_period           ,  
					xed.organization_code       ,  
					xed.item                    ,  
					xed.lite_org_code           ,  
					xed.cur_mrb_reserve_qty     ,  
					xed.cur_rma_reserve_qty     ,  
					xed.cur_sp_reserve_qty      ,  
					xed.cur_po_reserve_qty      ,  
					xed.cur_eo_reserve_qty      ,  
					xed.begin_eo_reserve_qty    ,  
					xed.begin_eo_qty_gaap       ,  
					xed.gaapcode_begin          ,  
					xed.trans_eo_reserve_qty ,  
					xed.buyback_eo_qty          ,  
					xed.total_eo_usage          ,  
					xed.unrel_eo_usage          ,  
					xed.gaap_code               ,  
					xed.provision_onhand        ,  
					xed.provision_po            ,  
					xed.keep_po_reserve         ,  
					xed.cm_item                 ,  
					xed.cm_oh                   ,  
					xed.eando_scrap             ,
					NVL(xed.eo_review_flag,'N') ,
					NVL(xed.frozen_flag,'N')
				INTO l_dashboard_rec_id			,
					l_fiscal_year			    ,
					l_fiscal_qtr                ,
					l_fiscal_period             ,
					l_organization_code         ,
					l_item                      ,
					l_lite_org_code             ,
					l_cur_mrb_reserve_qty       ,
					l_cur_rma_reserve_qty       ,
					l_cur_sp_reserve_qty        ,
					l_cur_po_reserve_qty        ,
					l_cur_eo_reserve_qty        ,
					l_begin_eo_reserve_qty      ,
					l_begin_eo_qty_gaap         ,
					l_gaapcode_begin            ,
					l_trans_eo_reserve_qty   ,
					l_buyback_eo_qty            ,
					l_total_eo_usage            ,
					l_unrel_eo_usage            ,
					l_gaap_code                 ,
					l_provision_onhand          ,
					l_provision_po              ,
					l_keep_po_reserve           ,
					l_cm_item                   ,
					l_cm_oh                     ,
					l_eando_scrap               ,
					l_eo_review_flag			,
					l_frozen_flag
				FROM xxsc.xxsc_eo_dashboard_tbl xed
				WHERE xed.fiscal_year = c_res(i).fiscal_year
				AND xed.fiscal_qtr = c_res(i).fiscal_qtr
				AND xed.fiscal_period = c_res(i).fiscal_period
				AND xed.item = c_res(i).item
				AND xed.organization_code = c_res(i).organization_code;
				
			EXCEPTION WHEN OTHERS THEN
				l_dashboard_rec_id			:= NULL;
				l_fiscal_year			    := NULL;
				l_fiscal_qtr                := NULL;
				l_fiscal_period             := NULL;
				l_organization_code         := NULL;
				l_item                      := NULL;
				l_lite_org_code             := NULL;
				l_cur_mrb_reserve_qty       := NULL;
				l_cur_rma_reserve_qty       := NULL;
				l_cur_sp_reserve_qty        := NULL;
				l_cur_po_reserve_qty        := NULL;
				l_cur_eo_reserve_qty        := NULL;
				l_begin_eo_reserve_qty      := NULL;
				l_begin_eo_qty_gaap         := NULL;
				l_gaapcode_begin            := NULL;
				l_trans_eo_reserve_qty   	:= NULL;
				l_buyback_eo_qty            := NULL;
				l_total_eo_usage            := NULL;
				l_unrel_eo_usage            := NULL;
				l_gaap_code                 := NULL;
				l_provision_onhand          := NULL;
				l_provision_po              := NULL;
				l_keep_po_reserve           := NULL;
				l_frozen_flag               := NULL;
				l_cm_item                   := NULL;
				l_cm_oh                     := NULL;
				l_eando_scrap               := NULL;
				l_valid_period				:= NULL;
				l_eo_review_flag			:= NULL;
				
			END;
			
			
			
			
			IF NVL(l_dashboard_rec_id,0) > 0 AND NVL(l_err_flag,'N') <> 'E' THEN ----if record exists in table then update with new values
				
				dbms_output.put_line('l_dashboard_rec_id '||l_dashboard_rec_id||' l_frozen_flag '||l_frozen_flag);
				
				---check if E&O review is already closed or Frozen
				IF NVL(l_eo_review_flag,'N') = 'C' OR NVL(l_frozen_flag,'N') = 'Y'  THEN
					l_err_flag := 'E';
					l_eo_status(i).record_id := c_res(i).reserve_rec_id;
					l_eo_status(i).status := 'ERROR';
					l_eo_status(i).error_msg := 'The period/item is frozen or closed';
				END IF;
				
				dbms_output.put_line('l_dashboard_rec_id '||l_dashboard_rec_id||' l_err_flag '||l_err_flag);
				
				---set values as data sheet if column value is not null else set the exisiting column value in table
				IF NVL(l_err_flag,'N') <> 'E' THEN
					IF c_res(i).cur_mrb_reserve_qty IS NOT NULL THEN
						l_cur_mrb_reserve_qty := c_res(i).cur_mrb_reserve_qty;
					ELSE
						l_cur_mrb_reserve_qty := l_cur_mrb_reserve_qty;
					END IF;
					
					IF c_res(i).cur_rma_reserve_qty IS NOT NULL THEN
						l_cur_rma_reserve_qty := c_res(i).cur_rma_reserve_qty;
					ELSE
						l_cur_rma_reserve_qty := l_cur_rma_reserve_qty;
					END IF;
					
					IF c_res(i).cur_sp_reserve_qty IS NOT NULL THEN
						l_cur_sp_reserve_qty := c_res(i).cur_sp_reserve_qty;
					ELSE
						l_cur_sp_reserve_qty := l_cur_sp_reserve_qty;
					END IF;
					
					IF c_res(i).cur_po_reserve_qty IS NOT NULL THEN
						l_cur_po_reserve_qty := c_res(i).cur_po_reserve_qty;
					ELSE
						l_cur_po_reserve_qty := l_cur_po_reserve_qty;
					END IF;
					
					IF c_res(i).cur_eo_reserve_qty IS NOT NULL THEN
						l_cur_eo_reserve_qty := c_res(i).cur_eo_reserve_qty;
					ELSE
						l_cur_eo_reserve_qty := l_cur_eo_reserve_qty;
					END IF;
					
					IF c_res(i).begin_eo_reserve_qty IS NOT NULL THEN
						l_begin_eo_reserve_qty := c_res(i).begin_eo_reserve_qty;
					ELSE
						l_begin_eo_reserve_qty := l_begin_eo_reserve_qty;
					END IF;
					
					IF c_res(i).begin_eo_qty_gaap IS NOT NULL THEN
						l_begin_eo_qty_gaap := c_res(i).begin_eo_qty_gaap;
					ELSE
						l_begin_eo_qty_gaap := l_begin_eo_qty_gaap;
					END IF;
					
					IF c_res(i).gaapcode_begin IS NOT NULL THEN
						l_gaapcode_begin := c_res(i).gaapcode_begin;
					ELSE
						l_gaapcode_begin := l_gaapcode_begin;
					END IF;
					
					IF c_res(i).trans_eo_reserve_qty IS NOT NULL THEN
						l_trans_eo_reserve_qty := c_res(i).trans_eo_reserve_qty;
					ELSE
						l_trans_eo_reserve_qty := l_trans_eo_reserve_qty;
					END IF;
					
					IF c_res(i).buyback_eo_qty IS NOT NULL THEN
						l_buyback_eo_qty := c_res(i).buyback_eo_qty;
					ELSE
						l_buyback_eo_qty := l_buyback_eo_qty;
					END IF;
					
					IF c_res(i).total_eo_usage IS NOT NULL THEN
						l_total_eo_usage := c_res(i).total_eo_usage;
					ELSE
						l_total_eo_usage := l_total_eo_usage;
					END IF;
					
					IF c_res(i).unrel_eo_usage IS NOT NULL THEN
						l_unrel_eo_usage := c_res(i).unrel_eo_usage;
					ELSE
						l_unrel_eo_usage := l_unrel_eo_usage;
					END IF;
					
					IF c_res(i).gaap_code IS NOT NULL THEN
						l_gaap_code := c_res(i).gaap_code;
					ELSE
						l_gaap_code := l_gaap_code;
					END IF;
					
					IF c_res(i).provision_onhand IS NOT NULL THEN
						l_provision_onhand := c_res(i).provision_onhand;
					ELSE
						l_provision_onhand := l_provision_onhand;
					END IF;
					
					IF c_res(i).provision_po IS NOT NULL THEN
						l_provision_po := c_res(i).provision_po;
					ELSE
						l_provision_po := l_provision_po;
					END IF;
					
					IF c_res(i).keep_po_reserve IS NOT NULL THEN
						l_keep_po_reserve := c_res(i).keep_po_reserve;
					ELSE
						l_keep_po_reserve := l_keep_po_reserve;
					END IF;
					
					IF c_res(i).frozen_flag IS NOT NULL THEN
						l_frozen_flag := c_res(i).frozen_flag;
					ELSE
						l_frozen_flag := l_frozen_flag;
					END IF;
					
					IF c_res(i).cm_item IS NOT NULL THEN
						l_cm_item := c_res(i).cm_item;
					ELSE
						l_cm_item := l_cm_item;
					END IF;
					
					IF c_res(i).cm_oh IS NOT NULL THEN
						l_cm_oh := c_res(i).cm_oh;
					ELSE
						l_cm_oh := l_cm_oh;
					END IF;
					
					IF c_res(i).eando_scrap IS NOT NULL THEN
						l_eando_scrap := c_res(i).eando_scrap;
					ELSE
						l_eando_scrap := l_eando_scrap;
					END IF;
					
					BEGIN
						UPDATE xxsc.xxsc_eo_dashboard_tbl xed
						SET xed.cur_mrb_reserve_qty    		= l_cur_mrb_reserve_qty    ,
							xed.cur_rma_reserve_qty     	= l_cur_rma_reserve_qty    ,
							xed.cur_sp_reserve_qty      	= l_cur_sp_reserve_qty     ,
							xed.cur_po_reserve_qty      	= l_cur_po_reserve_qty     ,
							xed.cur_eo_reserve_qty      	= l_cur_eo_reserve_qty     ,
							xed.eo_reserve_inc_provision 	= l_cur_eo_reserve_qty     ,
							xed.begin_eo_reserve_qty    	= l_begin_eo_reserve_qty   ,
							xed.begin_eo_qty_gaap       	= l_begin_eo_qty_gaap      ,
							xed.gaapcode_begin          	= l_gaapcode_begin         ,
							xed.trans_eo_reserve_qty 		= l_trans_eo_reserve_qty,
							xed.buyback_eo_qty          	= l_buyback_eo_qty         ,
							xed.total_eo_usage          	= l_total_eo_usage         ,
							xed.unrel_eo_usage          	= l_unrel_eo_usage         ,
							xed.gaap_code               	= l_gaap_code              ,
							xed.provision_onhand        	= l_provision_onhand       ,
							xed.provision_po            	= l_provision_po           ,
							xed.keep_po_reserve         	= l_keep_po_reserve        ,
							xed.frozen_flag             	= l_frozen_flag            ,
							xed.cm_item                 	= l_cm_item                ,
							xed.cm_oh                   	= l_cm_oh                  ,
							xed.eando_scrap             	= l_eando_scrap       	   ,
							xed.reserve_ref_id				= c_res(i).reserve_rec_id,
							xed.last_updated_by 			= p_user_id,
							xed.last_update_date 			= SYSDATE						
						WHERE dashboard_rec_id = l_dashboard_rec_id;
						
						UPDATE xxsc.xxsc_eo_reserve_tbl
						SET status = 'UPDATED',
							last_update_date = TRUNC(SYSDATE),
							last_updated_by = p_user_id
						WHERE  reserve_rec_id = c_res(i).reserve_rec_id;
						COMMIT;
				
					EXCEPTION WHEN OTHERS THEN
						l_err_flag := 'E';
						l_errm := SQLERRM;
						l_eo_status(i).record_id := c_res(i).reserve_rec_id;
						l_eo_status(i).status := 'ERROR';
						l_eo_status(i).error_msg := 'Error in updating data '||l_errm;
					END;
				END IF;--IF NVL(l_err_flag,'N') <> 'E' THEN
			ELSIF NVL(l_dashboard_rec_id,0) = 0 AND NVL(l_err_flag,'N') <> 'E' THEN ----if record not exists in table then insert as new record
			
				--to validate CM item data and load
				IF NVL(l_cm_or_lite,'N') = 'CM' AND NVL(c_res(i).cm_oh,0) <> 0 THEN ---if CM item and CM onhand not 0
					/***********************************************************************************************
					--Validation rules
						--	If it is CM item and CM on-hand is not zero, load the data if data match one of below conditions
						--	Other data columns are not blank except period, organization and item columns 
						--	If all other data columns are blank, then check below conditions (Check item in any organization)
						--	Total reserve (CUR_EO_RESERVE_QTY + CUR_PO_RESERVE_QTY) of previous period is greater than 0
						--	Unrelease E&O Usage (UNREL_EO_USAGE) of previous period is greater than 0  
						--	Vendor sales quantity (CON_ORD_SHIP) of loading period is greater than 0 
					************************************************************************************************/
				
					l_concat_column := 	c_res(i).cur_mrb_reserve_qty    ||
										c_res(i).cur_rma_reserve_qty    ||
										c_res(i).cur_sp_reserve_qty     ||
										c_res(i).cur_po_reserve_qty     ||
										c_res(i).cur_eo_reserve_qty     ||
										c_res(i).cur_eo_reserve_qty     ||
										c_res(i).begin_eo_reserve_qty   ||
										c_res(i).begin_eo_qty_gaap      ||
										c_res(i).gaapcode_begin         ||
										c_res(i).trans_eo_reserve_qty   ||
										c_res(i).buyback_eo_qty         ||
										c_res(i).total_eo_usage         ||
										c_res(i).unrel_eo_usage         ||
										c_res(i).gaap_code              ||
										c_res(i).provision_onhand       ||
										c_res(i).provision_po           ||
										c_res(i).keep_po_reserve        ||
										c_res(i).frozen_flag			||
										c_res(i).cm_item                ||
									--	c_res(i).cm_oh                  ||
										c_res(i).eando_scrap;
										
					
					IF l_concat_column IS NULL THEN ---if all the other columns are null
						BEGIN
							---below query is to get previous quarter 
							SELECT gpv.period_year,
								   gpv.quarter_num,
								   gpv.period_num
							INTO l_prev_year,
								 l_prev_qtr,
								 l_prev_period
							FROM  apps.gl_periods_v gpv
							WHERE gpv.end_date = (
										SELECT MIN(start_date) - 1
										FROM gl_periods_v gp
										WHERE gp.period_year = c_res(1).fiscal_year
											AND gp.quarter_num = c_res(1).fiscal_qtr
											AND gp.period_set_name = 'XXSC ACC CAL')
							AND gpv.period_set_name = 'XXSC ACC CAL';
						EXCEPTION WHEN OTHERS THEN
							l_prev_year := 0;
							l_prev_qtr  := 0;
							l_prev_period := 0;
						END;
						
						---to get total reserve and previous unrel usage
						BEGIN
							SELECT SUM(NVL(cur_eo_reserve_qty,0) + NVL(cur_po_reserve_qty,0)),
								   SUM(NVL(unrel_eo_usage,0))
							INTO l_total_res,
								  l_prev_unrel_usg
							FROM xxsc.xxsc_eo_dashboard_tbl
							WHERE fiscal_year = l_prev_year
							AND fiscal_qtr =l_prev_qtr
							AND fiscal_period = l_prev_period
							AND item = c_res(i).item;
						EXCEPTION WHEN OTHERS THEN
							l_total_res := 0;
							l_prev_unrel_usg := 0;
						END;
						
						BEGIN
							SELECT SUM(NVL(con_ord_ship,0))
							INTO l_con_ord_ship
							FROM xxsc.xxsc_eo_dashboard_tbl
							WHERE fiscal_year = c_res(i).fiscal_year	
							AND fiscal_qtr =	c_res(i).fiscal_qtr    
							AND fiscal_period = c_res(i).fiscal_period 
							AND item = c_res(i).item;
						EXCEPTION WHEN OTHERS THEN
							l_con_ord_ship := 0;
						END;
							
						IF NVL(l_total_res,0) <= 0 OR NVL(l_prev_unrel_usg,0) <= 0 OR NVL(l_con_ord_ship,0) <= 0  THEN
							l_err_flag := 'E';
							l_eo_status(i).record_id := c_res(i).reserve_rec_id;
							l_eo_status(i).status := 'ERROR';
							l_eo_status(i).error_msg := 'CM Data conditions failed ';
						END IF;
					END IF; --IF l_concat_column IS NULL THEN
				END IF; --IF NVL(l_cm_or_lite,'N') = 'CM' AND NVL(c_res(i).cm_oh,0) <> 0
				
				--to get period name
				BEGIN
					SELECT period_name
					  INTO l_period_name
					  FROM APPS.gl_periods_v a
					 WHERE 1=1
					   AND period_set_name = 'XXSC ACC CAL'
					   AND period_year = c_res(i).fiscal_year --2021
					   AND period_num  = c_res(i).fiscal_period; --1;
				 EXCEPTION
					WHEN OTHERS THEN
						 l_period_name := NULL;
				 END;
				 
				IF NVL(l_err_flag,'N') <> 'E' THEN
				--to get inventory item columns information
					BEGIN
						SELECT 
							msi.description item_descr,
							flv_typ.meaning item_type,
							msi.inventory_item_id,
							(
									CASE
										WHEN nvl(hier.l1_total_prod_line, 'X') = 'X' THEN
											NULL
										ELSE
											hier.l1_total_prod_line
											|| '.'
											|| hier.l2_market_segment
											|| '.'
											|| hier.l3_business_group
											|| '.'
											|| l4_business_unit
											|| '.'
											|| hier.l5_product_line
											|| '.'
											|| hier.l6_product_family
									END
								)                           prod_heirarchy,
								hier.l3_business_group_desc  l3_bus_group,
								hier.l4_business_unit_desc   l4_bus_unit,
								hier.l6_product_family_desc  l6_module,
								TO_CHAR(hier.gl_product_line)         gl_line_code,
								hier.gl_product_line_desc    gl_line_descr
						INTO l_item_descr,
							 l_item_type,
							 l_item_id,
							 l_prod_heirarchy,
							 l_l3_bus_group,
							 l_l4_bus_unit,
							 l_l6_module,
							 l_gl_line_code,
							 l_gl_line_descr
						FROM apps.mtl_system_items_b msi,
							 apps.xxsc_sales_hierarchy_details_v hier,
							 (SELECT
									flv.meaning,
									flv.lookup_code
								FROM
									applsys.fnd_lookup_values flv
								WHERE
										flv.lookup_type = 'ITEM_TYPE'
									AND flv.view_application_id = 3) flv_typ,
							   (SELECT
									flv.meaning,
									flv.lookup_code
								FROM
									applsys.fnd_lookup_values flv
								WHERE
										flv.lookup_type = 'MTL_PLANNING_MAKE_BUY'
									AND flv.view_application_id = 700) flv_proc
						WHERE 1=1--ROWNUM=1
						AND msi.item_type = flv_typ.lookup_code(+)
						AND msi.planning_make_buy_code =  flv_proc.lookup_code(+)
						AND  msi.sales_account = hier.code_combination_id(+)
						AND msi.segment1 = c_res(i).item--'X01009010-000'
						AND msi.organization_id =(
							CASE
								WHEN NOT EXISTS (SELECT 'X' FROM apps.mtl_system_items_b m 
								WHERE m.organization_id = NVL(l_org_id,0)
								AND m.segment1 = c_res(i).item
								) THEN
									89   --GLO
								ELSE
									TO_NUMBER(l_org_id)
							END
						);
					EXCEPTION WHEN OTHERS THEN
						l_item_descr				:= NULL;
						l_item_type                 := NULL;
						l_prod_heirarchy            := NULL;
						l_l3_bus_group              := NULL;
						l_l4_bus_unit               := NULL;
						l_l6_module                 := NULL;
						l_gl_line_code              := NULL;
						l_gl_line_descr             := NULL;
						l_item_id					:= NULL;
					END;
				
					BEGIN
						INSERT INTO xxsc.xxsc_eo_dashboard_tbl(
								dashboard_rec_id		,
								fiscal_year			    ,
								fiscal_qtr              , 
								fiscal_period           , 
								period_name				,
								organization_code       , 
								organization_id			,
								item                    , 
								item_id					,
								lite_org_code           , 
								reserve_ref_id			,
								cur_mrb_reserve_qty     , 
								cur_rma_reserve_qty     , 
								cur_sp_reserve_qty      , 
								cur_po_reserve_qty      , 
								cur_eo_reserve_qty      , 
								eo_reserve_inc_provision,
								begin_eo_reserve_qty    , 
								begin_eo_qty_gaap       , 
								gaapcode_begin          , 
								trans_eo_reserve_qty , 
								buyback_eo_qty          , 
								total_eo_usage          , 
								unrel_eo_usage          , 
								gaap_code               , 
								provision_onhand        , 
								provision_po            , 
								keep_po_reserve         , 
								frozen_flag				,
								cm_item                 , 
								cm_oh                   , 
								eando_scrap             ,
								creation_date			,
								created_by				,
								last_updated_by 		,
								last_update_date )
						VALUES (xxsc.xxsc_eo_dashb_seq.NEXTVAL,
								c_res(i).fiscal_year			    ,
								c_res(i).fiscal_qtr              ,
								c_res(i).fiscal_period           ,
								l_period_name				,
								c_res(i).organization_code       ,
								DECODE(l_cm_or_lite,'CM','CM',l_org_id),
								c_res(i).item                    ,
								l_item_id					,
								c_res(i).lite_org_code           ,
								c_res(i).reserve_rec_id			,
								c_res(i).cur_mrb_reserve_qty     ,
								c_res(i).cur_rma_reserve_qty     ,
								c_res(i).cur_sp_reserve_qty      ,
								c_res(i).cur_po_reserve_qty      ,
								c_res(i).cur_eo_reserve_qty      ,
								c_res(i).cur_eo_reserve_qty      ,
								c_res(i).begin_eo_reserve_qty    ,
								c_res(i).begin_eo_qty_gaap       ,
								c_res(i).gaapcode_begin          ,
								c_res(i).trans_eo_reserve_qty ,
								c_res(i).buyback_eo_qty          ,
								c_res(i).total_eo_usage          ,
								c_res(i).unrel_eo_usage          ,
								c_res(i).gaap_code               ,
								c_res(i).provision_onhand        ,
								c_res(i).provision_po            ,
								c_res(i).keep_po_reserve         ,
								c_res(i).frozen_flag				,
								c_res(i).cm_item                 ,
								c_res(i).cm_oh                   ,
								c_res(i).eando_scrap             ,
								SYSDATE			,
								p_user_id				,
								p_user_id		,
								SYSDATE );
								
						UPDATE xxsc.xxsc_eo_reserve_tbl
						SET status = 'INSERTED',
							last_update_date = TRUNC(SYSDATE),
							last_updated_by = p_user_id
						WHERE  reserve_rec_id = c_res(i).reserve_rec_id;
								
					EXCEPTION WHEN OTHERS THEN
						l_err_flag := 'E';
						l_errm := SQLERRM;
						l_eo_status(i).record_id := c_res(i).reserve_rec_id;
						l_eo_status(i).status := 'ERROR';
						l_eo_status(i).error_msg := 'Error in inserting data '||l_errm;
					END;
				END IF;
			END IF;---IF NVL(l_dashboard_rec_id,0) > 0 AND NVL(l_err_flag,'N') <> 'E' THEN
			
		END LOOP;
		
		EXIT WHEN cur_res%NOTFOUND;
				
	END LOOP;	
	CLOSE cur_res;
	
	---- update the status in Reserve staging table
	IF l_eo_status.COUNT >0 THEN
		FORALL i IN 1..l_eo_status.COUNT
			UPDATE xxsc.xxsc_eo_reserve_tbl 
			SET status = l_eo_status(i).status,
				error_msg = l_eo_status(i).error_msg
			WHERE reserve_rec_id = l_eo_status(i).record_id;
	END IF;
	
	COMMIT;
		
END reserve_data;

PROCEDURE cm_data(p_cm_batch_id IN NUMBER,
				  p_user_id IN NUMBER)
IS
/***********************************************************************************************
--this procedure is to process and update CM records from custom table to dashboard table 
************************************************************************************************/

--cursor to get all the records to be processed with batch id
CURSOR cur_cm(p_cm_batch_id NUMBER) IS
SELECT *
FROM  xxsc.xxsc_eo_cm_tbl a
WHERE a.cm_batch_id  = p_cm_batch_id 
AND UPPER(status) = 'NEW';    -- STATUS

TYPE eo_cm_tbl IS TABLE OF xxsc.xxsc_eo_cm_tbl%ROWTYPE;
c_cm eo_cm_tbl;

l_eo_status eo_status_tbl := eo_status;
l_eo_cm_data eo_cm_data_tbl := eo_cm_data;

l_dashboard_rec_id		NUMBER;
l_fiscal_year			VARCHAR2(20);
l_fiscal_qtr         	VARCHAR2(20);
l_fiscal_period      	VARCHAR2(20);
l_organization_code  	VARCHAR2(20);
l_item               	VARCHAR2(80);
l_lite_org_code			VARCHAR2(20);
l_cm_item               VARCHAR2(40);
l_item_cost             NUMBER(15,5);
l_net_oh                NUMBER(15,5);
l_open_po               NUMBER(15,5);
l_gross_demand_12mo     NUMBER(15,5);
l_valid_period_cnt		NUMBER;
l_err_flag				VARCHAR2(10);
l_period_num	 		NUMBER;
l_err_msg				VARCHAR2(2000);
l_cm_or_lite			VARCHAR2(20);
l_errm                  VARCHAR2(2000);
l_period_name			VARCHAR2(100);
l_valid_period			VARCHAR2(10);
l_eo_review_flag		VARCHAR2(10);
l_frozen_flag			VARCHAR2(10);
l_cm_lite_org			NUMBER;

l_item_descr			VARCHAR2(1000);
l_item_type             VARCHAR2(40);
l_prod_heirarchy        VARCHAR2(100);
l_l3_bus_group          VARCHAR2(40);
l_l4_bus_unit           VARCHAR2(40);
l_l6_module             VARCHAR2(40);
l_gl_line_code          VARCHAR2(40);
l_gl_line_descr         VARCHAR2(40);


l_item_id				NUMBER;
l_org_id				NUMBER;
l_user_name				VARCHAR2(250);


BEGIN
	OPEN cur_cm(p_cm_batch_id);
	LOOP
	
		FETCH cur_cm BULK COLLECT INTO c_cm;
 
		FOR i IN 1..c_cm.COUNT LOOP
			l_dashboard_rec_id			:= NULL;
			l_fiscal_year			    := NULL;
			l_fiscal_qtr                := NULL;
			l_fiscal_period             := NULL;
			l_organization_code         := NULL;
			l_item                      := NULL;
			l_lite_org_code             := NULL;
			l_cm_item           		:= NULL;
			l_item_cost                 := NULL;
			l_net_oh                    := NULL;
			l_open_po                   := NULL;
		    l_gross_demand_12mo         := NULL;
            l_valid_period_cnt	        := NULL;
            l_err_flag			        := NULL;
            l_period_num	 	        := NULL;
            l_err_msg			        := NULL;
            l_cm_or_lite		        := NULL;
            l_errm                      := NULL;
			l_item_id					:= NULL;
            l_org_id					:= NULL;
			l_period_name				:= NULL;
			l_valid_period				:= NULL;
			l_eo_review_flag	        := NULL;
			l_frozen_flag               := NULL;
			l_cm_lite_org				:= NULL;
			l_item_descr				:= NULL;
			l_item_type                 := NULL;
			l_prod_heirarchy            := NULL;
			l_l3_bus_group              := NULL;
			l_l4_bus_unit               := NULL;
			l_l6_module                 := NULL;
			l_gl_line_code              := NULL;
			l_gl_line_descr             := NULL;
			
			
        
			---to get the period number from Week
			IF c_cm(i).fiscal_period = 'WW05' THEN ---1st period of the quarter
				BEGIN
					SELECT gp.period_num,
						   gp.period_name
					INTO l_period_num,
						 l_period_name
					FROM gl_periods_v gp     
					WHERE gp.period_year = c_cm(i).fiscal_year
					AND gp.quarter_num = c_cm(i).fiscal_qtr
					AND gp.period_set_name = 'XXSC ACC CAL'
					AND gp.period_num =(
						SELECT MIN(gp.period_num)
						FROM gl_periods_v gp     
						WHERE gp.period_year = c_cm(i).fiscal_year
						AND gp.quarter_num = c_cm(i).fiscal_qtr
						AND gp.period_set_name = 'XXSC ACC CAL');
				EXCEPTION WHEN OTHERS THEN
					l_period_num    := 0;
					l_period_name	:= NULL;
				END;
			ELSIF c_cm(i).fiscal_period = 'WW13' THEN ---last period of the quarter
				BEGIN
					SELECT gp.period_num,
						   gp.period_name
					INTO l_period_num,
						 l_period_name
					FROM gl_periods_v gp     
					WHERE gp.period_year = c_cm(i).fiscal_year
					AND gp.quarter_num = c_cm(i).fiscal_qtr
					AND gp.period_set_name = 'XXSC ACC CAL'
					AND gp.period_num =(
						SELECT MAX(gp.period_num)
						FROM gl_periods_v gp     
						WHERE gp.period_year = c_cm(i).fiscal_year
						AND gp.quarter_num = c_cm(i).fiscal_qtr
						AND gp.period_set_name = 'XXSC ACC CAL');
				EXCEPTION WHEN OTHERS THEN
					l_period_num    := 0;
					l_period_name	:= NULL;
				END;
			ELSE 
				l_period_num    := 0;
			END IF;
			
			
			IF NVL(l_period_num,0) = 0 THEN
				l_err_flag := 'E';
				l_eo_status(i).record_id := c_cm(i).cm_rec_id;
				l_eo_status(i).status := 'ERROR';
				l_eo_status(i).error_msg := 'Invalid Week#';
			ELSIF c_cm(i).fiscal_year IS NOT NULL AND c_cm(i).fiscal_qtr IS NOT NULL THEN
				---to check if the fiscal year quarter and period is valid in EBS and not null
				BEGIN
					SELECT 'Y'
					INTO l_valid_period
					FROM gl_periods_v gp     
					WHERE gp.period_year = c_cm(i).fiscal_year
					AND gp.quarter_num = c_cm(i).fiscal_qtr
					AND gp.period_num = l_period_num
					AND gp.period_set_name = 'XXSC ACC CAL';
				EXCEPTION WHEN OTHERS THEN	
					l_valid_period := 'N';
				END;
				
				IF NVL(l_valid_period,'N') = 'N' THEN
					l_err_flag := 'E';
					l_eo_status(i).record_id := c_cm(i).cm_rec_id;
					l_eo_status(i).status := 'ERROR';
					l_eo_status(i).error_msg := 'Fiscal year/quarter/week# is invalid';
					
				END IF; --IF NVL(l_valid_period_cnt,0) = 0
			ELSE
				l_err_flag := 'E';
				l_eo_status(i).record_id := c_cm(i).cm_rec_id;
				l_eo_status(i).status := 'ERROR';
				l_eo_status(i).error_msg := 'Fiscal year/quarter/week# cannot be blank';
			END IF; --NVL(l_period_num,0) = 0 
			
			
			---to check if the oraganization is LITE or CM
			IF ASCII(SUBSTR(NVL(c_cm(i).organization_code,'X'),1,1)) BETWEEN 48 AND 57 THEN
			  l_cm_or_lite := 'LITE';
			ELSE
			  l_cm_or_lite := 'CM';
			END IF;
			
			----to check if inventory item and organization code is valid and not blank
			IF c_cm(i).item IS NOT NULL AND  c_cm(i).organization_code IS NOT NULL AND l_cm_or_lite = 'LITE'   THEN
				
				get_inv_item_dtls(c_cm(i).item,c_cm(i).organization_code,l_item_id,l_org_id);
				
				IF l_item_id = 0 OR l_org_id = 0 THEN
					l_err_flag := 'E';
					l_eo_status(i).record_id := c_cm(i).cm_rec_id;
					l_eo_status(i).status := 'ERROR';
					l_eo_status(i).error_msg := 'Inventory Item/Organization code is invalid';
					
				END IF;
			/*ELSIF c_cm(i).item IS NOT NULL  AND l_cm_or_lite = 'CM' THEN
				get_inv_item_dtls(c_cm(i).item,NVL(c_cm(i).lite_org_code,89),l_item_id,l_org_id);
				
				IF l_item_id = 0 OR l_org_id = 0 THEN
					l_err_flag := 'E';
					l_eo_status(i).record_id := c_cm(i).cm_rec_id;
					l_eo_status(i).status := 'ERROR';
					l_eo_status(i).error_msg := 'Inventory Item/Organization code is invalid';
				END IF;*/
			ELSIF c_cm(i).item IS NULL OR c_cm(i).organization_code IS NULL THEN
				l_err_flag := 'E';
				l_eo_status(i).record_id := c_cm(i).cm_rec_id;
				l_eo_status(i).status := 'ERROR';
				l_eo_status(i).error_msg := 'Inventory Item/Organization code  cannot be blank';
			END IF; --IF c_cm(i).item IS NOT NULL and c_cm(i).organization_code IS NOT NULL
			
			---to check if the CM Name and LITE Org is matching
			BEGIN
				SELECT COUNT(*)
				INTO l_cm_lite_org
				FROM apps.fnd_lookup_values flv
				WHERE flv.lookup_type ='XXSC_EO_CM_LITE_ORG'
				AND flv.lookup_code = c_cm(i).lite_org_code
                AND flv.meaning = c_cm(i).organization_code
				AND NVL(flv.enabled_flag,'N') ='Y'
				AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE));
			EXCEPTION WHEN OTHERS THEN 
				l_cm_lite_org:= 0;
			END;
			
			IF NVL(l_cm_lite_org,0) = 0 THEN
				l_err_flag := 'E';
				l_eo_status(i).record_id := c_cm(i).cm_rec_id;
				l_eo_status(i).status := 'ERROR';
				l_eo_status(i).error_msg := 'CM name and LITE Org don’t match';
			END IF;
		
		----to get the existing data from dashboard table
			BEGIN
				SELECT xed.dashboard_rec_id,	
					xed.fiscal_year			    ,
					xed.fiscal_qtr              ,  
					xed.fiscal_period           ,  
					xed.organization_code       ,  
					xed.item                    ,  
					xed.lite_org_code           ,  
					xed.cm_item                 ,  
					xed.item_cost         		,
					xed.net_oh                  ,
                    xed.open_po                 ,
                    xed.gross_demand_12mo       ,
					NVL(xed.eo_review_flag,'N') ,
					NVL(xed.frozen_flag,'N')
				INTO l_dashboard_rec_id			,
					l_fiscal_year			    ,
					l_fiscal_qtr                ,
					l_fiscal_period             ,
					l_organization_code         ,
					l_item                      ,
					l_lite_org_code             ,
					l_cm_item                   ,
					l_item_cost         		,
                    l_net_oh                    ,
                    l_open_po                   ,
                    l_gross_demand_12mo         ,
					l_eo_review_flag			,
					l_frozen_flag
				FROM xxsc.xxsc_eo_dashboard_tbl xed
				WHERE xed.fiscal_year = c_cm(i).fiscal_year
				AND xed.fiscal_qtr = c_cm(i).fiscal_qtr
				AND xed.fiscal_period = l_period_num
				AND xed.item = c_cm(i).item
				AND xed.organization_code = c_cm(i).organization_code;
				
			EXCEPTION WHEN OTHERS THEN
				l_dashboard_rec_id			:= NULL;
				l_fiscal_year				:= NULL;
				l_fiscal_qtr                := NULL;
				l_fiscal_period             := NULL;
				l_organization_code         := NULL;
				l_item                      := NULL;
				l_lite_org_code             := NULL;
				l_cm_item                   := NULL;
				l_item_cost                 := NULL;
				l_net_oh                    := NULL;
				l_open_po                   := NULL;
				l_gross_demand_12mo         := NULL;
				l_eo_review_flag	        := NULL;
				l_frozen_flag               := NULL;
			END;
			
			
			IF NVL(l_dashboard_rec_id,0) > 0 AND NVL(l_err_flag,'N') <> 'E' THEN ----if record exists in table update the data
				
				IF NVL(l_eo_review_flag,'N') IN ('A','C') OR NVL(l_frozen_flag,'N') = 'Y' AND NVL(l_err_flag,'E') <> 'E' THEN
					l_err_flag := 'E';
					l_eo_status(i).record_id := c_cm(i).cm_rec_id;
					l_eo_status(i).status := 'ERROR';
					l_eo_status(i).error_msg := 'The record is Frozen or Activated';
				END IF;
				
				----set values as data sheet if column value is not null else set the exisiting column value in table
				
				IF NVL(l_err_flag,'N') <> 'E' THEN
					IF c_cm(i).cm_item IS NOT NULL THEN
						l_cm_item := c_cm(i).cm_item;
					ELSE
						l_cm_item := l_cm_item;
					END IF;
					
					IF c_cm(i).item_cost IS NOT NULL THEN
						l_item_cost := c_cm(i).item_cost;
					ELSE
						l_item_cost := l_item_cost;
					END IF;
					
					IF c_cm(i).net_oh IS NOT NULL THEN
						l_net_oh := c_cm(i).net_oh;
					ELSE
						l_net_oh := l_net_oh;
					END IF;
					
					IF c_cm(i).open_po IS NOT NULL THEN
						l_open_po := c_cm(i).open_po;
					ELSE
						l_open_po := l_open_po;
					END IF;
					
					IF c_cm(i).gross_demand_12mo IS NOT NULL THEN
						l_gross_demand_12mo := c_cm(i).gross_demand_12mo;
					ELSE
						l_gross_demand_12mo := l_gross_demand_12mo;
					END IF;
					
					BEGIN
						UPDATE xxsc.xxsc_eo_dashboard_tbl xed
						SET xed.cm_item             = l_cm_item,
							xed.item_cost           = l_item_cost,
							xed.net_oh             	= l_net_oh,
							xed.open_po				= l_open_po,
							xed.gross_demand_12mo	= l_gross_demand_12mo,
							xed.full_demand			= l_gross_demand_12mo,
							xed.cmoh_ref_id			= 1,
							xed.last_updated_by 	= p_user_id,
							xed.last_update_date 	= SYSDATE						
						WHERE dashboard_rec_id = l_dashboard_rec_id;
						
						l_eo_status(i).record_id := c_cm(i).cm_rec_id;
						l_eo_status(i).status := 'UPDATED';
						l_eo_status(i).error_msg := NULL;
				
					EXCEPTION WHEN OTHERS THEN
						l_err_flag := 'E';
						l_errm := SQLERRM;
						l_eo_status(i).record_id := c_cm(i).cm_rec_id;
						l_eo_status(i).status := 'ERROR';
						l_eo_status(i).error_msg := 'Error in updating data '||l_errm;
					END;
				END IF;--IF NVL(l_err_flag,'N') <> 'E' THEN
				
			ELSIF NVL(l_dashboard_rec_id,0) = 0 AND NVL(l_err_flag,'N') <> 'E' THEN ----if record not exists in table then insert new records
				BEGIN
					---to get inventory item columns information
					SELECT 
						msi.description item_descr,
						flv_typ.meaning item_type,
						msi.inventory_item_id,
						(
								CASE
									WHEN nvl(hier.l1_total_prod_line, 'X') = 'X' THEN
										NULL
									ELSE
										hier.l1_total_prod_line
										|| '.'
										|| hier.l2_market_segment
										|| '.'
										|| hier.l3_business_group
										|| '.'
										|| l4_business_unit
										|| '.'
										|| hier.l5_product_line
										|| '.'
										|| hier.l6_product_family
								END
							)                           prod_heirarchy,
							hier.l3_business_group_desc  l3_bus_group,
							hier.l4_business_unit_desc   l4_bus_unit,
							hier.l6_product_family_desc  l6_module,
							hier.gl_product_line         gl_line_code,
							hier.gl_product_line_desc    gl_line_descr
					INTO l_item_descr,
						 l_item_type,
						 l_item_id,
						 l_prod_heirarchy,
						 l_l3_bus_group,
						 l_l4_bus_unit,
						 l_l6_module,
						 l_gl_line_code,
						 l_gl_line_descr
					FROM apps.mtl_system_items_b msi,
						 apps.xxsc_sales_hierarchy_details_v hier,
						 (SELECT
								flv.meaning,
								flv.lookup_code
							FROM
								applsys.fnd_lookup_values flv
							WHERE
									flv.lookup_type = 'ITEM_TYPE'
								AND flv.view_application_id = 3) flv_typ,
						   (SELECT
								flv.meaning,
								flv.lookup_code
							FROM
								applsys.fnd_lookup_values flv
							WHERE
									flv.lookup_type = 'MTL_PLANNING_MAKE_BUY'
								AND flv.view_application_id = 700) flv_proc
					WHERE 1=1--ROWNUM=1
					AND msi.item_type = flv_typ.lookup_code(+)
					AND msi.planning_make_buy_code =  flv_proc.lookup_code(+)
					AND  msi.sales_account = hier.code_combination_id(+)
					AND msi.segment1 = c_cm(i).item--'X01009010-000'
					AND msi.organization_id =(
						CASE
							WHEN NOT EXISTS (SELECT 'X' FROM apps.mtl_system_items_b m 
							WHERE m.organization_id = NVL(l_org_id,0)
							AND m.segment1 = c_cm(i).item
							) THEN
								89   --GLO
							ELSE
								TO_NUMBER(l_org_id)
						END
					);
				EXCEPTION WHEN OTHERS THEN
					l_item_descr				:= NULL;
					l_item_type                 := NULL;
					l_prod_heirarchy            := NULL;
					l_l3_bus_group              := NULL;
					l_l4_bus_unit               := NULL;
					l_l6_module                 := NULL;
					l_gl_line_code              := NULL;
					l_gl_line_descr             := NULL;
					l_item_id					:= NULL;
				END;
				 
				BEGIN
					INSERT INTO xxsc.xxsc_eo_dashboard_tbl(
							dashboard_rec_id		,
							fiscal_year			    ,
							fiscal_qtr              , 
							fiscal_period           , 
							period_name				,
							organization_code       , 
							organization_id			,
							item                    , 
							item_id					,
							lite_org_code           , 
							cm_item                 , 
							item_cost               ,
							net_oh             		,
							open_po             	,
							gross_demand_12mo       ,
							full_demand				,
							creation_date			,
							created_by				,
							last_updated_by 		,
							last_update_date 		,
							item_descr				,
							item_type               ,
							prod_heirarchy          ,
							l3_bus_group            ,
							l4_bus_unit             ,
							l6_module               ,
							gl_line_code            ,
							gl_line_descr           ,
							cmoh_ref_id
							)
					VALUES (xxsc.xxsc_eo_dashb_seq.NEXTVAL,
							c_cm(i).fiscal_year			  ,
							c_cm(i).fiscal_qtr            ,
							l_period_num        ,
							l_period_name				  ,
							c_cm(i).organization_code     ,
							DECODE(l_cm_or_lite,'CM','CM',l_org_id),
							c_cm(i).item,
							l_item_id,
							c_cm(i).lite_org_code,
							c_cm(i).cm_item ,
							c_cm(i).item_cost,
							c_cm(i).net_oh ,
							c_cm(i).open_po,
							c_cm(i).gross_demand_12mo,
							c_cm(i).gross_demand_12mo,
							SYSDATE,
							p_user_id,
							p_user_id,
							SYSDATE ,
							l_item_descr	,
							l_item_type     ,
							l_prod_heirarchy,
							l_l3_bus_group  ,
							l_l4_bus_unit   ,
							l_l6_module     ,
							l_gl_line_code  ,
							l_gl_line_descr ,
							1
							);
							
					l_eo_status(i).record_id := c_cm(i).cm_rec_id;
					l_eo_status(i).status := 'INSERTED';
					l_eo_status(i).error_msg := NULL;
							
				EXCEPTION WHEN OTHERS THEN
					l_err_flag := 'E';
					l_errm := SQLERRM;
					l_eo_status(i).record_id := c_cm(i).cm_rec_id;
					l_eo_status(i).status := 'ERROR';
					l_eo_status(i).error_msg := 'Error while inserting data '||l_errm;
					
				END;
			END IF;---IF NVL(l_dashboard_rec_id,0) > 0 AND NVL(l_err_flag,'N') <> 'E' THEN
		END LOOP;
		
		EXIT WHEN cur_cm%NOTFOUND;
				
	END LOOP;	
	CLOSE cur_cm;
	
	---- update the status in CM staging table
	IF l_eo_status.COUNT >0 THEN
		FORALL i IN 1..l_eo_status.COUNT
			UPDATE xxsc.xxsc_eo_cm_tbl 
			SET status = l_eo_status(i).status,
				error_msg = l_eo_status(i).error_msg
			WHERE cm_rec_id = l_eo_status(i).record_id;
	END IF;
	
	COMMIT;
END cm_data;

PROCEDURE cm_data_aggrigate(p_cm_batch_id IN NUMBER,
				  p_user_id IN NUMBER)
IS
/***********************************************************************************************
--this procedure is to process and update aggregation of CM data having multiple records from custom table to dashboard table 
************************************************************************************************/

--cursor to get all the records to be processed with batch id
CURSOR cur_cm(p_cm_batch_id NUMBER) IS
SELECT COUNT(1),
	xcm.fiscal_year,
	xcm.fiscal_qtr,
	xcm.fiscal_period,
	xcm.organization_code,
	xcm.item,
	CASE WHEN SUM(NVL(xcm.net_oh,0)) > 0 THEN 
		ROUND(SUM(DECODE(NVL(xcm.item_cost,0),0,1,item_cost)*xcm.net_oh)/SUM(xcm.net_oh),4)
	ELSE
		ROUND(SUM(NVL(xcm.item_cost,0))/COUNT(1),4)
	END item_cost,
	SUM(NVL(xcm.net_oh,0)) net_oh,
	SUM(NVL(xcm.open_po,0)) open_po,
	SUM(NVL(xcm.gross_demand_12mo,0)) gross_demand_12mo
FROM xxsc_eo_cm_tbl xcm
WHERE cm_batch_id = p_cm_batch_id
AND UPPER(status) = 'NEW'
--and item='22134606'
GROUP BY xcm.fiscal_year,
	xcm.fiscal_qtr,
	xcm.fiscal_period,
	xcm.organization_code,
	xcm.item
HAVING COUNT(1)>1;


TYPE eo_cm_tbl IS TABLE OF cur_cm%ROWTYPE;
c_cm eo_cm_tbl;

l_eo_status eo_status_tbl := eo_status;
l_eo_cm_data eo_cm_data_tbl := eo_cm_data;

l_dashboard_rec_id		NUMBER;
l_fiscal_year			VARCHAR2(20);
l_fiscal_qtr         	VARCHAR2(20);
l_fiscal_period      	VARCHAR2(20);
l_organization_code  	VARCHAR2(20);
l_item               	VARCHAR2(80);
l_lite_org_code			VARCHAR2(20);
l_cm_item               VARCHAR2(40);
l_item_cost             NUMBER(15,5);
l_net_oh                NUMBER(15,5);
l_open_po               NUMBER(15,5);
l_gross_demand_12mo     NUMBER(15,5);
l_valid_period_cnt		NUMBER;
l_err_flag				VARCHAR2(10);
l_period_num	 		NUMBER;
l_err_msg				VARCHAR2(2000);
l_cm_or_lite			VARCHAR2(20);
l_errm                  VARCHAR2(2000);
l_period_name			VARCHAR2(100);
l_valid_period			VARCHAR2(10);
l_eo_review_flag		VARCHAR2(10);
l_frozen_flag			VARCHAR2(10);
l_cm_lite_org			NUMBER;

l_item_descr			VARCHAR2(1000);
l_item_type             VARCHAR2(40);
l_prod_heirarchy        VARCHAR2(100);
l_l3_bus_group          VARCHAR2(40);
l_l4_bus_unit           VARCHAR2(40);
l_l6_module             VARCHAR2(40);
l_gl_line_code          VARCHAR2(40);
l_gl_line_descr         VARCHAR2(40);


l_item_id				NUMBER;
l_org_id				NUMBER;
l_user_name				VARCHAR2(250);


BEGIN
/***********************************************************************************************
Column Name		|	Aggregation Rule
----------------|-----------------------------------------------------------------------------------------------------------------------------
CM Item			|	Load the value of first records if there are multiple lines with same CM and LITE item 
Price			|	Total of price * on-hand / total of on-hand, sum the data by CM and LITE item, if total on-hand is 0, then average of price
On-hand			|	Sum of on-hand by CM and LITE item
On Order		|	Sum of on order by CM and LITE item
Project 1 Year	|	Sum of Project 1 Year by CM and LITE item
 
************************************************************************************************/
	
	OPEN cur_cm(p_cm_batch_id);
	LOOP
	
		FETCH cur_cm BULK COLLECT INTO c_cm;
 
		FOR i IN 1..c_cm.COUNT LOOP
			l_dashboard_rec_id			:= NULL;
			l_fiscal_year			    := NULL;
			l_fiscal_qtr                := NULL;
			l_fiscal_period             := NULL;
			l_organization_code         := NULL;
			l_item                      := NULL;
			l_lite_org_code             := NULL;
			l_cm_item           		:= NULL;
			l_item_cost                 := NULL;
			l_net_oh                    := NULL;
			l_open_po                   := NULL;
		    l_gross_demand_12mo         := NULL;
            l_valid_period_cnt	        := NULL;
            l_err_flag			        := NULL;
            l_period_num	 	        := NULL;
            l_err_msg			        := NULL;
            l_cm_or_lite		        := NULL;
            l_errm                      := NULL;
			l_item_id					:= NULL;
            l_org_id					:= NULL;
			l_period_name				:= NULL;
			l_valid_period				:= NULL;
			l_eo_review_flag	        := NULL;
			l_frozen_flag               := NULL;
			l_cm_lite_org				:= NULL;
			l_item_descr				:= NULL;
			l_item_type                 := NULL;
			l_prod_heirarchy            := NULL;
			l_l3_bus_group              := NULL;
			l_l4_bus_unit               := NULL;
			l_l6_module                 := NULL;
			l_gl_line_code              := NULL;
			l_gl_line_descr             := NULL;
			
			
        
			---to get the period number from Week
			IF c_cm(i).fiscal_period = 'WW05' THEN ---1st period of the quarter
				BEGIN
					SELECT gp.period_num,
						   gp.period_name
					INTO l_period_num,
						 l_period_name
					FROM gl_periods_v gp     
					WHERE gp.period_year = c_cm(i).fiscal_year
					AND gp.quarter_num = c_cm(i).fiscal_qtr
					AND gp.period_set_name = 'XXSC ACC CAL'
					AND gp.period_num =(
						SELECT MIN(gp.period_num)
						FROM gl_periods_v gp     
						WHERE gp.period_year = c_cm(i).fiscal_year
						AND gp.quarter_num = c_cm(i).fiscal_qtr
						AND gp.period_set_name = 'XXSC ACC CAL');
				EXCEPTION WHEN OTHERS THEN
					l_period_num    := 0;
					l_period_name	:= NULL;
				END;
			ELSIF c_cm(i).fiscal_period = 'WW13' THEN ---last period of the quarter
				BEGIN
					SELECT gp.period_num,
						   gp.period_name
					INTO l_period_num,
						 l_period_name
					FROM gl_periods_v gp     
					WHERE gp.period_year = c_cm(i).fiscal_year
					AND gp.quarter_num = c_cm(i).fiscal_qtr
					AND gp.period_set_name = 'XXSC ACC CAL'
					AND gp.period_num =(
						SELECT MAX(gp.period_num)
						FROM gl_periods_v gp     
						WHERE gp.period_year = c_cm(i).fiscal_year
						AND gp.quarter_num = c_cm(i).fiscal_qtr
						AND gp.period_set_name = 'XXSC ACC CAL');
				EXCEPTION WHEN OTHERS THEN
					l_period_num    := 0;
					l_period_name	:= NULL;
				END;
			ELSE 
				l_period_num    := 0;
			END IF;
			
			
			IF NVL(l_period_num,0) = 0 THEN
				l_err_flag := 'E';
				l_err_msg:= 'Invalid Week#';
			ELSIF c_cm(i).fiscal_year IS NOT NULL AND c_cm(i).fiscal_qtr IS NOT NULL THEN
				---to check if the fiscal year quarter and period is valid in EBS and not null
				BEGIN
					SELECT 'Y'
					INTO l_valid_period
					FROM gl_periods_v gp     
					WHERE gp.period_year = c_cm(i).fiscal_year
					AND gp.quarter_num = c_cm(i).fiscal_qtr
					AND gp.period_num = l_period_num
					AND gp.period_set_name = 'XXSC ACC CAL';
				EXCEPTION WHEN OTHERS THEN	
					l_valid_period := 'N';
				END;
				
				IF NVL(l_valid_period,'N') = 'N' THEN
					l_err_flag := 'E';
					l_err_msg:= 'Fiscal year/quarter/week# is invalid';
					
				END IF; --IF NVL(l_valid_period_cnt,0) = 0
			ELSE
				l_err_flag := 'E';
				l_err_msg := 'Fiscal year/quarter/week# cannot be blank';
			END IF; --NVL(l_period_num,0) = 0 
			
			BEGIN
				SELECT cm_item,
					lite_org_code
				INTO l_cm_item,
					 l_lite_org_code
				FROM xxsc.xxsc_eo_cm_tbl xcm
				WHERE xcm.fiscal_year = c_cm(i).fiscal_year
				AND xcm.fiscal_qtr = c_cm(i).fiscal_qtr 
				AND xcm.fiscal_period = c_cm(i).fiscal_period
				AND xcm.organization_code = c_cm(i).organization_code  
				AND xcm.item = c_cm(i).item
				AND xcm.cm_batch_id = p_cm_batch_id
				AND ROWNUM=1;
			EXCEPTION WHEN OTHERS THEN
				l_cm_item := NULL;
			END;
			
			
			---to check if the oraganization is LITE or CM
			IF ASCII(SUBSTR(NVL(c_cm(i).organization_code,'X'),1,1)) BETWEEN 48 AND 57 THEN
			  l_cm_or_lite := 'LITE';
			ELSE
			  l_cm_or_lite := 'CM';
			END IF;
			
			----to check if inventory item and organization code is valid and not blank
			IF c_cm(i).item IS NOT NULL AND  c_cm(i).organization_code IS NOT NULL AND l_cm_or_lite = 'LITE'   THEN
				
				get_inv_item_dtls(c_cm(i).item,c_cm(i).organization_code,l_item_id,l_org_id);
				
				IF l_item_id = 0 OR l_org_id = 0 THEN
					l_err_flag := 'E';
					l_err_msg:= 'Inventory Item/Organization code is invalid';
					
				END IF;
			/*ELSIF c_cm(i).item IS NOT NULL  AND l_cm_or_lite = 'CM' THEN
				get_inv_item_dtls(c_cm(i).item,NVL(c_cm(i).lite_org_code,89),l_item_id,l_org_id);
				
				IF l_item_id = 0 OR l_org_id = 0 THEN
					l_err_flag := 'E';
					l_eo_status(i).record_id := c_cm(i).cm_rec_id;
					l_eo_status(i).status := 'ERROR';
					l_eo_status(i).error_msg := 'Inventory Item/Organization code is invalid';
				END IF;*/
			ELSIF c_cm(i).item IS NULL OR c_cm(i).organization_code IS NULL THEN
				l_err_flag := 'E';
				l_err_msg := 'Inventory Item/Organization code  cannot be blank';
			END IF; --IF c_cm(i).item IS NOT NULL and c_cm(i).organization_code IS NOT NULL
			
			---to check if the CM Name and LITE Org is matching
			BEGIN
				SELECT COUNT(*)
				INTO l_cm_lite_org
				FROM apps.fnd_lookup_values flv
				WHERE flv.lookup_type ='XXSC_EO_CM_LITE_ORG'
				AND flv.lookup_code = l_lite_org_code
                AND flv.meaning = c_cm(i).organization_code
				AND NVL(flv.enabled_flag,'N') ='Y'
				AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE));
			EXCEPTION WHEN OTHERS THEN 
				l_cm_lite_org:= 0;
			END;
			
			IF NVL(l_cm_lite_org,0) = 0 THEN
				l_err_flag := 'E';
				l_err_msg := 'CM name and LITE Org don’t match';
			END IF;
		
		----to get the existing data from dashboard table
			BEGIN
				SELECT xed.dashboard_rec_id,	
					xed.fiscal_year			    ,
					xed.fiscal_qtr              ,  
					xed.fiscal_period           ,  
					xed.organization_code       ,  
					xed.item                    ,  
					--xed.lite_org_code           ,  
					xed.cm_item                 ,  
					xed.item_cost         		,
					xed.net_oh                  ,
                    xed.open_po                 ,
                    xed.gross_demand_12mo       ,
					NVL(xed.eo_review_flag,'N') ,
					NVL(xed.frozen_flag,'N')
				INTO l_dashboard_rec_id			,
					l_fiscal_year			    ,
					l_fiscal_qtr                ,
					l_fiscal_period             ,
					l_organization_code         ,
					l_item                      ,
					--l_lite_org_code             ,
					l_cm_item                   ,
					l_item_cost         		,
                    l_net_oh                    ,
                    l_open_po                   ,
                    l_gross_demand_12mo         ,
					l_eo_review_flag			,
					l_frozen_flag
				FROM xxsc.xxsc_eo_dashboard_tbl xed
				WHERE xed.fiscal_year = c_cm(i).fiscal_year
				AND xed.fiscal_qtr = c_cm(i).fiscal_qtr
				AND xed.fiscal_period = l_period_num
				AND xed.item = c_cm(i).item
				AND xed.organization_code = c_cm(i).organization_code;
				
			EXCEPTION WHEN OTHERS THEN
				l_dashboard_rec_id			:= NULL;
				l_fiscal_year				:= NULL;
				l_fiscal_qtr                := NULL;
				l_fiscal_period             := NULL;
				l_organization_code         := NULL;
				l_item                      := NULL;
				--l_lite_org_code             := NULL;
				l_cm_item                   := NULL;
				l_item_cost                 := NULL;
				l_net_oh                    := NULL;
				l_open_po                   := NULL;
				l_gross_demand_12mo         := NULL;
				l_eo_review_flag	        := NULL;
				l_frozen_flag               := NULL;
			END;
			
			
			
			
			IF NVL(l_dashboard_rec_id,0) > 0 AND NVL(l_err_flag,'N') <> 'E' THEN ----if record exists in table
				
				IF NVL(l_eo_review_flag,'N') IN ('A','C') OR NVL(l_frozen_flag,'N') = 'Y' AND NVL(l_err_flag,'E') <> 'E' THEN
					l_err_flag := 'E';
					l_err_msg := 'The record is Frozen or Activated';
				END IF;
				
				
				----set values as data sheet if column value is not null else set the exisiting column value in table
				IF NVL(l_err_flag,'N') <> 'E' THEN
					
					
					IF c_cm(i).item_cost IS NOT NULL THEN
						l_item_cost := c_cm(i).item_cost;
					ELSE
						l_item_cost := l_item_cost;
					END IF;
					
					IF c_cm(i).net_oh IS NOT NULL THEN
						l_net_oh := c_cm(i).net_oh;
					ELSE
						l_net_oh := l_net_oh;
					END IF;
					
					IF c_cm(i).open_po IS NOT NULL THEN
						l_open_po := c_cm(i).open_po;
					ELSE
						l_open_po := l_open_po;
					END IF;
					
					IF c_cm(i).gross_demand_12mo IS NOT NULL THEN
						l_gross_demand_12mo := c_cm(i).gross_demand_12mo;
					ELSE
						l_gross_demand_12mo := l_gross_demand_12mo;
					END IF;
					
					BEGIN
						UPDATE xxsc.xxsc_eo_dashboard_tbl xed
						SET xed.cm_item             = l_cm_item,
							xed.item_cost           = l_item_cost,
							xed.net_oh             	= l_net_oh,
							xed.open_po				= l_open_po,
							xed.gross_demand_12mo	= l_gross_demand_12mo,
							xed.full_demand			= l_gross_demand_12mo,
							xed.cmoh_ref_id			= 1,
							xed.last_updated_by 	= p_user_id,
							xed.last_update_date 	= SYSDATE						
						WHERE xed.fiscal_year = c_cm(i).fiscal_year
						AND xed.fiscal_qtr = c_cm(i).fiscal_qtr 
						AND xed.fiscal_period = l_period_num
						AND xed.organization_code = c_cm(i).organization_code  
						AND xed.item = c_cm(i).item;
						
						UPDATE xxsc.xxsc_eo_cm_tbl xcm
						SET status = 'UPDATED'
						WHERE xcm.fiscal_year = c_cm(i).fiscal_year
						AND xcm.fiscal_qtr = c_cm(i).fiscal_qtr 
						AND xcm.fiscal_period = c_cm(i).fiscal_period
						AND xcm.organization_code = c_cm(i).organization_code  
						AND xcm.item = c_cm(i).item
						AND xcm.cm_batch_id = p_cm_batch_id;
				
					EXCEPTION WHEN OTHERS THEN
						l_err_flag := 'E';
						l_errm := 'Error in updating data '||SQLERRM;
					END;
				END IF;--IF NVL(l_err_flag,'N') <> 'E' THEN
				
			ELSIF NVL(l_dashboard_rec_id,0) = 0 AND NVL(l_err_flag,'N') <> 'E' THEN ----if record not exists in table insert data
				BEGIN
					--to get inventory item column information
					SELECT 
						msi.description item_descr,
						flv_typ.meaning item_type,
						msi.inventory_item_id,
						(
								CASE
									WHEN nvl(hier.l1_total_prod_line, 'X') = 'X' THEN
										NULL
									ELSE
										hier.l1_total_prod_line
										|| '.'
										|| hier.l2_market_segment
										|| '.'
										|| hier.l3_business_group
										|| '.'
										|| l4_business_unit
										|| '.'
										|| hier.l5_product_line
										|| '.'
										|| hier.l6_product_family
								END
							)                           prod_heirarchy,
							hier.l3_business_group_desc  l3_bus_group,
							hier.l4_business_unit_desc   l4_bus_unit,
							hier.l6_product_family_desc  l6_module,
							TO_CHAR(hier.gl_product_line)         gl_line_code,
							hier.gl_product_line_desc    gl_line_descr
					INTO l_item_descr,
						 l_item_type,
						 l_item_id,
						 l_prod_heirarchy,
						 l_l3_bus_group,
						 l_l4_bus_unit,
						 l_l6_module,
						 l_gl_line_code,
						 l_gl_line_descr
					FROM apps.mtl_system_items_b msi,
						 apps.xxsc_sales_hierarchy_details_v hier,
						 (SELECT
								flv.meaning,
								flv.lookup_code
							FROM
								applsys.fnd_lookup_values flv
							WHERE
									flv.lookup_type = 'ITEM_TYPE'
								AND flv.view_application_id = 3) flv_typ,
						   (SELECT
								flv.meaning,
								flv.lookup_code
							FROM
								applsys.fnd_lookup_values flv
							WHERE
									flv.lookup_type = 'MTL_PLANNING_MAKE_BUY'
								AND flv.view_application_id = 700) flv_proc
					WHERE 1=1--ROWNUM=1
					AND msi.item_type = flv_typ.lookup_code(+)
					AND msi.planning_make_buy_code =  flv_proc.lookup_code(+)
					AND  msi.sales_account = hier.code_combination_id(+)
					AND msi.segment1 = c_cm(i).item--'X01009010-000'
					AND msi.organization_id =(
						CASE
							WHEN NOT EXISTS (SELECT 'X' FROM apps.mtl_system_items_b m 
							WHERE m.organization_id = NVL(l_org_id,0)
							AND m.segment1 = c_cm(i).item
							) THEN
								89   --GLO
							ELSE
								TO_NUMBER(l_org_id)
						END
					);
				EXCEPTION WHEN OTHERS THEN
					l_item_descr				:= NULL;
					l_item_type                 := NULL;
					l_prod_heirarchy            := NULL;
					l_l3_bus_group              := NULL;
					l_l4_bus_unit               := NULL;
					l_l6_module                 := NULL;
					l_gl_line_code              := NULL;
					l_gl_line_descr             := NULL;
					l_item_id					:= NULL;
				END;
				 
				BEGIN
					INSERT INTO xxsc.xxsc_eo_dashboard_tbl(
							dashboard_rec_id		,
							fiscal_year			    ,
							fiscal_qtr              , 
							fiscal_period           , 
							period_name				,
							organization_code       , 
							organization_id			,
							item                    , 
							item_id					,
							lite_org_code           , 
							cm_item                 , 
							item_cost               ,
							net_oh             		,
							open_po             	,
							gross_demand_12mo       ,
							full_demand				,
							creation_date			,
							created_by				,
							last_updated_by 		,
							last_update_date 		,
							item_descr				,
							item_type               ,
							prod_heirarchy          ,
							l3_bus_group            ,
							l4_bus_unit             ,
							l6_module               ,
							gl_line_code            ,
							gl_line_descr           ,
							cmoh_ref_id			
							)
					VALUES (xxsc.xxsc_eo_dashb_seq.NEXTVAL,
							c_cm(i).fiscal_year			  ,
							c_cm(i).fiscal_qtr            ,
							l_period_num        ,
							l_period_name				  ,
							c_cm(i).organization_code     ,
							DECODE(l_cm_or_lite,'CM','CM',l_org_id),
							c_cm(i).item,
							l_item_id,
							l_lite_org_code,
							l_cm_item ,
							c_cm(i).item_cost,
							c_cm(i).net_oh ,
							c_cm(i).open_po,
							c_cm(i).gross_demand_12mo,
							c_cm(i).gross_demand_12mo,
							SYSDATE,
							p_user_id,
							p_user_id,
							SYSDATE ,
							l_item_descr	,
							l_item_type     ,
							l_prod_heirarchy,
							l_l3_bus_group  ,
							l_l4_bus_unit   ,
							l_l6_module     ,
							l_gl_line_code  ,
							l_gl_line_descr ,
							1
							);
							
					UPDATE xxsc.xxsc_eo_cm_tbl xcm
					SET status = 'INSERTED'
					WHERE xcm.fiscal_year = c_cm(i).fiscal_year
					AND xcm.fiscal_qtr = c_cm(i).fiscal_qtr 
					AND xcm.fiscal_period = c_cm(i).fiscal_period
					AND xcm.organization_code = c_cm(i).organization_code  
					AND xcm.item = c_cm(i).item
					AND xcm.cm_batch_id = p_cm_batch_id;
							
				EXCEPTION WHEN OTHERS THEN
					l_err_flag := 'E';
					l_errm := 'Error while inserting data '||SQLERRM;
					
				END;
			END IF;---IF NVL(l_dashboard_rec_id,0) > 0 AND NVL(l_err_flag,'N') <> 'E' THEN
			
			---to update error status to staging table
			IF NVL(l_err_flag,'N') ='E' THEN
				BEGIN
					UPDATE xxsc.xxsc_eo_cm_tbl xcm
					SET status = 'ERROR',
						error_msg = TO_CHAR(l_err_msg)
					WHERE xcm.fiscal_year = c_cm(i).fiscal_year
					AND xcm.fiscal_qtr = c_cm(i).fiscal_qtr 
					AND xcm.fiscal_period = c_cm(i).fiscal_period
					AND xcm.organization_code = c_cm(i).organization_code  
					AND xcm.item = c_cm(i).item
					AND xcm.cm_batch_id = p_cm_batch_id;
				EXCEPTION WHEN OTHERS THEN 
					dbms_output.put_line('error in updating'||SQLERRM);
				END;
			END IF;
			
		END LOOP;
		
		
		EXIT WHEN cur_cm%NOTFOUND;
				
	END LOOP;	
	CLOSE cur_cm;
	
END cm_data_aggrigate;	

END xxsc_eo_data_load_pkg;
/
