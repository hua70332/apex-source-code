create or replace PACKAGE         "XXSC_EO_USAGECALC_PKG" AS 
/***********************************************************************************************
* TYPE                 :  Package Body 
* NAME                 :  XXSC_EO_USAGECALC_PKG.pkb  
* PURPOSE              :  Package to update multiple EO usage fields data in dashboard table.
* Date             Developer                        Version          Description
* ----------       ---------------------------      --------         ----------------------------
* 12-Mar-21        Rajeswar Mandumula               1.0              Initial Version
* 27-Oct-21        Uday Reddy Gurajala              2.0              Increased the size of the varilbles to 240 
*                                                                    l_last_disp_code1,l_last_disp_code3
* 30-Nov-21        Barry Huang                      2.1              Fix calculation bug and data issue
* 15-Dec-21					                        3.0		         Enhance code to support change of Phase III
*************************************************************************************************/

   g_package             VARCHAR2 (100)   :=  'XXSC_EO_USAGECALC_PKG';
   g_request_id          NUMBER           :=  fnd_global.conc_request_id;
   g_user_name           VARCHAR2 (30)    :=  fnd_global.user_name;
   g_responsibility      VARCHAR2 (100)   :=  fnd_global.resp_name;
   g_user_id             NUMBER           :=  NVL (fnd_profile.VALUE ('USER_ID'),-1);
   g_resp_id             NUMBER           :=  fnd_profile.VALUE ('RESP_ID');
   g_resp_appl_id        NUMBER           :=  fnd_profile.VALUE ('RESP_APPL_ID');
   g_created_by_module   VARCHAR2 (100)   :=  'XXSC';
   g_debug_msgs          VARCHAR2(1)      :=  'N';
   g_status              VARCHAR2 (1);
   g_error_message       VARCHAR2 (32767);


      PROCEDURE CALC_EO_USAGE (x_errbuf         OUT NOCOPY     VARCHAR2,
                           x_retcode        OUT NOCOPY     NUMBER,
						   p_fiscal_year     IN            VARCHAR2,
						   p_fiscal_period   IN            VARCHAR2,
						   p_item            IN            VARCHAR2,
						   p_item_org        IN            VARCHAR2,
						   p_data_source     IN            VARCHAR2,						   
						   p_calc_usage      IN            VARCHAR2,
						   p_calc_reeval     IN            VARCHAR2,
						   p_calc_reserve    IN            VARCHAR2,
						   p_calc_provision  IN            VARCHAR2,						   
                           p_debug_msgs      IN            VARCHAR2);

END XXSC_EO_USAGECALC_PKG;