create or replace PACKAGE BODY        "XXSC_EO_USAGECALC_PKG" AS 
/***********************************************************************************************
* TYPE                 :  Package Body 
* NAME                 :  XXSC_EO_USAGECALC_PKG.pkb  
* PURPOSE              :  Package to update multiple EO usage fields data in dashboard table.
* Date             Developer                        Version          Description
* ----------       ---------------------------      --------         ----------------------------
* 12-Mar-21        Rajeswar Mandumula               1.0              Initial Version
* 27-Oct-21        Uday Reddy Gurajala              2.0              Increased the size of the varilbles to 240 
*                                                                    l_last_disp_code1,l_last_disp_code3
* 30-Nov-21        Barry Huang                      2.1              Fix calculation bug and data issue
* 15-Dec-21		   Barry Huang                      3.0		         Enhance code to support change of Phase III
*************************************************************************************************/

/************************************************************************************************
* Excess and Obsolete Calculation Rules                                                         *
* EO-1. Standard Order ship                                                                     *
*   - Transaction Type ID = 33                                                                  *
*   - COGS account = 40005, 14332                                                               *
* EO-2. Consignment Order ship                                                                  *
*   - Transaction Type ID = 33                                                                  *
*   - COGS account = 40308, 40124                                                               *
* EO-3. Accounting Transaction                                                                  *
*   - Transaction Type ID = 1,4,8,31,32,40,41,42                                                *
*   - GL account <> 40312                                                                       *
* EO-4. E&O Scrap                                                                               *
*   - Transaction Type ID = 1,4,8,31,32,40,41,42                                                *
*   - GL account = 40312                                                                        *
* EO-5. WIP Issue                                                                               *
*   - Transaction Type ID = 35,43,38,48                                                         *
* EO-6. WIP Completion                                                                          *
*   - Transaction Type ID = 17, 44, 90                                                          *
* EO-7. PO Receipt                                                                              *
*   - Transaction Type ID = 18, 36, 71                                                          * 
*                                                                                               *
* Exclude Rule:                                                                                 * 
* ER-1. Exclude Expense Items                                                                   *
*   - ITEM_TYPE = EXP, EQT, REF, SRV, CAP                                                       *
* ER-2. Exclude subinventory that is not EO inventory                                           *
*   - The mateiral account of subinventory is '6%', 14313, 14217                                *
*   - Subinventory is 'x%', 'ADVRPL-ESI', 'ESI SG RMA', 'LSR SRVFBN'                            *
*   - Inventory Asset Flag is not Y                                                             *
*                                                                                               *
* Note:                                                                                         * 
*   1. Must update all related statements if there is any change to one rule.                   *
*      The statement can identify by code (EO-1, EO-2, ER-1 etc.)                               * 
*   2. These rules are also used in the below customized view                                   *
*       - XXSC_EO_INV_DETAILS_V                                                                 *
*       - XXSC_PERIOD_INV_ONHAND_V                                                              *             
************************************************************************************************/
TYPE EO_RCD IS RECORD
(
    EO_REC_ID               XXSC.XXSC_EO_DASHBOARD_TBL.DASHBOARD_REC_ID%TYPE,
    INVENTORY_ITEM          VARCHAR2(40),
    INVENTORY_ITEM_ID       XXSC.XXSC_EO_DASHBOARD_TBL.ITEM_ID%TYPE,
    ORGANIZATION_ID         XXSC.XXSC_EO_DASHBOARD_TBL.ORGANIZATION_ID%TYPE,
    ORGANIZATION_CODE       XXSC.XXSC_EO_DASHBOARD_TBL.ORGANIZATION_CODE%TYPE,
    ITEM_COST               XXSC.XXSC_EO_DASHBOARD_TBL.ITEM_COST%TYPE,
    ITEM_COST_PRE_PERIOD    XXSC.XXSC_EO_DASHBOARD_TBL.ITEM_COST%TYPE,
    ONHAND                  NUMBER(15,5),
    CM_OH                   NUMBER(15,5),
    OPEN_PO                 NUMBER(15,5),
    SRV_NET_OH              NUMBER(15,5),
    LOAN_NET_OH             NUMBER(15,5),
    BEGIN_EO_RESERVE        NUMBER(15,5),
    NET_RESERVE             NUMBER(15,5),
    BUYBACK_RESERVE         NUMBER(15,5),
    TRANS_RESERVE           NUMBER(15,5),
    PO_RESERVE              NUMBER(15,5),
    MRB_RESERVE             NUMBER(15,5),
    RMA_RESERVE             NUMBER(15,5),
    SP_RESERVE              NUMBER(15,5),
    UNREL_EO_USAGE          NUMBER(15,5),
    TOTAL_EO_USAGE          NUMBER(15,5),
    STD_ORD_SHIP            NUMBER(15,5),
    CON_ORD_SHIP            NUMBER(15,5),
    WIP_ISSUE               NUMBER(15,5),
    WIP_COMPLETION          NUMBER(15,5),
    INT_ORD_SHIP            NUMBER(15,5),
    INT_ORD_INTRANSIT       NUMBER(15,5),
    INT_ORD_RCV             NUMBER(15,5),
    MISC_ISSUE              NUMBER(15,5),
    EO_SCRAP                NUMBER(15,5),
    PO_RCV                  NUMBER(15,5),
    PO_RCV_INTRANSIT        NUMBER(15,5),
    TOTAL_PROVISION         NUMBER(15,5),
    GAAP_CODE               XXSC.XXSC_EO_DASHBOARD_TBL.GAAP_CODE%TYPE,
    EO_USAGE_CHANGE_FLAG    VARCHAR2(1),
    FROZEN_FLAG             VARCHAR2(1),
    EO_REVIEW_FLAG          VARCHAR2(1),
    COMMENT_FLAG            VARCHAR2(1),
    PREV_ONHAND             NUMBER(15,5),
    PREV_CM_OH              NUMBER(15,5),
    PREV_UNREL_USAGE        NUMBER(15,5),
    PREV_ENDING_RESERVE     NUMBER(15,5),
    PREV_PO_RESERVE         NUMBER(15,5),
    PREV_KEEP_PO_RESERVE    NUMBER(15,5),
    PREV_PO_INTRANSIT       NUMBER(15,5),
    PREV_INT_INTRANSIT      NUMBER(15,5),
    INV_ONHAND              NUMBER(15,5),
    INV_SRV_OH              NUMBER(15,5),
    INV_LOAN_OH             NUMBER(15,5),
    INV_STD_ORD_SHIP        NUMBER(15,5),
    INV_CON_ORD_SHIP        NUMBER(15,5),
    INV_WIP_ISSUE           NUMBER(15,5),
    INV_WIP_COMPLETION      NUMBER(15,5),
    INV_INT_ORD_SHIP        NUMBER(15,5),
    INV_INT_ORD_INTRANSIT   NUMBER(15,5),
    INV_INT_ORD_RCV         NUMBER(15,5),
    INV_MISC_ISSUE          NUMBER(15,5),
    INV_EO_SCRAP            NUMBER(15,5),
    INV_PO_RCV              NUMBER(15,5),
    INV_PO_RCV_INTRANSIT    NUMBER(15,5),
    INV_OTHERS              NUMBER(15,5)
);

TYPE TR_RCD IS RECORD
(
    INVENTORY_ITEM_ID       NUMBER(10),
    ORGANIZATION_ID         NUMBER(5),
    TRANSACTION_TYPE_ID     NUMBER(5),
    SOLD_TO_ID              NUMBER(10),
    SOLD_TO_NAME            VARCHAR2(100),
    RECEIVE_FROM_ID         NUMBER(10),
    RECEIVE_FROM_NAME       VARCHAR2(100),
    GL_ACCOUNT              VARCHAR2(40),
    SEQ                     INT,
    ORDER_SHIP              NUMBER(15,5),
    INT_SHIP                NUMBER(15,5),
    INT_RCV                 NUMBER(15,5),
    MISC_ISSUE              NUMBER(15,5)
);
    /**************************************************************************
    * PROCEDURE : DEBUG                                                       *
    * Purpose   : Procedure to debug the code or to write the log file        *
    **************************************************************************/
PROCEDURE DEBUG (pi_message IN VARCHAR2) IS
    BEGIN
	   IF UPPER(g_debug_msgs)='Y' THEN
            fnd_file.put_line (fnd_file.LOG, pi_message);
	   ELSE
	      NULL;
	   END IF;
END DEBUG;

/****************************************************************************
* PROCEDURE : CALC_EO_USAGE                                                 *
* Purpose   : Procedure to calculate EO usage                               *
* Parameters:                                                               *
*   p_fiscal_year:      Fiscal Year                                         *
*   p_fiscal_period:    Fiscal period number                                *
*   p_item:             Inventory Item                                      *
*   p_item_org:         Organization Code or CM Name                        *
*   p_data_source:      LITE or CM                                          *
*   p_calc_usage:       Indicate whether calculate E and O usage (Y/N)      *
*   p_calc_reeval:      Indicate whether revaluate E and O value (Y/N)      *
*   p_calc_reserve:     Indicate whether calculate net reserve and ending   * 
*                       reserve (Y/N)                                       *
*   p_calc_provison:    Indicate whether calculate provison and             *
*                       ending reser(Y/N)                                   *
*   p_debug_msgs:       Indicate whether print debug information (Y/N)      *                     
*****************************************************************************/	

  PROCEDURE CALC_EO_USAGE (x_errbuf         OUT NOCOPY     VARCHAR2,
                           x_retcode        OUT NOCOPY     NUMBER,
						   p_fiscal_year     IN            VARCHAR2,
						   p_fiscal_period   IN            VARCHAR2,
						   p_item            IN            VARCHAR2,
						   p_item_org        IN            VARCHAR2,
						   p_data_source     IN            VARCHAR2,						   
						   p_calc_usage      IN            VARCHAR2,
						   p_calc_reeval     IN            VARCHAR2,
						   p_calc_reserve    IN            VARCHAR2,
						   p_calc_provision  IN            VARCHAR2,						   
                           p_debug_msgs      IN            VARCHAR2) IS  

-- Variables of records
    TYPE EO_DATASET IS TABLE OF EO_RCD;
    TYPE TR_DATASET IS TABLE OF TR_RCD;

    cr_usage            EO_DATASET;
    cr_tr               TR_DATASET;
-- Constant data for search
    v_period_set APPS.GL_PERIODS_V.PERIOD_SET_NAME%TYPE := 'XXSC ACC CAL';
    v_exclude_trans_type_id VARCHAR2(10) :='10008';
    
-- Variables of key fields
	l_eo_rec_id        	NUMBER(15,5);
	l_rec_id            NUMBER(15,5);
	v_usage             VARCHAR2(1);
	v_reval             VARCHAR2(1);
	v_reserve           VARCHAR2(1);
	v_provision         VARCHAR2(1);

-- Variables of time period
	l_year	    		VARCHAR2(20);
	l_qtr		    	VARCHAR2(20);
	l_period	    	VARCHAR2(20);
	l_period_name		VARCHAR(20);

	l_prev_year	    	VARCHAR2(20);
	l_prev_qtr	    	VARCHAR2(20);
	l_prev_period		VARCHAR2(20);

	l_period_start		DATE;
	l_period_end		DATE;
	l_prev_period_start	DATE;
	l_prev_period_end	DATE;

-- Variables of ienventory item master
	
	l_item_cost 		NUMBER(15,5);
	l_prev_item_cost	NUMBER(15,5);
	l_eo_re_eval		NUMBER(15,5);
	l_org_descr         VARCHAR2(40);
	l_org_code	    	VARCHAR2(20);
	l_org_id	    	NUMBER(10);
	l_item              VARCHAR2(40);

-- Variables of EO data

	l_beg_reserve		NUMBER(15,5);
	l_trans_reserve		NUMBER(15,5);
	l_buyback_reserve	NUMBER(15,5);
	l_beg_unrel_reserve	NUMBER(15,5);
	l_po_reserve		NUMBER(15,5);
	l_mrb_reserve		NUMBER(15,5);
	l_rma_reserve		NUMBER(15,5);
	l_sp_reserve		NUMBER(15,5);
	l_eo_reserve        NUMBER(15,5);
	l_net_reserve       NUMBER(15,5);
	l_reserve_inc_prov	NUMBER(15,5);
	l_gaap_code		    VARCHAR2(20);

	l_onhand	    	NUMBER(15,5);
	l_net_inv	    	NUMBER(15,5);
	l_cm_oh		    	NUMBER(15,5);
	l_srv_oh	    	NUMBER(15,5);
	l_loan_oh	    	NUMBER(15,5);

	l_prev_onhand		    NUMBER(15,5);
	l_prev_cm_oh            NUMBER(15,5);
	l_prev_po_reserve       NUMBER(15,5);
	l_prev_keep_po_res	    NUMBER(15,5);
	l_prev_po_intransit	    NUMBER(15,5);
	l_prev_int_intransit    NUMBER(15,5);

	l_total_provision	NUMBER(15,5);
	l_provision_oh		NUMBER(15,5);
	l_provision_po		NUMBER(15,5);
	l_keep_po_res		NUMBER(15,5);

-- Variables of inventory transaction data
	l_po_rcv    		    NUMBER(15,5);
	l_po_rcv_intransit  	NUMBER(15,5);
	l_int_order_intransit	NUMBER(15,5);
	l_int_order_rcv         NUMBER(15,5);
	l_so_issue		        NUMBER(15,5);
	l_cons_ord_issue	    NUMBER(15,5);
	l_iso_issue 		    NUMBER(15,5);
	l_wip_issue		        NUMBER(15,5);
	l_misc_issue		    NUMBER(15,5);
	l_wip_completion	    NUMBER(15,5);
	l_eo_scrap	    	    NUMBER(15,5);
	l_others	    	    NUMBER(15,5);
	l_open_po	    	    NUMBER(15,5);

	l_sold_to_id        NUMBER(10);
	l_sold_to_org		VARCHAR(20);
	l_int_rcpt_dest		NUMBER(15,5);

-- Variables of EO usage
	l_total_eo_usage        NUMBER(15,5);
	l_unrel_eo_usage        NUMBER(15,5);
	l_cur_unrel_eo_usage    NUMBER(15,5);
	l_eo_usage_so           NUMBER(15,5);
	l_eo_usage_cons         NUMBER(15,5);        
	l_eo_usage_misc         NUMBER(15,5);
	l_eo_usage_int_ord      NUMBER(15,5);
	l_eo_usage_wip          NUMBER(15,5);
	l_eo_usage_others       NUMBER(15,5);

-- Variables of calculation
	l_active_inv		NUMBER(15,5);
	l_eo_variance		NUMBER(15,5);
	l_new_eo_qty		NUMBER(15,5);
	l_var1		    	NUMBER(15,5);
	l_var2		    	NUMBER(15,5);
	l_total_reserve		NUMBER(15,5);
	l_intship           NUMBER(15,5);
	l_qty	        	NUMBER(15,5);

	l_calc_flow_code	VARCHAR2(200):= NULL;
	l_rec_exists		NUMBER(10);

	l_transfer_flag		VARCHAR2(1);
	l_eo_usage_chg		VARCHAR2(1);
	l_transfer_flag_cnt	NUMBER(10);
	l_eo_cal_flag		VARCHAR2(1);
	l_length            INT;
	l_intship_comment  	VARCHAR2(250);
	l_intrcpt_comment  	VARCHAR2(250);
	l_ordship_comment  	VARCHAR2(250);
	l_misciss_comment  	VARCHAR2(350);
	l_reserve_comment  	VARCHAR2(500);

 /******************************************************************************
* CURSOR: cur_trans                                                            *
* Defintion: Sum of IntOrd Ship/Rcpt and Sales Order ship                      *
*                                                                              *
******************************************************************************/	
    CURSOR cur_trans (v_year VARCHAR2, v_period VARCHAR2, v_item_id NUMBER, v_org_id VARCHAR2) IS 
    SELECT mmt.INVENTORY_ITEM_ID
         ,mmt.ORGANIZATION_ID
         ,mmt.TRANSACTION_TYPE_ID
         ,mmt.SOLD_TO_ID
         ,mmt.SOLD_TO_NAME
         ,mmt.RECEIVE_FROM_ID
         ,mmt.RECEIVE_FROM_NAME
         ,mmt.GL_ACCOUNT
         ,(CASE 
            WHEN mmt.TRANSACTION_TYPE_ID = 62
            THEN    
                1
            WHEN mmt.TRANSACTION_TYPE_ID = 61
            THEN
                2
            WHEN mmt.TRANSACTION_TYPE_ID = 33
            THEN
                3
            ELSE
                4
          END) SEQ
         ,SUM(CASE
                WHEN mmt.TRANSACTION_TYPE_ID = 33 
                    AND EXISTS(SELECT 'X' FROM GL.GL_CODE_COMBINATIONS 
                            WHERE SEGMENT3 IN ('40005','14332','40124','40308')                     -- EO-1 SO ship/EO-2 Consignment order ship
                            AND CODE_COMBINATION_ID = mmt.DISTRIBUTION_ACCOUNT_ID)
                THEN
                    mmt.EO_QUANTITY
                ELSE
                    0
               END)  AS ORDER_SHIP
        , SUM(CASE
                WHEN mmt.TRANSACTION_TYPE_ID = 62
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END) AS INT_SHIP
        , SUM(CASE
                WHEN mmt.TRANSACTION_TYPE_ID = 61
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END) AS INT_RCV
        , SUM(CASE
                WHEN mmt.TRANSACTION_TYPE_ID = 99 AND NVL(mmt.GL_ACCOUNT,'X') <> 'X'
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END) AS MISC_ISSUE
     FROM 
     (SELECT mmt.ORGANIZATION_ID
            ,mmt.INVENTORY_ITEM_ID
            ,(CASE 
                WHEN NOT mmt.TRANSACTION_TYPE_ID IN( 33, 61, 62)
                THEN 
                    99
                ELSE
                    mmt.TRANSACTION_TYPE_ID
              END) TRANSACTION_TYPE_ID
            ,mmt.SUBINVENTORY_CODE
            ,mmt.TRANSACTION_QUANTITY
            ,mmt.PRIMARY_QUANTITY
            ,mmt.TRANSFER_ORGANIZATION_ID
            ,mmt.DISTRIBUTION_ACCOUNT_ID
            ,(CASE
              WHEN mmt.TRANSACTION_TYPE_ID = 62
              THEN
                 (mmt.TRANSFER_ORGANIZATION_ID)
              WHEN mmt.TRANSACTION_TYPE_ID = 33
              THEN
                 (SELECT SOLD_TO_ORG_ID
                    FROM APPS.OE_ORDER_LINES_ALL
                   WHERE LINE_ID = mmt.SOURCE_LINE_ID)
              ELSE
                 NULL
              END) AS SOLD_TO_ID
            ,(CASE
              WHEN mmt.TRANSACTION_TYPE_ID = 62
              THEN
                 (SELECT ORGANIZATION_CODE
                   FROM INV.MTL_PARAMETERS
                   WHERE ORGANIZATION_ID = mmt.TRANSFER_ORGANIZATION_ID)
              WHEN mmt.TRANSACTION_TYPE_ID = 33
              THEN
                 (SELECT DISTINCT hp.PARTY_NAME
                    FROM APPS.OE_ORDER_LINES_ALL sol
                        ,ar.hz_cust_accounts hca
                        ,apps.hz_parties hp
                   WHERE sol.LINE_ID = mmt.SOURCE_LINE_ID
                     AND hp.party_id = hca.party_id
                     AND sol.SOLD_TO_ORG_ID = hca.CUST_ACCOUNT_ID)
              ELSE
                 NULL
              END) AS SOLD_TO_NAME
            ,(CASE
              WHEN mmt.TRANSACTION_TYPE_ID = 61
              THEN
                 TO_CHAR(mmt.TRANSFER_ORGANIZATION_ID)
              ELSE
                 NULL
              END) AS RECEIVE_FROM_ID
            ,(CASE
              WHEN mmt.TRANSACTION_TYPE_ID = 61
              THEN
                 (SELECT ORGANIZATION_CODE
                    FROM INV.MTL_PARAMETERS
                   WHERE ORGANIZATION_ID = mmt.TRANSFER_ORGANIZATION_ID)
              ELSE
                 NULL
              END) AS RECEIVE_FROM_NAME
            ,(CASE 
              WHEN mmt.TRANSACTION_TYPE_ID IN (1,4,8,31,32,40,41,42)                            -- EO-3 Misc issue
                AND NOT EXISTS(SELECT 'X' FROM INV.MTL_TRANSACTION_ACCOUNTS mta, 
                                                    GL.GL_CODE_COMBINATIONS ref_acc
                                      WHERE mta.TRANSACTION_ID = mmt.TRANSACTION_ID 
                                      AND mta.REFERENCE_ACCOUNT = ref_acc.CODE_COMBINATION_ID
                                      AND mta.ACCOUNTING_LINE_TYPE = 2 
                                      AND ref_acc.SEGMENT3 = '40312' )
              THEN
                (SELECT DISTINCT SEGMENT1||'.'||SEGMENT2||'.'|| SEGMENT3||'.'||SEGMENT4||'.'||SEGMENT5||'.'||SEGMENT6||'.'||SEGMENT7
                FROM INV.MTL_TRANSACTION_ACCOUNTS mta, GL.GL_CODE_COMBINATIONS ref_acc
                  WHERE mta.TRANSACTION_ID = mmt.TRANSACTION_ID 
                  AND mta.REFERENCE_ACCOUNT = ref_acc.CODE_COMBINATION_ID
                  AND mta.ACCOUNTING_LINE_TYPE = 2)
              ELSE
                NULL
              END) AS GL_ACCOUNT
            ,(CASE                                                                              -- ER-2 Exclude non-EO subinventory
                WHEN msic.ASSET_INVENTORY = 1
                 AND NOT EXISTS
                        (SELECT 'X'
                          FROM GL.GL_CODE_COMBINATIONS gcci
                          WHERE ( gcci.SEGMENT3 IN ('14313', '14217')
                                     OR gcci.SEGMENT3 LIKE '6%')
                             AND msic.MATERIAL_ACCOUNT =
                                       gcci.CODE_COMBINATION_ID(+))
                 AND NOT EXISTS(SELECT 'X' FROM DUAL
                                WHERE mmt.SUBINVENTORY_CODE IN
                                      ('ADVRPL-ESI',
                                       'ESI SG RMA',
                                       'LSR SRVFBN')
                                    OR mmt.SUBINVENTORY_CODE LIKE 'x%')
                THEN
                   mmt.PRIMARY_QUANTITY
                ELSE
                   0
             END) AS EO_QUANTITY
    FROM APPS.MTL_MATERIAL_TRANSACTIONS mmt
        ,INV.MTL_SECONDARY_INVENTORIES msic
        ,APPS.ORG_ACCT_PERIODS oap
    WHERE 1 = 1
    AND msic.ORGANIZATION_ID = mmt.ORGANIZATION_ID
    AND msic.SECONDARY_INVENTORY_NAME = mmt.SUBINVENTORY_CODE
    AND mmt.ACCT_PERIOD_ID = oap.ACCT_PERIOD_ID
    AND mmt.ORGANIZATION_ID = oap.ORGANIZATION_ID
    AND mmt.TRANSACTION_TYPE_ID IN (61,62,33,1,4,8,31,32,40,41,42)                              -- EO-1/EO-2/EO-3
    AND mmt.PRIMARY_QUANTITY <> 0
    AND (mmt.LOGICAL_TRANSACTION IS NULL 
          OR (NOT mmt.LOGICAL_TRANSACTION IS NULL 
               AND mmt.TRANSACTION_ID = mmt.PARENT_TRANSACTION_ID))
    AND NOT EXISTS(SELECT 'X' FROM dual WHERE mmt.TRANSACTION_TYPE_ID = v_exclude_trans_type_id)
    AND oap.PERIOD_YEAR = v_year
    AND oap.PERIOD_NUM = v_period
    AND mmt.INVENTORY_ITEM_ID = v_item_id
    AND mmt.ORGANIZATION_ID = TO_NUMBER(v_org_id)
    ) mmt                 
    GROUP BY mmt.INVENTORY_ITEM_ID
            ,mmt.ORGANIZATION_ID
            ,mmt.TRANSACTION_TYPE_ID
            ,mmt.SOLD_TO_ID
            ,mmt.SOLD_TO_NAME
            ,mmt.RECEIVE_FROM_ID
            ,mmt.RECEIVE_FROM_NAME
            ,mmt.GL_ACCOUNT
    ORDER BY 2, 1, 9 ASC,13;
 /******************************************************************************
* CURSOR: prev_eo                                                             *
* Defintion: Query EO records which ending reserve isn't zero in previous    *
* period and not exists in the calculation period                             *
******************************************************************************/	
    CURSOR prev_eo(v_year VARCHAR2, v_period VARCHAR2,v_prev_year VARCHAR2,v_prev_period VARCHAR2, v_item VARCHAR2, v_item_org VARCHAR2) IS 
    SELECT A.*
	FROM XXSC.XXSC_EO_DASHBOARD_TBL A
	WHERE 1=1
        AND A.FISCAL_YEAR     = v_prev_year
        AND A.FISCAL_PERIOD   = v_prev_period
        AND ((p_data_source='LITE' AND NVL(A.ORGANIZATION_ID,'X')<>'CM') 
                OR (p_data_source='CM' AND NVL(A.ORGANIZATION_ID,'X')='CM') 
                OR (NVL(p_data_source,'X') = 'X'))
        AND ((NVL(v_item,'X') = A.ITEM) OR (v_item IS NULL))
        AND ((NVL(v_item_org,'X') = A.ORGANIZATION_CODE) OR (v_item_org IS NULL))
        AND (NVL(A.CUR_EO_RESERVE_QTY,0) + NVL(CUR_PO_RESERVE_QTY,0) > 0)
        AND NOT EXISTS(SELECT 'X' FROM XXSC.XXSC_EO_DASHBOARD_TBL B
                       WHERE B.ORGANIZATION_CODE = A.ORGANIZATION_CODE
                        AND B.ITEM = A.ITEM
                        AND B.FISCAL_YEAR = v_year
                        AND B.FISCAL_PERIOD = v_period);  

/******************************************************************************
* CURSOR: cur_eo                                                              *
* Defintion: Query all necessary data required for subledger calculation      *
*            Include:                                                         *
*               EO reserve data of calculation period                        *
*               EO reserve data of prebious period                           *
*               Inventory data                                                *
*               Transaction data                                              *
******************************************************************************/	
    CURSOR cur_eo(v_year VARCHAR2, v_period VARCHAR2,v_prev_year VARCHAR2,v_prev_period VARCHAR2, v_item VARCHAR2, v_item_org VARCHAR2, v_calc VARCHAR2) IS
	SELECT DISTINCT A.DASHBOARD_REC_ID AS EO_REC_ID,
        A.ITEM                  AS INVENTORY_ITEM,
        A.ITEM_ID               AS INVENTORY_ITEM_ID,
        A.ORGANIZATION_ID,
        A.ORGANIZATION_CODE,
        A.ITEM_COST,
        A.ITEM_COST_PRE_PERIOD,
--        A.SOLD_TO_ID            AS TRANSFOR_ORG_ID,
        A.NET_OH                AS ONHAND,
        A.CM_OH,
        A.OPEN_PO,
        DECODE(NVL(A.SRV_NET_OH,0),0,DECODE(SIGN(pio.SRV_ONHAND),-1,0,pio.SRV_ONHAND),A.SRV_NET_OH) SRV_NET_OH,
        DECODE(NVL(A.LOAN_NET_OH,0),0,DECODE(SIGN(pio.LOAN_ONHAND),-1,0,pio.LOAN_ONHAND),A.LOAN_NET_OH) LOAN_NET_OH,
        A.BEGIN_EO_RESERVE_QTY  AS BEGIN_EO_RESERVE,
        A.CUR_EO_RESERVE_QTY    AS NET_RESERVE,
        A.BUYBACK_EO_QTY        AS BUYBACK_RESERVE,
        A.TRANS_EO_RESERVE_QTY  AS TRANS_RESERVE,
        A.CUR_PO_RESERVE_QTY    AS PO_RESERVE,
        A.CUR_MRB_RESERVE_QTY   AS MRB_RESERVE,
        A.CUR_RMA_RESERVE_QTY   AS RMA_RESERVE,
        A.CUR_SP_RESERVE_QTY    AS SP_RESERVE,
        A.UNREL_EO_USAGE,
        A.TOTAL_EO_USAGE,
        DECODE(NVL(A.STD_ORD_SHIP,0),0,pio.STD_ORDER_SHIP,A.STD_ORD_SHIP)   AS STD_ORDER_SHIP,
        DECODE(NVL(A.CON_ORD_SHIP,0),0,pio.CON_ORDER_SHIP,A.CON_ORD_SHIP)   AS CON_ORD_SHIP,
        DECODE(NVL(A.WIP_ISSUE,0),0,pio.WIP_ISSUE,A.WIP_ISSUE)  AS WIP_ISSUE,
        DECODE(NVL(A.WIP_COMPLETION,0),0, pio.WIP_COMPLETION,A.WIP_COMPLETION) AS WIP_COMPLETION,
        DECODE(NVL(A.ISO_SHIP,0),0, pio.INT_ORDER_SHIP,A.ISO_SHIP)  AS INT_ORD_SHIP,
        DECODE(NVL(A.INT_ORDER_INTRANSIT,0),0, pio.INT_ORDER_INTRANSIT,A.INT_ORDER_INTRANSIT) AS INT_ORD_INTRANSIT,
        DECODE(NVL(A.INT_ORDER_RCV,0),0, pio.INT_ORDER_RCV,A.INT_ORDER_RCV) AS INT_ORD_RCV,
        DECODE(NVL(A.MISC_ISSUE,0),0,pio.MISC_ISSUE,A.MISC_ISSUE) AS MISC_ISSUE,
        A.EANDO_SCRAP           AS EO_SCRAP,
        DECODE(NVL(A.PO_RCV,0),0,pio.PO_RECEIVED,A.PO_RCV) AS PO_RCV,
        DECODE(NVL(A.PO_RCV_INTRANSIT,0),0,pio.PO_RCV_INTRANSIT,A.PO_RCV_INTRANSIT) AS PO_RCV_INTRANSIT,
        A.DISPOSITION_QTY1      AS TOTAL_PEOVISION,
        A.GAAP_CODE,
        A.EO_USAGE_CHANGE_FLAG,
        A.FROZEN_FLAG,
        A.EO_REVIEW_FLAG        AS EO_REVIEW_FLAG,
        SUBSTR(NVL(A.RESERVE_COMMENTS,'X'),1,1) AS COMMENT_FLAG,
--        NVL(prev_eo.PREV_ONHAND, pio.PREV_EO_ONHAND) AS PREV_ONHAND,
        pio.PREV_EO_ONHAND AS PREV_ONHAND,
        prev_eo.PREV_CM_OH,
        prev_eo.PREV_UNREL_RESERVE,
        prev_eo.PREV_ENDING_RESERVE,
        prev_eo.PREV_PO_RESERVE,
        prev_eo.PREV_KEEP_PO_RESERVE,
        NVL(prev_eo.PREV_PO_INTRANSIT,pio.PREV_PO_INTRANSIT) AS PREV_PO_INTRANSIT,
        NVL(prev_eo.PREV_INT_INTRANSIT,pio.PREV_INT_INTRANSIT) AS PREV_INT_INTRANSIT,
--        pio.PREV_PO_INTRANSIT,
--        pio.PREV_INT_INTRANSIT,
        DECODE(SIGN(pio.EO_ONHAND),-1,0,pio.EO_ONHAND)          AS INV_ONHAND,
        DECODE(SIGN(pio.SRV_ONHAND),-1,0,pio.SRV_ONHAND)        AS INV_SRV_OH,
        DECODE(SIGN(pio.LOAN_ONHAND),-1,0,pio.LOAN_ONHAND)      AS INV_LOAN_OH,
        pio.STD_ORDER_SHIP      AS INV_STD_ORD_SHIP,
        pio.CON_ORDER_SHIP      AS INV_CON_ORD_SHIP,
        pio.WIP_ISSUE           AS INV_WIP_ISSUE,
        pio.WIP_COMPLETION      AS INV_WIP_COMPLETION,
        pio.INT_ORDER_SHIP      AS INV_INT_ORD_SHIP,
        pio.INT_ORDER_INTRANSIT AS INV_INT_ORD_INTRANSIT,
        pio.INT_ORDER_RCV       AS INV_INT_ORD_RCV,
        pio.MISC_ISSUE          AS INV_MISC_ISSUE,
        pio.EO_SCRAP            AS INV_EO_SCRAP,
        pio.PO_RECEIVED         AS INV_PO_RCV,
        pio.PO_RCV_INTRANSIT    AS INV_PO_RCV_INTRANSIT,
        pio.INV_OTHERS          AS INV_OTHERS
    FROM XXSC.XXSC_EO_DASHBOARD_TBL A
        LEFT OUTER JOIN (SELECT C.DASHBOARD_REC_ID,RTRIM(C.ITEM) ITEM, C.ORGANIZATION_ID, C.ORGANIZATION_CODE
                            ,C.NET_OH AS PREV_ONHAND, NVL(C.CM_OH,0) AS PREV_CM_OH
                            ,NVL(C.UNREL_EO_USAGE,0) AS PREV_UNREL_RESERVE
                            ,NVL(C.EO_RESERVE_INC_PROVISION,NVL(C.CUR_EO_RESERVE_QTY,0)) AS PREV_ENDING_RESERVE
                            ,NVL(C.CUR_PO_RESERVE_QTY,0) AS PREV_PO_RESERVE
                            ,NVL(C.KEEP_PO_RESERVE,0) AS PREV_KEEP_PO_RESERVE
                            ,NVL(C.INT_ORDER_INTRANSIT,0) AS PREV_INT_INTRANSIT
                            ,NVL(C.PO_RCV_INTRANSIT,0) AS PREV_PO_INTRANSIT
          FROM XXSC.XXSC_EO_DASHBOARD_TBL C 
          WHERE C.FISCAL_YEAR    = v_prev_year
            AND C.FISCAL_PERIOD  = v_prev_period) prev_eo 
         ON prev_eo.ORGANIZATION_CODE = A.ORGANIZATION_CODE AND prev_eo.ITEM = A.ITEM
LEFT OUTER JOIN (
    SELECT * FROM(
        SELECT 
            FISCAL_YEAR
            ,FISCAL_PERIOD
            ,INVENTORY_ITEM_ID
            ,INVENTORY_ITEM
            ,ORGANIZATION_ID
            ,ORGANIZATION_CODE
            ,PRIMARY_UOM
            ,SUM(TOTAL_ONHAND) TOTAL_ONHAND
            ,SUM(NON_NET_ONHAND) NON_NET_ONHAND
            ,SUM(EO_ONHAND) EO_ONHAND
            ,SUM(PREV_EO_ONHAND) PREV_EO_ONHAND
            ,SUM(MRB_NONNETTABLE) MRB_NONNETTABLE
            ,SUM(RMA_NONNETTABLE) RMA_NONNETTABLE
            ,SUM(SRV_ONHAND) SRV_ONHAND
            ,SUM(LOAN_ONHAND) LOAN_ONHAND
            ,SUM(INT_ORDER_INTRANSIT) INT_ORDER_INTRANSIT
            ,SUM(PREV_INT_INTRANSIT) PREV_INT_INTRANSIT
            ,SUM(PO_RCV_INTRANSIT) PO_RCV_INTRANSIT
            ,SUM(PREV_PO_INTRANSIT) PREV_PO_INTRANSIT
            ,SUM(PO_RECEIVED) AS PO_RECEIVED
            ,SUM(STD_ORDER_SHIP) AS STD_ORDER_SHIP
            ,SUM(CON_ORDER_SHIP) AS CON_ORDER_SHIP
            ,SUM(WIP_ISSUE) AS WIP_ISSUE
            ,SUM(WIP_COMPLETION) AS WIP_COMPLETION
            ,SUM(INT_SHIP) AS INT_ORDER_SHIP
            ,SUM(INT_RCV) AS INT_ORDER_RCV
            ,SUM(MISC_ISSUE) AS MISC_ISSUE
            ,SUM(EO_SCRAP) AS EO_SCRAP
            ,SUM(INV_OTHERS - EO_SCRAP - MISC_ISSUE - INT_RCV - INT_SHIP - WIP_ISSUE - WIP_COMPLETION - STD_ORDER_SHIP - CON_ORDER_SHIP - PO_RECEIVED) AS INV_OTHERS
    FROM (
        SELECT 
            gp.PERIOD_YEAR AS FISCAL_YEAR
            ,gp.PERIOD_NUM AS FISCAL_PERIOD
            ,pt.INVENTORY_ITEM_ID
            ,item.SEGMENT1 INVENTORY_ITEM
            ,pt.ORGANIZATION_ID
            ,param.ORGANIZATION_CODE
            ,item.PRIMARY_UOM_CODE PRIMARY_UOM
--            ,RTRIM(param.ORGANIZATION_CODE)||'.'||RTRIM(item.SEGMENT1) MAPPING_CODE
            ,SUM(CASE WHEN pt.PERIOD_START_DATE > gp.END_DATE
                      THEN
                           pt.PRIMARY_QUANTITY
                      ELSE
                           0
                  END) TOTAL_ONHAND
            ,SUM(CASE WHEN pt.PERIOD_START_DATE > gp.END_DATE
                      THEN
                            pt.NON_NET_ONHAND
                      ELSE
                           0
                 END) NON_NET_ONHAND
            ,SUM(CASE WHEN pt.PERIOD_START_DATE > gp.END_DATE
                      THEN
                           NVL(pt.EO_ONHAND,0)
                      ELSE
                           0
                 END) + 
                 SUM(CASE WHEN pt.PERIOD_START_DATE > gp.END_DATE
                      THEN
                           NVL(pt.INT_ORDER_INTRANSIT,0)
                      ELSE
                           0
                 END) + 
                 SUM(CASE WHEN pt.PERIOD_START_DATE > gp.END_DATE
                      THEN
                           NVL(pt.PO_RCV_INTRANSIT,0)
                      ELSE
                           0
                 END) AS EO_ONHAND
            ,SUM(NVL(pt.EO_ONHAND,0) + NVL(pt.INT_ORDER_INTRANSIT,0) + NVL(pt.PO_RCV_INTRANSIT,0)) AS PREV_EO_ONHAND
            ,SUM(CASE WHEN pt.PERIOD_START_DATE > gp.END_DATE
                      THEN
                           pt.MRB_NONNETTABLE
                      ELSE
                           0
                  END) MRB_NONNETTABLE
            ,SUM(CASE WHEN pt.PERIOD_START_DATE > gp.END_DATE
                      THEN
                           pt.RMA_NONNETTABLE
                      ELSE
                           0
                 END) RMA_NONNETTABLE
            ,SUM(CASE WHEN pt.PERIOD_START_DATE > gp.END_DATE
                      THEN
                           pt.SRV_ONHAND
                      ELSE
                           0
                  END) SRV_ONHAND
            ,SUM(CASE WHEN pt.PERIOD_START_DATE > gp.END_DATE
                      THEN
                           pt.LOAN_ONHAND
                      ELSE
                           0
                 END) LOAN_ONHAND
            ,SUM(CASE WHEN pt.PERIOD_START_DATE > gp.END_DATE 
                       THEN
                           pt.INT_ORDER_INTRANSIT
                       ELSE
                            0
                  END) INT_ORDER_INTRANSIT
            ,SUM(pt.INT_ORDER_INTRANSIT) PREV_INT_INTRANSIT
            ,SUM(CASE WHEN pt.PERIOD_START_DATE > gp.END_DATE 
                       THEN
                           pt.PO_RCV_INTRANSIT
                       ELSE
                            0
                  END) PO_RCV_INTRANSIT
            ,SUM(pt.PO_RCV_INTRANSIT) PREV_PO_INTRANSIT
            ,0 AS PO_RECEIVED
            ,0 AS STD_ORDER_SHIP
            ,0 AS CON_ORDER_SHIP
            ,0 AS WIP_ISSUE
            ,0 AS WIP_COMPLETION
            ,0 AS INT_SHIP
            ,0 AS INT_RCV
            ,0 AS MISC_ISSUE
            ,0 AS EO_SCRAP
            ,0 AS INV_OTHERS
       FROM (
/*********************************************************************************************
* Inventory Data - onhand                                                                    *
* Total up inventory transaction quantity of Period for calculating on-hand of period        *
* Calculation Rules:                                                                         *
* 1. Exclude transaction which subinventory is not EO inventory                              *
*       - The mateiral account of subinventory is '6%', 14313, 14217                         *
*       - Subinventory is 'x%', 'ADVRPL-ESI', 'ESI SG RMA', 'LSR SRVFBN'                     *
*       - Inventory Asset Flag is not Y                                                      *
* 2. Sum transaction quantity by subinventory type and fiscal month                          *
*********************************************************************************************/ 
             SELECT 
                 mmt.INVENTORY_ITEM_ID
                 ,mmt.ORGANIZATION_ID
--                 ,mmt.SUBINVENTORY_CODE
                 ,SUM(mmt.PRIMARY_QUANTITY) AS PRIMARY_QUANTITY
                 ,SUM(CASE                                                                      -- ER-2 Exclude non-EO subinventory
                    WHEN --MSIC.AVAILABILITY_TYPE = 1 
                        msic.ASSET_INVENTORY = 1
						AND NOT EXISTS(SELECT 'X' FROM GL.GL_CODE_COMBINATIONS gcci 
                                 WHERE (gcci.SEGMENT3 IN ('14313','14217') OR gcci.SEGMENT3 LIKE '6%') 
                                        AND msic.MATERIAL_ACCOUNT = gcci.CODE_COMBINATION_ID(+)) 
                        AND NOT EXISTS(SELECT 'X' FROM dual 
                                        WHERE mmt.SUBINVENTORY_CODE IN ('ADVRPL-ESI', 'ESI SG RMA', 'LSR SRVFBN') 
                                        OR mmt.SUBINVENTORY_CODE LIKE 'x%')
                    THEN
                        mmt.PRIMARY_QUANTITY
                    ELSE
                        0
                    END) EO_ONHAND
                 ,SUM(CASE                            -- Belong to Non Nettable onhand
                        WHEN     MSIC.AVAILABILITY_TYPE = 2
                        THEN
                           mmt.PRIMARY_QUANTITY
                        ELSE
                           0
                     END) NON_NET_ONHAND
                 ,SUM(CASE     -- Belong to MRB Nonnettable Onhand
                    WHEN EXISTS(SELECT 'X' FROM dual WHERE MSIC.AVAILABILITY_TYPE = 2 
                        AND (mmt.SUBINVENTORY_CODE LIKE '%MRB%' 
                            OR mmt.SUBINVENTORY_CODE LIKE '%REWORK%' 
                            OR mmt.SUBINVENTORY_CODE LIKE '%RTV%'))
                    THEN
                        mmt.PRIMARY_QUANTITY
                    ELSE
                        0
                    END) MRB_NONNETTABLE
                 ,SUM(CASE     -- Belong to RMA Nonnettable Onhand
                    WHEN MSIC.AVAILABILITY_TYPE = 2 AND mmt.SUBINVENTORY_CODE LIKE '%RMA%' 
                    THEN
                        mmt.PRIMARY_QUANTITY
                    ELSE
                        0
                    END) RMA_NONNETTABLE
                 ,SUM(CASE     -- Belong to Service Onhand
                    WHEN (mmt.SUBINVENTORY_CODE LIKE '%SRV%' OR mmt.SUBINVENTORY_CODE LIKE '%RSC%')
                    THEN
                        mmt.PRIMARY_QUANTITY
                    ELSE
                        0
                    END) SRV_ONHAND
                 ,SUM(CASE     -- Belong to Loan Onhand
                    WHEN mmt.SUBINVENTORY_CODE LIKE '%LOAN%' 
                    THEN
                        mmt.PRIMARY_QUANTITY
                    ELSE
                        0
                    END) LOAN_ONHAND
                 ,SUM(INT_ORDER_SHIP + INT_ORDER_RCV) INT_ORDER_INTRANSIT
                 ,SUM(mmt.PO_RCV) AS PO_RCV_INTRANSIT
				 ,mmt.PERIOD_START_DATE
             FROM 
				(
				SELECT mmt.INVENTORY_ITEM_ID
                    ,mmt.TRANSFER_ORGANIZATION_ID AS ORGANIZATION_ID
                    ,'IN_TRANSIT' AS SUBINVENTORY_CODE
					,0 AS PRIMARY_QUANTITY
					,mmt.PRIMARY_QUANTITY AS INT_ORDER_SHIP
					,0 AS INT_ORDER_RCV
					,0 AS PO_RCV
					,oap.PERIOD_START_DATE
				FROM APPS.MTL_MATERIAL_TRANSACTIONS mmt 
				    ,APPS.ORG_ACCT_PERIODS oap 
                    ,APPS.GL_PERIODS_V gp 
				WHERE 1 = 1
                AND mmt.ACCT_PERIOD_ID = oap.ACCT_PERIOD_ID
                AND mmt.ORGANIZATION_ID = oap.ORGANIZATION_ID
                AND gp.PERIOD_SET_NAME = v_period_set
                AND gp.PERIOD_YEAR = v_prev_year
                AND gp.PERIOD_NUM = v_prev_period
                AND oap.PERIOD_START_DATE > gp.END_DATE
				AND mmt.TRANSACTION_TYPE_ID = 62
--				AND mmt.INVENTORY_ITEM_ID = 317516
				UNION ALL
				SELECT mmt.INVENTORY_ITEM_ID
                    ,mmt.ORGANIZATION_ID
                    ,mmt.SUBINVENTORY_CODE
					,mmt.PRIMARY_QUANTITY * -1 AS PRIMARY_QUANTITY
					,0 AS INT_ORDER_SHIP
                    ,(CASE WHEN mmt.TRANSACTION_TYPE_ID = 61
                           THEN
                                mmt.PRIMARY_QUANTITY
                            ELSE
                                0
                      END) AS INT_ORDER_RCV
                    ,(CASE WHEN mmt.TRANSACTION_TYPE_ID IN (18, 36, 71)
                           THEN
                                mmt.PRIMARY_QUANTITY
                            ELSE
                                0
                      END) AS PO_RCV
					,oap.PERIOD_START_DATE
				FROM APPS.MTL_MATERIAL_TRANSACTIONS mmt 
				    ,APPS.ORG_ACCT_PERIODS oap 
                    ,APPS.GL_PERIODS_V gp
				WHERE 1=1
				AND mmt.ACCT_PERIOD_ID = oap.ACCT_PERIOD_ID
				AND mmt.ORGANIZATION_ID = oap.ORGANIZATION_ID
                AND gp.PERIOD_SET_NAME = v_period_set
				AND gp.PERIOD_YEAR = v_prev_year
                AND gp.PERIOD_NUM = v_prev_period
                AND TRUNC(oap.PERIOD_START_DATE)> gp.END_DATE
				AND mmt.PRIMARY_QUANTITY <> 0
                AND (mmt.LOGICAL_TRANSACTION IS NULL OR (NOT mmt.LOGICAL_TRANSACTION IS NULL AND mmt.TRANSACTION_ID = mmt.PARENT_TRANSACTION_ID))
                AND NOT EXISTS(SELECT 'X' FROM dual WHERE mmt.TRANSACTION_TYPE_ID = v_exclude_trans_type_id)
--                AND mmt.INVENTORY_ITEM_ID = 317516
				  ) mmt			            
                 ,INV.MTL_SECONDARY_INVENTORIES msic                 
            WHERE 1 = 1
                  AND msic.ORGANIZATION_ID(+) = mmt.ORGANIZATION_ID
                  AND msic.SECONDARY_INVENTORY_NAME(+) = mmt.SUBINVENTORY_CODE
            GROUP BY mmt.INVENTORY_ITEM_ID,mmt.ORGANIZATION_ID,mmt.PERIOD_START_DATE --, mmt.SUBINVENTORY_CODE
        UNION ALL 
/*********************************************************************************************
* Sum latest on-hand stock for calculating on-hand of period                                 *
* Calculation Rules:                                                                         *
* 1. Exclude on-hand stock which subinventory is not EO inventory                            *
* 2. Sum on-hand stock by subinventory type                                                  *
*********************************************************************************************/
             SELECT moq.INVENTORY_ITEM_ID
                   ,moq.ORGANIZATION_ID
--                   ,moq.SUBINVENTORY_CODE
                   ,SUM(moq.PRIMARY_TRANSACTION_QUANTITY) AS PRIMARY_QUANTITY
                   ,SUM((CASE                                                                   -- ER-2 Exclude non-EO subinventory
                        WHEN     --MSIC.AVAILABILITY_TYPE = 1
                             msic.ASSET_INVENTORY = 1
                             AND NOT EXISTS
                                    (SELECT 'X'
                                       FROM GL.GL_CODE_COMBINATIONS gcci
                                      WHERE     (   gcci.SEGMENT3 IN
                                                       ('14313', '14217')
                                                 OR gcci.SEGMENT3 LIKE '6%')
                                            AND msic.MATERIAL_ACCOUNT =
                                                   gcci.CODE_COMBINATION_ID(+))
                             AND NOT EXISTS
                                    (SELECT 'X'
                                       FROM DUAL
                                      WHERE    moq.SUBINVENTORY_CODE IN
                                                  ('ADVRPL-ESI',
                                                   'ESI SG RMA',
                                                   'LSR SRVFBN')
                                            OR moq.SUBINVENTORY_CODE LIKE 'x%')
                        THEN
                           moq.PRIMARY_TRANSACTION_QUANTITY
                        ELSE
                           0
                     END))
                       EO_ONHAND
                   ,SUM((CASE                            -- Belong to Non Nettable onhand
                        WHEN     MSIC.AVAILABILITY_TYPE = 2
                        THEN
                           moq.PRIMARY_TRANSACTION_QUANTITY
                        ELSE
                           0
                     END))
                       NON_NET_ONHAND
                   ,SUM((CASE                  -- Belong to MRB Nonnettable Onhand
                        WHEN EXISTS
                                (SELECT 'X'
                                   FROM DUAL
                                  WHERE     MSIC.AVAILABILITY_TYPE = 2
                                        AND (   moq.SUBINVENTORY_CODE LIKE
                                                   '%MRB%'
                                             OR moq.SUBINVENTORY_CODE LIKE
                                                   '%REWORK%'
                                             OR moq.SUBINVENTORY_CODE LIKE
                                                   '%RTV%'))
                        THEN
                           moq.PRIMARY_TRANSACTION_QUANTITY
                        ELSE
                           0
                     END))
                       MRB_NONNETTABLE
                   ,SUM((CASE                  -- Belong to RMA Nonnettable Onhand
                        WHEN     MSIC.AVAILABILITY_TYPE = 2
                             AND moq.SUBINVENTORY_CODE LIKE '%RMA%'
                        THEN
                           moq.PRIMARY_TRANSACTION_QUANTITY
                        ELSE
                           0
                     END))
                       RMA_NONNETTABLE
                   ,SUM((CASE                          -- Belong to Service Onhand
                        WHEN moq.SUBINVENTORY_CODE LIKE '%SRV%' OR moq.SUBINVENTORY_CODE LIKE '%RSC%'
                        THEN
                           moq.PRIMARY_TRANSACTION_QUANTITY
                        ELSE
                           0
                     END))
                       SRV_ONHAND
                   ,SUM((CASE                             -- Belong to Loan Onhand
                        WHEN moq.SUBINVENTORY_CODE LIKE '%LOAN%' 
                        THEN
                           moq.PRIMARY_TRANSACTION_QUANTITY
                        ELSE
                           0
                     END))
                       LOAN_ONHAND
                   ,0 AS INT_ORDER_INTRANSIT
                   ,0 AS PO_RCV_INTRANSIT
                   ,TO_DATE ('9999-12-31', 'YYYY-MM-DD') AS PERIOD_START_DATE
               FROM inv.MTL_ONHAND_QUANTITIES_DETAIL moq,
                    INV.MTL_SECONDARY_INVENTORIES msic
              WHERE     1 = 1
                    AND msic.ORGANIZATION_ID = moq.ORGANIZATION_ID
                    AND msic.SECONDARY_INVENTORY_NAME = moq.SUBINVENTORY_CODE
--                    AND moq.INVENTORY_ITEM_ID = 317516
              GROUP BY moq.INVENTORY_ITEM_ID,moq.ORGANIZATION_ID --,moq.SUBINVENTORY_CODE
              
    UNION ALL
/******************************************************************************
* * Inventory Data - Receiving
* Sum latest SUPPLY quantity of Period for calculating receive intransit of   *
* PO and Int Order                                                            * 
******************************************************************************/    
            SELECT ms.ITEM_ID INVENTORY_ITEM_ID
                ,ms.TO_ORGANIZATION_ID AS ORGANIZATION_ID
                ,0 AS PRIMARY_QUANTITY
                ,0 AS EO_ONHAND
                ,0 AS NON_NET_ONHAND
                ,0 AS MRB_NONNETTABLE
                ,0 AS RMA_NONNETTABLE
                ,0 AS SRV_ONHAND
                ,0 AS LOAN_ONHAND
                ,DECODE(NVL(PO_HEADER_ID,0),0,ROUND(ms.TO_ORG_PRIMARY_QUANTITY,6),0) AS INT_ORDER_INTRANSIT
                ,DECODE(NVL(ms.FROM_ORGANIZATION_ID,0),0,ROUND(ms.TO_ORG_PRIMARY_QUANTITY,6),0) AS PO_RCV_INTRANSIT
                ,TO_DATE ('9999-12-31', 'YYYY-MM-DD') AS PERIOD_START_DATE
            FROM inv.MTL_SUPPLY ms
            WHERE 1=1
            AND ms.SUPPLY_TYPE_CODE IN ( 'SHIPMENT' , 'RECEIVING') 
            AND ms.DESTINATION_TYPE_CODE = 'INVENTORY'
--            AND ms.ITEM_ID = 317516
    UNION ALL
/**************************************************************************************
* Inventory Data - Receiving                                                          *
* Total receiving transaction quantity of Period for calculating PO receive intransit *
* Calculation Rules:                                                                  *
* 1. Sum reveived quantity by fiscal month                                            *
* 2. Reduce quantity of Correct and Return to Vendor transaction posted at RECEIVINVG *
**************************************************************************************/   
        SELECT rsl.ITEM_ID INVENTORY_ITEM_ID
            ,rt.ORGANIZATION_ID
            ,0 AS PRIMARY_QUANTITY
            ,0 AS EO_ONHAND
            ,0 AS NON_NET_ONHAND
            ,0 AS MRB_NONNETTABLE
            ,0 AS RMA_NONNETTABLE
            ,0 AS SRV_ONHAND
            ,0 AS LOAN_ONHAND
            ,0 AS INT_ORDER_INTRANSIT
            ,(CASE 
                WHEN rt.TRANSACTION_TYPE = 'RECEIVE'
                THEN
                    ROUND(rt.PRIMARY_QUANTITY,6) * -1
                WHEN rt.TRANSACTION_TYPE = 'CORRECT'
                 AND EXISTS(SELECT 'X' FROM APPS.RCV_TRANSACTIONS rtr
                                WHERE rtr.TRANSACTION_TYPE = 'RECEIVE' 
                                AND rt.PARENT_TRANSACTION_ID = rtr.TRANSACTION_ID)
                THEN
                    ROUND(rt.PRIMARY_QUANTITY,6) * -1
                WHEN rt.TRANSACTION_TYPE = 'RETURN TO VENDOR'
                    AND rt.DESTINATION_TYPE_CODE = 'RECEIVING'
                THEN
                    ROUND(rt.PRIMARY_QUANTITY,6)
                ELSE
                    0
             END) AS PO_RCV_INTRANSIT
            ,gpt.START_DATE AS PERIOD_START_DATE
        FROM APPS.RCV_TRANSACTIONS RT
           ,APPS.RCV_SHIPMENT_LINES RSL
           ,APPS.GL_PERIODS_V gp
           ,APPS.GL_PERIODS_V gpt
        WHERE 1=1
        AND RSL.SHIPMENT_LINE_ID = RT.SHIPMENT_LINE_ID
        AND gp.PERIOD_SET_NAME = v_period_set
        AND gpt.PERIOD_SET_NAME = v_period_set
--        AND rsl.SHIPMENT_LINE_STATUS_CODE = 'FULLY RECEIVED'
        AND EXISTS(SELECT 'X' FROM apps.PO_DISTRIBUTIONS_ALL pda 
                    WHERE pda.DESTINATION_TYPE_CODE = 'INVENTORY'
                        AND pda.PO_HEADER_ID = rsl.PO_HEADER_ID
                        AND pda.PO_LINE_ID = rsl.PO_LINE_ID
                        AND pda.LINE_LOCATION_ID = rsl.PO_LINE_LOCATION_ID)
        AND TRUNC(rt.TRANSACTION_DATE) BETWEEN gpt.START_DATE AND gpt.END_DATE
        AND TRUNC(rt.TRANSACTION_DATE) > gp.END_DATE
--        AND rt.TRANSACTION_TYPE = 'RECEIVE'
        AND rt.SOURCE_DOCUMENT_CODE = 'PO'
        AND gp.PERIOD_YEAR = v_prev_year
        AND gp.PERIOD_NUM = v_prev_period
--        AND rsl.ITEM_ID = 317516
        )  pt
		,APPS.GL_PERIODS_V gp
		,APPS.MTL_SYSTEM_ITEMS_B item
        ,INV.MTL_PARAMETERS param
      WHERE     1 = 1
			AND item.INVENTORY_ITEM_ID = pt.INVENTORY_ITEM_ID
			AND item.ORGANIZATION_ID = pt.ORGANIZATION_ID
			AND pt.ORGANIZATION_ID = param.ORGANIZATION_ID
			AND gp.PERIOD_SET_NAME = v_period_set
--			AND pt.PERIOD_START_DATE > gp.END_DATE
			AND gp.PERIOD_YEAR = v_year 
			AND gp.PERIOD_NUM = v_period
--            AND pt.INVENTORY_ITEM_ID =   3390
   GROUP BY pt.INVENTORY_ITEM_ID,
            item.SEGMENT1,
            pt.ORGANIZATION_ID,
            param.ORGANIZATION_CODE,
            item.PRIMARY_UOM_CODE,
--            pt.SUBINVENTORY_CODE,
            gp.PERIOD_YEAR,
            gp.PERIOD_NUM
    UNION ALL
/**********************************************************************************
* Transaction Data                                                                *
* Total up inventory transaction quantity of Period for EO Subledger calculation  *
* Calculation Rules:                                                              *
* 1. Exclude transaction which subinventory is not EO inventory                   *
* 2. Sum transaction quantity by transaction type and fiscal month                *
**********************************************************************************/ 
        SELECT oap.PERIOD_YEAR AS FISCAL_YEAR
         ,oap.PERIOD_NUM AS FISCAL_PERIOD
         ,mmt.INVENTORY_ITEM_ID
         ,(SELECT item.SEGMENT1
			FROM APPS.MTL_SYSTEM_ITEMS_B item
			WHERE item.INVENTORY_ITEM_ID = mmt.INVENTORY_ITEM_ID
				AND item.ORGANIZATION_ID = mmt.ORGANIZATION_ID) INVENTORY_ITEM
         ,mmt.ORGANIZATION_ID
         ,param.ORGANIZATION_CODE
         ,(SELECT item.PRIMARY_UOM_CODE
			FROM APPS.MTL_SYSTEM_ITEMS_B item
			WHERE item.INVENTORY_ITEM_ID = mmt.INVENTORY_ITEM_ID
				AND item.ORGANIZATION_ID = mmt.ORGANIZATION_ID) AS PRIMARY_UOM
         ,0 AS TOTAL_ONHAND
         ,0 AS NON_NET_ONHAND
         ,0 AS EO_ONHAND
         ,0 AS PREV_EO_ONHAND
         ,0 AS MRB_NONNETTABLE
         ,0 AS RMA_NONNETTABLE
         ,0 AS SRV_ONHAND
         ,0 AS LOAN_ONHAND
         ,0 AS INT_ORDER_INTRANSIT
         ,0 AS PREV_INT_INTRANSIT
         ,0 AS PO_RCV_INTRANSIT
         ,0 AS PREV_PO_INTRANSIT
         ,SUM(CASE WHEN mmt.TRANSACTION_TYPE_ID IN (18,36,71)                                   -- EO-7 PO Receipt
                THEN
                    mmt.EO_QUANTITY
                ELSE
                    0
            END) AS PO_RECEIVED
          ,SUM(CASE WHEN mmt.TRANSACTION_TYPE_ID = 33                                           -- EO-1 SO Ship
                    AND EXISTS(SELECT 'X' FROM GL.GL_CODE_COMBINATIONS 
                                WHERE (SEGMENT3 ='40005' OR SEGMENT3 = '14332')
                                AND CODE_COMBINATION_ID = mmt.DISTRIBUTION_ACCOUNT_ID)
                THEN
                    mmt.EO_QUANTITY
                ELSE
                    0
            END) AS STD_ORDER_SHIP
         ,SUM(CASE
                WHEN mmt.TRANSACTION_TYPE_ID = 33                                               -- EO-2 Consignment Order Ship
                    AND EXISTS(SELECT 'X' FROM GL.GL_CODE_COMBINATIONS 
                                WHERE (SEGMENT3 ='40124' OR SEGMENT3 = '40308')
                                AND CODE_COMBINATION_ID = mmt.DISTRIBUTION_ACCOUNT_ID)
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END)  AS CON_ORDER_SHIP
        , SUM(CASE WHEN mmt.TRANSACTION_TYPE_ID IN (35,43, 38, 48)                              -- EO-5 WIP Issue
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END) AS WIP_ISSUE
         , SUM(CASE WHEN mmt.TRANSACTION_TYPE_ID IN (44,17,90)                                  -- EO-6 WIP Completion
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END) AS WIP_COMPLETION
        , SUM(CASE
                WHEN mmt.TRANSACTION_TYPE_ID = 62
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END) AS INT_SHIP
        , SUM(CASE
                WHEN mmt.TRANSACTION_TYPE_ID = 61
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END) AS INT_RCV
        , SUM(CASE
                WHEN mmt.TRANSACTION_TYPE_ID IN (1,4,8,31,32,40,41,42)                          -- EO-3 Misc Issue
                     AND NOT EXISTS(SELECT 'X' FROM INV.MTL_TRANSACTION_ACCOUNTS mta, 
                                                    GL.GL_CODE_COMBINATIONS ref_acc
                                      WHERE mta.TRANSACTION_ID = mmt.TRANSACTION_ID 
                                      AND mta.REFERENCE_ACCOUNT = ref_acc.CODE_COMBINATION_ID
                                      AND mta.ACCOUNTING_LINE_TYPE = 2 
                                      AND ref_acc.SEGMENT3 = '40312' )
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END) AS MISC_ISSUE
        , SUM(CASE
                WHEN mmt.TRANSACTION_TYPE_ID IN (1,4,8,31,32,40,41,42)                          -- EO-4 EO Scrap
                     AND EXISTS(SELECT 'X' FROM INV.MTL_TRANSACTION_ACCOUNTS mta, 
                                                GL.GL_CODE_COMBINATIONS ref_acc
                                  WHERE mta.TRANSACTION_ID = mmt.TRANSACTION_ID 
                                  AND mta.REFERENCE_ACCOUNT = ref_acc.CODE_COMBINATION_ID
                                  AND mta.ACCOUNTING_LINE_TYPE = 2 
                                  AND ref_acc.SEGMENT3 = '40312' )
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END) AS EO_SCRAP
        , SUM(CASE 
                WHEN mmt.TRANSACTION_TYPE_ID IN (2,52)
                THEN 
                    0
                ELSE
                    mmt.EO_QUANTITY
              END) AS INV_OTHERS
--         ,mmt.CREATION_DATE
     FROM 
         (SELECT mmt.TRANSACTION_ID
                ,mmt.ACCT_PERIOD_ID
                ,mmt.ORGANIZATION_ID
                ,mmt.INVENTORY_ITEM_ID
                ,mmt.TRANSACTION_DATE
                ,mmt.CREATION_DATE   
                ,mmt.TRANSACTION_TYPE_ID
                ,mmt.SUBINVENTORY_CODE
                ,mmt.TRANSACTION_QUANTITY
                ,mmt.TRANSACTION_UOM
                ,mmt.PRIMARY_QUANTITY
                ,mmt.TRANSFER_ORGANIZATION_ID
                ,mmt.DISTRIBUTION_ACCOUNT_ID
                ,(CASE                                                                          -- ER-2 Exclude non-EO subinventory
                        WHEN msic.ASSET_INVENTORY = 1
                         AND NOT EXISTS
                                (SELECT 'X'
                                  FROM GL.GL_CODE_COMBINATIONS gcci
                                  WHERE ( gcci.SEGMENT3 IN ('14313', '14217')
                                             OR gcci.SEGMENT3 LIKE '6%')
                                     AND msic.MATERIAL_ACCOUNT =
                                               gcci.CODE_COMBINATION_ID(+))
                         AND NOT EXISTS(SELECT 'X' FROM DUAL
                                        WHERE mmt.SUBINVENTORY_CODE IN
                                              ('ADVRPL-ESI',
                                               'ESI SG RMA',
                                               'LSR SRVFBN')
                                            OR mmt.SUBINVENTORY_CODE LIKE 'x%')
                        THEN
                           mmt.PRIMARY_QUANTITY
                        ELSE
                           0
                     END) AS EO_QUANTITY
          FROM APPS.MTL_MATERIAL_TRANSACTIONS mmt
              ,INV.MTL_SECONDARY_INVENTORIES msic
           WHERE 1 = 1
            AND msic.ORGANIZATION_ID = mmt.ORGANIZATION_ID
            AND msic.SECONDARY_INVENTORY_NAME = mmt.SUBINVENTORY_CODE
            AND mmt.PRIMARY_QUANTITY <> 0
            AND (mmt.LOGICAL_TRANSACTION IS NULL 
                OR (NOT mmt.LOGICAL_TRANSACTION IS NULL 
                    AND mmt.TRANSACTION_ID = mmt.PARENT_TRANSACTION_ID))
            AND NOT EXISTS(SELECT 'X' FROM dual WHERE mmt.TRANSACTION_TYPE_ID = v_exclude_trans_type_id)
--            AND mmt.INVENTORY_ITEM_ID = 317516
         ) mmt
         ,APPS.ORG_ACCT_PERIODS oap
         ,INV.MTL_PARAMETERS param
    WHERE     1 = 1
          AND mmt.ACCT_PERIOD_ID = oap.ACCT_PERIOD_ID
          AND mmt.ORGANIZATION_ID = oap.ORGANIZATION_ID
          AND param.ORGANIZATION_ID = mmt.ORGANIZATION_ID
          AND oap.PERIOD_YEAR = v_year
          AND oap.PERIOD_NUM = v_period
    GROUP BY oap.PERIOD_YEAR, oap.PERIOD_NUM
            ,mmt.INVENTORY_ITEM_ID
            ,mmt.ORGANIZATION_ID
            ,param.ORGANIZATION_CODE
    ) txn
    GROUP BY txn.FISCAL_YEAR, txn.FISCAL_PERIOD
            ,txn.INVENTORY_ITEM_ID
            ,txn.INVENTORY_ITEM
            ,txn.ORGANIZATION_ID
            ,txn.ORGANIZATION_CODE
            ,txn.PRIMARY_UOM
    )) pio ON pio.ORGANIZATION_CODE  = A.ORGANIZATION_CODE AND pio.INVENTORY_ITEM = A.ITEM
    WHERE 1=1
        AND NVL(A.FISCAL_YEAR,0)     = v_year
        AND NVL(A.FISCAL_PERIOD,0)   = v_period
        AND ((p_data_source='LITE' AND NOT NVL(A.ORGANIZATION_ID,'X') = 'CM') 
                OR (p_data_source='CM' AND NVL(A.ORGANIZATION_ID,'X') = 'CM') 
                OR (NVL(p_data_source,'X') = 'X'))
        AND ((v_item IS NOT NULL AND v_item = A.ITEM) OR (v_item IS NULL))
        AND ((v_item_org IS NOT NULL AND v_item_org = A.ORGANIZATION_CODE) OR (v_item_org IS NULL))
        AND NVL(A.FROZEN_FLAG,'N') = 'N'
        AND ((NVL(EO_CALC_FLAG,'X') = 'X' AND v_calc = 'Y') OR v_calc = 'N')
        AND NVL(A.EO_REVIEW_FLAG,'R') <> 'C'
        ORDER BY A.ORGANIZATION_CODE,A.ITEM;
/******* Cursor of cur_eo  **************************************************/   

/*** Main Program ***/
 BEGIN  

-- Assigining value to g_debug_msgs to print or not to print messages in log file

   g_debug_msgs := p_debug_msgs;
   
   v_usage      := NVL(p_calc_usage, 'N');
   v_reval      := NVL(p_calc_reeval, 'N');
   v_reserve    := NVL(p_calc_reserve, 'N');
   v_provision  := NVL(p_calc_provision, 'N');

 /******************************************************************************
* Step-1: Get period data of calculation period and previous period           *
******************************************************************************/ 
    l_year      := p_fiscal_year;
    l_period    := p_fiscal_period;
    l_qtr       := TRUNC((TO_NUMBER(l_period ) - 1)/3) + 1;
    
    BEGIN
        SELECT QUARTER_NUM, PERIOD_NAME, START_DATE, END_DATE
        INTO l_qtr, l_period_name,l_period_start,l_period_end
        FROM APPS.GL_PERIODS_V
        WHERE 1=1
        AND PERIOD_SET_NAME = v_period_set
        AND PERIOD_YEAR = TO_NUMBER(l_year)
        AND PERIOD_NUM  = TO_NUMBER(l_period);
    EXCEPTION
     WHEN OTHERS THEN
          l_period_name := NULL;
          l_qtr := NULL;	  
    END;

    SELECT PERIOD_YEAR, QUARTER_NUM, PERIOD_NUM, START_DATE, END_DATE
    INTO l_prev_year, l_prev_qtr, l_prev_period,l_prev_period_start,l_prev_period_end
    FROM APPS.GL_PERIODS_V
    WHERE 1=1
    AND PERIOD_SET_NAME = v_period_set
    AND l_period_start - 1 BETWEEN START_DATE AND END_DATE;		  

    DEBUG('Values of period: p_year: '|| l_year || ' - p_period: '|| l_period ||' - prev_year: '||l_prev_year||' - prev_period: '||l_prev_period||'- calc'||v_usage||v_reval||v_reserve||v_provision);
 
/******************************************************************************
* Step-2: Opening cursor of previous period                                   *
*         To add miss records into calculation period                         *
******************************************************************************/
	FOR cr_prev in prev_eo(l_year, l_period, l_prev_year, l_prev_period, p_item, p_item_org) LOOP

--		DEBUG('Value of previous period for item   : '||cr_prev.ITEM);

        IF NVL(cr_prev.ORGANIZATION_ID,'X') = 'CM' THEN
            IF MOD(l_period, 3) = 0 THEN
                l_po_reserve := 0;
            ELSE
                IF l_period = 3 * l_qtr - 2 THEN 
                    l_po_reserve := cr_prev.KEEP_PO_RESERVE;
                ELSE
                    l_po_reserve := cr_prev.CUR_PO_RESERVE_QTY;
                END IF;
            END IF;
            l_gaap_code  := cr_prev.GAAP_CODE;
        ELSE
            IF l_period = 3 * l_qtr - 2 THEN 
                l_po_reserve := cr_prev.KEEP_PO_RESERVE;
            ELSE
                l_po_reserve := cr_prev.CUR_PO_RESERVE_QTY;
            END IF;
--            l_po_reserve := 0;
            l_gaap_code  := NULL;
        END IF;
        
       DEBUG('Inserted for item   : '||cr_prev.ITEM||' - '||cr_prev.ORGANIZATION_CODE||' - '||p_fiscal_year||' - '||cr_prev.CUR_EO_RESERVE_QTY);
        
       INSERT INTO xxsc.XXSC_EO_DASHBOARD_TBL(DASHBOARD_REC_ID, 
                                                 ITEM,
                                                 ITEM_ID,
                                                 ORGANIZATION_CODE,
                                                 ORGANIZATION_ID,
                                                 LITE_ORG_CODE,
                                                 ORG_DESCR,
                                                 FISCAL_YEAR,
                                                 FISCAL_QTR,
                                                 FISCAL_PERIOD,
                                                 PERIOD_NAME,
                                                 ITEM_TYPE ,
                                                 L3_BUS_GROUP,
                                                 L4_BUS_UNIT,
                                                 L6_MODULE,
                                                 GL_LINE_CODE,
                                                 GL_LINE_DESCR,
                                                 ITEM_DESCR,
                                                 ITEM_STATUS,
                                                 CM_ITEM,
                                                 BEGIN_EO_RESERVE_QTY,
                                                 CUR_PO_RESERVE_QTY,
                                                 GAAP_CODE,
                                                 CREATION_DATE)
            VALUES(XXSC.XXSC_EO_DASHB_SEQ.NEXTVAL,
                   cr_prev.ITEM,
                   cr_prev.ITEM_ID,
                   cr_prev.ORGANIZATION_CODE,
                   cr_prev.ORGANIZATION_ID,
                   cr_prev.LITE_ORG_CODE,
                   cr_prev.ORG_DESCR,
                   l_year,
                   l_qtr,
                   l_period,
                   l_period_name,
                   cr_prev.ITEM_TYPE,
                   cr_prev.L3_BUS_GROUP,
                   cr_prev.L4_BUS_UNIT,
                   cr_prev.L6_MODULE,
                   cr_prev.GL_LINE_CODE,
                   cr_prev.GL_LINE_DESCR,
                   cr_prev.ITEM_DESCR,
                   cr_prev.ITEM_STATUS,
                   cr_prev.CM_ITEM,
                   NVL(cr_prev.EO_RESERVE_INC_PROVISION,NVL(cr_prev.CUR_EO_RESERVE_QTY,0)),
                   l_po_reserve,
                   l_gaap_code,
                   TRUNC(SYSDATE));
                     
        COMMIT;

	END LOOP;

    l_eo_cal_flag := 'N';
    l_transfer_flag_cnt := 0;
    
    DEBUG('Output Column|eo_id|item id|item|org id|org code|Prev onhand|Prev po intransit|Prev int intransit|Prev Ending Reserve|Prev Unrel. Reserve|onhand|CM OH|SRV OH|Loan OH|Open PO|PO rcv|PO intransit|Int Order Received|Std Order|Consign Order|Int Order|WIP Issue|WIP Completion|Misc. Issue|EO Scarp|Inv Others|buyback EO|Int Rcpt EO|Open PO Reserve|Net Inv|Net Reserve|Ending Reserve|EO Usage|Unrel. EO|EOU SO|EOU CO|EOU Misc.|EOU Int|EOU WIP|EOU Others|Total Reserve| Total Provision|Provision oh|Provison PO|Keep PO Reserve|item cost|prev cost|reeval|sold to org');

<<RECALC_USAGE>>

/******************************************************************************
* Step-3: Opening cursor of calculation period                                *
******************************************************************************/  
    OPEN cur_eo(l_year, l_period, l_prev_year,l_prev_period, p_item, p_item_org, l_eo_cal_flag);

LOOP
     
    FETCH cur_eo BULK COLLECT INTO cr_usage LIMIT 1000;
    EXIT WHEN NVL(cur_eo%ROWCOUNT,0) = 0;
        
    FOR idx IN cr_usage.FIRST..cr_usage.LAST LOOP
	
		l_item_cost         := NULL;
		l_prev_item_cost    := NULL;
		
		l_unrel_eo_usage    := NULL;
		l_cur_unrel_eo_usage:= NULL;
		l_eo_usage_so       := NULL;
		l_eo_usage_cons     := NULL;
		l_eo_usage_misc     := NULL;
		l_eo_usage_int_ord  := NULL;
		l_eo_usage_wip      := NULL;
		l_total_eo_usage    := NULL;
		l_eo_re_eval        := NULL;
		l_eo_reserve        := NULL;
		l_po_reserve        := NULL;
		l_mrb_reserve       := NULL;
		l_rma_reserve       := NULL;
		l_sp_reserve        := NULL;
        l_trans_reserve     := NULL;
        l_buyback_reserve   := NULL;
		l_beg_reserve       := NULL;
		l_net_reserve       := NULL;
		l_total_reserve     := 0;
		l_wip_completion    := 0;
		l_po_rcv_intransit  := 0;
		l_provision_oh      := 0;		
		l_int_rcpt_dest     := 0;
		l_sold_to_id        := 0;
		l_active_inv        := 0;
		l_new_eo_qty        := 0;
		l_var1              := 0;
		l_var2              := 0;
		l_qty               := 0;
		l_transfer_flag     :='N';
		l_org_code          := NULL;
		l_org_descr         := NULL;

		l_rec_exists        := 0;
		l_reserve_inc_prov  := 0;
		
		l_calc_flow_code    := 'CFC';
		l_intship           := 0;
		l_length            := 0;
		l_intship_comment   := NULL;
		l_intrcpt_comment   := NULL;
		l_ordship_comment   := NULL;
		l_misciss_comment   := NULL;
		l_reserve_comment   := NULL;

/******************************************************************************
* Step-4: Extracting value from cursor to variables                           *
******************************************************************************/
       
            l_eo_rec_id         := cr_usage(idx).EO_REC_ID;
--            l_org_id		    := cr_usage.ORGANIZATION_ID;
--            l_org_code          := cr_usage.ORGANIZATION_CODE;
--            l_item              := cr_usage(idx).INVENTORY_ITEM;
            IF NVL(cr_usage(idx).BEGIN_EO_RESERVE,0) = 0 THEN
                l_beg_reserve       := NVL(cr_usage(idx).PREV_ENDING_RESERVE, NVL(cr_usage(idx).BEGIN_EO_RESERVE,0));
            ELSE
                l_beg_reserve       := NVL(cr_usage(idx).BEGIN_EO_RESERVE,0);
            END IF;

            l_trans_reserve     := NVL(cr_usage(idx).TRANS_RESERVE,0);
            l_buyback_reserve   := NVL(cr_usage(idx).BUYBACK_RESERVE,0);
            l_eo_scrap          := NVL(cr_usage(idx).EO_SCRAP,NVL(cr_usage(idx).INV_EO_SCRAP,0));
            l_so_issue          := NVL(cr_usage(idx).STD_ORD_SHIP,0);
            l_cons_ord_issue    := NVL(cr_usage(idx).CON_ORD_SHIP,0);
            l_iso_issue         := NVL(cr_usage(idx).INT_ORD_SHIP,0);
            l_wip_issue         := NVL(cr_usage(idx).WIP_ISSUE,0);
            l_wip_completion    := NVL(cr_usage(idx).WIP_COMPLETION,0);
            l_misc_issue        := NVL(cr_usage(idx).MISC_ISSUE,0);
            l_onhand            := NVL(cr_usage(idx).ONHAND,0);
                        
            IF cr_usage(idx).ORGANIZATION_ID = 'CM' THEN
                
--                IF l_onhand = 0 THEN
--                    l_onhand := NVL(cr_usage(idx).CM_OH,0);
--                END IF;
                
                IF NVL(cr_usage(idx).CM_OH,0) = 0 THEN
                    l_cm_oh         := l_onhand;
                ELSE
                    l_cm_oh         := cr_usage(idx).CM_OH;
                END IF;
            ELSE
                
                IF l_onhand = 0 OR NVL(cr_usage(idx).INV_ONHAND,0) = 0 THEN
                    l_onhand := NVL(cr_usage(idx).INV_ONHAND,0);
                END IF;
                l_cm_oh         := NVL(cr_usage(idx).CM_OH,0);           
            END IF;
            
            l_srv_oh            := NVL(cr_usage(idx).SRV_NET_OH,0);
            l_loan_oh           := NVL(cr_usage(idx).LOAN_NET_OH,0); 
            l_others            := NVL(cr_usage(idx).INV_OTHERS,0);    
            l_open_po           := NVL(cr_usage(idx).OPEN_PO,0);       
            l_po_rcv            := NVL(cr_usage(idx).PO_RCV,0);
            l_po_rcv_intransit  := NVL(cr_usage(idx).PO_RCV_INTRANSIT,0);
            l_int_order_rcv     := NVL(cr_usage(idx).INT_ORD_RCV,0);
            l_int_order_intransit  := NVL(cr_usage(idx).INT_ORD_INTRANSIT,0);
            l_eo_usage_chg      := NVL(cr_usage(idx).EO_USAGE_CHANGE_FLAG,'N');
            l_total_eo_usage    := NVL(cr_usage(idx).TOTAL_EO_USAGE,0);
            l_cur_unrel_eo_usage:= NVL(cr_usage(idx).UNREL_EO_USAGE,0);
            l_total_provision   := NVL(cr_usage(idx).TOTAL_PROVISION,0);
            l_sold_to_id        := NULL;
            l_sold_to_org       := NULL;
            l_eo_reserve        := NVL(cr_usage(idx).NET_RESERVE,0);
            l_po_reserve        := NVL(cr_usage(idx).PO_RESERVE,0);
            
            IF l_po_reserve = 0 THEN
                IF l_period = 3 * l_qtr - 2 THEN
                    l_po_reserve := NVL(cr_usage(idx).PREV_KEEP_PO_RESERVE, 0);
                ELSE
                    l_po_reserve := NVL(cr_usage(idx).PREV_PO_RESERVE, 0);
                END IF;
            END IF;
            
            l_mrb_reserve       := NVL(cr_usage(idx).MRB_RESERVE,0);
            l_rma_reserve       := NVL(cr_usage(idx).RMA_RESERVE,0);
            l_sp_reserve        := NVL(cr_usage(idx).SP_RESERVE,0);
            
            l_prev_onhand       := NVL(cr_usage(idx).PREV_ONHAND,0);
            l_beg_unrel_reserve := NVL(cr_usage(idx).PREV_UNREL_USAGE,0);
            l_prev_cm_oh        := NVL(cr_usage(idx).PREV_CM_OH,0);
            l_prev_int_intransit:= NVL(cr_usage(idx).PREV_INT_INTRANSIT,0);
            l_prev_po_intransit := NVL(cr_usage(idx).PREV_PO_INTRANSIT,0);
            l_prev_keep_po_res  := NVL(cr_usage(idx).PREV_KEEP_PO_RESERVE, 0);
            l_prev_po_reserve   := NVL(cr_usage(idx).PREV_PO_RESERVE, 0);

/******************************************************************************
* Step-5: Determine reserve calculation flag by                               *
*         the sum of begin reserve, int receipt reserve and buyback reserve   *
*   If Calculation flag = 'X', calculate reserve of EO reocrd                *
*   If calculation flag = NULL, only update item cost                         *
******************************************************************************/
        l_qty := l_beg_reserve + l_po_reserve + l_mrb_reserve + l_rma_reserve + l_sp_reserve + l_trans_reserve + ABS(l_buyback_reserve);
         
        IF (NVL(l_qty,0) <> 0) OR l_total_provision > 0 OR l_eo_scrap > 0 OR l_eo_reserve > 0 THEN
            l_eo_cal_flag := 'X';
        ELSE
            l_eo_cal_flag := NULL;
        END IF;
	
DEBUG('Value of EO Rcd - l_eo_rec_id'|| l_eo_rec_id|| ' - l_beg_reserve:'|| l_beg_reserve|| ' - l_trans_reserve:'|| l_trans_reserve|| ' - l_buyback_reserve:'|| l_buyback_reserve|| ' - l_onhand:'|| l_onhand|| ' - l_cm_oh:'|| l_cm_oh|| ' - l_po_rcv:'||l_po_rcv||' - l_so_issue:'|| 
l_so_issue|| ' - l_cons_ord_issue:'||l_cons_ord_issue||' - l_iso_issue:'||l_iso_issue||' - l_wip_issue:'||l_wip_issue||' - l_wip_completion:'||l_wip_completion||' - l_misc_issue:'||l_misc_issue||' - l_eo_scrap:'||l_eo_scrap||' - l_eo_usage_chg:'|| l_eo_usage_chg|| ' - l_total_eo_usage:'||
l_total_eo_usage||' - l_cur_unrel_eo_usage:'||l_cur_unrel_eo_usage||' - l_total_provision:'||l_total_provision||' - l_eo_reserve:'||l_eo_reserve||' - l_po_reserve:'||l_po_reserve||' - l_mrb_reserve:'||l_mrb_reserve||' - l_rma_reserve:'||
l_rma_reserve||' - l_sp_reserve:' ||l_sp_reserve||' - l_others:' ||l_others||' - l_srv:' ||l_srv_oh);
    
/******************************************************************************
* Step-6: Extract LITE/CM item cost/price by calculation period               *
******************************************************************************/

        IF NVL(l_period,'X') <> 'X'  AND v_reval = 'Y' THEN
            IF NVL(cr_usage(idx).ITEM_COST,0) = 0 THEN
                IF cr_usage(idx).ORGANIZATION_ID <> 'CM' THEN
                    
                    BEGIN
                        SELECT STANDARD_COST INTO l_item_cost
                        FROM 
                           (SELECT csc.STANDARD_COST
                            FROM apps.CST_STANDARD_COSTS csc 
    --                            INNER JOIN apps.CST_COST_UPDATES ccu ON csc.COST_UPDATE_ID = ccu.COST_UPDATE_ID
                            WHERE 1=1
                            AND csc.inventory_item_id = cr_usage(idx).INVENTORY_ITEM_ID
                            AND csc.organization_id   = cr_usage(idx).ORGANIZATION_ID                    
    --                        AND ccu.STATUS = 3
                            AND TRUNC(csc.STANDARD_COST_REVISION_DATE) <= l_period_end
                            ORDER BY csc.STANDARD_COST_REVISION_DATE DESC)
                        WHERE ROWNUM = 1;
                    EXCEPTION
                        WHEN OTHERS THEN
                            l_item_cost := NVL(cr_usage(idx).ITEM_COST,0);							 
                    END;
                    
                ELSE
                    BEGIN
                        SELECT NVL(ITEM_COST,0) INTO l_item_cost
                        FROM
                         (  SELECT ITEM_COST
                            FROM XXSC.XXSC_EO_DASHBOARD_TBL A
                            WHERE 1=1
                            AND A.ORGANIZATION_CODE = cr_usage(idx).ORGANIZATION_CODE
                            AND A.ITEM = cr_usage(idx).INVENTORY_ITEM
                            AND A.FISCAL_YEAR||A.FISCAL_QTR <= l_year||l_qtr
                            AND NVL(A.ITEM_COST,0) > 0
                            ORDER BY A.FISCAL_YEAR DESC, A.FISCAL_PERIOD DESC)
                         WHERE ROWNUM = 1;
                    EXCEPTION
                        WHEN OTHERS THEN
                            l_item_cost := NVL(cr_usage(idx).ITEM_COST,0);	 
                    END; 

                END IF;
            ELSE
                l_item_cost := NVL(cr_usage(idx).ITEM_COST,0);
            END IF;							

/******************************************************************************
* Step-7: Extract LITE/CM item cost/price by previous period                  *
******************************************************************************/

            IF NVL(cr_usage(idx).ITEM_COST_PRE_PERIOD,0) = 0 THEN
                IF cr_usage(idx).ORGANIZATION_ID <> 'CM' THEN                
                    BEGIN
                        SELECT NVL(STANDARD_COST,l_item_cost) INTO l_prev_item_cost
                        FROM 
                           (SELECT csc.STANDARD_COST
                            FROM apps.CST_STANDARD_COSTS csc 
--                                INNER JOIN apps.CST_COST_UPDATES ccu ON csc.COST_UPDATE_ID = ccu.COST_UPDATE_ID
                            WHERE 1=1
                            AND csc.inventory_item_id = cr_usage(idx).INVENTORY_ITEM_ID
                            AND csc.organization_id   = cr_usage(idx).ORGANIZATION_ID                   
--                            AND ccu.STATUS = 3
                            AND TRUNC(csc.STANDARD_COST_REVISION_DATE) <= l_prev_period_end
                            ORDER BY csc.STANDARD_COST_REVISION_DATE DESC)
                        WHERE ROWNUM = 1;
                    EXCEPTION
                        WHEN OTHERS THEN
                            l_prev_item_cost := l_item_cost;	
                    END;

                ELSE
                    BEGIN
                        SELECT NVL(ITEM_COST,l_item_cost) INTO l_prev_item_cost
                        FROM
                         (  SELECT ITEM_COST
                            FROM XXSC.XXSC_EO_DASHBOARD_TBL A
                            WHERE 1=1
                            AND A.ORGANIZATION_CODE = cr_usage(idx).ORGANIZATION_CODE
                            AND A.ITEM = cr_usage(idx).INVENTORY_ITEM
                            AND A.FISCAL_YEAR||A.FISCAL_QTR < l_year||l_qtr
                            AND NVL(A.ITEM_COST,0)>0
                            ORDER BY A.FISCAL_YEAR DESC, A.FISCAL_PERIOD DESC)
                         WHERE ROWNUM = 1;
                    EXCEPTION
                        WHEN OTHERS THEN
                            l_prev_item_cost := l_item_cost;
                    END;

                END IF;
            ELSE
                l_prev_item_cost := NVL(cr_usage(idx).ITEM_COST_PRE_PERIOD,0);
            END IF;
		END IF;

/******************************************************************************
* Step-A8: Consolidate IntOrd ship/Rcpt and sales order ship into comment      *
******************************************************************************/   		
		IF cr_usage(idx).ORGANIZATION_ID <> 'CM' AND v_usage = 'Y'
            AND (ABS(l_iso_issue) + l_int_order_rcv + ABS(l_so_issue) + ABS(l_cons_ord_issue) + ABS(l_misc_issue))>0 
            AND cr_usage(idx).COMMENT_FLAG = 'X' THEN
  
            OPEN cur_trans(l_year, l_period, NVL(cr_usage(idx).INVENTORY_ITEM_ID,0),NVL(cr_usage(idx).ORGANIZATION_ID,'0'));
            
            LOOP
             
                FETCH cur_trans BULK COLLECT INTO cr_tr LIMIT 100;
            
                EXIT WHEN NVL(cur_trans%ROWCOUNT,0) = 0;
                
                IF NVL(l_intship_comment,'X') = 'X' THEN
                    l_intship_comment := 'IntShip';
                END IF;
                
                IF NVL(l_intrcpt_comment,'X') = 'X' THEN
                    l_intrcpt_comment := 'IntRcpt';
                END IF;
                
                IF NVL(l_ordship_comment,'X') = 'X' THEN
                    l_ordship_comment := 'OrdShip';
                END IF;
                
                IF NVL(l_misciss_comment,'X') = 'X' THEN
                    l_misciss_comment := 'Account';
                END IF;
                
                FOR i IN cr_tr.FIRST..cr_tr.LAST LOOP
                    IF cr_tr(i).TRANSACTION_TYPE_ID = 62 AND NVL(cr_tr(i).INT_SHIP,0)<>0 AND l_length < 490 THEN
                        IF NVL(l_intship_comment,'X') = 'IntShip' THEN           -- Pick first int order shipment as transfer reserve
                            l_sold_to_id := cr_tr(i).SOLD_TO_ID;
                            l_sold_to_org := cr_tr(i).SOLD_TO_NAME;
                            l_intship := ABS(NVL(cr_tr(i).INT_SHIP,0));
                        END IF;
                        l_intship_comment := l_intship_comment||'|'||TRIM(NVL(cr_tr(i).SOLD_TO_NAME,'NA'))||':'||ROUND(NVL(cr_tr(i).INT_SHIP,0),2);
                    END IF;
                    
                    IF cr_tr(i).TRANSACTION_TYPE_ID = 61 AND NVL(cr_tr(i).INT_RCV,0)<>0 AND l_length < 490 THEN
                        l_intrcpt_comment := l_intrcpt_comment||'|'||TRIM(NVL(cr_tr(i).RECEIVE_FROM_NAME,'NA'))||':'||ROUND(NVL(cr_tr(i).INT_RCV,0),2);
                    END IF;
                    
                    IF cr_tr(i).TRANSACTION_TYPE_ID = 33 AND NVL(cr_tr(i).ORDER_SHIP,0) <> 0 AND l_length < 490 THEN
                        l_ordship_comment := l_ordship_comment||'|'||TRIM(NVL(cr_tr(i).SOLD_TO_NAME,'NA'))||':'||ROUND(NVL(cr_tr(i).ORDER_SHIP,0),2);
                    END IF;
                    
                    IF cr_tr(i).TRANSACTION_TYPE_ID = 99 AND NVL(cr_tr(i).MISC_ISSUE,0) <> 0 AND l_length < 490 THEN
                        l_misciss_comment := l_misciss_comment||'|'||TRIM(NVL(cr_tr(i).GL_ACCOUNT,'NA'))||':'||ROUND(NVL(cr_tr(i).MISC_ISSUE,0),2);
                    END IF;
                    
                    l_length := LENGTH(l_intship_comment) + LENGTH(l_intrcpt_comment) + LENGTH(l_ordship_comment) + LENGTH(l_misciss_comment);
                    
                END LOOP;
                
                EXIT WHEN cur_trans%NOTFOUND OR l_length >= 490;
            END LOOP;
            
            l_calc_flow_code   := l_calc_flow_code||'-'||'010';
            
            l_reserve_comment := l_intship_comment||';'||l_intrcpt_comment||';'||l_ordship_comment||';'||l_misciss_comment;
       
            CLOSE cur_trans;
		END IF;
		
		DEBUG('l_reserve_comment: '||NVL(l_reserve_comment,cr_usage(idx).COMMENT_FLAG));
		
/******************************************************************************
* Step-9: Cacluate reserve of LITE item                                       *
******************************************************************************/
		IF l_eo_cal_flag = 'X' THEN

            IF cr_usage(idx).ORGANIZATION_ID <> 'CM' THEN   -- LITE Item
            
                IF NVL(v_usage, 'N') = 'N' THEN
                    GOTO END_CALC_USAGE;
                END IF;
/******************************************************************************
* Step-9.1: Get CM onhand of LITE item which unrelease reserve > 0            *
******************************************************************************/               
                IF l_beg_unrel_reserve >0 AND NVL(l_cm_oh,0) = 0 THEN
                
                    BEGIN
                        SELECT NVL(CM_OH,0) INTO l_cm_oh
                        FROM XXSC.XXSC_EO_DASHBOARD_TBL
                        WHERE ITEM = cr_usage(idx).INVENTORY_ITEM
                            AND LITE_ORG_CODE = cr_usage(idx).ORGANIZATION_CODE
                            AND FISCAL_YEAR = l_year
                            AND FISCAL_PERIOD = l_period
                            AND ORGANIZATION_ID = 'CM'
                            AND ROWNUM = 1;
                    EXCEPTION
                    WHEN OTHERS THEN
                        l_cm_oh := NVL(l_cm_oh,0);
                    END;

                END IF;
                
--                 DEBUG('Values: l_prev_onhand: '||l_prev_onhand||', l_beg_reserve: '||l_beg_reserve||', l_beg_unrel_reserve: '||l_beg_unrel_reserve||', l_trans_reserve: '||l_trans_reserve||', l_buyback_reserve: '||l_buyback_reserve);
/******************************************************************************
* Step-9.2: Initializing variables of reserve                                 *
******************************************************************************/ 
                l_new_eo_qty    := (NVL(l_beg_reserve,0) + NVL(l_trans_reserve,0) + NVL(l_buyback_reserve,0)) + NVL(l_eo_scrap,0) - ABS(NVL(l_beg_unrel_reserve,0));
                l_var1          := l_new_eo_qty - l_onhand;
				l_var2          := ABS(l_cons_ord_issue) - l_cm_oh;
				
				IF l_beg_reserve > l_beg_unrel_reserve THEN
                    IF l_prev_onhand - l_beg_reserve + l_beg_unrel_reserve > 0 THEN
                        l_active_inv := l_prev_onhand - l_beg_reserve + l_beg_unrel_reserve;
                    ELSE
                        l_active_inv := 0;
                    END IF;
				ELSE
                    l_active_inv := l_prev_onhand;
				END IF;
				
				IF l_po_rcv + l_po_rcv_intransit - l_prev_po_intransit > l_buyback_reserve THEN
                    l_active_inv :=  l_active_inv + l_po_rcv + l_po_rcv_intransit - l_prev_po_intransit - l_buyback_reserve;
				END IF;
				
				IF l_int_order_intransit - l_prev_int_intransit + l_int_order_rcv > l_trans_reserve THEN
                    l_active_inv :=  l_active_inv + l_int_order_intransit - l_prev_int_intransit + l_int_order_rcv - l_trans_reserve;
				END IF;
				
				IF l_wip_issue + l_wip_completion > 0 THEN
                    l_active_inv :=  l_active_inv + l_wip_issue + l_wip_completion;
				END IF;
				
				IF l_misc_issue > 0 THEN
                    l_active_inv :=  l_active_inv + l_misc_issue;
				END IF;
				
				IF l_eo_scrap > 0 THEN
                    l_active_inv :=  l_active_inv - l_eo_scrap;
				END IF;
				
				IF l_var1 >0 THEN
                    l_eo_variance := l_var1;
                ELSE
                    l_eo_variance := 0;
				END IF;
                
                l_unrel_eo_usage := l_beg_unrel_reserve;
                
                l_calc_flow_code   := l_calc_flow_code||'-'||'100';
                
        DEBUG('value(8.2): l_new_eo_qty: '||l_new_eo_qty||', l_var1:'||l_var1||', l_var2: '||l_var2||', l_active_inv: '||l_active_inv||', l_eo_variance: '||l_eo_variance);

/******************************************************************************
* Step-9.3: Check standard order ship (COGS account = 40005)                  *
******************************************************************************/                 
                IF ((l_eo_variance > 0) AND (NVL(l_so_issue,0) <> 0)) THEN

                    IF ABS(l_so_issue) > l_active_inv THEN

                        IF (ABS(l_so_issue) - l_active_inv) > l_eo_variance THEN
                            l_eo_usage_so := l_eo_variance;
                            l_eo_variance := 0;
                            l_calc_flow_code   := l_calc_flow_code||'-'||'211';
                        ELSE
                            l_eo_variance := l_eo_variance - (ABS(l_so_issue) - l_active_inv);
                            l_eo_usage_so := ABS(l_so_issue) - l_active_inv;
                            l_calc_flow_code   := l_calc_flow_code||'-'||'212'; 
                        END IF;
                        
                        l_active_inv := 0;
                    ELSE
                        l_active_inv := l_active_inv - ABS(l_so_issue);
                        l_eo_usage_so := 0;
                        l_calc_flow_code   := l_calc_flow_code||'-'||'220';
                        
                    END IF;

                ELSE
                    l_eo_usage_so := 0;
                    l_calc_flow_code := l_calc_flow_code||'-'||'230';
                END IF;

               DEBUG('Values(8.3): l_active_inv: '||l_active_inv||', l_eo_variance: '||l_eo_variance||', so_issue: '||NVL(l_so_issue,0)||', eo_usage_so: '||NVL(l_eo_usage_so,0));

/******************************************************************************
* Step-9.4: Check consignment order ship (COGS account = 40308/40124)         *
******************************************************************************/

                IF ((NVL(l_eo_variance,0) > 0) AND (NVL(l_cons_ord_issue,0) <> 0)) THEN
                
                    IF ABS(l_cons_ord_issue) > l_active_inv THEN
                                                
                        IF (ABS(l_cons_ord_issue) - l_active_inv) > l_eo_variance THEN
                            l_eo_usage_cons := l_eo_variance;
                            l_eo_variance := 0;
                            l_calc_flow_code   := l_calc_flow_code||'-'||'311';
                        ELSE
                            l_eo_variance := l_eo_variance - (ABS(l_cons_ord_issue) - l_active_inv);
                            l_eo_usage_cons := ABS(l_cons_ord_issue) - l_active_inv;
                            l_calc_flow_code   := l_calc_flow_code||'-'||'312';
                        END IF;
                        
                        l_active_inv := 0;
                    ELSE
                        l_active_inv := l_active_inv - ABS(l_cons_ord_issue);
                        l_eo_usage_cons := 0;
                        l_calc_flow_code   := l_calc_flow_code||'-'||'320';
                        
                    END IF;
                ELSE
                     l_eo_usage_cons := 0;
                     l_calc_flow_code := l_calc_flow_code||'-'||'330';
                END IF;
/******************************************************************************
* Step-9.5: Check unrelease reserve and CM onhand                             *
******************************************************************************/                
                IF (NVL(l_beg_unrel_reserve,0) + l_eo_usage_cons) > NVL(l_cm_oh,0) THEN
                    l_unrel_eo_usage := l_cm_oh;
                    
--                    IF NVL(l_beg_unrel_reserve,0)+ l_eo_usage_cons - NVL(l_cm_oh,0)> l_eo_variance THEN
--                        l_eo_variance := 0;
--                    ELSE
--                        l_eo_variance := l_eo_variance - (NVL(l_beg_unrel_reserve,0)+ l_eo_usage_cons - NVL(l_cm_oh,0));
--                    END IF;
                    l_eo_usage_cons  := NVL(l_beg_unrel_reserve,0)+ l_eo_usage_cons - NVL(l_cm_oh,0);
                    l_calc_flow_code := l_calc_flow_code||'-'||'341';
                ELSE
                    IF NVL(l_cm_oh,0) <> 0 THEN
                        l_unrel_eo_usage := NVL(l_beg_unrel_reserve,0) + l_eo_usage_cons;
                    END IF;
                    l_eo_usage_cons := 0;
                    l_calc_flow_code := l_calc_flow_code||'-'||'342';
                END IF;
             
                DEBUG('Values(8.5): l_active_inv: '||l_active_inv||', l_eo_variance: '||l_eo_variance||', cons_ord_issue: '||NVL(l_cons_ord_issue,0)||', eo_usage_cons: '||NVL(l_eo_usage_cons,0)||', l_beg_unrel_reserve: '||l_beg_unrel_reserve||', l_unrel_eo_usage: '||l_unrel_eo_usage);

/******************************************************************************
* Step-9.6: Check miscellaneous issue (GL account <> 40312)                   *
******************************************************************************/ 	
				IF ((l_eo_variance > 0) AND (l_misc_issue < 0)) THEN

                    IF ABS(l_misc_issue) > l_active_inv THEN
                        l_calc_flow_code   := l_calc_flow_code||'-'||'410';
                        IF (ABS(l_misc_issue) - l_active_inv) > l_eo_variance THEN
                            l_eo_usage_misc := l_eo_variance;
                            l_eo_variance := 0;
                            l_calc_flow_code   := l_calc_flow_code||'-'||'411';
                        ELSE
                            l_eo_variance := l_eo_variance - (ABS(l_misc_issue) - l_active_inv);
                            l_eo_usage_misc := ABS(l_misc_issue) - l_active_inv;
                            l_calc_flow_code   := l_calc_flow_code||'-'||'412'; 
                        END IF;
                        
                        l_active_inv := 0;
                    ELSE
                        l_active_inv := l_active_inv - ABS(l_misc_issue);
                        l_eo_usage_misc := 0;
                        l_calc_flow_code   := l_calc_flow_code||'-'||'420';
                        
                    END IF;

                ELSE
                    l_eo_usage_misc := 0;
                    l_calc_flow_code := l_calc_flow_code||'-'||'430';
                END IF;
                
                DEBUG('Values(8.6): l_active_inv: '||l_active_inv||', l_eo_variance: '||l_eo_variance||', misc_issue: '||NVL(l_misc_issue,0)||', eo_usage_misc: '||NVL(l_eo_usage_misc,0));

/******************************************************************************
* Step-9.7: Check int order ship                                              *
******************************************************************************/ 
				IF ((l_eo_variance > 0) AND ABS(l_iso_issue)>0) THEN

                    IF ABS(l_iso_issue) > l_active_inv THEN

                        IF (ABS(l_iso_issue) - l_active_inv) > l_eo_variance THEN
                            
                            l_eo_usage_int_ord := l_eo_variance;
                            l_eo_variance := 0;
                            l_calc_flow_code   := l_calc_flow_code||'-'||'511';
                        ELSE
                            l_eo_variance := l_eo_variance - (ABS(l_iso_issue) - l_active_inv);
                            l_eo_usage_int_ord := ABS(l_iso_issue) - l_active_inv;
                            l_calc_flow_code   := l_calc_flow_code||'-'||'512'; 
                        END IF;
                        
                        l_active_inv := 0;
                    ELSE
                        l_active_inv := l_active_inv - ABS(l_iso_issue);
                        l_eo_usage_int_ord := 0;
                        l_calc_flow_code   := l_calc_flow_code||'-'||'520';
                        
                    END IF;

                ELSE
                    l_eo_usage_int_ord := 0;
                    l_calc_flow_code := l_calc_flow_code||'-'||'530';
                END IF;
                
                IF NVL(l_eo_usage_int_ord,0) <> 0 THEN
                    l_int_rcpt_dest  := l_eo_usage_int_ord;
                    l_calc_flow_code := l_calc_flow_code||'-'||'541';
                ELSE
                    l_int_rcpt_dest  := 0;
                    l_calc_flow_code := l_calc_flow_code||'-'||'542';
                END IF;
                
                DEBUG('Values(8.7): l_active_inv: '||l_active_inv||', l_eo_variance: '||l_eo_variance||', iso_issue: '||NVL(l_iso_issue,0)||', eo_usage_int_ord: '||NVL(l_eo_usage_int_ord,0)||', int_rcpt_dest: '||NVL(l_int_rcpt_dest,0));
 
/******************************************************************************
* Step-9.8: Check WIP issue                                                   *
******************************************************************************/ 
				IF ((l_eo_variance > 0) AND (l_wip_issue + l_wip_completion)<0) THEN

                    IF ABS(l_wip_issue + l_wip_completion) > l_active_inv THEN

                        IF (ABS(l_wip_issue + l_wip_completion) - l_active_inv) > l_eo_variance THEN
                            
                            l_eo_usage_wip := l_eo_variance;
                            l_eo_variance := 0;
                            l_calc_flow_code   := l_calc_flow_code||'-'||'611';
                        ELSE
                            l_eo_variance := l_eo_variance - (ABS(l_wip_issue + l_wip_completion) - l_active_inv);
                            l_eo_usage_wip := ABS(l_wip_issue + l_wip_completion) - l_active_inv;
                            l_calc_flow_code   := l_calc_flow_code||'-'||'612'; 
                        END IF;
                        
                        l_active_inv := 0;
                    ELSE
                        l_active_inv := l_active_inv - ABS(l_wip_issue + l_wip_completion);
                        l_eo_usage_wip := 0;
                        l_calc_flow_code   := l_calc_flow_code||'-'||'620';
                        
                    END IF;

                ELSE
                    l_eo_usage_wip := 0;
                    l_calc_flow_code := l_calc_flow_code||'-'||'630';
                END IF;
                
                DEBUG('Values(8.8): l_active_inv: '||l_active_inv||', l_eo_variance: '||l_eo_variance||',wip_issue+completion: '||NVL(l_wip_issue+l_wip_completion,0)||', eo_usage_wip: '||NVL(l_eo_usage_wip,0));

               
 <<END_CALC_USAGE>>
 
/******************************************************************************
* Step-9.9: Check whether EO usage is changed by manual                      *
******************************************************************************/ 
                IF v_usage = 'Y' AND NOT l_eo_usage_chg IN ('T','A') THEN
                    l_total_eo_usage := NVL(l_eo_usage_so,0)+ NVL(l_eo_usage_cons,0) + NVL(l_eo_usage_misc,0)+NVL(l_eo_usage_int_ord,0)+NVL(l_eo_usage_wip,0);
                    l_calc_flow_code := l_calc_flow_code||'-'||'701A';
                END IF;

/******************************************************************************
* Step-9.10: Check whether unrelease reserve is changed by manual             *
******************************************************************************/ 

                IF v_usage = 'N' OR l_eo_usage_chg IN ('U','A') THEN
                    l_unrel_eo_usage := l_cur_unrel_eo_usage;
                END IF;

                DEBUG('Value of l_unrel_eo_usage after update '||l_unrel_eo_usage);
 
/******************************************************************************
* Step-9.11: Calculate ending reserve, net inventory and provision            *
******************************************************************************/ 
                l_net_reserve := (NVL(l_beg_reserve,0) + NVL(l_trans_reserve,0) + NVL(l_buyback_reserve,0)) + NVL(l_eo_scrap,0) - NVL(l_total_eo_usage,0);
                
                l_eo_usage_others := 0;
                
                IF l_net_reserve < 0 THEN
                    l_net_reserve := NVL(l_unrel_eo_usage,0);
                    l_calc_flow_code := l_calc_flow_code||'-'||'703';
                ELSIF l_net_reserve - NVL(l_unrel_eo_usage,0) > l_onhand THEN
                
                    l_eo_usage_others := l_net_reserve - NVL(l_unrel_eo_usage,0) - l_onhand;
                    l_net_reserve := l_onhand + NVL(l_unrel_eo_usage,0);   
                                     
                    l_calc_flow_code := l_calc_flow_code||'-'||'704';
                END IF;
                
                IF v_usage = 'Y' AND NOT (l_eo_usage_chg IN ('T','A')) THEN
                    l_total_eo_usage := l_total_eo_usage + + l_eo_usage_others;
                    l_calc_flow_code := l_calc_flow_code||'-'||'701B';
                END IF;

/******************************************************************************
* Step-9.12: Check whether there is reserve ship out by int order             *
******************************************************************************/ 
			    IF l_int_rcpt_dest > 0 THEN

/******************************************************************************
* Step-9.13: Extract sold to organization of int order                        *
* If there is multiple sold to organization,                                  *
*          only pick sold to organization which ship quantity is most         *
******************************************************************************/ 

                    l_calc_flow_code := l_calc_flow_code||'-'||'707';

                    DEBUG('Values(inter-comp) of l_intship_qty: '||l_intship||' l_eo_usage_int_ord: '||l_int_rcpt_dest||' l_sold_to_org - '||l_sold_to_org);					

                    IF l_int_rcpt_dest < l_intship THEN
                        l_intship := l_int_rcpt_dest;
                    END IF;
                    
                    IF NVL(l_sold_to_org,'X') <> 'X' THEN
                        l_rec_exists := 0;
                        l_rec_id := 0;
/******************************************************************************
* Step-9.14: Check whether there is EO record exists                         *
*    If yes, update existing record                                           *
*    If not exist, insert new record                                          * 
******************************************************************************/ 
						BEGIN
                            SELECT B.DASHBOARD_REC_ID
                                INTO l_rec_id
                            FROM XXSC.XXSC_EO_DASHBOARD_TBL B	
                            WHERE B.ITEM            = cr_usage(idx).INVENTORY_ITEM
                            AND B.FISCAL_YEAR       = l_year
                            AND B.FISCAL_PERIOD     = l_period
                            AND B.ORGANIZATION_ID   = l_sold_to_id;
						EXCEPTION
						      WHEN OTHERS THEN
							       l_rec_exists := 0;
							       l_rec_id := 0;
						END;

                        IF NVL(l_rec_id,0) = 0 THEN
                        
                            SELECT NAME 
                                INTO l_org_descr
                            FROM APPS.HR_ALL_ORGANIZATION_UNITS
                            WHERE ORGANIZATION_ID = l_sold_to_id;

                            INSERT INTO xxsc.XXSC_EO_DASHBOARD_TBL(DASHBOARD_REC_ID, 
                                             ITEM,
                                             ITEM_ID,
                                             ORGANIZATION_CODE,
                                             ORGANIZATION_ID,
                                             LITE_ORG_CODE,
                                             ORG_DESCR,
                                             FISCAL_YEAR,
                                             FISCAL_QTR,
                                             FISCAL_PERIOD,
                                             PERIOD_NAME,
                                             ITEM_TYPE ,
                                             L3_BUS_GROUP,
                                             L4_BUS_UNIT,
                                             L6_MODULE,
                                             GL_LINE_CODE,
                                             GL_LINE_DESCR,
                                             ITEM_DESCR,
                                             ITEM_STATUS,
                                             CM_ITEM,
                                             TRANS_EO_RESERVE_QTY,
                                             EO_CALC_FLAG,
                                             CREATION_DATE)
                            SELECT XXSC.XXSC_EO_DASHB_SEQ.NEXTVAL AS DASHBOARD_REC_ID,
                                   ITEM,
                                   ITEM_ID,
                                   l_sold_to_org,
                                   l_sold_to_id,
                                   NULL,
                                   l_org_descr,
                                   l_year,
                                   l_qtr,
                                   l_period,
                                   l_period_name,
                                   ITEM_TYPE,
                                   L3_BUS_GROUP,
                                   L4_BUS_UNIT,
                                   L6_MODULE,
                                   GL_LINE_CODE,
                                   GL_LINE_DESCR,
                                   ITEM_DESCR,
                                   ITEM_STATUS,
                                   CM_ITEM,
                                   l_intship,
                                   'X',
                                   TRUNC(SYSDATE)
                            FROM XXSC.XXSC_EO_DASHBOARD_TBL
                            WHERE DASHBOARD_REC_ID = cr_usage(idx).EO_REC_ID;

                            l_calc_flow_code := l_calc_flow_code||'-'||'708';
                            
                        ELSIF v_reserve = 'Y' THEN
                        
                            UPDATE XXSC.XXSC_EO_DASHBOARD_TBL
                               SET TRANS_EO_RESERVE_QTY = l_intship,EO_CALC_FLAG = 'X'
                             WHERE DASHBOARD_REC_ID = l_rec_id;

                            l_calc_flow_code := l_calc_flow_code||'-'||'709';
                        END IF;
                        COMMIT;
                        
                        l_transfer_flag := 'Y';
                    END IF;

				 END IF;   

/******************************************************************************
* Step-9.15: Reevaluate EO reserve value                                     *
******************************************************************************/ 

                l_eo_re_eval := NVL(l_beg_reserve,0) * (NVL(l_item_cost,0) - NVL(l_prev_item_cost,0));

            END IF; --LITE Item
            
/******************************************************************************
* Step-10: Cacluate reserve of CM item                                         *
******************************************************************************/
            IF cr_usage(idx).ORGANIZATION_ID = 'CM' THEN     -- CM Item

                l_calc_flow_code := l_calc_flow_code||'-'||'720';
                
--                l_cm_oh := l_onhand;
/******************************************************************************
* Step-10.1: Check calculation period to determine calculation items           *
* First period of quarter: Ending reserve                                     *
* First period of quarter: Ending reserve, Usage, Reevaluation                *
* First period of quarter: Ending reserve, PO reserve                         *
******************************************************************************/
                CASE
                    WHEN l_period = 3 * l_qtr - 2    -- First period of quarter
                    THEN
                       l_net_reserve := NVL(l_beg_reserve,0) + NVL(l_buyback_reserve,0) + NVL(l_eo_scrap,0) - ABS(NVL(l_total_eo_usage,0));
                       l_eo_re_eval := 0;
                         
--                       IF l_po_reserve = 0 THEN
--                            l_po_reserve := l_prev_keep_po_res;
--                       END IF;
                        
                       l_calc_flow_code := l_calc_flow_code||'-'||'721';

                    WHEN l_period = 3 * l_qtr - 1     -- Second period of quarter
                    THEN                
                        IF l_eo_usage_chg IN ('T','A') THEN 
                            l_total_eo_usage := NVL(l_total_eo_usage,0);
                            l_calc_flow_code := l_calc_flow_code||'-'||'722';
                        ELSE
                        
                            IF NVL(l_beg_reserve,0) + NVL(l_buyback_reserve,0) + NVL(l_eo_scrap,0) > NVL(l_cm_oh,0) THEN
                                l_total_eo_usage := NVL(l_beg_reserve,0) + NVL(l_buyback_reserve,0) + NVL(l_eo_scrap,0) - NVL(l_cm_oh,0);
                            ELSE
                                l_total_eo_usage := 0;
                            END IF;
                          l_calc_flow_code := l_calc_flow_code||'-'||'723';
       
                        END IF;
                        
--                        IF NVL(l_po_reserve,0) = 0 THEN
--                            l_po_reserve := l_prev_po_reserve;
--                        END IF;
                        
                        l_net_reserve := NVL(l_beg_reserve,0) + NVL(l_buyback_reserve,0) + NVL(l_eo_scrap,0) - NVL(l_total_eo_usage,0);
                        l_eo_re_eval := NVL(l_beg_reserve,0) * (NVL(l_item_cost,0) - NVL(l_prev_item_cost,0));

                    WHEN MOD(l_period, 3) = 0    -- Third period of quarter
                    THEN
                
                        l_net_reserve := NVL(l_beg_reserve,0) + NVL(l_buyback_reserve,0) + NVL(l_eo_scrap,0)- NVL(l_total_eo_usage,0);
                        
--                        l_po_reserve := 0;

                        l_eo_re_eval := 0;
                        l_calc_flow_code := l_calc_flow_code||'-'||'724';

                END CASE;
                
                IF l_net_reserve < 0 THEN 
                    l_net_reserve := 0;
                END IF;
  
            END IF;

/******************************************************************************
* Step-11: Cacluate final data(Total Reserve, Net Inv) and Provision          *
******************************************************************************/
            IF v_reserve = 'N' THEN                     -- Not calculate reserve, pick original net reserve
                l_net_reserve := l_eo_reserve;
            END IF;
            
            l_total_reserve:= l_net_reserve + l_po_reserve + l_mrb_reserve + l_rma_reserve + l_sp_reserve;
            
            IF l_onhand + l_open_po - l_total_reserve - l_srv_oh - l_loan_oh > 0 THEN
                l_net_inv := l_onhand + l_open_po - l_total_reserve - l_srv_oh - l_loan_oh;
            ELSE
                l_net_inv := 0;
            END IF; 
            
            IF v_provision = 'N' THEN
                l_total_provision   := NULL;
                l_provision_oh      := NULL;
                l_provision_po      := NULL;
                l_keep_po_res       := NULL;
                l_calc_flow_code := l_calc_flow_code||'-'||'730';
            ELSE 
                 IF l_onhand + l_po_reserve - l_total_reserve - l_srv_oh - l_loan_oh > 0 THEN
                    IF l_total_provision > l_onhand + l_po_reserve - l_total_reserve - l_srv_oh - l_loan_oh THEN
                        l_provision_oh := l_onhand + l_po_reserve - l_total_reserve - l_srv_oh - l_loan_oh;
                    ELSE
                        l_provision_oh := l_total_provision;
                    END IF;
                ELSE
                    l_provision_oh := 0;
                END IF;
                
                l_provision_po := l_total_provision - l_provision_oh;
                
                IF l_provision_po + l_po_reserve > l_open_po THEN
                    l_keep_po_res := l_open_po;
                ELSE
                    l_keep_po_res := l_provision_po + l_po_reserve;
                END IF;
                
                l_calc_flow_code := l_calc_flow_code||'-'||'731';
            END IF;
            
            l_reserve_inc_prov := l_net_reserve + NVL(l_provision_oh,0);

            DEBUG('Values(Final):l_net_inv: '||l_net_inv||', l_total_reserve: '||l_total_reserve||', total_eo_usage: '||l_total_eo_usage||', l_net_reserve: '||l_net_reserve||', provision_oh: '||l_provision_oh||', reserve_provision: '||l_reserve_inc_prov);

/******************************************************************************
* Step-12: Update calculation result to EO dashboard table                   *
******************************************************************************/
           
            l_calc_flow_code := l_calc_flow_code||'-'||'800';
                  
            UPDATE XXSC.XXSC_EO_DASHBOARD_TBL
            SET EO_REEVALUATION        = DECODE(v_reval, 'Y',l_eo_re_eval,EO_REEVALUATION),
                UNREL_EO_USAGE         = DECODE(v_usage, 'Y',l_unrel_eo_usage,UNREL_EO_USAGE),
                TOTAL_EO_USAGE         = DECODE(v_usage, 'Y',l_total_eo_usage,TOTAL_EO_USAGE),
                EO_USAGE_SO            = DECODE(v_usage, 'Y',l_eo_usage_so,EO_USAGE_SO),
                EO_USAGE_CONORD        = DECODE(v_usage, 'Y',l_eo_usage_cons,EO_USAGE_CONORD),
                EO_USAGE_MISC_ISSUE    = DECODE(v_usage, 'Y',l_eo_usage_misc,EO_USAGE_MISC_ISSUE),
                EO_USAGE_INT_ORD       = DECODE(v_usage, 'Y',l_eo_usage_int_ord,EO_USAGE_INT_ORD),
                EO_USAGE_WIP           = DECODE(v_usage, 'Y',l_eo_usage_wip,EO_USAGE_WIP),
                EO_USAGE_OTHERS        = DECODE(v_usage, 'Y',l_eo_usage_others,EO_USAGE_OTHERS),
                CUR_EO_RESERVE_QTY     = DECODE(v_reserve, 'Y',l_net_reserve,CUR_EO_RESERVE_QTY),
                EO_RESERVE_INC_PROVISION = DECODE(v_reserve, 'Y',l_reserve_inc_prov,DECODE(v_provision, 'Y', l_reserve_inc_prov,EO_RESERVE_INC_PROVISION)),
                CUR_PO_RESERVE_QTY     = DECODE(v_reserve, 'Y',NVL(l_po_reserve,CUR_PO_RESERVE_QTY),CUR_PO_RESERVE_QTY),
                BEGIN_EO_RESERVE_QTY   = DECODE(NVL(BEGIN_EO_RESERVE_QTY,0),0, l_beg_reserve, BEGIN_EO_RESERVE_QTY),            
--                NET_INV                 = DECODE(NVL(v_reserve,'N'), 'Y',l_net_inv, NET_INV),
                PROVISION_ONHAND       = DECODE(v_provision, 'Y',NVL(l_provision_oh,0), PROVISION_ONHAND),
                PROVISION_PO           = DECODE(v_provision, 'Y',NVL(l_provision_po,0), PROVISION_PO),
                KEEP_PO_RESERVE        = DECODE(v_provision, 'Y',NVL(l_keep_po_res,0), KEEP_PO_RESERVE),  
                SOLD_TO_ID              = l_sold_to_id,
                SOLD_TO_NAME            = l_sold_to_org,
                PROVISION_REF_ID        = 1
            WHERE DASHBOARD_REC_ID      = l_eo_rec_id;

            COMMIT;
            
        ELSE
            l_calc_flow_code := l_calc_flow_code||'-'||'801';
            
            l_net_inv := l_onhand;
            
             l_net_inv := NVL(l_onhand,0) + NVL(l_open_po,0) - NVL(l_srv_oh,0) - NVL(l_loan_oh,0);
            
            UPDATE XXSC.XXSC_EO_DASHBOARD_TBL
            SET  
                 CUR_PO_RESERVE_QTY         = NULL,
                 EO_RESERVE_INC_PROVISION   = NULL,
                 PROVISION_ONHAND           = NULL,
                 PROVISION_PO               = NULL,
                 KEEP_PO_RESERVE            = NULL
            WHERE DASHBOARD_REC_ID    = l_eo_rec_id;         
        
        END IF;

/******************************************************************************
* Step-13: Update general result to EO dashboard table                        *
******************************************************************************/        
        UPDATE XXSC.XXSC_EO_DASHBOARD_TBL
         SET ITEM_COST              = DECODE(v_reval, 'Y',NVL(l_item_cost, ITEM_COST), ITEM_COST),
             ITEM_COST_PRE_PERIOD   = DECODE(v_reval, 'Y',NVL(l_prev_item_cost,ITEM_COST_PRE_PERIOD),ITEM_COST_PRE_PERIOD), 
             RESERVE_COMMENTS       = DECODE(v_usage, 'Y', NVL(l_reserve_comment,RESERVE_COMMENTS),RESERVE_COMMENTS), 
             CALC_FLOW_CODE         = l_calc_flow_code,
             PERIOD_NAME            = l_period_name,
             NET_OH                 = DECODE(NVL(CMOH_REF_ID,0), 1, NET_OH, DECODE(SIGN(ROUND(l_onhand - NVL(NET_OH,0),5)), 0, NET_OH, l_onhand)),
             STD_ORD_SHIP           = l_so_issue,
             CON_ORD_SHIP           = l_cons_ord_issue,
             WIP_ISSUE              = l_wip_issue,
             WIP_COMPLETION         = l_wip_completion,
             ISO_SHIP               = l_iso_issue,
             MISC_ISSUE             = l_misc_issue,
             EANDO_SCRAP            = DECODE(NVL(EANDO_SCRAP,0.001),0.001,l_eo_scrap,EANDO_SCRAP),
             PO_RCV                 = l_po_rcv,
             PO_RCV_INTRANSIT       = l_po_rcv_intransit,
             INT_ORDER_RCV          = l_int_order_rcv,
             INT_ORDER_INTRANSIT    = l_int_order_intransit,
             CM_OH                  = DECODE(NVL(CM_OH,0),0,l_cm_oh,CM_OH),
             TOTAL_PROVISION        = DISPOSITION_QTY1,
             TOTAL_EXCEPTION        = DISPOSITION_QTY2,
--             LOAN_NET_OH            = DECODE(NVL(LOAN_NET_OH,0),0, l_loan_oh,LOAN_NET_OH),
--             SRV_NET_OH             = DECODE(NVL(SRV_NET_OH,0),0, l_srv_oh,SRV_NET_OH),
             EO_CALC_FLAG           = DECODE(l_transfer_flag, 'Y', DECODE(EO_CALC_FLAG,'X',EO_CALC_FLAG,'Y'),'Y'),
             LAST_UPDATE_DATE       = SYSDATE
        WHERE DASHBOARD_REC_ID    = l_eo_rec_id;

        DEBUG('Output Value|'||NVL(cr_usage(idx).eo_rec_id,'')||'|'
        ||NVL(cr_usage(idx).inventory_item_id,'')||'|'||cr_usage(idx).inventory_item||'|'||NVL(cr_usage(idx).ORGANIZATION_ID,'')||'|'||NVL(cr_usage(idx).organization_code,'')||'|'||NVL(l_prev_onhand,0)||'|'||NVL(l_prev_po_intransit,0)||'|'||NVL(l_prev_int_intransit,0)||'|'||NVL(l_beg_reserve,0)||'|'||NVL(l_beg_unrel_reserve,0)||'|'
        ||NVL(l_onhand,0)||'|'||NVL(l_cm_oh,0)||'|'||NVL(l_srv_oh,0)||'|'||NVL(l_loan_oh,0)||'|'||NVL(l_open_po,0)||'|'||NVL(l_po_rcv,0)||'|'||NVL(l_po_rcv_intransit,0)||'|'||NVL(l_int_order_intransit+l_int_order_rcv,0)||'|'||NVL(l_so_issue,0)||'|'||NVL(l_cons_ord_issue,0)||'|'||NVL(l_iso_issue,0)||'|'||NVL(l_wip_issue,0)||'|'
        ||NVL(l_wip_completion,0)||'|'||NVL(l_misc_issue,0)||'|'||NVL(l_eo_scrap,0)||'|'||NVL(l_others,0)||'|'||NVL(l_buyback_reserve,0)||'|'||NVL(l_trans_reserve,0)||'|'||NVL(l_po_reserve,0)||'|'||NVL(l_net_inv,0)||'|'||NVL(l_net_reserve,0)||'|'||NVL(l_reserve_inc_prov,0)||'|'||NVL(l_total_eo_usage,0)||'|'||NVL(l_unrel_eo_usage,0)||'|'
        ||NVL(l_eo_usage_so,0)||'|'||NVL(l_eo_usage_cons,0)||'|'||NVL(l_eo_usage_misc,0)||'|'||NVL(l_eo_usage_int_ord,0)||'|'||NVL(l_eo_usage_wip,0)||'|'||NVL(l_eo_usage_others,0)||'|'||NVL(l_total_reserve,0)||'|'||NVL(l_total_provision,0)||'|'||NVL(l_provision_oh,0)||'|'||NVL(l_provision_po,0)||'|'||NVL(l_keep_po_res,0)||'|'
        ||NVL(l_item_cost,0)||'|'||NVL(l_prev_item_cost,0)||'|'||NVL(l_eo_re_eval,0)||'|'||NVL(l_sold_to_org,'')||'|'||l_transfer_flag);
        
        DEBUG('Calculation Flow Code : '||l_calc_flow_code);

        END LOOP;

        COMMIT;
        
        EXIT WHEN cur_eo%NOTFOUND;
        
    END LOOP; -- End of Cursor cr_usage 
    
    CLOSE cur_eo;
/******************************************************************************
* Step-12: Determine whether there is reserve transfer to orther organizaiton *
*   If yes, recalculate EO records with int receipt reserve                  *
******************************************************************************/
    l_transfer_flag_cnt := l_transfer_flag_cnt + 1;
    
    IF (l_transfer_flag = 'Y') AND (l_transfer_flag_cnt < 5) THEN
    
        l_eo_cal_flag := 'Y';
        GOTO RECALC_USAGE;
        
    END IF;

--      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, '');
      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, '');
      FND_FILE.PUT_LINE ( FND_FILE.OUTPUT, '======================================================================================================================================================');
      FND_FILE.PUT_LINE ( FND_FILE.OUTPUT, '============================================**List of Usage Calculation Details**=====================================================================');
--      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, '');
--      FND_FILE.PUT_LINE (FND_FILE.OUTPUT, '');
      FND_FILE.PUT_LINE ( FND_FILE.OUTPUT, '======================================================================================================================================================');
--      FND_FILE.PUT_LINE ( FND_FILE.OUTPUT, '  FISCAL YEAR     FISCAL PERIOD    ORGANIZATION         ITEM                  Unrelease     EO Usage   EO Usage  EO Usage   EO Usage  Current EO ');
--      FND_FILE.PUT_LINE ( FND_FILE.OUTPUT, '------------------------------------------------------------------------------------------------------------------------------------------------------');
--      FND_FILE.PUT_LINE ( FND_FILE.OUTPUT, '------------------------------------------------------------------------------------------------------------------------------------------------------');

EXCEPTION 
WHEN OTHERS THEN 
  FND_FILE.PUT_LINE(FND_FILE.LOG,'The Program got error out and the error message is :  '||sqlerrm);
    g_error_message := 'The Program got error out and the error message is :  '||sqlerrm|| g_error_message ;
    x_retcode := 2; 
    x_errbuf := g_error_message;
    
    IF prev_eo%ISOPEN THEN
        CLOSE prev_eo;
    END IF;
    
    IF cur_eo%ISOPEN THEN
        CLOSE cur_eo;
    END IF;
    
    IF cur_trans%ISOPEN THEN
        CLOSE cur_trans;
    END IF;
    
END CALC_EO_USAGE;


END XXSC_EO_USAGECALC_PKG;