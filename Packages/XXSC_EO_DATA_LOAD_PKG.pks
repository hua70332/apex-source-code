CREATE OR REPLACE PACKAGE apps.xxsc_eo_data_load_pkg AS

--====================================================================================
-- $Header: XXSC_EO_DATA_LOAD_PKG.sql, v1.1, 03-JAN-2021
--------------------------------------------------------------------------------
--
-- File Name:   XXSC_EO_DATA_LOAD_PKG.sql
--address
-- Description: This package is to get data loaded in staging from APEX for Decision, Reserve, 
--				 CM and CM Usage validate the data and process it to dashborad table. 
--
--
-- History:

-- Version  Date           Modified By                Comments
-- -------  -----------    -------------            -----------------------------------------------------------------------------
--     1.0  03-JAN-2021    Ganesh Raghavan           Original version


TYPE eo_status_rec IS RECORD ( 
	 record_id NUMBER,
	 status VARCHAR2(40),
	 error_msg	VARCHAR2(2000));

TYPE eo_status_tbl IS TABLE OF eo_status_rec
INDEX BY BINARY_INTEGER;

eo_status eo_status_tbl;

TYPE eo_cm_data_rec IS RECORD ( 
	dashboard_rec_id NUMBER,
	fiscal_year	VARCHAR2(20)	,
	fiscal_qtr	VARCHAR2(20)	,
	fiscal_period	VARCHAR2(20)	,
	organization_code	VARCHAR2(20)	,
	item	VARCHAR2(80)	,
	lite_org_code	VARCHAR2(20)	,
	cm_item	VARCHAR2(40)	,
	item_cost	NUMBER(15,5)	,
	net_oh	NUMBER(15,5)	,
	open_po	NUMBER(15,5)	,
	gross_demand_12mo	NUMBER(15,5));

TYPE eo_cm_data_tbl IS TABLE OF eo_cm_data_rec
INDEX BY BINARY_INTEGER;

eo_cm_data eo_cm_data_tbl;

PROCEDURE decision_data(p_decision_batch_id IN NUMBER,
						p_user_id IN NUMBER);

PROCEDURE reserve_data(p_reserve_batch_id IN NUMBER,
						p_user_id IN NUMBER);
						
PROCEDURE cm_data(p_cm_batch_id IN NUMBER,
				  p_user_id IN NUMBER);
				  
PROCEDURE cm_data_aggrigate(p_cm_batch_id IN NUMBER,
				  p_user_id IN NUMBER);

END xxsc_eo_data_load_pkg;
/