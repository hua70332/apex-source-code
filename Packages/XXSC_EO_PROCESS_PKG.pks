CREATE OR REPLACE PACKAGE apps.xxsc_eo_process_pkg AS

--====================================================================================
-- $Header: xxsc_eo_process_pkg.sql, v1.1, 22-NOV-2021
--------------------------------------------------------------------------------
--
-- File Name:   XXSC_EO_PROCESS_PKG.sql
--address
-- Description: This package is to insert or update data in xxsc_eo_dashboard_tbl table
--
--
-- History:

-- Version  Date           Modified By                Comments
-- -------  -----------    -------------            -----------------------------------------------------------------------------
--     1.0  14-Dec-2021    Ganesh Raghavan           Original version

TYPE eo_review_rec IS RECORD (
	 dashboard_rec_id xxsc.xxsc_eo_dashboard_tbl.dashboard_rec_id%TYPE,
	 disposition_qty1 xxsc.xxsc_eo_dashboard_tbl.disposition_qty1%TYPE,
	 disposition_code1 xxsc.xxsc_eo_dashboard_tbl.disposition_code1%TYPE,
	 recomm_action1 xxsc.xxsc_eo_dashboard_tbl.recomm_action1%TYPE,
	 comments_1 xxsc.xxsc_eo_dashboard_tbl.comments_1%TYPE,
	 disposition_qty2 xxsc.xxsc_eo_dashboard_tbl.disposition_qty2%TYPE,
	 disposition_code2 xxsc.xxsc_eo_dashboard_tbl.disposition_code2%TYPE,
	 recomm_action2 xxsc.xxsc_eo_dashboard_tbl.recomm_action2%TYPE,
	 comments_2 xxsc.xxsc_eo_dashboard_tbl.comments_2%TYPE,
	 completion_dt xxsc.xxsc_eo_dashboard_tbl.completion_dt%TYPE,
	 action_comment xxsc.xxsc_eo_dashboard_tbl.action_comment%TYPE,
	 action_owner xxsc.xxsc_eo_dashboard_tbl.action_owner%TYPE,
	 review_comment xxsc.xxsc_eo_dashboard_tbl.review_comment%TYPE,
	 planner_approved xxsc.xxsc_eo_dashboard_tbl.planner_approved%TYPE,
	 planner_approved_by xxsc.xxsc_eo_dashboard_tbl.planner_approved_by%TYPE,
	 planner_approved_dt xxsc.xxsc_eo_dashboard_tbl.planner_approved_dt%TYPE,
	 plm_approved xxsc.xxsc_eo_dashboard_tbl.plm_approved%TYPE,
	 plm_approved_by xxsc.xxsc_eo_dashboard_tbl.plm_approved_by%TYPE,
	 plm_approved_dt xxsc.xxsc_eo_dashboard_tbl.plm_approved_dt%type
	 );
	 

TYPE eo_review_tbl IS TABLE OF eo_review_rec
INDEX BY BINARY_INTEGER;
    
    eo_review eo_review_tbl;
	
TYPE eo_parameter_rec IS RECORD (
	 organization_code xxsc.xxsc_eo_dashboard_tbl.organization_code%TYPE,
	 item xxsc.xxsc_eo_dashboard_tbl.item%TYPE,
	 l3_bus_group xxsc.xxsc_eo_dashboard_tbl.l3_bus_group%TYPE,
	 l4_bus_unit xxsc.xxsc_eo_dashboard_tbl.l4_bus_unit%TYPE,
	 l6_module xxsc.xxsc_eo_dashboard_tbl.l6_module%TYPE,
	 gl_line_descr xxsc.xxsc_eo_dashboard_tbl.gl_line_descr%TYPE,
	 planner_name xxsc.xxsc_eo_dashboard_tbl.planner_name%TYPE,
	 buyer_name xxsc.xxsc_eo_dashboard_tbl.buyer_name%TYPE,
	 plm_name xxsc.xxsc_eo_dashboard_tbl.plm_name%TYPE,
	 lite_cm_item VARCHAR2(10),
	 period VARCHAR2(10),
	 fiscal_year xxsc.xxsc_eo_dashboard_tbl.fiscal_year%TYPE,
	 fiscal_qtr xxsc.xxsc_eo_dashboard_tbl.fiscal_qtr%TYPE,
	 week VARCHAR2(10),
	 fiscal_period xxsc.xxsc_eo_dashboard_tbl.fiscal_period%TYPE, 
	 item_with_reserve VARCHAR2(100),
	 eo_usage VARCHAR2(10),
	 reevaluation VARCHAR2(10),
	 eo_reserve VARCHAR2(10),
	 provision VARCHAR2(10)
	 );
	 

TYPE eo_parameter_tbl IS TABLE OF eo_parameter_rec
INDEX BY BINARY_INTEGER;
    
    eo_parameter eo_parameter_tbl;

TYPE eo_error_rec IS RECORD (
	fiscal_year xxsc.xxsc_eo_dashboard_tbl.fiscal_year%TYPE,
	fiscal_qtr xxsc.xxsc_eo_dashboard_tbl.fiscal_qtr%TYPE,
	fiscal_period xxsc.xxsc_eo_dashboard_tbl.fiscal_period%TYPE,
	organization_code xxsc.xxsc_eo_dashboard_tbl.organization_code%TYPE,
	item xxsc.xxsc_eo_dashboard_tbl.item%TYPE,
	error_msg VARCHAR2(2000)
	);

TYPE eo_error_tbl IS TABLE OF eo_error_rec
INDEX BY BINARY_INTEGER;
    
    eo_error eo_error_tbl;
	
TYPE eo_subledger_rec IS RECORD (
	 dashboard_rec_id xxsc.xxsc_eo_dashboard_tbl.dashboard_rec_id%TYPE,
	 cm_oh xxsc.xxsc_eo_dashboard_tbl.cm_oh%TYPE,
	 buyback_open_po xxsc.xxsc_eo_dashboard_tbl.buyback_open_po%TYPE,
	 buyback_qty xxsc.xxsc_eo_dashboard_tbl.buyback_qty%TYPE,
	 cur_mrb_reserve_qty xxsc.xxsc_eo_dashboard_tbl.cur_mrb_reserve_qty%TYPE,
	 cur_rma_reserve_qty xxsc.xxsc_eo_dashboard_tbl.cur_rma_reserve_qty%TYPE,
	 cur_sp_reserve_qty xxsc.xxsc_eo_dashboard_tbl.cur_sp_reserve_qty%TYPE,
	 cur_po_reserve_qty xxsc.xxsc_eo_dashboard_tbl.cur_po_reserve_qty%TYPE,
	 begin_eo_reserve_qty xxsc.xxsc_eo_dashboard_tbl.begin_eo_reserve_qty%TYPE,
	 begin_eo_qty_gaap xxsc.xxsc_eo_dashboard_tbl.begin_eo_qty_gaap%TYPE,
	 gaapcode_begin xxsc.xxsc_eo_dashboard_tbl.gaapcode_begin%TYPE,
	 buyback_eo_qty xxsc.xxsc_eo_dashboard_tbl.buyback_eo_qty%TYPE,
	 trans_eo_reserve_qty xxsc.xxsc_eo_dashboard_tbl.trans_eo_reserve_qty%TYPE,
	 cur_eo_reserve_qty xxsc.xxsc_eo_dashboard_tbl.cur_eo_reserve_qty%TYPE,
	 eando_scrap xxsc.xxsc_eo_dashboard_tbl.eando_scrap%TYPE,
	 total_eo_usage xxsc.xxsc_eo_dashboard_tbl.total_eo_usage%TYPE,
	 unrel_eo_usage xxsc.xxsc_eo_dashboard_tbl.unrel_eo_usage%TYPE,
	 gaap_code xxsc.xxsc_eo_dashboard_tbl.gaap_code%TYPE,
	 provision_onhand xxsc.xxsc_eo_dashboard_tbl.provision_onhand%TYPE,
	 provision_po xxsc.xxsc_eo_dashboard_tbl.provision_po%TYPE,
	 keep_po_reserve xxsc.xxsc_eo_dashboard_tbl.keep_po_reserve%TYPE
	 );
	 

TYPE eo_subledger_tbl IS TABLE OF eo_subledger_rec
INDEX BY BINARY_INTEGER;
    
    eo_subledger eo_subledger_tbl;
	
TYPE eo_ww_total_rec IS RECORD ( 
	 fiscal_year xxsc.xxsc_eo_dashboard_tbl.fiscal_year%TYPE,
	 fiscal_period xxsc.xxsc_eo_dashboard_tbl.fiscal_period%TYPE,
	 item xxsc.xxsc_eo_dashboard_tbl.item%TYPE,
	 ww_total_reserve	xxsc.xxsc_eo_dashboard_tbl.ww_total_reserve%TYPE);
	 
TYPE eo_ww_total_tbl IS TABLE OF eo_ww_total_rec
INDEX BY BINARY_INTEGER;
    
    eo_ww_total eo_ww_total_tbl;
	
TYPE eo_dashboard_rec IS RECORD ( 	
	dashboard_rec_id	xxsc.xxsc_eo_dashboard_tbl.dashboard_rec_id%TYPE,
	fiscal_year	xxsc.xxsc_eo_dashboard_tbl.fiscal_year%TYPE,
	fiscal_qtr	xxsc.xxsc_eo_dashboard_tbl.fiscal_qtr%TYPE,
	fiscal_period	xxsc.xxsc_eo_dashboard_tbl.fiscal_period%TYPE,
	organization_code	xxsc.xxsc_eo_dashboard_tbl.organization_code%TYPE,
	organization_id		xxsc.xxsc_eo_dashboard_tbl.organization_code%TYPE,
	item	xxsc.xxsc_eo_dashboard_tbl.item%TYPE,
	frozen_flag	xxsc.xxsc_eo_dashboard_tbl.frozen_flag%TYPE,
	eo_review_flag	xxsc.xxsc_eo_dashboard_tbl.eo_review_flag%TYPE,
	net_oh xxsc.xxsc_eo_dashboard_tbl.net_oh%TYPE,
	cm_oh xxsc.xxsc_eo_dashboard_tbl.cm_oh%TYPE,
	buyback_eo_qty xxsc.xxsc_eo_dashboard_tbl.buyback_eo_qty%TYPE,
	cur_eo_reserve_qty xxsc.xxsc_eo_dashboard_tbl.cur_eo_reserve_qty%TYPE,
	cur_po_reserve_qty	xxsc.xxsc_eo_dashboard_tbl.cur_po_reserve_qty%TYPE,
	total_res xxsc.xxsc_eo_dashboard_tbl.net_inv%TYPE,
	open_po xxsc.xxsc_eo_dashboard_tbl.open_po%TYPE,
	srv_net_oh xxsc.xxsc_eo_dashboard_tbl.srv_net_oh%TYPE,
	loan_net_oh xxsc.xxsc_eo_dashboard_tbl.loan_net_oh%TYPE,
	usage_6mo xxsc.xxsc_eo_dashboard_tbl.usage_6mo%TYPE,
	gross_demand_12mo xxsc.xxsc_eo_dashboard_tbl.gross_demand_12mo%TYPE,
	cmoh_ref_id xxsc.xxsc_eo_dashboard_tbl.cmoh_ref_id%TYPE,
	provision_ref_id xxsc.xxsc_eo_dashboard_tbl.provision_ref_id%TYPE,
	item_cost	xxsc.xxsc_eo_dashboard_tbl.item_cost%TYPE,
	rec_id_ww05	xxsc.xxsc_eo_dashboard_tbl.dashboard_rec_id%TYPE,
	last_eo_exception	xxsc.xxsc_eo_dashboard_tbl.last_eo_exception%TYPE,
	last_eo_exc_reason	xxsc.xxsc_eo_dashboard_tbl.last_eo_exc_reason%TYPE,
	last_eo_provision	xxsc.xxsc_eo_dashboard_tbl.last_eo_provision%TYPE,
	last_eo_prov_reason	xxsc.xxsc_eo_dashboard_tbl.last_eo_prov_reason%TYPE,
	last_excess_usage	xxsc.xxsc_eo_dashboard_tbl.last_excess_usage%TYPE,
	last_excess_demand	xxsc.xxsc_eo_dashboard_tbl.last_excess_demand%TYPE,
	last_comment	xxsc.xxsc_eo_dashboard_tbl.last_comment%TYPE,
	last_action_exception	xxsc.xxsc_eo_dashboard_tbl.last_action_exception%TYPE,
	last_action_provision	xxsc.xxsc_eo_dashboard_tbl.last_action_provision%TYPE,
	last_action	xxsc.xxsc_eo_dashboard_tbl.last_action%TYPE,
	last_sce_approved_by	xxsc.xxsc_eo_dashboard_tbl.last_sce_approved_by%TYPE,
	ep_type1	xxsc.xxsc_eo_dashboard_tbl.ep_type1%TYPE,
	disposition_code1	xxsc.xxsc_eo_dashboard_tbl.disposition_code1%TYPE,
	disposition_qty1	xxsc.xxsc_eo_dashboard_tbl.disposition_qty1%TYPE,
	recomm_action1	xxsc.xxsc_eo_dashboard_tbl.recomm_action1%TYPE,
	comments_1	xxsc.xxsc_eo_dashboard_tbl.comments_1%TYPE,
	ep_type2	xxsc.xxsc_eo_dashboard_tbl.ep_type2%TYPE,
	disposition_code2	xxsc.xxsc_eo_dashboard_tbl.disposition_code2%TYPE,
	disposition_qty2	xxsc.xxsc_eo_dashboard_tbl.disposition_qty2%TYPE,
	recomm_action2	xxsc.xxsc_eo_dashboard_tbl.recomm_action2%TYPE,
	comments_2	xxsc.xxsc_eo_dashboard_tbl.comments_2%TYPE,
	ep_type3	xxsc.xxsc_eo_dashboard_tbl.ep_type3%TYPE,
	disposition_code3	xxsc.xxsc_eo_dashboard_tbl.disposition_code3%TYPE,
	disposition_qty3	xxsc.xxsc_eo_dashboard_tbl.disposition_qty3%TYPE,
	recomm_action3	xxsc.xxsc_eo_dashboard_tbl.recomm_action3%TYPE,
	comments_3	xxsc.xxsc_eo_dashboard_tbl.comments_3%TYPE,
	not_review	xxsc.xxsc_eo_dashboard_tbl.not_review%TYPE,
	total_exception	xxsc.xxsc_eo_dashboard_tbl.total_exception%TYPE,
	total_provision	xxsc.xxsc_eo_dashboard_tbl.total_provision%TYPE,
	action_comment	xxsc.xxsc_eo_dashboard_tbl.action_comment%TYPE,
	completion_dt	xxsc.xxsc_eo_dashboard_tbl.completion_dt%TYPE,
	sys_recomm	xxsc.xxsc_eo_dashboard_tbl.sys_recomm%TYPE,
	sys_reasons	xxsc.xxsc_eo_dashboard_tbl.sys_reasons%TYPE,
	sys_comments	xxsc.xxsc_eo_dashboard_tbl.sys_comments%TYPE,
	review_comment	xxsc.xxsc_eo_dashboard_tbl.review_comment%TYPE,
	action_owner	xxsc.xxsc_eo_dashboard_tbl.action_owner%TYPE,
	planner_approved	xxsc.xxsc_eo_dashboard_tbl.planner_approved%TYPE,
	planner_approved_by	xxsc.xxsc_eo_dashboard_tbl.planner_approved_by%TYPE,
	planner_approved_dt	xxsc.xxsc_eo_dashboard_tbl.planner_approved_dt%TYPE,
	plm_approved	xxsc.xxsc_eo_dashboard_tbl.plm_approved%TYPE,
	plm_approved_by	xxsc.xxsc_eo_dashboard_tbl.plm_approved_by%TYPE,
	net_excess_usage    	xxsc.xxsc_eo_dashboard_tbl.net_excess_usage    %TYPE,
	net_excess_demand 	xxsc.xxsc_eo_dashboard_tbl.net_excess_demand %TYPE,
	ww_net_inv  xxsc.xxsc_eo_dashboard_tbl.ww_net_inv%TYPE,
	ww_total_reserve xxsc.xxsc_eo_dashboard_tbl.ww_total_reserve%TYPE,
	net_inv  xxsc.xxsc_eo_dashboard_tbl.net_inv%TYPE,
	cal_excess  xxsc.xxsc_eo_dashboard_tbl.net_excess_usage%TYPE,
	total_excess_demand xxsc.xxsc_eo_dashboard_tbl.total_excess_demand%TYPE
	);

TYPE eo_dashboard_tbl IS TABLE OF eo_dashboard_rec
INDEX BY BINARY_INTEGER;

eo_dashboard eo_dashboard_tbl;
	
TYPE eo_dashboard_id_rec IS RECORD ( 
	 dashboard_rec_id xxsc.xxsc_eo_dashboard_tbl.dashboard_rec_id%TYPE,
	 fiscal_year xxsc.xxsc_eo_dashboard_tbl.fiscal_year%TYPE,
	 fiscal_qtr xxsc.xxsc_eo_dashboard_tbl.fiscal_qtr%TYPE,
	 fiscal_period xxsc.xxsc_eo_dashboard_tbl.fiscal_period%TYPE,
	 organization_code xxsc.xxsc_eo_dashboard_tbl.organization_code%TYPE,
	 item xxsc.xxsc_eo_dashboard_tbl.item%TYPE,
	 frozen_flag xxsc.xxsc_eo_dashboard_tbl.frozen_flag%TYPE,
	 eo_review_flag xxsc.xxsc_eo_dashboard_tbl.eo_review_flag%TYPE,
	 rec_id_ww05	xxsc.xxsc_eo_dashboard_tbl.dashboard_rec_id%TYPE);

TYPE eo_dashboard_id_tbl IS TABLE OF eo_dashboard_id_rec
INDEX BY BINARY_INTEGER;

eo_dashboard_id eo_dashboard_id_tbl;
	 
	
PROCEDURE eo_review_activate(p_eo_parameter IN eo_parameter_tbl := eo_parameter,
							 p_user_id		IN NUMBER,
							 p_rec_count	OUT NUMBER,
							 p_eo_error		OUT  eo_error_tbl );

PROCEDURE eo_cancel_review_activate(p_eo_parameter IN eo_parameter_tbl := eo_parameter,
							 p_user_id		IN NUMBER,
							 p_rec_count	OUT NUMBER,
							 p_eo_error		OUT  eo_error_tbl );

PROCEDURE update_eo_review(p_eo_review IN eo_review_tbl := eo_review,
							 p_user_id		IN NUMBER);

PROCEDURE eo_confrim_reserve(p_eo_parameter IN eo_parameter_tbl := eo_parameter,
							 p_user_id		IN NUMBER,
							 p_rec_count	OUT NUMBER,
							 p_eo_error		OUT  eo_error_tbl );

PROCEDURE eo_usage_calculation(p_eo_parameter IN eo_parameter_tbl := eo_parameter,
							 p_user_id		IN NUMBER,
							 p_request_id 	OUT NUMBER);
							 
PROCEDURE update_eo_subledger(p_eo_subledger IN eo_subledger_tbl := eo_subledger,
							 p_user_id		IN NUMBER,
							 p_error_flag	OUT VARCHAR2,
							 p_error_msg	OUT VARCHAR2);

PROCEDURE clean_cm_data(p_user_id IN NUMBER,
						p_error_flag OUT VARCHAR2,
						p_error_msg	OUT VARCHAR2);

END xxsc_eo_process_pkg;
/