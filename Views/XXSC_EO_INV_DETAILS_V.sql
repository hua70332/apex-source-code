
  CREATE OR REPLACE FORCE VIEW "XXSC"."XXSC_EO_INV_DETAILS_V" ("TRANSACTION_ID", "ORGANIZATION_CODE", "INVENTORY_ITEM", "DESCRIPTION", "TRANSACTION_DATE", "FISCAL_YEAR", "FISCAL_PERIOD", "INVENTORY_ITEM_ID", "ORGANIZATION_ID", "SUBINVENTORY_CODE", "TRANSACTION_QUANTITY", "TRANSACTION_UOM", "PRIMARY_QUANTITY", "PRIMARY_UOM", "TRANSACTION_TYPE_ID", "TRANSACTION_TYPE_NAME", "TRANSACTION_SOURCE_TYPE_ID", "SRC_NUMBER", "TRANSACTION_SOURCE_ID", "GL_ACCOUNT", "SOLD_TO_ID", "SOLD_TO_NAME", "RECEIVE_FROM_ID", "RECEIVE_FROM_NAME", "PO_RECEIVED", "STD_ORDER_SHIP", "CON_ORDER_SHIP", "WIP_ISSUE", "WIP_COMPLETION", "INT_SHIP", "INT_RCV", "MISC_ISSUE", "EO_SCRAP", "EO_USAGE", "CREATION_DATE", "LAST_UPDATE_DATE") AS 
  SELECT mmt.TRANSACTION_ID
         ,param.ORGANIZATION_CODE
         ,(SELECT item.SEGMENT1
			FROM APPS.MTL_SYSTEM_ITEMS_B item
			WHERE item.INVENTORY_ITEM_ID = mmt.INVENTORY_ITEM_ID
				AND item.ORGANIZATION_ID = mmt.ORGANIZATION_ID) INVENTORY_ITEM
         ,(SELECT item.DESCRIPTION
			FROM APPS.MTL_SYSTEM_ITEMS_B item
			WHERE item.INVENTORY_ITEM_ID = mmt.INVENTORY_ITEM_ID
				AND item.ORGANIZATION_ID = mmt.ORGANIZATION_ID) DESCRIPTION
         ,mmt.TRANSACTION_DATE
         ,oap.PERIOD_YEAR AS FISCAL_YEAR
         ,oap.PERIOD_NUM AS FISCAL_PERIOD
         ,mmt.INVENTORY_ITEM_ID
         ,mmt.ORGANIZATION_ID
         ,mmt.SUBINVENTORY_CODE
         ,mmt.TRANSACTION_QUANTITY
         ,mmt.TRANSACTION_UOM
         ,mmt.PRIMARY_QUANTITY
         ,(SELECT item.PRIMARY_UOM_CODE
			FROM APPS.MTL_SYSTEM_ITEMS_B item
			WHERE item.INVENTORY_ITEM_ID = mmt.INVENTORY_ITEM_ID
				AND item.ORGANIZATION_ID = mmt.ORGANIZATION_ID) AS PRIMARY_UOM
         ,mtt.TRANSACTION_TYPE_ID
         ,mtt.TRANSACTION_TYPE_NAME
         ,mmt.TRANSACTION_SOURCE_TYPE_ID
         ,(CASE
              WHEN mmt.TRANSACTION_SOURCE_TYPE_ID = 1
              THEN
                 (SELECT 'PO_NUMBER: ' || poh.SEGMENT1
                    FROM apps.PO_HEADERS_ALL poh
                   WHERE poh.PO_HEADER_ID = mmt.TRANSACTION_SOURCE_ID)
              WHEN mmt.TRANSACTION_SOURCE_TYPE_ID = 2
              THEN
                 (SELECT 'SO_NUMBER: ' || SEGMENT1
                    FROM apps.MTL_SALES_ORDERS
                   WHERE SALES_ORDER_ID = mmt.TRANSACTION_SOURCE_ID)
              WHEN mmt.TRANSACTION_SOURCE_TYPE_ID = 4
              THEN
                 (SELECT 'MO_NUMBER: ' || REQUEST_NUMBER
                    FROM apps.MTL_TXN_REQUEST_HEADERS
                   WHERE REQUEST_NUMBER =
                            TO_CHAR (mmt.TRANSACTION_SOURCE_ID))
              WHEN mmt.TRANSACTION_SOURCE_TYPE_ID = 5
              THEN
                 (SELECT 'WIP_NUMBER: ' || WIP_ENTITY_NAME
                    FROM apps.WIP_ENTITIES
                   WHERE WIP_ENTITY_ID = mmt.TRANSACTION_SOURCE_ID)
              WHEN mmt.TRANSACTION_SOURCE_TYPE_ID = 7
              THEN
                 (SELECT 'INT_REQ_NUMBER: ' || prh.SEGMENT1
                    FROM PO.PO_REQUISITION_HEADERS_ALL prh
                   WHERE prh.REQUISITION_HEADER_ID =
                            mmt.TRANSACTION_SOURCE_ID)
              WHEN mmt.TRANSACTION_SOURCE_TYPE_ID = 8
              THEN
                 (SELECT 'INT_SO_NUMBER: ' || SEGMENT1
                    FROM apps.MTL_SALES_ORDERS
                   WHERE SALES_ORDER_ID = mmt.TRANSACTION_SOURCE_ID)
              WHEN mmt.TRANSACTION_SOURCE_TYPE_ID = 10
              THEN
                 (SELECT 'PHY_NAME: ' || PHYSICAL_INVENTORY_NAME
                    FROM apps.MTL_PHYSICAL_INVENTORIES
                   WHERE PHYSICAL_INVENTORY_ID = mmt.TRANSACTION_SOURCE_ID)
              WHEN mmt.TRANSACTION_SOURCE_TYPE_ID = 6
              THEN
                 (SELECT SEGMENT1
                    FROM apps.MTL_GENERIC_DISPOSITIONS
                   WHERE DISPOSITION_ID = mmt.TRANSACTION_SOURCE_ID)
              WHEN mmt.TRANSACTION_SOURCE_TYPE_ID = 13
              THEN
                 (SELECT 'WIP_NUMBER: ' || WIP_ENTITY_NAME
                    FROM apps.WIP_ENTITIES
                   WHERE WIP_ENTITY_ID = mmt.TRANSACTION_SOURCE_ID)
              ELSE
                 NULL
           END)
             SRC_NUMBER
         ,mmt.TRANSACTION_SOURCE_ID
         ,(CASE
            WHEN mmt.TRANSACTION_TYPE_ID = 33
            THEN
                (SELECT SEGMENT3 FROM GL.GL_CODE_COMBINATIONS
                WHERE CODE_COMBINATION_ID = mmt.DISTRIBUTION_ACCOUNT_ID)
            WHEN mmt.TRANSACTION_TYPE_ID IN (1,4,8,31,32,40,41,42)
            THEN
                (SELECT DISTINCT ref_acc.SEGMENT3
                 FROM INV.MTL_TRANSACTION_ACCOUNTS mta, 
                      GL.GL_CODE_COMBINATIONS ref_acc
                  WHERE mta.TRANSACTION_ID = mmt.TRANSACTION_ID 
                  AND mta.REFERENCE_ACCOUNT = ref_acc.CODE_COMBINATION_ID
                  AND mta.ACCOUNTING_LINE_TYPE = 2)
            ELSE
                NULL
           END) AS GL_ACCOUNT
         ,(CASE
              WHEN mmt.TRANSACTION_TYPE_ID = 62
              THEN
                 (mmt.TRANSFER_ORGANIZATION_ID)
              WHEN mmt.TRANSACTION_TYPE_ID = 33
              THEN
                 (SELECT SOLD_TO_ORG_ID
                    FROM APPS.OE_ORDER_LINES_ALL
                   WHERE LINE_ID = mmt.SOURCE_LINE_ID)
              ELSE
                 0
           END) AS SOLD_TO_ID,
          (CASE
              WHEN mmt.TRANSACTION_TYPE_ID = 62
              THEN
                 (SELECT ORGANIZATION_CODE
                    FROM INV.MTL_PARAMETERS
                   WHERE ORGANIZATION_ID = mmt.TRANSFER_ORGANIZATION_ID)
              WHEN mmt.TRANSACTION_TYPE_ID = 33
              THEN
                 (SELECT DISTINCT hp.PARTY_NAME
                    FROM APPS.OE_ORDER_LINES_ALL sol
                        ,ar.hz_cust_accounts hca
                        ,apps.hz_parties hp
                   WHERE sol.LINE_ID = mmt.SOURCE_LINE_ID
                     AND hp.party_id = hca.party_id
                     AND sol.SOLD_TO_ORG_ID = hca.CUST_ACCOUNT_ID)
              ELSE
                 NULL
           END) AS SOLD_TO_NAME
         , (CASE
              WHEN mmt.TRANSACTION_TYPE_ID = 61
              THEN
                 TO_CHAR(mmt.TRANSFER_ORGANIZATION_ID)
              WHEN mmt.TRANSACTION_TYPE_ID IN (18,36,71)
              THEN
                 (SELECT DISTINCT ass.SEGMENT1
                    FROM apps.PO_HEADERS_ALL poh, apps.AP_SUPPLIERS ass
                    WHERE poh.PO_HEADER_ID = mmt.TRANSACTION_SOURCE_ID
                    AND ass.VENDOR_ID = poh.VENDOR_ID)
              ELSE
                 NULL
           END) AS RECEIVE_FROM_ID
         , (CASE
              WHEN mmt.TRANSACTION_TYPE_ID = 61
              THEN
                 (SELECT ORGANIZATION_CODE
                    FROM INV.MTL_PARAMETERS
                   WHERE ORGANIZATION_ID = mmt.TRANSFER_ORGANIZATION_ID)
              WHEN mmt.TRANSACTION_TYPE_ID IN (18,36,71)
              THEN
                 (SELECT DISTINCT ass.VENDOR_NAME
                    FROM apps.PO_HEADERS_ALL poh, apps.AP_SUPPLIERS ass
                    WHERE poh.PO_HEADER_ID = mmt.TRANSACTION_SOURCE_ID
                    AND ass.VENDOR_ID = poh.VENDOR_ID)
              ELSE
                 NULL
           END) AS RECEIVE_FROM_NAME
         ,(CASE WHEN mmt.TRANSACTION_TYPE_ID IN (18,36,71)
                THEN
                    mmt.EO_QUANTITY
                ELSE
                    0
            END) AS PO_RECEIVED
          ,(CASE WHEN mmt.TRANSACTION_TYPE_ID = 33 
                    AND EXISTS(SELECT 'X' FROM GL.GL_CODE_COMBINATIONS 
                                WHERE (SEGMENT3 ='40005' OR SEGMENT3 = '14332')
                                AND CODE_COMBINATION_ID = mmt.DISTRIBUTION_ACCOUNT_ID)
                THEN
                    mmt.EO_QUANTITY
                ELSE
                    0
            END) AS STD_ORDER_SHIP
         ,(CASE
                WHEN mmt.TRANSACTION_TYPE_ID = 33
                    AND EXISTS(SELECT 'X' FROM GL.GL_CODE_COMBINATIONS 
                                WHERE (SEGMENT3 ='40124' OR SEGMENT3 = '40308')
                                AND CODE_COMBINATION_ID = mmt.DISTRIBUTION_ACCOUNT_ID)
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END)  AS CON_ORDER_SHIP
        , (CASE WHEN mmt.TRANSACTION_TYPE_ID IN (35,43, 38, 48)
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END) AS WIP_ISSUE
         , (CASE WHEN mmt.TRANSACTION_TYPE_ID IN (44,17,90)
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END) AS WIP_COMPLETION
        , (CASE
                WHEN mmt.TRANSACTION_TYPE_ID = 62
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END) AS INT_SHIP
        , (CASE
                WHEN mmt.TRANSACTION_TYPE_ID = 61
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END) AS INT_RCV
        , (CASE
                WHEN mmt.TRANSACTION_TYPE_ID IN (1,4,8,31,32,40,41,42) 
                     AND NOT EXISTS(SELECT 'X' FROM INV.MTL_TRANSACTION_ACCOUNTS mta, 
                                                    GL.GL_CODE_COMBINATIONS ref_acc
                                      WHERE mta.TRANSACTION_ID = mmt.TRANSACTION_ID 
                                      AND mta.REFERENCE_ACCOUNT = ref_acc.CODE_COMBINATION_ID
                                      AND mta.ACCOUNTING_LINE_TYPE = 2 
                                      AND ref_acc.SEGMENT3 = '40312' )
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END) AS MISC_ISSUE
        , (CASE
                WHEN mmt.TRANSACTION_TYPE_ID IN (1,4,8,31,32,40,41,42) 
                     AND EXISTS(SELECT 'X' FROM INV.MTL_TRANSACTION_ACCOUNTS mta, 
                                                GL.GL_CODE_COMBINATIONS ref_acc
                                  WHERE mta.TRANSACTION_ID = mmt.TRANSACTION_ID 
                                  AND mta.REFERENCE_ACCOUNT = ref_acc.CODE_COMBINATION_ID
                                  AND mta.ACCOUNTING_LINE_TYPE = 2 
                                  AND ref_acc.SEGMENT3 = '40312' )
                THEN 
                    mmt.EO_QUANTITY
                ELSE
                    0
               END) AS EO_SCRAP
        ,(CASE
            WHEN mmt.TRANSACTION_TYPE_ID IN (1,31)      -- Account Issue, Account Alias Issue
                 AND NOT EXISTS (SELECT 'X' FROM apps.MTL_GENERIC_DISPOSITIONS
                                  WHERE DISPOSITION_ID = mmt.TRANSACTION_SOURCE_ID
                                  AND (UPPER(SEGMENT1) LIKE '%CYCLE COUNT%' 
                                        OR UPPER(SEGMENT1) LIKE '%RMA%'
                                        OR UPPER(SEGMENT1) LIKE '%SCRAP%'))
                 AND EXISTS(SELECT SEGMENT1 FROM apps.MTL_GENERIC_DISPOSITIONS
                            WHERE DISPOSITION_ID = mmt.TRANSACTION_SOURCE_ID)
            THEN
                mmt.PRIMARY_QUANTITY
            WHEN mmt.TRANSACTION_TYPE_ID = 33           -- Sales Order Issue
                 AND NOT EXISTS(SELECT 'X' FROM apps.MTL_SALES_ORDERS
                                WHERE SALES_ORDER_ID = mmt.TRANSACTION_SOURCE_ID
                                AND SEGMENT2 LIKE '%RMA%'
--                                AND (SEGMENT1 LIKE '6100%'
--                                     OR SEGMENT1 LIKE '7100%'
--                                     OR SEGMENT1 LIKE '8100%'
--                                     OR SEGMENT1 LIKE '9910%'
--                                     OR SEGMENT1 LIKE '7710%')
                                     )
            THEN
                mmt.PRIMARY_QUANTITY
            WHEN mmt.TRANSACTION_TYPE_ID = 35    -- WIP Component Issue
                AND NOT EXISTS(SELECT 'X' FROM apps.WIP_ENTITIES
                               WHERE WIP_ENTITY_ID = mmt.TRANSACTION_SOURCE_ID
                               AND WIP_ENTITY_NAME LIKE '%RMA%')
                AND NOT EXISTS(SELECT 'X' FROM APPS.MTL_SYSTEM_ITEMS_B item
                                WHERE item.INVENTORY_ITEM_ID = mmt.INVENTORY_ITEM_ID
                                AND item.ORGANIZATION_ID = mmt.ORGANIZATION_ID
                                AND item.ITEM_TYPE IN ('FG','FGNC'))
            THEN
                mmt.PRIMARY_QUANTITY
            ELSE
                0
          END) EO_USAGE  
        ,mmt.CREATION_DATE   
        ,mmt.LAST_UPDATE_DATE

     FROM 
         (SELECT mmt.TRANSACTION_ID
                ,mmt.ACCT_PERIOD_ID
                ,mmt.ORGANIZATION_ID
                ,mmt.INVENTORY_ITEM_ID
                ,mmt.TRANSACTION_DATE
                ,mmt.CREATION_DATE   
                ,mmt.LAST_UPDATE_DATE
                ,mmt.TRANSACTION_TYPE_ID
                ,mmt.SUBINVENTORY_CODE
                ,mmt.TRANSACTION_QUANTITY
                ,mmt.TRANSACTION_UOM
                ,mmt.PRIMARY_QUANTITY
                ,mmt.TRANSFER_ORGANIZATION_ID
                ,mmt.TRANSACTION_SOURCE_TYPE_ID
                ,mmt.TRANSACTION_SOURCE_ID
                ,mmt.SOURCE_LINE_ID
                ,mmt.DISTRIBUTION_ACCOUNT_ID
                ,(CASE                            -- Belong to EandO Onhand
                        WHEN msic.ASSET_INVENTORY = 1
                         AND NOT EXISTS
                                (SELECT 'X'
                                  FROM GL.GL_CODE_COMBINATIONS gcci
                                  WHERE ( gcci.SEGMENT3 IN ('14313', '14217')
                                             OR gcci.SEGMENT3 LIKE '6%')
                                     AND msic.MATERIAL_ACCOUNT =
                                               gcci.CODE_COMBINATION_ID(+))
                         AND NOT EXISTS(SELECT 'X' FROM DUAL
                                        WHERE mmt.SUBINVENTORY_CODE IN
                                              ('ADVRPL-ESI',
                                               'ESI SG RMA',
                                               'LSR SRVFBN')
                                            OR mmt.SUBINVENTORY_CODE LIKE 'x%')
                        THEN
                           mmt.PRIMARY_QUANTITY
                        ELSE
                           0
                     END) AS EO_QUANTITY
          FROM APPS.MTL_MATERIAL_TRANSACTIONS mmt
              ,INV.MTL_SECONDARY_INVENTORIES msic
           WHERE 1 = 1
            AND msic.ORGANIZATION_ID = mmt.ORGANIZATION_ID
            AND msic.SECONDARY_INVENTORY_NAME = mmt.SUBINVENTORY_CODE
            AND mmt.PRIMARY_QUANTITY <> 0
            AND (mmt.LOGICAL_TRANSACTION IS NULL 
                OR (NOT mmt.LOGICAL_TRANSACTION IS NULL 
                    AND mmt.TRANSACTION_ID = mmt.PARENT_TRANSACTION_ID))
            AND NOT EXISTS(SELECT 'X' FROM dual WHERE mmt.TRANSACTION_TYPE_ID = '10008')
         ) mmt
         ,APPS.MTL_TRANSACTION_TYPES mtt
         ,INV.MTL_PARAMETERS param
         ,APPS.ORG_ACCT_PERIODS oap
    WHERE     1 = 1
          AND mmt.ACCT_PERIOD_ID = oap.ACCT_PERIOD_ID
          AND mmt.ORGANIZATION_ID = oap.ORGANIZATION_ID
          AND param.ORGANIZATION_ID = mmt.ORGANIZATION_ID
          AND mmt.TRANSACTION_TYPE_ID = mtt.TRANSACTION_TYPE_ID
--          AND mmt.TRANSACTION_TYPE_ID = 61
--          AND oap.PERIOD_YEAR = 2022
--          AND oap.PERIOD_NUM = 4
;

