CREATE TABLE xxsc.xxsc_eo_cm_tbl 
(cm_rec_id	number	,
fiscal_year	varchar2(20)	,
fiscal_qtr	varchar2(20)	,
fiscal_period	varchar2(20)	,
organization_code	varchar2(20)	,
item	varchar2(80)	,
lite_org_code	varchar2(20)	,
cm_item	varchar2(40)	,
item_cost	number(15,5)	,
net_oh	number(15,5)	,
open_po	number(15,5)	,
gross_demand_12mo	number(15,5)	,
cm_batch_id	number	,
status	varchar2(40)	,
error_msg	varchar2(1000)	,
creation_date	date	,
created_by	number	,
last_update_date	date	,
last_updated_by	number	,
filename	varchar2(1000));

